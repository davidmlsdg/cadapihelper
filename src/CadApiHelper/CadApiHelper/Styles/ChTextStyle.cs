﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.GraphicsInterface;
#endif
#if ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.GraphicsInterface;
#endif
#if BRCAD
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
    using Teigha.GraphicsInterface;
#endif

#if GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.GraphicsInterface;
#endif

using CadApiHelper.Core;

namespace CadApiHelper.Styles
{
    /// <summary>
    /// ChTextStyle - Manager TextStyle
    /// </summary>
    public static class ChTextStyle
    {
        #region Public Methods

        /// <summary>
        /// Gets the text style identifier.
        /// </summary>
        /// <param name="styleTextName">Name of the style text.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId GetTextStyleId(string styleTextName)
        {
            ObjectId id = ObjectId.Null;
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    TextStyleTable tst;
                    tst = acTrans.GetObject(db.TextStyleTableId, OpenMode.ForRead) as TextStyleTable;

                    if (tst == null)
                    {
                        return id;
                    }
                    if (tst.Has(styleTextName))
                        id = tst[styleTextName];
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Print("TsTextStyle - {0}", ex.Message);
                }
            }
            return id;
        }

        /// <summary>
        /// Gets the text style.
        /// </summary>
        /// <param name="styleTextName">Name of the style text.</param>
        /// <returns>TextStyleTableRecord</returns>
        public static TextStyleTableRecord GetTextStyle(string styleTextName)
        {
            return GetTextStyle(GetTextStyleId(styleTextName));
        }

        /// <summary>
        /// Gets the text style.
        /// </summary>
        /// <param name="styleId">The style identifier.</param>
        /// <returns>TextStyleTableRecord</returns>
        public static TextStyleTableRecord GetTextStyle(ObjectId styleId)
        {
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;
            using (var trans = db.TransactionManager.StartTransaction())
            {
                return trans.GetObject(styleId, OpenMode.ForRead) as TextStyleTableRecord;
            }
        }

        /// <summary>
        /// Determines whether [has text style] [the specified style text name].
        /// </summary>
        /// <param name="styleTextName">Name of the style text.</param>
        /// <returns>
        ///   <c>true</c> if [has text style] [the specified style text name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasTextStyle(string styleTextName)
        {
            return !GetTextStyleId(styleTextName).IsNull;
        }

        /// <summary>
        /// Creates the text style.
        /// </summary>
        /// <param name="styleName">Name of the style.</param>
        /// <param name="fontName">Name of the font.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateTextStyle(string styleName, string fontName)
        {
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    var tst = acTrans.GetObject(db.TextStyleTableId, OpenMode.ForRead) as TextStyleTable;

                    if (tst.Has(styleName))
                        return tst[styleName];

                    var tstr = new TextStyleTableRecord
                    {
                        Name = styleName,
                        Font = new FontDescriptor(fontName, false, false, 0, 0),
                    };
                    var id = tst.Add(tstr);
                    acTrans.AddNewlyCreatedDBObject(tstr, true);
                    return id;
                }
                catch (System.Exception ex)
                {
                    System.Diagnostics.Debug.Print("TsTextStyle - {0}", ex.Message);
                }
            }
            return null;
        }

        #endregion Public Methods
    }
}
