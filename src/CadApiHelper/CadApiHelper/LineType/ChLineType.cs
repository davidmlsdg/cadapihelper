﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
#endif
#if ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
#endif
#if BRCAD
    using Bricscad.ApplicationServices;
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
#endif
#if GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
#endif

using System.Collections.Generic;
using CadApiHelper.Core;
using CadApiHelper.Entities.Edit;

namespace CadApiHelper.LineType
{
    public class ChLineType
    {
        #region Public Methods

        /// <summary>
        /// Gets the name of the linetype.
        /// </summary>
        /// <param name="linetypeId">The linetype identifier.</param>
        /// <returns>Name of linetype</returns>
        public static string GetLinetypeName(ChObjectId linetypeId)
        {
            using (var lt = new ChEditDbObject<LinetypeTableRecord>(linetypeId))
            {
                return lt.DbObject.Name;
            }
        }

        /// <summary>
        /// Gets the linetypes.
        /// </summary>
        /// <returns>List of the names of linetypes</returns>
        public static List<string> GetLinetypes()
        {
            List<string> lista = new List<string>();
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                LinetypeTable ltt = tr.GetObject(db.LinetypeTableId, OpenMode.ForRead) as LinetypeTable;
                foreach (ObjectId id in ltt)
                {
                    using (ChEditDbObject<LinetypeTableRecord> lt = new ChEditDbObject<LinetypeTableRecord>(id))
                    {
                        lista.Add(lt.DbObject.Name);
                    }
                }
            }
            return lista;
        }

        /// <summary>
        /// Gets the object identifier.
        /// </summary>
        /// <param name="lineTypeName">Name of the line type.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId GetObjectId(string lineTypeName)
        {
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                LinetypeTable ltt = tr.GetObject(db.LinetypeTableId, OpenMode.ForRead) as LinetypeTable;
                return ltt.Id;
            }
        }

        /// <summary>
        /// Determines whether the specified line type name has linetype.
        /// </summary>
        /// <param name="lineTypeName">Name of the line type.</param>
        /// <returns>
        ///   <c>true</c> if the specified line type name has linetype; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasLinetype(string lineTypeName)
        {
            var lineTypeId = GetObjectId(lineTypeName);
            return !lineTypeId.IsNull;
        }

        /// <summary>
        /// Loads the linetype.
        /// </summary>
        /// <param name="lineTypeName">Name of the line type.</param>
        /// <param name="lineTypeFileName">Name of the line type file.</param>
        /// <returns>True if linetype was loaded</returns>
        public static bool LoadLinetype(string lineTypeName, string lineTypeFileName)
        {
            //using (TsDocument doc = new TsDocument())
            //  {
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;
            try
            {
                db.LoadLineTypeFile(lineTypeName, lineTypeFileName);
            }
            catch (System.Exception)
            {
                return false;
            }
            // }
            return true;
        }

        #endregion Public Methods
    }
}
