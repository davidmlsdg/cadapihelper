﻿#if ZWCAD

using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.DatabaseServices;
using acApp = ZwSoft.ZwCAD.ApplicationServices;
using ZwSoft.ZwCAD.EditorInput;

#elif BRCAD
    using Teigha.Geometry;
    using Teigha.DatabaseServices;
    using acApp = Bricscad.ApplicationServices;
    using Bricscad.EditorInput;
#elif ACAD
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.EditorInput;
using acApp = Autodesk.AutoCAD.ApplicationServices;
#endif
#if GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
using acApp = GrxCAD.ApplicationServices;
using GrxCAD.EditorInput;
#endif

using CadApiHelper.Geometry;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CadApiHelper.Core;
using System.Windows.Forms;
using CadApiHelper.Entities.Edit;

namespace CadApiHelper.EditorInput
{
    /// <summary>
    /// Classe that has methods of the interaction the user
    /// </summary>
    public static class ChEditor
    {
        #region Fields

        private static string m_keywordResult = "";

        #endregion Fields

        #region Private Methods

        private static Point3dCollection ConvertTsPointCollectionToPoint3dCollection(List<IChPoint> points)
        {
            var pontos = new Point3dCollection();
            for (int i = 0; i < points.Count; i++)
                pontos.Add(points[i].To3dPoint());
            return pontos;
        }

        private static PromptAngleOptions GetPromptAngleOptions(string userMessage, bool allowNone, bool allowZero, IChPoint basePoint, bool useBasePoint)
        {
            var pao = new PromptAngleOptions(userMessage);
            pao.AllowNone = allowNone;
            pao.AllowZero = allowZero;
            pao.BasePoint = basePoint.To3dPoint();
            pao.UseBasePoint = useBasePoint;
            pao.UseAngleBase = true;
            pao.UseDashedLine = true;
            pao.DefaultValue = 0;
            pao.UseDefaultValue = true;
            return pao;
        }

        private static PromptDoubleOptions GetPromptDoubleOptions(string userMessage, bool allowNegative, bool allowNone, bool allowZero)
        {
            var pdo = new PromptDoubleOptions(userMessage);
            pdo.AllowNegative = allowNegative;
            pdo.AllowNone = allowNone;
            pdo.AllowZero = allowZero;
            return pdo;
        }

        private static PromptDoubleResult GetPromptDoubleResult(ref double result, Editor ed, PromptDoubleOptions pdo)
        {
            var retorno = ed.GetDouble(pdo);
            result = retorno.Value;
            return retorno;
        }

        private static PromptPointOptions GetPromptPointOptions(string userMessage, IChPoint basePoint = null, bool? useBasePoint = null, bool? allowNone = null)
        {
            var ppo = new PromptPointOptions(userMessage);
            if (allowNone.HasValue)
                ppo.AllowNone = allowNone.Value;
            if (basePoint != null)
                ppo.BasePoint = basePoint.To3dPoint();
            if (useBasePoint.HasValue)
                ppo.UseBasePoint = useBasePoint.Value;
            ppo.UseDashedLine = true;
            return ppo;
        }

        private static PromptPointResult GetPromptPointResult(ref IChPoint pointResult, Editor ed, PromptPointOptions ppo)
        {
            var retorno = ed.GetPoint(ppo);
            pointResult = ChPoint.Create(retorno.Value);
            return retorno;
        }

        private static PromptStringOptions GetStringPromptOptions(string message, bool permitiEspacos)
        {
            var pso = new PromptStringOptions(message);
            pso.AllowSpaces = permitiEspacos;
            return pso;
        }

        private static void Pso_KeywordInput(object sender, SelectionTextInputEventArgs e)
        {
            m_keywordResult = e.Input;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Get angle from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="allowNone">Allow None</param>
        /// <param name="allowZero">Allow zero</param>
        /// <param name="basePoint">Base point</param>
        /// <param name="useBasePoint">Use Base Point</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="keywords">Keywords</param>
        /// <returns>PromptDoubleResult</returns>
        public static PromptDoubleResult GetAngle(ref double result, string userMessage, bool allowNone, bool allowZero,
            IChPoint basePoint, bool useBasePoint, double defaultValue, params string[] keywords)
        {
            var ed = GetEditor();

            var pao = GetPromptAngleOptions(userMessage, allowNone, allowZero, basePoint, useBasePoint);
            pao.DefaultValue = defaultValue;
            pao.UseDefaultValue = true;

            if (keywords != null)
            {
                for (int i = 0; i < keywords.Length; i++)
                    if (keywords[i] != "")
                        pao.Keywords.Add(keywords[i]);
            }
            var retorno = ed.GetAngle(pao);
            result = retorno.Value;
            return retorno;
        }

        /// <summary>
        /// Get angle from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="basePoint">Base point</param>
        /// <param name="useBasePoint">Use Base Point</param>
        /// <returns>True if successful</returns>
        public static bool GetAngle(ref double result, string userMessage, IChPoint basePoint, bool useBasePoint)
        {
            var retorno = GetAngle(ref result, userMessage, true, true, basePoint, useBasePoint, 0, "");
            return retorno.Status == PromptStatus.OK;
        }

        /// <summary>
        /// Gets the boundary.
        /// </summary>
        /// <param name="point">The point to generate the boundary.</param>
        /// <param name="polyline">The polyline.</param>
        /// <returns>True if the boundary was created</returns>
        public static bool GetBoundary(IChPoint point, ref ChPolyline polyline)
        {
            Editor ed = GetEditor();
            DBObjectCollection collection = ed.TraceBoundary(new Point3d(point.X, point.Y, 0), false);

            if (collection.Count == 0)
                return false;

            Polyline pl = collection[0] as Polyline;
            if (pl == null)
                return false;

            polyline = new ChPolyline(pl);
            polyline.ObjectId = pl.ObjectId;
            if (polyline.Vertices.Count > 0)
            {
                polyline.FixVertices();
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the corner point from prompt.
        /// </summary>
        /// <param name="cornerPoint">The corner point.</param>
        /// <param name="userMessage">The user message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="allowNone">if set to <c>true</c> [allow none].</param>
        /// <returns>if it was successful</returns>
        public static bool GetCorner(ref IChPoint cornerPoint, string userMessage, Point3d basePoint, bool allowNone)
        {
            var ed = GetEditor();
            var ptOp = new PromptCornerOptions(userMessage, basePoint);
            ptOp.AllowNone = allowNone;
            ptOp.UseDashedLine = true;

            PromptPointResult ptResult;
            ptResult = ed.GetCorner(ptOp);

            if (ptResult.Status == PromptStatus.OK)
            {
                cornerPoint = ChPoint.Create(ptResult.Value);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Gets the database from current drawing.
        /// </summary>
        /// <returns>Database</returns>
        public static Database GetDatabase()
        {
            return acApp.Application.DocumentManager.MdiActiveDocument.Database;
        }

        /// <summary>
        /// Gets the document of the current drawing.
        /// </summary>
        /// <returns>Document</returns>
        public static acApp.Document GetDocument()
        {
            return acApp.Application.DocumentManager.MdiActiveDocument;
        }

        /// <summary>
        /// Gets the document collection.
        /// </summary>
        /// <returns>DocumentCollection</returns>
        public static acApp.DocumentCollection GetDocumentCollection()
        {
            return acApp.Application.DocumentManager;
        }

        /// <summary>
        /// Get double from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="allowNegative">Allow negative</param>
        /// <param name="allowNone">Allow None</param>
        /// <param name="allowZero">Allow zero</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="keywords">Keywords</param>
        /// <returns>PromptDoubleResult</returns>
        public static PromptDoubleResult GetDouble(ref double result, string userMessage, bool allowNegative, bool allowNone, bool allowZero, double defaultValue, params string[] keywords)
        {
            var ed = GetEditor();

            var pdo = GetPromptDoubleOptions(userMessage, allowNegative, allowNone, allowZero);
            pdo.DefaultValue = defaultValue;
            pdo.UseDefaultValue = true;
            foreach (string str in keywords)
            {
                pdo.Keywords.Add(str);
            }

            var retorno = ed.GetDouble(pdo);
            result = retorno.Value;
            return retorno;
        }

        /// <summary>
        /// Get double from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="allowNegative">Allow negative</param>
        /// <param name="allowNone">Allow None</param>
        /// <param name="allowZero">Allow zero</param>
        /// <param name="keywords">Keywords</param>
        /// <returns>PromptDoubleResult</returns>
        public static PromptDoubleResult GetDouble(ref double result, string userMessage, bool allowNegative, bool allowNone, bool allowZero, params string[] keywords)
        {
            var ed = GetEditor();

            var pdo = GetPromptDoubleOptions(userMessage, allowNegative, allowNone, allowZero);
            foreach (string str in keywords)
            {
                pdo.Keywords.Add(str);
            }

            return GetPromptDoubleResult(ref result, ed, pdo);
        }

        /// <summary>
        /// Get double from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="allowNegative">Allow negative</param>
        /// <param name="allowNone">Allow None</param>
        /// <param name="allowZero">Allow zero</param>
        /// <returns>PromptDoubleResult</returns>
        public static PromptDoubleResult GetDouble(ref double result, string userMessage, bool allowNegative, bool allowNone, bool allowZero)
        {
            var ed = ChEditor.GetEditor();
            var pdo = GetPromptDoubleOptions(userMessage, allowNegative, allowNone, allowZero);
            return GetPromptDoubleResult(ref result, ed, pdo);
        }

        /// <summary>
        /// Get double from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <returns>True if successful.</returns>
        public static bool GetDouble(ref double result, string userMessage)
        {
            var retorno = GetDouble(ref result, userMessage, true, false, true);
            return retorno.Status == PromptStatus.OK;
        }

        /// <summary>
        /// Get double from prompt
        /// </summary>
        /// <param name="result">result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="defaultValue">Default value</param>
        /// <returns>True if successful.</returns>
        public static bool GetDouble(ref double result, string userMessage, double defaultValue)
        {
            var retorno = GetDouble(ref result, userMessage, true, true, true);
            if (retorno.Status != PromptStatus.OK)
                result = defaultValue;

            return true;
        }

        /// <summary>
        /// Gets the editor.
        /// </summary>
        /// <returns>Editor</returns>
        public static Editor GetEditor()
        {
            return acApp.Application.DocumentManager.MdiActiveDocument.Editor;
        }

        /// <summary>
        /// Select the entity and return the ObjectId
        /// </summary>
        /// <param name="userMessage">User message</param>
        /// <param name="allowedClasses">Allowed classes</param>
        /// <param name="exactMatch">Exact match</param>
        /// <returns></returns>
        public static ChObjectId GetEntity(string userMessage, List<Type> allowedClasses = null, bool exactMatch = false)
        {
            var pt = ChPoint.Create();
            return GetEntity(userMessage, ref pt, allowedClasses, exactMatch);
        }

        /// <summary>
        /// Select the entity and returns the ObjectId
        /// </summary>
        /// <param name="userMessage">User message</param>
        /// <param name="point">Point</param>
        /// <param name="allowedClasses">Allowed classes</param>
        /// <param name="exactMatch">Exact match</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId GetEntity(string userMessage, ref IChPoint point, List<Type> allowedClasses = null, bool exactMatch = false)
        {
            var ed = GetEditor();
            var peo = new PromptEntityOptions(userMessage);
            if (allowedClasses != null && allowedClasses.Count > 0)
            {
                peo.SetRejectMessage("\nItem invalido");
                foreach (var item in allowedClasses)
                    peo.AddAllowedClass(item, exactMatch);
            }
            var pr = ed.GetEntity(peo);
            if (pr.Status != PromptStatus.OK)
                return ObjectId.Null;

            point = pr.PickedPoint.ToPoint();
            return pr.ObjectId;
        }

        /// <summary>
        /// Get integer from prompt
        /// </summary>
        /// <param name="result">Result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="allowNegative">Allow negative</param>
        /// <param name="allowNone">Allow none</param>
        /// <param name="allowZero">Allow zero</param>
        /// <param name="defaultValue">Default value</param>
        /// <param name="keywords">Keywords</param>
        /// <returns>PromptIntegerResult</returns>
        public static PromptIntegerResult GetInteger(ref int result, string userMessage, bool allowNegative, bool allowNone, bool allowZero,
            int? defaultValue = null, params string[] keywords)
        {
            var ed = GetEditor();

            var pdo = GetPromptIntegerOptions(userMessage, allowNegative, allowNone, allowZero);
            if (defaultValue.HasValue)
            {
                pdo.DefaultValue = defaultValue.Value;
                pdo.UseDefaultValue = true;
            }

            foreach (string str in keywords)
            {
                pdo.Keywords.Add(str);
            }

            return GetIntegerReturnPromptIntegerResult(ref result, ed, pdo);
        }

        /// <summary>
        /// Get integer from prompt
        /// </summary>
        /// <param name="result">Result</param>
        /// <param name="userMessage">User Message</param>
        /// <param name="defaultValue">Default value</param>
        /// <returns>PromptIntegerResult</returns>
        public static bool GetInteger(ref int result, string userMessage, int? defaultValue = null)
        {
            var retorno = GetInteger(ref result, userMessage, true, false, true, defaultValue);
            return retorno.Status == PromptStatus.OK;
        }

        /// <summary>
        /// Gets the integer and return prompt integer result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="editor">The editor.</param>
        /// <param name="promptIntegerOptions">The prompt integer options.</param>
        /// <returns>PromptIntegerResult</returns>
        public static PromptIntegerResult GetIntegerReturnPromptIntegerResult(ref int result, Editor editor, PromptIntegerOptions promptIntegerOptions)
        {
            var retorno = editor.GetInteger(promptIntegerOptions);
            result = retorno.Value;
            return retorno;
        }

        /// <summary>
        /// Gets the keyword.
        /// </summary>
        /// <returns>Keyword</returns>
        public static string GetKeyword()
        {
            var temp = m_keywordResult;
            m_keywordResult = "";
            return temp;
        }

        /// <summary>
        /// Gets the keywords.
        /// </summary>
        /// <param name="keywork">The keywork.</param>
        /// <param name="message">The message.</param>
        /// <param name="keywords">The keywords.</param>
        /// <returns>True if successful</returns>
        public static bool GetKeywords(ref string keywork, string message, string[] keywords)
        {
            var ed = ChEditor.GetEditor();
            var result = ed.GetKeywords(message, keywords);
            keywork = result.StringResult;
            return result.Status == PromptStatus.OK;
        }

        /// <summary>
        /// Gets the keywords.
        /// </summary>
        /// <param name="promptKeywordOptions">The prompt keyword options.</param>
        /// <returns>PromptResult</returns>
        public static PromptResult GetKeywords(PromptKeywordOptions promptKeywordOptions)
        {
            var ed = GetEditor();
            return ed.GetKeywords(promptKeywordOptions);
        }

        /// <summary>
        /// Gets the line segment.
        /// </summary>
        /// <param name="userMessageFirstPoint">The user message first point.</param>
        /// <param name="userMessageSecondPoint">The user message second point.</param>
        /// <returns>IChLineSegment</returns>
        public static IChLineSegment GetLineSegment(string userMessageFirstPoint, string userMessageSecondPoint)
        {
            Editor ed = GetEditor();
            Point3d ptInicial = new Point3d();

            PromptPointOptions ptOp = new PromptPointOptions(userMessageFirstPoint);
            ptOp.AllowNone = false;
            ptOp.UseDashedLine = true;

            PromptPointResult ptResult;
            ptResult = ed.GetPoint(ptOp);
            if (ptResult.Status != PromptStatus.OK)
                return null;

            ptInicial = ptResult.Value;

            ptOp.BasePoint = ptInicial;
            ptOp.UseBasePoint = true;
            ptOp.Message = userMessageSecondPoint;
            ptResult = ed.GetPoint(ptOp);
            if (ptResult.Status != PromptStatus.OK)
                return null;

            return ChLineSegment.Create(ptInicial.ToPoint(), ptResult.Value.ToPoint());
        }

        /// <summary>
        /// Gets the point.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="userMessage">The user message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="useBasePoint">The use base point.</param>
        /// <param name="allowNone">The allow none.</param>
        /// <param name="allowArbitraryInput">if set to <c>true</c> [allow arbitrary input].</param>
        /// <returns>True if successful</returns>
        public static bool GetPoint(ref IChPoint result, string userMessage, IChPoint basePoint = null, bool? useBasePoint = null,
            bool? allowNone = null, bool allowArbitraryInput = false)
        {
            var promptResult = GetPointReturnPromptPointResult(ref result, userMessage, basePoint, useBasePoint, allowNone, allowArbitraryInput);
            return promptResult.Status == PromptStatus.OK;
        }

        /// <summary>
        /// Gets the point or keyword.
        /// </summary>
        /// <param name="userMessage">The user message.</param>
        /// <param name="defaultKeyword">The default keyword.</param>
        /// <param name="keywordResult">The keyword result.</param>
        /// <param name="pointResult">The point result.</param>
        /// <param name="keywords">The keywords.</param>
        /// <returns>True if successful</returns>
        public static bool GetPointKeyWord(string userMessage, string defaultKeyword, ref string keywordResult, ref IChPoint pointResult, params string[] keywords)
        {
            Editor ed = GetEditor();
            PromptPointOptions op = new PromptPointOptions(userMessage);

            foreach (string item in keywords)
                op.Keywords.Add(item);

            if (defaultKeyword != "")
                op.Keywords.Default = defaultKeyword;

            PromptPointResult res = ed.GetPoint(op);

            switch (res.Status)
            {
                case PromptStatus.Keyword:
                    keywordResult = res.StringResult;
                    break;

                case PromptStatus.OK:
                    pointResult = res.Value.ToPoint();
                    break;

                default:
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Gets the point.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="userMessage">The user message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="useBasePoint">The use base point.</param>
        /// <param name="allowNone">The allow none.</param>
        /// <param name="allowArbitraryInput">if set to <c>true</c> [allow arbitrary input].</param>
        /// <param name="keywords">The keywords.</param>
        /// <returns>PromptPointResult</returns>
        public static PromptPointResult GetPointReturnPromptPointResult(ref IChPoint result, string userMessage, IChPoint basePoint = null, bool? useBasePoint = null,
            bool? allowNone = null, bool allowArbitraryInput = false, params string[] keywords)
        {
            var ed = GetEditor();

            var ppo = GetPromptPointOptions(userMessage, basePoint, useBasePoint, allowNone);
            ppo.AllowArbitraryInput = allowArbitraryInput;
            for (int i = 0; i < keywords.Length; i++)
                if (keywords[i] != "")
                    ppo.Keywords.Add(keywords[i]);
            return GetPromptPointResult(ref result, ed, ppo);
        }

        /// <summary>
        /// Gets the prompt integer options.
        /// </summary>
        /// <param name="userMessage">The user message.</param>
        /// <param name="allowNegative">if set to <c>true</c> [allow negative].</param>
        /// <param name="allowNone">if set to <c>true</c> [allow none].</param>
        /// <param name="allowZero">if set to <c>true</c> [allow zero].</param>
        /// <returns>PromptIntegerOptions</returns>
        public static PromptIntegerOptions GetPromptIntegerOptions(string userMessage, bool allowNegative, bool allowNone, bool allowZero)
        {
            var pdo = new PromptIntegerOptions(userMessage);
            pdo.AllowNegative = allowNegative;
            pdo.AllowNone = allowNone;
            pdo.AllowZero = allowZero;
            return pdo;
        }

        /// <summary>
        /// Gets the string.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="userMessage">The user message.</param>
        /// <param name="allowSpaces">if set to <c>true</c> [allow spaces].</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns>PromptResult</returns>
        public static PromptResult GetString(ref string result, string userMessage, bool allowSpaces = false, string defaultValue = null, params string[] keywords)
        {
            var ed = GetEditor();
            var pso = GetStringPromptOptions(userMessage, allowSpaces);
            if (defaultValue != null)
            {
                pso.DefaultValue = defaultValue;
                pso.UseDefaultValue = true;
            }
            if (keywords != null)
            {
                foreach (string str in keywords)
                    pso.Keywords.Add(str);
            }
            return GetStringReturnPromptResult(ref result, ed, pso);
        }

        /// <summary>
        /// Gets the string and return prompt result.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <param name="editor">The editor.</param>
        /// <param name="promptStringOptions">The prompt string options.</param>
        /// <returns>PromptResult</returns>
        public static PromptResult GetStringReturnPromptResult(ref string result, Editor editor, PromptStringOptions promptStringOptions)
        {
            var ps = editor.GetString(promptStringOptions);
            result = ps.StringResult;
            return ps;
        }

        /// <summary>
        /// Selects all entities of drawing.
        /// </summary>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectAll()
        {
            return SelectAll(null);
        }

        /// <summary>
        /// Selects all of drawing.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectAll(ChSelectionFilterCollection filter)
        {
            var ed = ChEditor.GetEditor();
            PromptSelectionResult psr;
            if (filter == null)
                psr = ed.SelectAll();
            else
                psr = ed.SelectAll(filter.GetSelectionFilter());

            if (psr.Status != PromptStatus.OK)
                return new ObjectIdCollection();
            else
                return new ObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Selects by cross polygon.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectCrossPolygon(ChPointCollection points, ChSelectionFilterCollection filter = null)
        {
            var ed = ChEditor.GetEditor();
            PromptSelectionResult psr;
            if (filter == null)
                psr = ed.SelectCrossingPolygon(points.GetPoint3dCollection());
            else
                psr = ed.SelectCrossingPolygon(points.GetPoint3dCollection(), filter.GetSelectionFilter());

            if (psr.Status != PromptStatus.OK)
                return new ChObjectIdCollection();
            else
                return new ChObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Selects by cross window.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectCrossWindow(IChPoint point1, IChPoint point2, ChSelectionFilterCollection filter = null)
        {
            var ed = GetEditor();
            PromptSelectionResult psr;
            if (filter == null)
                psr = ed.SelectCrossingWindow(point1.To3dPoint(), point2.To3dPoint());
            else
                psr = ed.SelectCrossingWindow(point1.To3dPoint(), point2.To3dPoint(), filter.GetSelectionFilter());

            if (psr.Status != PromptStatus.OK)
                return new ChObjectIdCollection();
            else
                return new ChObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Selects the fence.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectFence(ChPointCollection points, ChSelectionFilterCollection filter = null)
        {
            var ed = ChEditor.GetEditor();
            PromptSelectionResult psr;
            if (filter == null)
                psr = ed.SelectFence(points.GetPoint3dCollection());
            else
                psr = ed.SelectFence(points.GetPoint3dCollection(), filter.GetSelectionFilter());

            if (psr.Status != PromptStatus.OK)
                return new ChObjectIdCollection();
            else
                return new ChObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Selects the last entity.
        /// </summary>
        /// <returns>ChObjectId</returns>
        public static ChObjectId SelectLast()
        {
            var ed = GetEditor();

            var psr = ed.SelectLast();
            if (psr.Status == PromptStatus.OK && psr.Value.Count > 0)
            {
                var ids = psr.Value.GetObjectIds();
                return ids[0];
            }

            return ObjectId.Null;
        }

        /// <summary>
        /// Selects on screen.
        /// </summary>
        /// <param name="userMessage">The user message.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectOnScreen(string userMessage, ChSelectionFilterCollection filter = null)
        {
            var psr = SelectOnScreen(userMessage, filter, null, "");
            if (psr.Status != PromptStatus.OK)
                return new ChObjectIdCollection();
            else
                return new ChObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Selects on screen.
        /// </summary>
        /// <param name="userMessage">The user message.</param>
        /// <param name="filter">The filter.</param>
        /// <param name="keywords">The keywords.</param>
        /// <param name="defaultkeyword">The default keyword.</param>
        /// <returns>PromptSelectionResult</returns>
        public static PromptSelectionResult SelectOnScreen(string userMessage, ChSelectionFilterCollection filter, List<string> keywords, string defaultkeyword)
        {
            var pso = new PromptSelectionOptions();
            pso.MessageForAdding = userMessage;
            pso.KeywordInput += Pso_KeywordInput;

            var ed = GetEditor();
            if (keywords != null)
            {
                foreach (string item in keywords)
                    pso.Keywords.Add(item);
                if (defaultkeyword != "")
                    pso.Keywords.Default = defaultkeyword;
            }

            PromptSelectionResult psr;

            if (filter == null)
                psr = ed.GetSelection(pso);
            else
                psr = ed.GetSelection(pso, filter.GetSelectionFilter());

            return psr;
        }

        /// <summary>
        /// Selects by polygon.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection SelectPolygon(ChPointCollection points, ChSelectionFilterCollection filter = null)
        {
            var ed = GetEditor();
            PromptSelectionResult psr;
            if (filter == null)
                psr = ed.SelectWindowPolygon(points.GetPoint3dCollection());
            else
                psr = ed.SelectWindowPolygon(points.GetPoint3dCollection(), filter.GetSelectionFilter());

            if (psr.Status != PromptStatus.OK)
                return new ChObjectIdCollection();
            else
                return new ChObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Selects by window.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <param name="filter">The filter.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection selectWindow(IChPoint point1, IChPoint point2, ChSelectionFilterCollection filter = null)
        {
            var ed = GetEditor();
            PromptSelectionResult psr;
            if (filter == null)
                psr = ed.SelectWindow(point1.To3dPoint(), point2.To3dPoint());
            else
                psr = ed.SelectWindow(point1.To3dPoint(), point2.To3dPoint(), filter.GetSelectionFilter());

            if (psr.Status != PromptStatus.OK)
                return new ChObjectIdCollection();
            else
                return new ChObjectIdCollection(psr.Value.GetObjectIds());
        }

        /// <summary>
        /// Sends the command for execute on prompt.
        /// </summary>
        /// <param name="command">The command.</param>
        public static void SendCommad(string command)
        {
            var doc = GetDocument();
            doc.SendStringToExecute(command, true, false, false);
        }

        /// <summary>
        /// Sets the focus cad application.
        /// </summary>
        public static void SetFocusCadApp()
        {
#if ACAD
            Autodesk.AutoCAD.Internal.Utils.SetFocusToDwgView();
#elif ZWCAD
            ZwSoft.ZwCAD.Internal.Utils.SetFocusToDwgView();
#elif BRCAD
            acApp.Application.MainWindow.Focus();
#endif
        }

        /// <summary>
        /// Shows modal dialog.
        /// </summary>
        /// <param name="formToShow">The form to show.</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ShowModalDialog(Form formToShow)
        {
            return acApp.Application.ShowModalDialog(formToShow);
        }

        /// <summary>
        /// Shows the modal window.
        /// </summary>
        /// <param name="formToShow">The form to show.</param>
        /// <returns>bool?</returns>
        public static bool? ShowModalWindow(System.Windows.Window formToShow)
        {
            return acApp.Application.ShowModalWindow(formToShow);
        }

        /// <summary>
        /// Shows the modeless dialog.
        /// </summary>
        /// <param name="formToShow">The form to show.</param>
        public static void ShowModelessDialog(Form formToShow)
        {
            acApp.Application.ShowModelessDialog(formToShow);
        }

        /// <summary>
        /// Shows the modeless window.
        /// </summary>
        /// <param name="formToShow">The form to show.</param>
        public static void ShowModelessWindow(System.Windows.Window formToShow)
        {
            acApp.Application.ShowModelessWindow(formToShow);
        }

        /// <summary>
        /// Writes message to the prompt.
        /// </summary>
        /// <param name="message">The message.</param>
        public static void WriteMessage(string message)
        {
            var ed = GetEditor();
            ed.WriteMessage(message + "\n");
        }

        /// <summary>
        /// Zooms at specified point, height and width.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="height">The height.</param>
        /// <param name="width">The width.</param>
        public static void Zoom(IChPoint point, double height, double width)
        {
            var view = new ViewTableRecord();
            view.CenterPoint = point.To2dPoint();
            view.Height = height;
            view.Width = width;
            ChEditor.GetEditor().SetCurrentView(view);
        }

        /// <summary>
        /// Zooms by ObjectId entity.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        public static void Zoom(ChObjectId objectId)
        {
            IChPoint pt = null;
            double altura = 0, largura = 0;
            var ent = new ChEditEntity(objectId);
            using (ent)
            {
                if (ent.DbObject == null)
                    return;

                var bounds = ent.DbObject.Bounds;
                if (bounds != null)
                {
                    pt = ChPoint.Create(bounds.Value.MinPoint);
                    pt = pt.Mid(bounds.Value.MaxPoint.ToPoint());
                    altura = (bounds.Value.MaxPoint.Y - bounds.Value.MinPoint.Y) * 1.1;
                    largura = (bounds.Value.MaxPoint.X - bounds.Value.MinPoint.X) * 1.1;
                }
            }
            Zoom(pt, altura, largura);
        }

        #endregion Public Methods
    }
}