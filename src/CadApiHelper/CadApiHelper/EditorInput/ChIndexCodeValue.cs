﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadApiHelper.EditorInput
{
    /// <summary>
    /// Class that represent index code and value for ChSelectionFilterCollection
    /// </summary>
    public class ChIndexCodeValue
    {
        #region Private Campos

        private int indexCode;
        private object value;

        #endregion Private Campos

        #region Public Construtoras

        /// <summary>
        /// Initializes a new instance of the <see cref="ChIndexCodeValue"/> class.
        /// </summary>
        /// <param name="indexCode">The index code.</param>
        /// <param name="value">The value.</param>
        public ChIndexCodeValue(int indexCode, object value)
        {
            this.indexCode = indexCode;
            this.value = value;
        }

        #endregion Public Construtoras

        #region Public Propriedades

        /// <summary>
        /// Gets the index code.
        /// </summary>
        /// <value>
        /// The index code.
        /// </value>
        public int IndexCode { get { return indexCode; } }

        /// <summary>
        /// Gets the value.
        /// </summary>
        /// <value>
        /// The value.
        /// </value>
        public object Value { get { return value; } }

        #endregion Public Propriedades
    }
}
