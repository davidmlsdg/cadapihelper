﻿#if ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
#elif BRCAD
    using Teigha.Geometry;
    using Teigha.DatabaseServices;
    using acApp = Bricscad.ApplicationServices;
    using Bricscad.EditorInput;
#elif ACAD
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
#elif GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.EditorInput;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadApiHelper.EditorInput
{
    /// <summary>
    /// Class that represent the filter of SelectionSet
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List&lt;CadApiHelper.EditorInput.ChIndexCodeValue&gt;" />
    public class ChSelectionFilterCollection : List<ChIndexCodeValue>
    {
        #region Public Methods

        /// <summary>
        /// Initializes a new instance of the <see cref="ChSelectionFilterCollection"/> class.
        /// </summary>
        public ChSelectionFilterCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChSelectionFilterCollection"/> class.
        /// </summary>
        /// <param name="indexCode">The index code.</param>
        /// <param name="value">The value.</param>
        public ChSelectionFilterCollection(int indexCode, object value)
        {
            Add(indexCode, value);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChSelectionFilterCollection"/> class.
        /// </summary>
        /// <param name="pair">The pair.</param>
        public ChSelectionFilterCollection(ChIndexCodeValue pair)
        {
            Add(pair);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChSelectionFilterCollection"/> class.
        /// </summary>
        /// <param name="lista">The lista.</param>
        public ChSelectionFilterCollection(IEnumerable<ChIndexCodeValue> lista)
        {
            AddRange(lista);
        }

        /// <summary>
        /// Adds the specified index.
        /// </summary>
        /// <param name="indexCode">The index code.</param>
        /// <param name="value">The value.</param>
        public void Add(int indexCode, object value)
        {
            Add(new ChIndexCodeValue(indexCode, value));
        }

        /// <summary>
        /// Gets the selection filter.
        /// </summary>
        /// <returns>SelectionFilter</returns>
        public SelectionFilter GetSelectionFilter()
        {
            if (Count == 0)
                return null;

            TypedValue[] valores = new TypedValue[this.Count];
            for (int i = 0; i < this.Count; i++)
                valores[i] = new TypedValue(this[i].IndexCode, this[i].Value);

            return new SelectionFilter(valores);
        }

        #endregion Public Methods
    }
}
