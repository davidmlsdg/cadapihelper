﻿#if ACAD
using acdb = Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
#elif ZWCAD
using acdb = ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.Geometry;
#elif BRCAD
using acdb = Teigha.DatabaseServices;
using Teigha.Geometry;
#elif GCAD
using acdb = GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#endif

using System;
using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.Entities;
using CadApiHelper.EditorInput;
using CadApiHelper.Layer;

namespace CadApiHelper.Dimension
{
    /// <summary>
    /// Class to Create Dimensions
    /// </summary>
    public static class ChDimension
    {
        #region Public Methods

        /// <summary>
        /// Creates the rotated dimension.
        /// </summary>
        /// <param name="rotation">The rotation.</param>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateRotatedDimension(double rotation, IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint, ChObjectId styleId)
        {
            return CreateRotatedDimension(rotation, line1Point, line2Point, dimensionLinePoint, styleId);
        }

        /// <summary>
        /// Creates the rotated dimension.
        /// </summary>
        /// <param name="rotation">The rotation.</param>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="layer">The layer.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateRotatedDimension(double rotation, IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint, ChObjectId styleId, string layer)
        {
            return CreateRotatedDimension(rotation, line1Point, line2Point, dimensionLinePoint, styleId, layer);
        }

        /// <summary>
        /// Creates the rotated dimension.
        /// </summary>
        /// <param name="rotation">The rotation.</param>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="dimensionText">The dimension text.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateRotatedDimension(double rotation, IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint,
            string dimensionText, ChObjectId styleId, double scale = -1)
        {
            acdb.RotatedDimension ent = new acdb.RotatedDimension(rotation, line1Point.To3dPoint(), line2Point.To3dPoint(), 
                dimensionLinePoint.To3dPoint(), dimensionText, styleId);
            if (scale > 0)
                ent.Dimscale = scale;
            return ChEntity.AddEntityToDatabase(ent);
        }

        /// <summary>
        /// Creates the rotated dimension.
        /// </summary>
        /// <param name="rotation">The rotation.</param>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="scale">The scale.</param>
        /// <param name="annotative">if set to <c>true</c> [annotative].</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateRotatedDimension(double rotation, IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint,
            ChObjectId styleId, string layer, double scale = -1, bool annotative = false)
        {
            acdb.RotatedDimension ent = new acdb.RotatedDimension();
            ent.SetDatabaseDefaults();
            ent.Rotation = rotation;
            ent.XLine1Point = line1Point.To3dPoint();
            ent.XLine2Point = line2Point.To3dPoint();
            ent.DimLinePoint = dimensionLinePoint.To3dPoint();
            ent.DimensionStyle = styleId;
            if (scale > 0)
                ent.Dimscale = scale;
            if (annotative)
                ent.Annotative = acdb.AnnotativeStates.True;
            return AddToDatabase(layer, ent);
        }

        /// <summary>
        /// Creates the rotated dimension.
        /// </summary>
        /// <param name="rotation">The rotation.</param>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="dimensionText">The dimension text.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="scale">The scale.</param>
        /// <param name="annotative">if set to <c>true</c> [annotative].</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateRotatedDimension(double rotation, IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint,
            string dimensionText, ChObjectId styleId, string layer, double scale = -1, bool annotative = false)
        {
            acdb.RotatedDimension ent = new acdb.RotatedDimension(rotation, line1Point.To3dPoint(), line2Point.To3dPoint(), 
                dimensionLinePoint.To3dPoint(), dimensionText, styleId);
            ent.SetDatabaseDefaults();
            if (scale > 0)
                ent.Dimscale = scale;
            if (annotative)
                ent.Annotative = acdb.AnnotativeStates.True;
            return AddToDatabase(layer, ent);
        }

        /// <summary>
        /// Creates the aligned dimension.
        /// </summary>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateAlignedDimension(IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint, ChObjectId styleId)
        {
            return CreateAlignedDimension(line1Point, line2Point, dimensionLinePoint, styleId);
        }

        /// <summary>
        /// Creates the aligned dimension.
        /// </summary>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="layer">The layer.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateAlignedDimension(IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint, ChObjectId styleId, string layer)
        {
            return CreateAlignedDimension(line1Point, line2Point, dimensionLinePoint, styleId, layer);
        }

        /// <summary>
        /// Creates the aligned dimension.
        /// </summary>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="dimensionText">The dimension text.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateAlignedDimension(IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint, string dimensionText, ChObjectId styleId)
        {
            acdb.AlignedDimension ent = new acdb.AlignedDimension(line1Point.To3dPoint(), line2Point.To3dPoint(), 
                dimensionLinePoint.To3dPoint(), dimensionText, styleId);
            return ChEntity.AddEntityToDatabase(ent);
        }

        /// <summary>
        /// Creates the aligned dimension.
        /// </summary>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="dimensionLinePoint">The dimension line point.</param>
        /// <param name="dimensionText">The dimension text.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateAlignedDimension(IChPoint line1Point, IChPoint line2Point, IChPoint dimensionLinePoint,
            string dimensionText, ChObjectId styleId, string layer, double scale = -1)
        {
            acdb.AlignedDimension ent = new acdb.AlignedDimension(line1Point.To3dPoint(), line2Point.To3dPoint(), 
                dimensionLinePoint.To3dPoint(), dimensionText, styleId);
            if (scale > 0)
                ent.Dimscale = scale;
            return AddToDatabase(layer, ent);
        }

        /// <summary>
        /// Creates the angular dimension.
        /// </summary>
        /// <param name="centerPoint">The center point.</param>
        /// <param name="line1Point">The line1 point.</param>
        /// <param name="line2Point">The line2 point.</param>
        /// <param name="arcPoint">The arc point.</param>
        /// <param name="dimensionText">The dimension text.</param>
        /// <param name="dimensionStyle">The dimension style.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateAngularDimension(Point3d centerPoint, Point3d line1Point, Point3d line2Point, Point3d arcPoint,
            string dimensionText, ChObjectId dimensionStyle, string layer = null, double scale = -1)
        {
            acdb.Point3AngularDimension ent = new acdb.Point3AngularDimension(centerPoint, line1Point, line2Point, arcPoint, dimensionText, dimensionStyle);
            if (scale > 0)
                ent.Dimscale = scale;
            return AddToDatabase(layer, ent);
        }

        /// <summary>
        /// Gets the dimension style identifier.
        /// </summary>
        /// <param name="styleName">Name of the style.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId GetDimensionStyleId(string styleName)
        {
            acdb.ObjectId ret = acdb.ObjectId.Null;

            var db = ChEditor.GetDatabase();
            if (db == null)
                return ret;

            using (acdb.Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    if (!(acTrans.GetObject(db.DimStyleTableId, acdb.OpenMode.ForRead) is acdb.DimStyleTable dst))
                        return ret;

                    if (dst.Has(styleName))
                        ret = dst[styleName];
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.Print(e.Message);
                    return ret;
                }
            }

            return ret;
        }

        #endregion Public Methods

        private static acdb.ObjectId AddToDatabase(string layer, acdb.Dimension ent)
        {
            var layerId = ChLayers.GetObjectId(layer);
            if (layerId.IsNull)
            {
                ChLayers.CreateLayer(layer);
                layerId = ChLayers.GetObjectId(layer);
            }
            if (!layerId.IsNull)
                ent.LayerId = layerId;

            return ChEntity.AddEntityToDatabase(ent);
        }
    }
}
