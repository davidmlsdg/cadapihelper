﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.Geometry;
#elif BRCAD
using acApp = Bricscad.ApplicationServices.Application;
using Teigha.DatabaseServices;
using Teigha.Geometry;
#elif GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#endif

using System;
using System.Linq;
using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.EditorInput;
using CadApiHelper.Styles;

namespace CadApiHelper.Dimension
{
    /// <summary>
    /// 
    /// </summary>
    public static class ChLeader
    {
        /// <summary>
        /// Creates the leader.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="contents">The contents.</param>
        /// <param name="textHeight">Height of the text.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateLeader(ChPointCollection points, string contents, double textHeight, double scale = -1)//textHeight
        {
            if (points == null || points.Count < 2)
                return new ChObjectId();

            MText mText = GetMtext(points.Last(), textHeight, contents);

            Leader leader = new Leader();
            if (scale > 0)
                leader.Dimscale = scale;

            for (int i = 0; i < points.Count; i++)
                leader.AppendVertex(points[i].To3dPoint());

            return CreateLeader(leader, mText);
        }

        /// <summary>
        /// Creates the leader.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="mtext">The mtext.</param>
        /// <param name="style">The style.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateLeader(ChPointCollection points, MText mtext, ChObjectId style)
        {
            if (points == null || points.Count < 2)
                return new ChObjectId();

            var leader = new Leader();
            leader.DimensionStyle = style;

            for (int i = 0; i < points.Count; i++)
                leader.AppendVertex(points[i].To3dPoint());
            return CreateLeader(leader, mtext);
        }

        /// <summary>
        /// Creates the leader.
        /// </summary>
        /// <param name="leader">The leader.</param>
        /// <param name="mtext">The mtext.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateLeader(Leader leader, MText mtext)
        {
            var id = new ChObjectId();
            Database db = ChEditor.GetDatabase();
            if (db == null)
                return id;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    BlockTableRecord btr = acTrans.GetObject(db.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;

                    if (btr == null)
                        return id;

                    if (mtext != null)
                    {
                        ObjectId idText = btr.AppendEntity(mtext);
                        acTrans.AddNewlyCreatedDBObject(mtext, true);
                        leader.Annotation = idText;
                    }

                    id = btr.AppendEntity(leader);
                    acTrans.AddNewlyCreatedDBObject(leader, true);
                    acTrans.Commit();
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.Print(ex.Message);
                    return id;
                }
            }

            return id;
        }

        private static MText GetMtext(IChPoint point, ChObjectId textStyleId, string contents, AttachmentPoint attachmentPoint)
        {
            MText mText = new MText();
            mText.SetDatabaseDefaults();
            mText.Attachment = attachmentPoint;
            mText.Contents = contents;
            mText.Location = point.To3dPoint();
            mText.TextStyleId = textStyleId;
            var textStyle = ChTextStyle.GetTextStyle(textStyleId);
            if (textStyle != null)
                mText.TextHeight = textStyle.TextSize;
            return mText;
        }

        private static MText GetMtext(IChPoint point, double textHeight, string contents)
        {
            MText mText = new MText();
            mText.SetDatabaseDefaults();
            mText.Contents = contents;
            mText.TextHeight = textHeight;
            mText.Location = point.To3dPoint();
            return mText;
        }
    }
}
