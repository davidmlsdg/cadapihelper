﻿#if ACAD
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
#elif BRCAD
    using Teigha.DatabaseServices;
    using acApp = Bricscad.ApplicationServices.Application;
#elif GCAD
    using GrxCAD.DatabaseServices;
    using acApp = GrxCAD.ApplicationServices.Application;
#endif


using System;
using CadApiHelper.Core;
using CadApiHelper.Entities.Edit;

namespace CadApiHelper.Groups
{
    /// <summary>
    /// ChGroup - Manager groups 
    /// </summary>
    public static class ChGroup
    {
        /// <summary>
        /// Creates the group.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <param name="selectable">if set to <c>true</c> [selectable].</param>
        /// <param name="ids">The ids.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateGroup(string groupName = "", bool selectable = true, ChObjectIdCollection ids = null)
        {
            if (string.IsNullOrEmpty(groupName))
                groupName = CreateGroupName();
            var grb = new Group(groupName, selectable);
#if ZWCAD
            grb.Name = groupName;
            grb.Selectable = selectable;
#endif

            Database acCurDb = acApp.DocumentManager.MdiActiveDocument.Database;
            var id = new ChObjectId();
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {
                var acDbTbl = acTrans.GetObject(acCurDb.GroupDictionaryId, OpenMode.ForWrite) as DBDictionary;
                if (acDbTbl == null)
                    return ObjectId.Null;

                id = acDbTbl.SetAt(groupName, grb);
                acTrans.AddNewlyCreatedDBObject(grb, true);
                acTrans.Commit();
            }

            if (id.IsNull)
                return id;

            using (var gr = new ChEditDbObject<Group>(id, true))
            {
                if (gr.DbObject == null)
                    return id;

                if (ids != null && ids.Count > 0)
                    gr.DbObject.Append(ids);
                gr.Comit();
            }

            return id;
        }

        /// <summary>
        /// Creates the name of the group.
        /// </summary>
        /// <returns>Group name</returns>
        public static string CreateGroupName()
        {
            string groupName = "";
            while (true)
            {
                groupName = "G" + DateTime.Now.Ticks.ToString();
                if (!HasGroupName(groupName))
                    return groupName;
            }
        }

        /// <summary>
        /// Determines whether [has group name] [the specified group name].
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <returns>
        ///   <c>true</c> if [has group name] [the specified group name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasGroupName(string groupName)
        {
            Database acCurDb = acApp.DocumentManager.MdiActiveDocument.Database;
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {
                var acDbTbl = acTrans.GetObject(acCurDb.GroupDictionaryId, OpenMode.ForRead) as DBDictionary;
                return acDbTbl.Contains(groupName);
            }
        }

        /// <summary>
        /// Gets the itens of group.
        /// </summary>
        /// <param name="idGroup">The identifier group.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection GetItensGroup(ObjectId idGroup)
        {
            using (var ent = new ChEditEntity(idGroup))
            {
                if (ent.DbObject == null)
                    return null;

                foreach (ObjectId id in ent.DbObject.GetPersistentReactorIds())
                {
                    using (var entGr = new ChEditDbObject<Group>(id))
                    {
                        if (entGr.DbObject == null || entGr.DbObject.NumEntities == 0)
                            return null;
                        return new ObjectIdCollection(entGr.DbObject.GetAllEntityIds());
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the itens group.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection GetItensGroup(string groupName)
        {
            var id = GetIdGroup(groupName);
            if (id.IsNull)
                return null;

            using (var ent = new ChEditDbObject<Group>(id))
            {
                if (ent.DbObject == null)
                    return null;
                return new ObjectIdCollection(ent.DbObject.GetAllEntityIds());
            }
        }

        /// <summary>
        /// Gets the identifier group.
        /// </summary>
        /// <param name="groupName">Name of the group.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId GetIdGroup(string groupName)
        {
            Database acCurDb = acApp.DocumentManager.MdiActiveDocument.Database;
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {
                var acDbTbl = acTrans.GetObject(acCurDb.GroupDictionaryId, OpenMode.ForRead) as DBDictionary;
                if (acDbTbl == null)
                    return ObjectId.Null;
                return acDbTbl.GetAt(groupName);
            }
        }
    }
}
