﻿#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.ApplicationServices;
#endif
#if ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.ApplicationServices;
#endif
#if BRCAD
using Bricscad.ApplicationServices;
using Teigha.DatabaseServices;
using Teigha.Geometry;
#endif

#if GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
using GrxCAD.ApplicationServices;
#endif


using System;
using System.Collections.Generic;
using System.Linq;
using CadApiHelper.Core;
using CadApiHelper.Layer;
using CadApiHelper.Geometry;
using CadApiHelper.Entities.Edit;

namespace CadApiHelper.Entities
{
    /// <summary>
    /// Create entities
    /// </summary>
    public static class ChCreateEntities
    {
        #region Layer

        /// <summary>
        /// Gets the layer identifier.
        /// </summary>
        /// <param name="layer">The layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns></returns>
        private static ChObjectId GetLayerId(string layer, short colorIndex)
        {
            if (layer == "")
                return null;

            if (ChLayers.HasLayer(layer))
                return ChLayers.GetObjectId(layer);

            ChLayers.CreateLayer(layer, colorIndex);
            return ChLayers.GetObjectId(layer);
        }

        /// <summary>
        /// Sets the layer.
        /// </summary>
        /// <param name="ent">The ent.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        private static void SetLayer(Entity ent, string layer, short colorIndex)
        {
            var idLayer = GetLayerId(layer, colorIndex);
            if (idLayer != null && !idLayer.IsNull)
                ent.LayerId = idLayer;
        }

        #endregion Layer

        #region Line

        /// <summary>
        /// Creates the line.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateLine(IChPoint startPoint, IChPoint endPoint, string layerName = "", short colorIndex = 257)
        {
            Line l = new Line(startPoint.To3dPoint(), endPoint.To3dPoint());
            SetLayer(l, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(l);
        }

        /// <summary>
        /// Creates the line.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateLine(IChLineSegment lineSegment, string layerName = "", short colorIndex = 257)
        {
            return CreateLine(lineSegment.StartPoint, lineSegment.EndPoint, layerName, colorIndex);
        }
        #endregion Line

        #region Point

        /// <summary>
        /// Creates the point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePoint(IChPoint point, string layerName = "", short colorIndex = 257)
        {
            var ent = new DBPoint();
            ent.SetDatabaseDefaults();
            ent.Position = point.To3dPoint();

            SetLayer(ent, layerName, colorIndex);
            ChObjectId idpt = ChEntity.AddEntityToDatabase(ent);
            return idpt;
        }

        #endregion Point

        #region Polilyne

        /// <summary>
        /// Creates the donut.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="internalDiameter">The internal diameter.</param>
        /// <param name="externalDiameter">The external diameter.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateDonut(IChPoint center, double internalDiameter, double externalDiameter, string layerName = "", short colorIndex = 257)
        {
            double lwt = externalDiameter - internalDiameter;
            Point2d pt1 = new Point2d(center.X - (internalDiameter + lwt / 2), center.Y);
            Point2d pt2 = new Point2d(center.X + (internalDiameter + lwt / 2), center.Y);
            Polyline pline = new Polyline();
            pline.AddVertexAt(0, pt1, 1.0, lwt, lwt);
            pline.AddVertexAt(1, pt2, 1.0, lwt, lwt);
            pline.AddVertexAt(2, pt1, 0.0, lwt, lwt);
            pline.Closed = true;
            SetLayer(pline, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(pline);
        }

        /// <summary>
        /// Creates the poly line.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyline(IChPoint startPoint, IChPoint endPoint, string layerName = "", short colorIndex = 257)
        {
            return CreatePolyline(new List<IChPoint> { startPoint, endPoint }, false, layerName, colorIndex);
        }

        /// <summary>
        /// Creates the polyline.
        /// </summary>
        /// <param name="polyline">The polyline.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyline(ChPolyline polyline, string layerName = "", short colorIndex = 257)
        {
            Polyline ent = CreatePolyByChPolyline(polyline);
            SetLayer(ent, layerName, colorIndex);
            ChObjectId idLine = ChEntity.AddEntityToDatabase(ent);
            return idLine;
        }

        /// <summary>
        /// Creates the poly by ch polyline.
        /// </summary>
        /// <param name="polyline">The polyline.</param>
        /// <returns>Polyline</returns>
        public static Polyline CreatePolyByChPolyline(ChPolyline polyline)
        {
            var segs = polyline.GetSegments().ToList();
            Polyline ent = new Polyline();

            for (int i = 0; i < segs.Count(); i++)
            {
                if (i == 0)
                    ent.AddVertexAt(i, segs[i].StartPoint.To2dPoint(), segs[i].Bulge, 0, 0);
                ent.AddVertexAt(i + 1, segs[i].EndPoint.To2dPoint(), 0, 0, 0);
            }
            ent.Closed = polyline.Closed;
            return ent;
        }

        /// <summary>
        /// Creates the polyline.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyline(List<IChPoint> points, bool closed, string layerName = "", short colorIndex = 257)
        {
            Polyline ent = new Polyline(points.Count);
            for (int i = 0; i < points.Count; i++)
                ent.AddVertexAt(i, points[i].To2dPoint(), 0, 0, 0);
            ent.Closed = closed;
            SetLayer(ent, layerName, colorIndex);
            ChObjectId idLine = ChEntity.AddEntityToDatabase(ent);
            return idLine;
        }

        /// <summary>
        /// Appends the vertice polyline.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="point">The point.</param>
        /// <param name="bulge">The bulge.</param>
        /// <param name="startWidth">The start width.</param>
        /// <param name="endWidth">The end width.</param>
        /// <returns>True if the vertice was appended</returns>
        public static bool AppendVerticePolyline(ChObjectId id, IChPoint point, double bulge = 0, double startWidth = 0, double endWidth = 0)
        {
            return AppendVerticePolyline(id, new List<IChPoint> { point }, bulge, startWidth, endWidth);
        }

        /// <summary>
        /// Appends the vertice to polyline.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="points">The points.</param>
        /// <param name="bulge">The bulge.</param>
        /// <param name="startWidth">The start width.</param>
        /// <param name="endWidth">The end width.</param>
        /// <returns>True if vertice was appended</returns>
        public static bool AppendVerticePolyline(ChObjectId id, List<IChPoint> points, double bulge = 0, double startWidth = 0, double endWidth = 0)
        {
            var pol = new ChEditDbObject<Polyline>(id, OpenMode.ForWrite);
            using (pol)
            {
                if (pol.DbObject == null)
                    return false;
                for (int i = 0; i < points.Count; i++)
                    pol.DbObject.AddVertexAt(pol.DbObject.NumberOfVertices + i, points[i].To2dPoint(), bulge, startWidth, endWidth);
                pol.Comit();
            }
            return true;
        }

        /// <summary>
        /// Creates the poly line2d.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyLine2d(IChPoint startPoint, IChPoint endPoint, string layerName = "", short colorIndex = 257)
        {
            return CreatePolyLined2d(new List<IChPoint> { startPoint, endPoint }, false, layerName, colorIndex);
        }

        /// <summary>
        /// Creates the polylined2d.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyLined2d(List<IChPoint> points, bool closed, string layerName = "", short colorIndex = 257)
        {
            Polyline2d ent = new Polyline2d();
            for (int i = 0; i < points.Count; i++)
                ent.AppendVertex(new Vertex2d(points[i].To3dPoint(), 0, 0, 0, 0));
            ent.Closed = closed;
            SetLayer(ent, layerName, colorIndex);
            ChObjectId idEnt = ChEntity.AddEntityToDatabase(ent);
            return idEnt;
        }

        /// <summary>
        /// Appends the vertice polyline2d.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="point">The point.</param>
        /// <param name="bulge">The bulge.</param>
        /// <param name="startWidth">The start width.</param>
        /// <param name="endWidth">The end width.</param>
        /// <param name="tangent">The tangent.</param>
        /// <returns>True if the point was appended</returns>
        public static bool AppendVerticePolyline2d(ChObjectId id, IChPoint point, double bulge = 0, double startWidth = 0, double endWidth = 0, double tangent = 0)
        {
            return AppendVerticePolyline2d(id, new List<IChPoint> { point }, bulge, startWidth, endWidth, tangent);
        }

        /// <summary>
        /// Appends the vertice polyline2d.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="points">The points.</param>
        /// <param name="bulge">The bulge.</param>
        /// <param name="startWidth">The start width.</param>
        /// <param name="endWidth">The end width.</param>
        /// <param name="tangent">The tangent.</param>
        /// <returns>True if the points was appended</returns>
        public static bool AppendVerticePolyline2d(ChObjectId id, List<IChPoint> points, double bulge = 0, double startWidth = 0, double endWidth = 0, double tangent = 0)
        {
            var pol = new ChEditDbObject<Polyline2d>(id, OpenMode.ForWrite);
            using (pol)
            {
                if (pol.DbObject == null)
                    return false;
                for (int i = 0; i < points.Count; i++)
                    pol.DbObject.AppendVertex(new Vertex2d(points[i].To3dPoint(), bulge, startWidth, endWidth, tangent));
                pol.Comit();
            }
            return true;
        }

        /// <summary>
        /// Creates the poly line3d.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyLine3d(IChPoint startPoint, IChPoint endPoint, string layerName = "", short colorIndex = 257)
        {
            return CreatePolyLined3d(new List<IChPoint> { startPoint, endPoint }, false, layerName, colorIndex);
        }

        /// <summary>
        /// Creates the polylined3d.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreatePolyLined3d(List<IChPoint> points, bool closed, string layerName = "", short colorIndex = 257)
        {
            Polyline3d ent = new Polyline3d();
            for (int i = 0; i < points.Count; i++)
                ent.AppendVertex(new PolylineVertex3d(points[i].To3dPoint()));
            ent.Closed = closed;
            SetLayer(ent, layerName, colorIndex);
            ChObjectId idEnt = ChEntity.AddEntityToDatabase(ent);
            return idEnt;
        }

        /// <summary>
        /// Appends the vertice polyline3d.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="point">The point.</param>
        /// <returns>True if the vertice was appended</returns>
        public static bool AppendVerticePolyline3d(ChObjectId id, IChPoint point)
        {
            return AppendVerticePolyline3d(id, new List<IChPoint> { point });
        }

        /// <summary>
        /// Appends the vertice polyline3d.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="points">The points.</param>
        /// <returns>True if the vertice was appended</returns>
        public static bool AppendVerticePolyline3d(ChObjectId id, List<IChPoint> points)
        {
            var pol = new ChEditDbObject<Polyline3d>(id, OpenMode.ForWrite);
            using (pol)
            {
                if (pol.DbObject == null)
                    return false;
                for (int i = 0; i < points.Count; i++)
                    pol.DbObject.AppendVertex(new PolylineVertex3d(points[i].To3dPoint()));
                pol.Comit();
            }
            return true;
        }

        /// <summary>
        /// Creates the rectangle.
        /// </summary>
        /// <param name="corner1">The corner1.</param>
        /// <param name="corner2">The corner2.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateRectangle(IChPoint corner1, IChPoint corner2, string layerName = "", short colorIndex = 257)
        {
            var vertices = new List<IChPoint>();
            vertices.Add(corner1);
            vertices.Add(ChPoint.Create(corner2.X, corner1.Y, 0));
            vertices.Add(corner2);
            vertices.Add(ChPoint.Create(corner1.X, corner2.Y, 0));
            return CreatePolyline(vertices, true, layerName, colorIndex);
        }

        /// <summary>
        /// Closes the polyline.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="onlyStartPointEqualEndPoint">if set to <c>true</c> [only start point equal end point].</param>
        /// <returns>True if the polyline was closed</returns>
        public static bool ClosePolyline(ChObjectId id, bool onlyStartPointEqualEndPoint = true)//onlyStartPointEqualEndPoint
        {
            var ent = new ChEditDbObject<Polyline>(id, OpenMode.ForWrite);
            using (ent)
            {
                if (ent.DbObject == null)
                    return false;
                if (!onlyStartPointEqualEndPoint)
                {
                    ent.DbObject.Closed = true;
                    ent.Comit();
                    return true;
                }
                else if (onlyStartPointEqualEndPoint && ent.DbObject.NumberOfVertices >= 3)
                {
                    var ptInicial = ent.DbObject.GetPoint3dAt(0);
                    var ptFinal = ent.DbObject.GetPoint3dAt(ent.DbObject.NumberOfVertices - 1);
                    if (ptInicial != ptFinal)
                        return false;
                    ent.DbObject.Closed = true;
                    ent.Comit();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Fecha a polyline
        /// </summary>
        /// <param name="id">ObjectId</param>
        /// <returns>True se a polyline for fechada caso contrario false</returns>
        public static bool ClosePolyline2d(ChObjectId id)
        {
            var ent = new ChEditDbObject<Polyline2d>(id, OpenMode.ForWrite);
            using (ent)
            {
                if (ent.DbObject == null)
                    return false;

                ent.DbObject.Closed = true;
                ent.Comit();
                return true;
            }
        }

        /// <summary>
        /// Fecha a polyline
        /// </summary>
        /// <param name="id">ObjectId</param>
        /// <returns>True se a polyline for fechada caso contrario false</returns>
        public static bool ClosePolyline3d(ChObjectId id)
        {
            var ent = new ChEditDbObject<Polyline3d>(id, OpenMode.ForWrite);
            using (ent)
            {
                if (ent.DbObject == null)
                    return false;

                ent.DbObject.Closed = true;
                ent.Comit();
                return true;
            }
        }

        /// <summary>
        /// Offsets the polyline.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="distance">The distance.</param>
        /// <param name="sidePoint">The side point.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection OffsetPolyline(ChObjectId id, double distance, IChPoint sidePoint)
        {
            var ent = new ChEditPolyline(id);
            DBObjectCollection curvCols;
            using (ent)
            {
                if (ent.DbObject == null)
                    return null;

                distance = CorrectsOffsetDistance(ent, sidePoint, distance);
                curvCols = ent.DbObject.GetOffsetCurves(distance);
            }

            if (curvCols == null || curvCols.Count == 0)
                return null;

            ChObjectIdCollection idsPolys = new ChObjectIdCollection();
            foreach (DBObject item in curvCols)
            {
                ChObjectId idEnt = ChEntity.AddEntityToDatabase((Entity)item);
                idsPolys.Add(idEnt);
            }
            return idsPolys;
        }

        /// <summary>
        /// Correct the offset distance.
        /// </summary>
        /// <param name="editPolyline">The edit polyline.</param>
        /// <param name="sidePoint">The side point.</param>
        /// <param name="distance">The distance.</param>
        /// <returns></returns>
        private static double CorrectsOffsetDistance(ChEditPolyline editPolyline, IChPoint sidePoint, double distance)
        {
            Curve pCurv = null;
            Point3d pDir = (Point3d)(Application.GetSystemVariable("VIEWDIR"));
            Vector3d vDir = pDir.GetAsVector();
            Vector3d vNormal = Vector3d.ZAxis;

            pCurv = editPolyline.DbObject as Curve;
            if (pCurv.IsPlanar)
            {
                Plane plane = pCurv.GetPlane();
                vNormal = plane.Normal;
                sidePoint = sidePoint.To3dPoint().Project(plane, vDir).ToPoint();
            }
            Point3d pNear = pCurv.GetClosestPointTo(sidePoint.To3dPoint(), true);
            Vector3d vSide = sidePoint.To3dPoint() - pNear;
            Vector3d vDeriv;
            try
            {
                vDeriv = pCurv.GetFirstDerivative(pNear);
            }
            catch (Exception)
            {
                var area = editPolyline.DbObject.GetArea();
                return area > 0 ? distance : distance * -1;
            }
            if (vNormal.CrossProduct(vDeriv).DotProduct(vSide) < 0.0)
                return distance;
            else
                return distance * -1;
        }

        #endregion Polilyne

        #region Texto

        /// <summary>
        /// Creates the text.
        /// </summary>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="text">The text.</param>
        /// <param name="altura">The altura.</param>
        /// <param name="angle">The angle.</param>
        /// <param name="textStyleId">The text style identifier.</param>
        /// <param name="textHorizontalMode">The text horizontal mode.</param>
        /// <param name="textVerticalMode">The text vertical mode.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateText(IChPoint insertPoint, string text, double altura, double angle,
            ChObjectId textStyleId, TextHorizontalMode textHorizontalMode = TextHorizontalMode.TextMid,
            TextVerticalMode textVerticalMode = TextVerticalMode.TextVerticalMid, string layerName = "", short colorIndex = 257)
        {
            var tx = new DBText();
            tx.SetDatabaseDefaults();
            tx.HorizontalMode = textHorizontalMode;
            tx.VerticalMode = textVerticalMode;
            tx.Position = insertPoint.To3dPoint();
            tx.AlignmentPoint = insertPoint.To3dPoint();
            tx.Rotation = angle;
            tx.Height = altura;
            tx.TextStyleId = textStyleId;
            tx.TextString = text;
            SetLayer(tx, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(tx);
        }

        #endregion Texto

        #region Circle

        /// <summary>
        /// Creates the circle.
        /// </summary>
        /// <param name="center">The center.</param>
        /// <param name="radius">The radius.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns></returns>
        public static ChObjectId CreateCircle(IChPoint center, double radius, string layerName = "", short colorIndex = 257)
        {
            var c = new Circle();
            c.SetDatabaseDefaults();
            c.Center = center.To3dPoint();
            c.Radius = radius;
            SetLayer(c, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(c);
        }

        /// <summary>
        /// Creates the circle.
        /// </summary>
        /// <param name="circle">The circle.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateCircle(IChCircle circle, string layerName = "", short colorIndex = 257)
        {
            return CreateCircle(circle.Center, circle.Radius, layerName, colorIndex);
        }
        #endregion Circle

        #region Hatch

        /// <summary>
        /// Creates the hatch.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="pattern">The pattern.</param>
        /// <param name="angle">The angle.</param>
        /// <param name="patternScale">The pattern scale.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateHatch(ChObjectIdCollection ids, string pattern, double angle, double patternScale, string layerName = "", short colorIndex = 257)
        {
            Hatch h = new Hatch();
            h.SetDatabaseDefaults();
            h.Normal = new Vector3d(0.0, 0.0, 1.0);
            h.PatternAngle = angle;
            h.Elevation = 0.0;
            h.PatternScale = patternScale;
            h.SetHatchPattern(HatchPatternType.PreDefined, pattern);
            h.Associative = true;
            h.AppendLoop((int)HatchLoopTypes.Default, ids);
            h.EvaluateHatch(true);
            SetLayer(h, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(h);
        }

        #endregion Hatch

        #region Solid

        /// <summary>
        /// Creates the solid.
        /// </summary>
        /// <param name="p1">The p1.</param>
        /// <param name="p2">The p2.</param>
        /// <param name="p3">The p3.</param>
        /// <param name="p4">The p4.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateSolid(IChPoint p1, IChPoint p2, IChPoint p3, IChPoint p4, string layerName = "", short colorIndex = 257)
        {
            Solid s = new Solid(p1.To3dPoint(), p2.To3dPoint(), p3.To3dPoint(), p4.To3dPoint());
            s.SetDatabaseDefaults();
            SetLayer(s, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(s);
        }

        /// <summary>
        /// Creates the solid.
        /// </summary>
        /// <param name="p1">The p1.</param>
        /// <param name="p2">The p2.</param>
        /// <param name="p3">The p3.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId CreateSolid(IChPoint p1, IChPoint p2, IChPoint p3, string layerName = "", short colorIndex = 257)
        {
            Solid s = new Solid(p1.To3dPoint(), p2.To3dPoint(), p3.To3dPoint());
            s.SetDatabaseDefaults();
            SetLayer(s, layerName, colorIndex);
            return ChEntity.AddEntityToDatabase(s);
        }

        #endregion Solid
    }
}
