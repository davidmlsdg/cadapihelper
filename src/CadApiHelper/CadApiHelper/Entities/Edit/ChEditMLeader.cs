﻿#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
using Teigha.DatabaseServices;
#elif GCAD
using GrxCAD.DatabaseServices;
#endif

using CadApiHelper.Core;
namespace CadApiHelper.Entities.Edit
{
    /// <summary>
    /// Edit MLeader
    /// </summary>
    /// <seealso cref="CadApiHelper.Entities.Edit.ChEditDbObject&lt;MLeader&gt;" />
    public class ChEditMLeader : ChEditDbObject<MLeader>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditMLeader"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="modo">The modo.</param>
        public ChEditMLeader(ChObjectId objectId, OpenMode modo) : base(objectId, modo)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditMLeader"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        public ChEditMLeader(ChObjectId objectId, bool edit = false) : base(objectId, edit)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditMLeader"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="db">The database.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        public ChEditMLeader(ChObjectId objectId, Database db, bool edit) : base(objectId, db, edit)
        {
        }
    }
}
