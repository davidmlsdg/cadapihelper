﻿#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
using Teigha.DatabaseServices;
#elif GCAD
using GrxCAD.DatabaseServices;
#endif

using CadApiHelper.Core;

namespace CadApiHelper.Entities.Edit
{
    /// <summary>
    /// Edit BlockReference
    /// </summary>
    public class ChEditBlockReference : ChEditDbObject<BlockReference>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditBlockReference"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="modo">The modo.</param>
        public ChEditBlockReference(ChObjectId objectId, OpenMode modo) : base(objectId, modo)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditBlockReference"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        public ChEditBlockReference(ChObjectId objectId, bool edit = false) : base(objectId, edit)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditBlockReference"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="db">The database.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        public ChEditBlockReference(ChObjectId objectId, Database db, bool edit) : base(objectId, db, edit)
        {
        }
    }
}
