﻿#if ACAD
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
#elif ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
    using Bricscad.ApplicationServices;
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
#endif

using System;
using CadApiHelper.Core;

namespace CadApiHelper.Entities.Edit
{
    /// <summary>
    /// ChEditDbObject class
    /// </summary>
    /// <typeparam name="T">DBObject</typeparam>
    /// <seealso cref="System.IDisposable" />
    public class ChEditDbObject<T> : IDisposable where T : DBObject
    {
        private Database db;
        private T m_entidade;
        private OpenMode m_modo;
        private ObjectId m_objectId;
        private Transaction m_trans;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditDbObject{T}"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="modo">The modo.</param>
        public ChEditDbObject(ChObjectId objectId, OpenMode modo)
        {
            m_objectId = objectId;
            m_modo = modo;
            this.db = acApp.DocumentManager.MdiActiveDocument.Database;
            StartTransation();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditDbObject{T}"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        public ChEditDbObject(ChObjectId objectId, bool edit = false)
        {
            m_objectId = objectId;
            m_modo = edit ? OpenMode.ForWrite : OpenMode.ForRead;
            this.db = acApp.DocumentManager.MdiActiveDocument.Database;
            StartTransation();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEditDbObject{T}"/> class.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="db">The database.</param>
        /// <param name="edit">if set to <c>true</c> [edit].</param>
        public ChEditDbObject(ChObjectId objectId, Database db, bool edit)
        {
            m_objectId = objectId;
            m_modo = edit ? OpenMode.ForWrite : OpenMode.ForRead;
            if (db == null)
                this.db = acApp.DocumentManager.MdiActiveDocument.Database;
            else
                this.db = db;
            StartTransation();
        }

        /// <summary>
        /// Finalizes an instance of the <see cref="ChEditDbObject{T}"/> class.
        /// </summary>
        ~ChEditDbObject()
        {
            AbortTransaction();
        }

        /// <summary>
        /// Gets the database object.
        /// </summary>
        /// <value>
        /// The database object.
        /// </value>
        public T DbObject
        {
            get
            {
                if (m_objectId.IsNull)
                    return null;

                StartTransation();
                if (m_entidade == null)
                {
                    m_entidade = m_trans.GetObject(m_objectId, m_modo) as T;
                    if (m_entidade == null)
                        AbortTransaction();
                }
                return m_entidade;
            }
        }

        /// <summary>
        /// Gets the transaction.
        /// </summary>
        /// <value>
        /// The transaction.
        /// </value>
        public Transaction Transaction
        {
            get { return m_trans; }
        }

        /// <summary>
        /// Aborts the changes.
        /// </summary>
        public void Abort()
        {
            AbortTransaction();
        }

        /// <summary>
        /// Comits the changes.
        /// </summary>
        public void Comit()
        {
            ComitTransaction();
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            AbortTransaction();
        }

        private void AbortTransaction()
        {
            if (m_trans != null)
            {
                m_trans.Abort();
                m_trans.Dispose();
                m_trans = null;
                m_entidade = null;
            }
        }

        private void ComitTransaction()
        {
            if (m_trans != null)
            {
                m_trans.Commit();
                m_trans.Dispose();
                m_trans = null;
                m_entidade = null;
            }
        }

        private void StartTransation()
        {
            if (m_trans == null)
                m_trans = db.TransactionManager.StartTransaction();
        }
    }
}