﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
#endif

#if ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.Geometry;
#endif

#if BRCAD
    using Bricscad.ApplicationServices;
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
    using Bricscad.EditorInput;
    using Teigha.Colors;
#endif

#if GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
#endif

using System.Text;
using CadApiHelper.Entities.Edit;
using CadApiHelper.Core;
using System.Collections.Generic;
using CadApiHelper.EditorInput;
using CadApiHelper.Geometry;
using CadApiHelper.Layer;

namespace CadApiHelper.Entities
{
    /// <summary>
    /// ChEntity - Manager Entity
    /// </summary>
    public static class ChEntity
    {
        #region Private Fields

        private static StringBuilder erros = new StringBuilder();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Highlights the entity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="highlight">if set to <c>true</c> [highlight].</param>
        public static void HighlightEntity(ChObjectId id, bool highlight = true)
        {
            var view = new ViewTableRecord();
            var ent = new ChEditEntity(id, OpenMode.ForWrite);
            using (ent)
            {
                if (ent.DbObject == null)
                    return;
                ObjectId[] ids = new ObjectId[1];
                ids[0] = id;

                var index = new SubentityId();
                var path = new FullSubentityPath(ids, index);
                if (highlight)
                    ent.DbObject.Highlight(path, true);
                else
                    ent.DbObject.Unhighlight(path, true);
                ent.Comit();
            }
            ChEditor.GetEditor().SetCurrentView(view);
        }

        /// <summary>
        /// Highlights the entity and zoom.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="highlight">if set to <c>true</c> [highlight].</param>
        public static void HighlightEntityAndZoom(ChObjectId id, bool highlight = true)
        {
            var view = new ViewTableRecord();
            var ent = new ChEditEntity(id, OpenMode.ForWrite);
            using (ent)
            {
                if (ent.DbObject == null)
                    return;

                var bounds = ent.DbObject.Bounds;
                if (bounds != null)
                {
                    IChPoint pt = bounds.Value.MinPoint.ToPoint();
                    pt = pt.Mid(bounds.Value.MaxPoint.ToPoint());
                    view.CenterPoint = pt.To2dPoint();
                    view.Height = (bounds.Value.MaxPoint.Y - bounds.Value.MinPoint.Y) * 1.1;
                    view.Width = (bounds.Value.MaxPoint.X - bounds.Value.MinPoint.X) * 1.1;
                }
                ObjectId[] ids = new ObjectId[1];
                ids[0] = id;

                var index = new SubentityId();
                var path = new FullSubentityPath(ids, index);
                if (highlight)
                    ent.DbObject.Highlight(path, true);
                else
                    ent.DbObject.Unhighlight(path, true);
                ent.Comit();
            }
            ChEditor.GetEditor().SetCurrentView(view);
        }

        /// <summary>
        /// Gets the entity.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>Entity</returns>
        public static Entity GetEntity(ChObjectId id)
        {
            using (var ent = new ChEditEntity(id))
            {
                return ent.DbObject;
            }
        }

        /// <summary>
        /// Copies the specified user message.
        /// </summary>
        /// <param name="userMessage">The user message.</param>
        public static void Copy(string userMessage = "\nSelect the entities to copy")
        {
            Database db = ChEditor.GetDatabase();
            Editor ed = ChEditor.GetEditor();
            PromptEntityOptions options = new PromptEntityOptions(userMessage);
            PromptEntityResult acSSPrompt = ed.GetEntity(options);
            if (acSSPrompt.Status != PromptStatus.OK)
                return;

            ObjectIdCollection collection = new ObjectIdCollection();
            collection.Add(acSSPrompt.ObjectId);
            ObjectId ModelSpaceId = SymbolUtilityServices.GetBlockModelSpaceId(db);
            IdMapping mapping = new IdMapping();
            db.DeepCloneObjects(collection, ModelSpaceId, mapping, false);
            using (Transaction Tx = db.TransactionManager.StartTransaction())
            {
                IdPair pair1 = mapping[acSSPrompt.ObjectId];
                Entity ent = Tx.GetObject(pair1.Value, OpenMode.ForWrite) as Entity;
                Tx.Commit();
            }
        }

        /// <summary>
        /// Adds the entity to database.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="database">The database.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId AddEntityToDatabase(Entity entity, Database database = null)
        {
            erros.Length = 0;
            ObjectId objId;
            if (database == null)
                database = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction acTrans = database.TransactionManager.StartTransaction())
            {
                try
                {
                    var btr = acTrans.GetObject(database.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;
                    if (btr == null)
                    {
                        return new ObjectId();
                    }
                    objId = btr.AppendEntity(entity);
                    acTrans.AddNewlyCreatedDBObject(entity, true);
                    acTrans.Commit();
                }
                catch (System.Exception e)
                {
                    erros.AppendLine(e.Message);
                    System.Diagnostics.Debug.Print(e.Message);
                    acTrans.Abort();
                    return new ObjectId();
                }
            }
            return objId;
        }


        /// <summary>
        /// Adds the entities to database.
        /// </summary>
        /// <param name="entities">The entities.</param>
        /// <param name="database">The database.</param>
        /// <returns>Quantity of entities that was added</returns>
        public static int AddEntitiesToDatabase(IEnumerable<Entity> entities, Database database = null)
        {
            erros.Length = 0;
            ObjectId objId;
            if (database == null)
                database = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction acTrans = database.TransactionManager.StartTransaction())
            {
                int contagem = 0;
                try
                {
                    var btr = acTrans.GetObject(database.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;
                    if (btr == null)
                        return contagem;


                    foreach (var ent in entities)
                    {
                        objId = btr.AppendEntity(ent);
                        acTrans.AddNewlyCreatedDBObject(ent, true);
                        if (!objId.IsNull)
                            contagem++;
                    }

                    acTrans.Commit();
                    return contagem;
                }
                catch (System.Exception e)
                {
                    erros.AppendLine(e.Message);
                    System.Diagnostics.Debug.Print(e.Message);
                    acTrans.Abort();
                    return contagem;
                }
            }
        }

        /// <summary>
        /// Gets all entities from database.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection GetAllEntitiesFromDatabase(Database database = null)
        {
            erros.Length = 0;
            if (database == null)
                database = acApp.DocumentManager.MdiActiveDocument.Database;

            ObjectIdCollection objId = new ObjectIdCollection();
            using (Transaction acTrans = database.TransactionManager.StartTransaction())
            {
                try
                {
                    BlockTableRecord btr = acTrans.GetObject(database.CurrentSpaceId, OpenMode.ForWrite) as BlockTableRecord;

                    if (btr == null)
                    {
                        return new ObjectIdCollection();
                    }
                    foreach (ObjectId item in btr)
                        objId.Add(item);
                    acTrans.Commit();
                }
                catch (System.Exception e)
                {
                    erros.AppendLine(e.Message);
                    acTrans.Abort();
                    return new ObjectIdCollection();
                }
            }
            return objId;
        }

        /// <summary>
        /// Deletes the entities.
        /// </summary>
        /// <param name="objectIds">The object ids.</param>
        /// <param name="database">The database.</param>
        public static void DelEntities(ChObjectIdCollection objectIds, Database database = null)
        {
            foreach (var id in objectIds)
                DelEntity(id, database);
        }

        /// <summary>
        /// Deletes the entity.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="database">The database.</param>
        /// <returns>True if the entity was deleted</returns>
        public static bool DelEntity(ObjectId objectId, Database database = null)
        {
            erros.Length = 0;
            if (database == null)
                database = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction acTrans = database.TransactionManager.StartTransaction())
            {
                try
                {
                    var eAux = acTrans.GetObject(objectId, OpenMode.ForWrite) as Entity;
                    if (eAux == null)
                        return false;

                    eAux.Erase();
                    acTrans.Commit();
                }
                catch (System.Exception e)
                {
                    erros.AppendLine(e.Message);
                    acTrans.Abort();
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Explodes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="database">The database.</param>
        /// <returns>ChObjectIdCollection</returns>
        public static ChObjectIdCollection Explode(ObjectId id, Database database = null)
        {
            ChObjectIdCollection lista = new ChObjectIdCollection();
            DBObjectCollection ents = null;

            var ent = new ChEditEntity(id, database, false);
            using (ent)
            {
                if (ent.DbObject == null)
                    return null;

                ents = new DBObjectCollection();
                ent.DbObject.Explode(ents);
            }

            foreach (DBObject item in ents)
            {
                if (database == null)
                    lista.Add(AddEntityToDatabase((Entity)item));
                else
                    lista.Add(AddEntityToDatabase((Entity)item, database));
            }

            return lista;
        }

        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <returns>Errors</returns>
        public static string GetErrors()//errors
        {
            return erros.ToString();
        }

        /// <summary>
        /// Gets the xdata.
        /// </summary>
        /// <param name="objId">The object identifier.</param>
        /// <param name="database">The database.</param>
        /// <returns>ResultBuffer or null</returns>
        public static ResultBuffer GetXdata(ObjectId objId, Database database = null)
        {
            erros.Length = 0;
            if (database == null)
                database = HostApplicationServices.WorkingDatabase;
            ResultBuffer rb;
            try
            {
                using (Transaction tr = database.TransactionManager.StartTransaction())
                {
                    var obj = tr.GetObject(objId, OpenMode.ForRead);
                    if (obj == null || obj.XData == null)
                    {
                        return null;
                    }
                    rb = obj.XData;
                    tr.Commit();
                }
                return rb;
            }
            catch (System.Exception e)
            {
                erros.AppendLine(e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the xdata.
        /// </summary>
        /// <param name="objId">The object identifier.</param>
        /// <param name="appName">Name for identify XDATA.</param>
        /// <param name="database">The database.</param>
        /// <returns>ResultBuffer or null</returns>
        public static ResultBuffer GetXdata(ObjectId objId, string appName, Database database = null)
        {
            ResultBuffer rb = GetXdata(objId, database);
            if (rb == null)
                return null;

            ResultBuffer rbNew = new ResultBuffer();
            bool isSerialized = false;
            foreach (TypedValue item in rb.AsArray())
            {
                if (item.TypeCode == 1001)
                    isSerialized = item.Value.ToString() == appName;
                if (isSerialized)
                    rbNew.Add(item);
            }
            return rbNew;
        }

        /// <summary>
        /// Sets the color.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="indexColor">Color of the index.</param>
        /// <param name="database">The database.</param>
        /// <returns>True if the color was changed</returns>
        public static bool SetColor(ObjectId id, short indexColor, Database database = null)
        {
            using (var ent = new ChEditDbObject<Entity>(id, database, true))
            {
                if (ent.DbObject == null)
                    return false;
                ent.DbObject.ColorIndex = indexColor;
                ent.Comit();
                return true;
            }
        }

        /// <summary>
        /// Sets the layer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="database">The database.</param>
        /// <returns>True if the layer for entity was changed</returns>
        public static bool SetLayer(ObjectId id, string layerName, Database database = null)
        {
            ObjectId idLayer = ChLayers.GetObjectId(layerName);
            if (idLayer.IsNull || id.IsNull)
                return false;
            return SetLayer(id, idLayer);
        }

        /// <summary>
        /// Sets the layer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="layerId">The layer identifier.</param>
        /// <param name="db">The database.</param>
        /// <returns>True if the layer for entity was changed</returns>
        public static bool SetLayer(ObjectId id, ObjectId layerId, Database db = null)
        {
            using (var ent = new ChEditDbObject<Entity>(id, db, true))
            {
                if (ent.DbObject == null || layerId.IsNull)
                    return false;
                ent.DbObject.LayerId = layerId;
                ent.Comit();
                return true;
            }
        }

        /// <summary>
        /// Sets the type of the line.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="style">The style.</param>
        /// <param name="database">The database.</param>
        /// <returns>True if the Linetype was changed</returns>
        public static bool SetLineType(ObjectId id, string style, Database database = null)
        {
            if (id.IsNull || style == null || style.Trim().Length == 0)
                return false;
            using (var ent = new ChEditDbObject<Entity>(id, database, true))
            {
                if (ent.DbObject == null)
                    return false;
                ent.DbObject.Linetype = style;
                ent.Comit();
                return true;
            }
        }

        /// <summary>
        /// Sets the type of the line.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="styleId">The style identifier.</param>
        /// <param name="database">The database.</param>
        /// <returns>True if the Linetype was changed</returns>
        public static bool SetLineType(ObjectId id, ObjectId styleId, Database database = null)
        {
            if (database == null)
                database = HostApplicationServices.WorkingDatabase;
            using (var ent = new ChEditDbObject<Entity>(id, database, true))
            {
                if (ent.DbObject == null || styleId.IsNull)
                    return false;
                ent.DbObject.LinetypeId = styleId;
                ent.Comit();
                return true;
            }

        }

        /// <summary>
        /// Registries the xdata.
        /// </summary>
        /// <param name="appNames">The application names.</param>
        /// <returns>True if all xdata names was registered</returns>
        public static bool RegistryXdata(IEnumerable<string> appNames)
        {
            erros.Length = 0;
            bool temErros = false;
            Database db = HostApplicationServices.WorkingDatabase;
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                RegAppTable regAppTable = (RegAppTable)tr.GetObject(db.RegAppTableId, OpenMode.ForWrite);
                foreach (var appname in appNames)
                {
                    if (!regAppTable.Has(appname))
                    {
                        try
                        {
                            using (RegAppTableRecord regAppRecord = new RegAppTableRecord())
                            {
                                regAppRecord.Name = appname;
                                regAppTable.Add(regAppRecord);
                                tr.AddNewlyCreatedDBObject(regAppRecord, true);
                            }
                        }
                        catch (System.Exception e)
                        {
                            temErros = true;
                            erros.AppendLine(e.Message);
                            throw;
                        }
                    }
                }
                tr.Commit();
            }
            return !temErros;
        }

        /// <summary>
        /// Sets the xdata.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="values">The values.</param>
        /// <returns>True if xdata was include</returns>
        public static bool SetXdata(DBObject entity, string applicationName, params TypedValue[] values)
        {
            erros.Length = 0;
            Database db = HostApplicationServices.WorkingDatabase;
            // using(TsDocument doc = new TsDocument()) {
            try
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    RegAppTable regAppTable = (RegAppTable)tr.GetObject(db.RegAppTableId, OpenMode.ForRead);
                    if (!regAppTable.Has(applicationName))
                    {
                        using (RegAppTableRecord regAppRecord = new RegAppTableRecord())
                        {
                            regAppRecord.Name = applicationName;
                            regAppTable.UpgradeOpen();
                            regAppTable.Add(regAppRecord);
                            regAppTable.DowngradeOpen();
                            tr.AddNewlyCreatedDBObject(regAppRecord, true);
                        }
                    }
                    entity.XData = GetCleanXData(entity.XData, new ResultBuffer(values), applicationName);
                    tr.Commit();
                }
                return true;
            }
            catch (System.Exception e)
            {
                erros.AppendLine(e.Message);
                return false;
            }
            // }
        }

        /// <summary>
        /// Sets the xdata.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="applicationName">Name of the application.</param>
        /// <param name="values">The values.</param>
        /// <returns>True if the xdata is include</returns>
        public static bool SetXdata(ObjectId id, string applicationName, params TypedValue[] values)
        {
            erros.Length = 0;
            Database db = HostApplicationServices.WorkingDatabase;
            bool result = false;
            // using (TsDocument doc = new TsDocument()) {
            try
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    var ent = tr.GetObject(id, OpenMode.ForWrite, true);
                    result = SetXdata(ent, applicationName, values);
                    tr.Commit();
                }
                return result;
            }
            catch (System.Exception e)
            {
                erros.AppendLine(e.Message);
                return false;
            }
            // }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Gets the clean x data.
        /// </summary>
        /// <param name="rbOld">The rb old.</param>
        /// <param name="rbNew">The rb new.</param>
        /// <param name="appName">Name of the application.</param>
        /// <returns></returns>
        private static ResultBuffer GetCleanXData(ResultBuffer rbOld, ResultBuffer rbNew, string appName)
        {
            ResultBuffer rbTemp = new ResultBuffer();
            if (rbOld == null)
                rbOld = new ResultBuffer();

            if (rbNew == null)
                rbNew = new ResultBuffer();

            bool isThisAppName = false;
            foreach (TypedValue item in rbOld.AsArray())
            {
                if (item.TypeCode == 1001)
                    isThisAppName = item.Value.ToString() == appName;

                if (!isThisAppName)
                    rbTemp.Add(item);
            }

            foreach (TypedValue item in rbNew)
                rbTemp.Add(item);

            return rbTemp;
        }

        #endregion Private Methods
    }
}
