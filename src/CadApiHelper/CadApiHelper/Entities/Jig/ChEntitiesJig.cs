﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
#elif ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.GraphicsInterface;
#elif BRCAD
using Teigha.DatabaseServices;
using Bricscad.EditorInput;
using Teigha.Geometry;
using Teigha.GraphicsInterface;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.EditorInput;

namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// ChEntitiesJig - Jig entities
    /// </summary>
    /// <seealso cref="DrawJig" />
    public class ChEntitiesJig : DrawJig
    {
        private JigBehavior behavior;
        private ChObjectIdCollection ids;
        private Dictionary<JigAction, string> actionsMessages;
        private IChPoint basePoint = ChPoint.Create();
        private IChPoint destinationPoint = ChPoint.Create();
        private double angle = 0;
        private double scale = 1;
        private List<Entity> entities = new List<Entity>();
        private KeyValuePair<JigAction, string> action;
        private IChLineSegment mirrorLine = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEntitiesJig"/> class.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="actionsMessages">The actions messages.</param>
        /// <param name="behavior">The behavior.</param>
        public ChEntitiesJig(ChObjectIdCollection ids, IChPoint basePoint, Dictionary<JigAction, string> actionsMessages,
            JigBehavior behavior = JigBehavior.OnlyShadow)
        {
            this.ids = ids;
            this.basePoint = basePoint;
            basePoint = basePoint.To3dPoint().TransformBy(GetMatrixUCS()).ToPoint();
            this.actionsMessages = actionsMessages;
            this.behavior = behavior;
            destinationPoint = this.basePoint;
        }

        /// <summary>
        /// Gets or sets the mirror line.
        /// </summary>
        /// <value>
        /// The mirror line.
        /// </value>
        public IChLineSegment MirrorLine
        {
            get { return mirrorLine; }
            set { mirrorLine = value; }
        }

        private Matrix3d GetMatrixUCS()
        {
            return ChEditor.GetEditor().CurrentUserCoordinateSystem;
        }

        /// <summary>
        /// Gets or sets the destination point.
        /// </summary>
        /// <value>
        /// The destination point.
        /// </value>
        public IChPoint DestinationPoint
        {
            get { return destinationPoint; }
            set { destinationPoint = value; }
        }

        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        /// <summary>
        /// Gets or sets the scale.
        /// </summary>
        /// <value>
        /// The scale.
        /// </value>
        public double Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        /// <summary>
        /// Gets the pt base.
        /// </summary>
        /// <value>
        /// The pt base.
        /// </value>
        public IChPoint PtBase
        {
            get
            {
                return basePoint;
            }
        }

        private Matrix3d Transformation
        {
            get
            {

                var vet = destinationPoint.To3dPoint()  - basePoint.To3dPoint();
                var mat = Matrix3d.Scaling(scale, destinationPoint.To3dPoint()).
                 PostMultiplyBy(Matrix3d.Rotation(angle, Vector3d.ZAxis, destinationPoint.To3dPoint())).
                 PostMultiplyBy(Matrix3d.Displacement(vet));
                if ( mirrorLine != null && mirrorLine.StartPoint != null && mirrorLine.EndPoint != null)
                {
                    mat.PostMultiplyBy(Matrix3d.Mirroring(mirrorLine.GetLine3d()));
                }
                return mat;
            }
        }

        /// <summary>
        /// Start the jigs action.
        /// </summary>
        /// <returns></returns>
        public bool Jig()
        {
            if (ids == null || ids.Count == 0)
                return false;
            var db = ChEditor.GetDatabase();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                ProcessaEntities(tr);
                try
                {
                    PromptResult pr;
                    foreach (var item in actionsMessages)
                    {
                        action = item;
                        pr = ChEditor.GetEditor().Drag(this);
                        if (pr.Status == PromptStatus.Cancel || pr.Status == PromptStatus.Error)
                        {
                            tr.Abort();
                            return false;
                        }
                    }
                }
                catch
                {
                    tr.Abort();
                }
                AbortComitEntities(tr);
                return true;
            }
        }

        private void AbortComitEntities(Transaction tr)
        {
            if (entities == null || entities.Count == 0)
            {
                tr.Abort();
                return;
            }

            switch (behavior)
            {
                case JigBehavior.OnlyShadow:
                    tr.Abort();
                    break;

                case JigBehavior.ChangeEntities:
                    for (int i = 0; i < entities.Count; i++)
                        entities[i].TransformBy(Transformation);
                    tr.Commit();
                    break;

                case JigBehavior.MakeCopy:
                    tr.Abort();
                    //implementar deepClone
                    break;

                default:
                    tr.Abort();
                    break;
            }

            entities.Clear();
        }

        /// <summary>
        /// Samplers the specified prompts.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <returns>SamplerStatus</returns>
        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            switch (action.Key)
            {
                case JigAction.Move:
                    return JigSamplerActions.MoveAction(prompts, action.Value, ref basePoint, ref destinationPoint);

                case JigAction.Rotate:
                    return JigSamplerActions.RotateAction(prompts, action.Value, ref destinationPoint, ref angle);

                case JigAction.Scale_Distance:
                    return JigSamplerActions.ScaleAction(prompts, action.Value, ref destinationPoint, ref scale);

                case JigAction.Mirror:
                    return JigSamplerActions.MirrorAction(prompts, action.Value, ref basePoint, ref destinationPoint, ref mirrorLine);

                default:
                    return SamplerStatus.NoChange;
            }
        }

        private void ProcessaEntities(Transaction tr)
        {
            for (int i = 0; i < ids.Count; i++)
            {
                Entity ent = (Entity)tr.GetObject(ids[i], OpenMode.ForWrite);
                if (ent != null)
                    entities.Add(ent);
            }
        }

        /// <summary>
        /// Worlds the draw.
        /// </summary>
        /// <param name="draw">The draw.</param>
        /// <returns></returns>
        protected override bool WorldDraw(WorldDraw draw)
        {
            Matrix3d mat = Transformation;
            WorldGeometry geo = draw.Geometry;
            if (geo != null)
            {
                geo.PushModelTransform(mat);

                foreach (Entity ent in entities)
                    geo.Draw(ent);

                geo.PopModelTransform();
            }

            return true;
        }
    }
}
