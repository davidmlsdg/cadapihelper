﻿namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// JigBehavior enum
    /// </summary>
    public enum JigBehavior
    {
        /// <summary>
        /// Only shadow
        /// </summary>
        OnlyShadow,

        /// <summary>
        /// Change entities
        /// </summary>
        ChangeEntities,

        /// <summary>
        /// Make copy
        /// </summary>
        MakeCopy,

        /// <summary>
        /// Insert in database
        /// </summary>
        InsertInDatabase,
    }
}
