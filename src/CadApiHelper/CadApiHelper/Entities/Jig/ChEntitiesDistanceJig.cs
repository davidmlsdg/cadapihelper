﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
#elif ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.GraphicsInterface;
#elif BRCAD
using Teigha.DatabaseServices;
using Bricscad.EditorInput;
using Teigha.Geometry;
using Teigha.GraphicsInterface;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
#endif

using System.Collections.Generic;
using System.Linq;
using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.EditorInput;

namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// Jig entities
    /// </summary>
    /// <seealso cref="DrawJig" />
    public class ChEntitiesDistanceJig : DrawJig
    {
        private Dictionary<JigAction, string> messageActions;
        private IChPoint basePoint = ChPoint.Create();
        private IChPoint destinationPoint = ChPoint.Create();
        private double angle = 0;
        private double scale = 1;
        private List<DataJigDistance> dataJig = new List<DataJigDistance>();
        private KeyValuePair<JigAction, string> action;
        private IChLineSegment mirroLine = ChLineSegment.Create(ChPoint.Create(), ChPoint.Create());
        private bool inserir;
        private string msgScale;

        /// <summary>
        /// Gets or sets the entity identifier.
        /// </summary>
        /// <value>
        /// The entity identifier.
        /// </value>
        public ChObjectId EntityId { get; set; }

        /// <summary>
        /// Gets or sets the current data jig.
        /// </summary>
        /// <value>
        /// The current data jig.
        /// </value>
        public DataJigDistance CurrentDataJig { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEntitiesDistanceJig"/> class.
        /// </summary>
        /// <param name="dataJig">The data jig.</param>
        /// <param name="ptBase">The pt base.</param>
        /// <param name="msgScale">The MSG scale.</param>
        /// <param name="inserir">if set to <c>true</c> [inserir].</param>
        public ChEntitiesDistanceJig(List<DataJigDistance> dataJig, IChPoint ptBase, string msgScale, bool inserir = false)
        {
            this.msgScale = msgScale;
            var pt = new Point3d(ptBase.X, ptBase.Y, ptBase.Z);
            basePoint = pt.TransformBy(GetMatrixUCS()).ToPoint();
            destinationPoint = basePoint;
            messageActions = new Dictionary<JigAction, string>();
            messageActions.Add(JigAction.Scale_Distance, msgScale);
            this.inserir = inserir;
            //m_operacoesMsgs.Add(JigAction.Rotacionar, "\nAngulo: ");
            if (dataJig.Any())
            {
                dataJig = dataJig.OrderBy(x => x.Distance).ToList();
                CurrentDataJig = this.dataJig.First();
                SetPontoBaseDestino();
            }
            else
                this.dataJig = new List<DataJigDistance>();
        }

        private void SetPontoBaseDestino()
        {
            if (CurrentDataJig.Point != null)
            {
                basePoint = CurrentDataJig.Point;
                destinationPoint = CurrentDataJig.Point;
            }
        }

        /// <summary>
        /// Gets or sets the mirror line.
        /// </summary>
        /// <value>
        /// The mirror line.
        /// </value>
        public IChLineSegment MirrorLine
        {
            get { return mirroLine; }
            set { mirroLine = value; }
        }

        private Matrix3d GetMatrixUCS()
        {
            return ChEditor.GetEditor().CurrentUserCoordinateSystem;
        }

        /// <summary>
        /// Gets or sets the destination point.
        /// </summary>
        /// <value>
        /// The destination point.
        /// </value>
        public IChPoint DestinationPoint
        {
            get { return destinationPoint; }
            set { destinationPoint = value; }
        }

        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        /// <summary>
        /// Gets or sets the scale.
        /// </summary>
        /// <value>
        /// The scale.
        /// </value>
        public double Scale
        {
            get { return scale; }
            set { scale = value; }
        }

        /// <summary>
        /// Gets the pt base.
        /// </summary>
        /// <value>
        /// The pt base.
        /// </value>
        public IChPoint PtBase
        {
            get
            {
                return basePoint;
            }
        }

        private Matrix3d Transformation
        {
            get
            {
                var mat = Matrix3d.Rotation(angle, Vector3d.ZAxis, basePoint.To3dPoint());
                var ent = dataJig.Find(x => x.Distance >= scale);
                if (ent != null)
                {
                    CurrentDataJig = ent;
                    SetPontoBaseDestino();
                }

                return mat;
            }
        }

        /// <summary>
        /// Jigs this instance.
        /// </summary>
        /// <returns></returns>
        public bool Jig()
        {
            if (CurrentDataJig == null)
                return false;
            var db = ChEditor.GetDatabase();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                try
                {
                    PromptResult pr;
                    foreach (var item in messageActions)
                    {
                        action = item;
                        pr = ChEditor.GetEditor().Drag(this);
                        if (pr.Status == PromptStatus.Cancel || pr.Status == PromptStatus.Error)
                        {
                            tr.Abort();
                            return false;
                        }
                    }
                }
                catch
                {
                    tr.Abort();
                    return false;
                }
            }
            AddToDatabase();
            return true;
        }

        /// <summary>
        /// Adds to database.
        /// </summary>
        private void AddToDatabase()
        {
            if (CurrentDataJig != null && inserir)
            {
                CurrentDataJig.Entity.TransformBy(Transformation);
                EntityId = ChEntity.AddEntityToDatabase(CurrentDataJig.Entity);
            }
        }

        /// <summary>
        /// Samplers the specified prompts.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <returns></returns>
        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            return JigSamplerActions.ScaleAction(prompts, msgScale, ref basePoint, ref scale);
        }

        /// <summary>
        /// Worlds the draw.
        /// </summary>
        /// <param name="draw">The draw.</param>
        /// <returns></returns>
        protected override bool WorldDraw(WorldDraw draw)
        {
            Matrix3d mat = Transformation;
            WorldGeometry geo = draw.Geometry;
            if (geo != null)
            {
                geo.PushModelTransform(mat);
                geo.Draw(CurrentDataJig.Entity);
                geo.PopModelTransform();
            }

            return true;
        }
    }
}
