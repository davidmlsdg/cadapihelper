﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
#elif ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.GraphicsInterface;
#elif BRCAD
using Teigha.DatabaseServices;
using Bricscad.EditorInput;
using Teigha.Geometry;
using Teigha.GraphicsInterface;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
#endif

using CadApiHelper.Geometry;
using CadApiHelper.EditorInput;
using CadApiHelper.Core;

namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// ChEntityMirrorJigger - Mirror Jig for one entity
    /// </summary>
    /// <seealso cref="EntityJig" />
    public class ChEntityMirrorJigger : EntityJig
    {
        private int curJigFactorIndex = 1;
        private IChPoint point1 = null, point2 = null;
        private Matrix3d lastMat = Matrix3d.Identity;
        private string message = "";
        private JigBehavior behaviorJig;

        private Editor Editor
        {
            get
            {
                return ChEditor.GetEditor();
            }
        }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public ChObjectId ObjectId { get; set; }

        private Matrix3d UCS
        {
            get
            {
                return Editor.CurrentUserCoordinateSystem;
            }
        }

        /// <summary>
        /// Construtora
        /// </summary>
        /// <param name="entity">Entidade</param>
        /// <param name="basePoint">Ponto base</param>
        /// <param name="message">Mensagem apresentada ao usuário</param>
        /// <param name="behaviorJig">Comportamento</param>
        public ChEntityMirrorJigger(Entity entity, IChPoint basePoint, string message,
            JigBehavior behaviorJig = JigBehavior.ChangeEntities) : base(entity)
        {
            point1 = basePoint.To3dPoint().TransformBy(UCS).ToPoint();//.TransformBy(UCS);
            this.message = message;
            this.behaviorJig = behaviorJig;
        }

        /// <summary>
        /// Samplers the specified prompts.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <returns>SamplerStatus</returns>
        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            switch (curJigFactorIndex)
            {
                case 1:
                    var op = new JigPromptPointOptions(message);
#if ZWCAD || BRCAD
                    op.UserInputControls = UserInputControls.Accept3dCoordinates;
#else
                    op.UserInputControls = UserInputControls.GovernedByUCSDetect | UserInputControls.UseBasePointElevation
                        | UserInputControls.Accept3dCoordinates;
#endif
                    op.BasePoint = point1.To3dPoint();
                    var result = prompts.AcquirePoint(op);
                    if (result.Status == PromptStatus.Cancel)
                        return SamplerStatus.Cancel;

                    if (point2 != null && result.Value.IsEqualTo(point2.To3dPoint()))
                        return SamplerStatus.NoChange;
                    else
                    {
                        point2 = result.Value.ToPoint();
                        return SamplerStatus.OK;
                    }
            }
            return SamplerStatus.OK;
        }

        /// <summary>
        /// Updates the matrix.
        /// </summary>
        /// <returns></returns>
        protected override bool Update()
        {
            var mat = Matrix3d.Mirroring(new Line3d(point1.To3dPoint(), point2.To3dPoint()));
            Entity.TransformBy(lastMat.PostMultiplyBy(mat));
            lastMat = mat;
            return true;
        }

        /// <summary>
        /// Start the Jig for the entity.
        /// </summary>
        /// <param name="transaction">The transaction.</param>
        /// <returns></returns>
        public PromptStatus Jig(Transaction transaction)
        {
            try
            {
                var pr = ChEditor.GetEditor().Drag(this);
                if (pr.Status != PromptStatus.OK)
                {
                    transaction.Abort();
                    return pr.Status;
                }
                AbortOrComitEntity(transaction);
            }
            catch
            {
                return PromptStatus.Error;
            }
            return PromptStatus.OK;
        }

        private void AbortOrComitEntity(Transaction tr)
        {
            if (Entity == null)
            {
                tr.Abort();
                return;
            }

            switch (behaviorJig)
            {
                case JigBehavior.OnlyShadow:
                    tr.Abort();
                    break;

                case JigBehavior.ChangeEntities:
                    //Entity.TransformBy(lastMat);
                    tr.Commit();
                    break;

                case JigBehavior.MakeCopy:
                    tr.Abort();
                    //implementar deepClone
                    break;

                default:
                    tr.Abort();
                    break;
            }
        }
    }
}
