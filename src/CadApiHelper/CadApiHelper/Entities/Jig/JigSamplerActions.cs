﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
#elif ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.GraphicsInterface;
#elif BRCAD
    using Teigha.Geometry;
    using Bricscad.EditorInput;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
#endif

using System.Diagnostics;
using CadApiHelper.Geometry;

namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// Jig Sampler Actions
    /// </summary>
    public class JigSamplerActions
    {
        /// <summary>
        /// Actions scale.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <param name="message">The message.</param>
        /// <param name="destinationPoint">The destination point.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>SamplerStatus</returns>
        public static SamplerStatus ScaleAction(JigPrompts prompts, string message, ref IChPoint destinationPoint, ref double scale)//
        {
            JigPromptDistanceOptions prOp = new JigPromptDistanceOptions();
            prOp.UseBasePoint = true;
            prOp.BasePoint = destinationPoint.To3dPoint();
            prOp.Message = message;
            prOp.DefaultValue = 1;
#if ZWCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.InitialBlankTerminatesInput | UserInputControls.NullResponseAccepted;
#elif BRCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.NullResponseAccepted
               | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#else
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput | UserInputControls.NullResponseAccepted
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#endif
            PromptDoubleResult prResult3 = prompts.AcquireDistance(prOp);
            if (prResult3.Status == PromptStatus.Cancel && prResult3.Status == PromptStatus.Error)
                return SamplerStatus.Cancel;

            if (prResult3.Value.Equals(scale))
                return SamplerStatus.NoChange;
            else
            {
                scale = prResult3.Value;
                return SamplerStatus.OK;
            }
        }

        /// <summary>
        /// Rotates action.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <param name="message">The message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="angle">The angle.</param>
        /// <returns>SamplerStatus</returns>
        public static SamplerStatus RotateAction(JigPrompts prompts, string message, ref IChPoint basePoint, ref double angle)
        {
            JigPromptAngleOptions prOp = new JigPromptAngleOptions();
            prOp.Message = message;
            prOp.UseBasePoint = true;
            prOp.BasePoint = basePoint.To3dPoint();
            prOp.DefaultValue = 0;

#if ZWCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.InitialBlankTerminatesInput | UserInputControls.NullResponseAccepted;
#elif BRCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.NullResponseAccepted
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#else
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput | UserInputControls.NullResponseAccepted
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#endif
            PromptDoubleResult prResult2 = prompts.AcquireAngle(prOp);

            if (prResult2.Status == PromptStatus.Cancel && prResult2.Status == PromptStatus.Error)
                return SamplerStatus.Cancel;

            if (prResult2.Value.Equals(angle))
                return SamplerStatus.NoChange;
            else
            {
                angle = prResult2.Value;
                return SamplerStatus.OK;
            }
        }

        /// <summary>
        /// Moves action.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <param name="message">The message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="destinationPoint">The destination point.</param>
        /// <returns>SamplerStatus</returns>
        public static SamplerStatus MoveAction(JigPrompts prompts, string message, ref IChPoint basePoint, ref IChPoint destinationPoint)
        {
            JigPromptPointOptions prOp = new JigPromptPointOptions();
            prOp.Message = message;
            prOp.BasePoint = basePoint.To3dPoint();
            prOp.UseBasePoint = true;
            prOp.Cursor = CursorType.RubberBand;

#if ZWCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.InitialBlankTerminatesInput;
#elif BRCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates 
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#else
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#endif
            PromptPointResult prResult1 = prompts.AcquirePoint(prOp);
            if (prResult1.Status == PromptStatus.Cancel && prResult1.Status == PromptStatus.Error)
                return SamplerStatus.Cancel;

            if (prResult1.Value.IsEqualTo(basePoint.To3dPoint()))
                return SamplerStatus.NoChange;
            else
            {
                destinationPoint = ChPoint.Create(prResult1.Value);
                return SamplerStatus.OK;
            }
        }

        /// <summary>
        /// Mirrors action.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <param name="message">The message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="destinationPoint">The destination point.</param>
        /// <param name="mirrorLine">The mirror line.</param>
        /// <returns>SamplerStatus</returns>
        public static SamplerStatus MirrorAction(JigPrompts prompts, string message, ref IChPoint basePoint, ref IChPoint destinationPoint, ref IChLineSegment mirrorLine)
        {
            var prOp = new JigPromptPointOptions();
            prOp.Message = message;
            prOp.BasePoint = basePoint.To3dPoint();
            prOp.UseBasePoint = true;
            prOp.Cursor = CursorType.RubberBand;

#if ZWCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.InitialBlankTerminatesInput;
#elif BRCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#else
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#endif
            PromptPointResult prResult1 = prompts.AcquirePoint(prOp);
            if (prResult1.Status == PromptStatus.Cancel && prResult1.Status == PromptStatus.Error)
                return SamplerStatus.Cancel;

            if (prResult1.Value.IsEqualTo(basePoint.To3dPoint()))
                return SamplerStatus.NoChange;
            else
            {
                destinationPoint = prResult1.Value.ToPoint();
                mirrorLine = ChLineSegment.Create(basePoint, destinationPoint);
                return SamplerStatus.OK;
            }
        }

        /// <summary>
        /// Distances and angle action.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <param name="message">The message.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="destinationPoint">The destination point.</param>
        /// <param name="angle">The angle.</param>
        /// <returns>DistanceAngleAction</returns>
        public static SamplerStatus DistanceAngleAction(JigPrompts prompts, string message, IChPoint basePoint, ref IChPoint destinationPoint, ref double angle)//operation distance angle
        {
            var prOp = new JigPromptPointOptions();
            prOp.Message = message;
            prOp.BasePoint = basePoint.To3dPoint();
            prOp.UseBasePoint = true;
            prOp.Cursor = CursorType.RubberBand;

#if ZWCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.InitialBlankTerminatesInput;
#elif BRCAD
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#else
            prOp.UserInputControls = UserInputControls.Accept3dCoordinates | UserInputControls.AnyBlankTerminatesInput
                | UserInputControls.GovernedByOrthoMode | UserInputControls.GovernedByUCSDetect | UserInputControls.InitialBlankTerminatesInput;
#endif
            var prResult1 = prompts.AcquirePoint(prOp);
            if (prResult1.Status == PromptStatus.Cancel && prResult1.Status == PromptStatus.Error)
                return SamplerStatus.Cancel;

            if (prResult1.Value.IsEqualTo(basePoint.To3dPoint()))
                return SamplerStatus.NoChange;
            else
            {
                destinationPoint = prResult1.Value.ToPoint();
                angle = basePoint.AngleFromXAxis(destinationPoint);
                return SamplerStatus.OK;
            }
        }
    }
}
