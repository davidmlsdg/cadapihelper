﻿namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// JigAction enum
    /// </summary>
    public enum JigAction
    {
        /// <summary>
        /// Move
        /// </summary>
        Move,
        /// <summary>
        /// Rotate
        /// </summary>
        Rotate,
        /// <summary>
        /// Scale by distance
        /// </summary>
        Scale_Distance,
        /// <summary>
        /// Mirror
        /// </summary>
        Mirror,
    }
}
