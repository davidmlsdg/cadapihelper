﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.GraphicsInterface;
#elif ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.GraphicsInterface;
#elif BRCAD
using Teigha.DatabaseServices;
using Bricscad.EditorInput;
using Teigha.Geometry;
using Teigha.GraphicsInterface;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.EditorInput;

namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// TsEntityJig Jig one entity
    /// </summary>
    /// <seealso cref="EntityJig" />
    public class ChEntityJig : EntityJig
    {
        private JigBehavior behavior;
        private Dictionary<JigAction, string> actionsMessages;
        private IChPoint basePoint;
        private IChPoint destinationPoint;
        private double angle = 0;
        private double scale = 1;
        private IChLineSegment mirrorLine = null;
        private KeyValuePair<JigAction, string> action;
        private ChObjectId objectId = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChEntityJig"/> class.
        /// </summary>
        /// <param name="entity">The entity.</param>
        /// <param name="basePoint">The base point.</param>
        /// <param name="actionsMessages">The actions messages.</param>
        /// <param name="behavior">The behavior.</param>
        public ChEntityJig(Entity entity, IChPoint basePoint, Dictionary<JigAction, string> actionsMessages,
            JigBehavior behavior = JigBehavior.OnlyShadow) : base(entity)
        {
            this.basePoint = basePoint;
            this.basePoint = this.basePoint.To3dPoint().TransformBy(GetMatrixUCS()).ToPoint();
            this.actionsMessages = actionsMessages;
            this.behavior = behavior;
            destinationPoint = this.basePoint;
        }

        /// <summary>
        /// Gets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public ChObjectId ObjectId { get { return objectId; } }

        private Matrix3d GetMatrixUCS()
        {
            return ChEditor.GetEditor().CurrentUserCoordinateSystem;
        }

        /// <summary>
        /// Ponto de destino
        /// </summary>
        public IChPoint PtDestino
        {
            get
            {
                return destinationPoint;
            }
        }

        /// <summary>
        /// Gets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public double Angle
        {
            get
            {
                return angle;
            }
        }

        /// <summary>
        /// Gets the scale.
        /// </summary>
        /// <value>
        /// The scale.
        /// </value>
        public double Scale
        {
            get
            {
                return scale;
            }
        }

        /// <summary>
        /// Gets the base point.
        /// </summary>
        /// <value>
        /// The base point.
        /// </value>
        public IChPoint BasePoint
        {
            get
            {
                return basePoint;
            }
        }

        /// <summary>
        /// Start the Jig action.
        /// </summary>
        /// <returns>True if successful</returns>
        public bool Jig()
        {
            var db = ChEditor.GetDatabase();
            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                try
                {
                    PromptResult pr;
                    foreach (var item in actionsMessages)
                    {
                        action = item;
                        pr = ChEditor.GetEditor().Drag(this);
                        if (pr.Status == PromptStatus.Cancel || pr.Status == PromptStatus.Error)
                            return false;
                    }
                    switch (behavior)
                    {
                        case JigBehavior.MakeCopy:
                            break;

                        case JigBehavior.InsertInDatabase:
                            objectId = ChEntity.AddEntityToDatabase(Entity);
                            tr.Commit();
                            break;
                    }
                }
                catch
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// Samplers the specified prompts.
        /// </summary>
        /// <param name="prompts">The prompts.</param>
        /// <returns>SamplerStatus</returns>
        protected override SamplerStatus Sampler(JigPrompts prompts)
        {
            switch (action.Key)
            {
                case JigAction.Move:
                    return JigSamplerActions.MoveAction(prompts, action.Value, ref basePoint, ref destinationPoint);

                case JigAction.Rotate:
                    return JigSamplerActions.RotateAction(prompts, action.Value, ref destinationPoint, ref angle);

                case JigAction.Scale_Distance:
                    return JigSamplerActions.ScaleAction(prompts, action.Value, ref destinationPoint, ref scale);

                case JigAction.Mirror:
                    return JigSamplerActions.MirrorAction(prompts, action.Value, ref basePoint, ref destinationPoint, ref mirrorLine);

                default:
                    return SamplerStatus.NoChange;
            }
        }

        /// <summary>
        /// Updates the matrix for represent the jig.
        /// </summary>
        /// <returns></returns>
        protected override bool Update()
        {
            switch (action.Key)
            {
                case JigAction.Move:
                    var mat1 = Matrix3d.Displacement(basePoint.To3dPoint().GetVectorTo(destinationPoint.To3dPoint()));
                    Entity.TransformBy(mat1);
                    break;

                case JigAction.Rotate:
                    var mat2 = Matrix3d.Rotation(angle, Vector3d.ZAxis.TransformBy(GetMatrixUCS()), destinationPoint.To3dPoint());
                    Entity.TransformBy(mat2);
                    break;

                case JigAction.Scale_Distance:
                    var mat3 = Matrix3d.Scaling(scale, destinationPoint.To3dPoint());
                    Entity.TransformBy(mat3);
                    break;

                case JigAction.Mirror:
                    var mat4 = Matrix3d.Mirroring(mirrorLine.GetLine3d());
                    Entity.TransformBy(mat4);
                    break;

                default:
                    return false;
            }
            return true;
        }
    }
}
