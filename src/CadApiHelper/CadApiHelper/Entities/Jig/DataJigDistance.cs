﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
#endif

#if ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
#endif

#if BRCAD
    using Bricscad.ApplicationServices;
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
    using Teigha.Colors;
#endif

#if GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.EditorInput;
using GrxCAD.Geometry;
using GrxCAD.GraphicsInterface;
#endif

using CadApiHelper.Geometry;


namespace CadApiHelper.Entities.Jig
{
    /// <summary>
    /// DataJigDistance instance - Data for Jig by distance
    /// </summary>
    public class DataJigDistance
    {
        /// <summary>
        /// Gets or sets the item.
        /// </summary>
        /// <value>
        /// The item.
        /// </value>
        public object Item { get; set; }

        /// <summary>
        /// Gets or sets the distance.
        /// </summary>
        /// <value>
        /// The distance.
        /// </value>
        public double Distance { get; set; }

        /// <summary>
        /// Gets or sets the entity.
        /// </summary>
        /// <value>
        /// The entity.
        /// </value>
        public Entity Entity { get; set; }

        /// <summary>
        /// Gets or sets the point.
        /// </summary>
        /// <value>
        /// The point.
        /// </value>
        public IChPoint Point { get; set; } //JigBehavior
    }
}
