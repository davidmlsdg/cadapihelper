﻿using CadApiHelper.Core;

namespace CadApiHelper.Block
{
    /// <summary>
    /// ChAttribute class
    /// </summary>
    public class ChAttribute
    {
        #region Private Fields

        private ChObjectId atributeObjectId;
        private string tagString;
        private string textString;
        private double angle;
        private bool changeAngle;

        /// <summary>
        /// Gets or sets a value indicating whether [change angle].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [change angle]; otherwise, <c>false</c>.
        /// </value>
        public bool ChangeAngle
        {
            get { return changeAngle; }
            set
            {
                changeAngle = value;
            }
        }

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets the atribute identifier.
        /// </summary>
        /// <value>
        /// The atribute identifier.
        /// </value>
        public ChObjectId AtributeObjectId
        {
            get { return atributeObjectId; }
        }

        /// <summary>
        /// Gets the tag string.
        /// </summary>
        /// <value>
        /// The tag string.
        /// </value>
        public string TagString
        {
            get { return tagString; }
        }

        /// <summary>
        /// Gets or sets the text string.
        /// </summary>
        /// <value>
        /// The text string.
        /// </value>
        public string TextString
        {
            get { return textString; }
            set { textString = value; }
        }

        /// <summary>
        /// Gets or sets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public double Angle
        {
            get { return angle; }
            set { angle = value; }
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChAttribute"/> class.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="text">The text.</param>
        public ChAttribute(string tag, string text)
        {
            tagString = tag;
            textString = text;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChAttribute"/> class.
        /// </summary>
        /// <param name="tag">The tag.</param>
        /// <param name="text">The text.</param>
        /// <param name="atributeObjectId">The atribute object identifier.</param>
        /// <param name="angle">The angle.</param>
        public ChAttribute(string tag, string text, ChObjectId atributeObjectId, double angle)
        {
            tagString = tag;
            textString = text;
            this.atributeObjectId = atributeObjectId;
        }
    }
    #endregion Public Constructors
}
