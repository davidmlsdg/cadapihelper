﻿using CadApiHelper.Core;
using CadApiHelper.Entities.Edit;

using System;
using System.Collections.Generic;

namespace CadApiHelper.Block
{
    /// <summary>
    /// Collection of ChAttribute
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List&lt;CadApiHelper.Block.ChAttribute&gt;" />
    public class ChAttributeCollection : List<ChAttribute>
    {
        /// <summary>
        /// Gets or sets the block reference object identifier.
        /// </summary>
        /// <value>
        /// The block reference object identifier.
        /// </value>
        public ChObjectId BlockReferenceObjectId { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChAttributeCollection"/> class.
        /// </summary>
        /// <param name="blockReferenceObjectId">The block reference object identifier.</param>
        public ChAttributeCollection(ChObjectId blockReferenceObjectId)
        {
            BlockReferenceObjectId = blockReferenceObjectId;
        }
        public void UpdateAttributes()
        {
            if (BlockReferenceObjectId.IsNull)
                return;

            var atts = new ChAttributeCollection(BlockReferenceObjectId);
            SetValuesAtributes(atts);
            foreach (var item in atts)
            {
                if (item.AtributeObjectId.IsNull)
                {

                    atts.Add(item);
                    ChBlockReference.SetAttributes(atts);
                    continue;
                }
                var att = new ChEditAttributeReference(item.AtributeObjectId, true);
                using (att)
                {
                    att.DbObject.TextString = item.TextString;
                    att.Comit();
                }
            }
        }

        private void SetValuesAtributes(ChAttributeCollection atts)
        {
            foreach (var item in atts)
            {
                var at = Find(x => x.TagString.Equals(item.TagString, StringComparison.OrdinalIgnoreCase));
                if (at == null)
                    continue;


            }
        }
    }
}
