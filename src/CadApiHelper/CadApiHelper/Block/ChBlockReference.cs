﻿#if ACAD
    using Autodesk.AutoCAD.ApplicationServices.Core;
    using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
    using Autodesk.AutoCAD.DatabaseServices;
    using Autodesk.AutoCAD.Geometry;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.ApplicationServices;
using ZwSoft.ZwCAD.Geometry;
#elif BRCAD
    using Teigha.DatabaseServices;
    using Bricscad.ApplicationServices;
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.Geometry;
#elif GCAD
    using GrxCAD.DatabaseServices;
    using GrxCAD.ApplicationServices;
    using acApp = GrxCAD.ApplicationServices.Application;
    using GrxCAD.Geometry;
#endif


using System;
using System.Collections.Generic;
using System.Text;

using CadApiHelper.Entities.Edit;

using System.IO;
using System.Drawing;

using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.Layer;
using CadApiHelper.Entities;
using CadApiHelper.Entities.Jig;

namespace CadApiHelper.Block
{
    /// <summary>
    /// 
    /// </summary>
    public static class ChBlockReference
    {
        #region Private Fields

        private static StringBuilder errors = new StringBuilder();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Gets the attribute block.
        /// </summary>
        /// <param name="blockId">The block identifier.</param>
        /// <returns>ChAttributeCollection</returns>
        public static ChAttributeCollection GetAttributeBlock(ChObjectId blockId)
        {
            var atributos = new ChAttributeCollection(blockId);
            var ent = new ChEditDbObject<BlockReference>(blockId);
            using (ent)
            {
                if (ent.DbObject == null)
                    return null;

                foreach (ObjectId id in ent.DbObject.AttributeCollection)
                {
                    var att = new ChEditDbObject<AttributeReference>(id);
                    using (att)
                    {
                        atributos.Add(new ChAttribute(att.DbObject.Tag, att.DbObject.TextString, id, att.DbObject.Rotation));
                    }
                }
            }
            return atributos;
        }

        /// <summary>
        /// Gets the block reference.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="blockName">Name of the block.</param>
        /// <param name="blockId">The block identifier.</param>
        /// <returns>BlockReference</returns>
        public static BlockReference GetBlockReference(string blockFileName, IChPoint insertPoint, string blockName, ref ChObjectId blockId)
        {
            blockName = blockName.Trim();
            if (!IsBlockLoaded(blockName, ref blockId))
            {
                if (!LoadBlock(blockFileName, blockName, ref blockId))
                {
                    errors.AppendLine("Could not load block!!");
                    return null;
                }
            }

            var br = new BlockReference(insertPoint.To3dPoint(), blockId);
            br.SetDatabaseDefaults();
            return br;
        }

        /// <summary>
        /// Gets the block reference.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <returns>BlockReference</returns>
        public static BlockReference GetBlockReference(string blockFileName, IChPoint insertPoint)
        {
            var blockId = new ChObjectId();
            if (blockFileName == "" || !File.Exists(blockFileName))
                return null;

            string blockName = GetBlockName(blockFileName);
            blockName = blockName.Trim();
            if (!IsBlockLoaded(blockName, ref blockId))
            {
                if (!LoadBlock(blockFileName, blockName, ref blockId))
                {
                    errors.AppendLine("Could not load block!!");
                    return null;
                }
            }

            BlockReference br = new BlockReference(insertPoint.To3dPoint(), blockId);
            br.SetDatabaseDefaults();
            return br;
        }

        /// <summary>
        /// Gets the DWG preview.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <returns>Bitmap</returns>
        public static Bitmap GetDwgPreview(string path)
        {
            return ChThumbnailReader.GetThumbnail(path);
        }

        /// <summary>
        /// Gets the errors.
        /// </summary>
        /// <returns>Errors</returns>
        public static string GetErrors() //GetErrors
        {
            return errors.ToString();
        }

        /// <summary>
        /// Inserts the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="scale">The scale.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId InsertBlock(string blockFileName, IChPoint insertPoint, double rotation, double scale)
        {
            return InsertBlock(blockFileName, insertPoint, rotation, scale, -1, "");
        }

        /// <summary>
        /// Inserts the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="scale">The scale.</param>
        /// <param name="angleAttribute">The angle attribute.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId InsertBlock(string blockFileName, IChPoint insertPoint, double rotation, double scale, double angleAttribute)
        {
            return InsertBlock(blockFileName, insertPoint, rotation, scale, angleAttribute, "");
        }

        /// <summary>
        /// Inserts the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="scale">The scale.</param>
        /// <param name="angleAttribute">The angle attribute.</param>
        /// <param name="layer">The layer.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId InsertBlock(string blockFileName, IChPoint insertPoint, double rotation, double scale, double angleAttribute, string layer)
        {
            return InsertBlock(blockFileName, insertPoint, rotation, scale, scale, scale, angleAttribute, layer);
        }

        /// <summary>
        /// Inserts the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="scaleX">The scale x.</param>
        /// <param name="scaleY">The scale y.</param>
        /// <param name="scaleZ">The scale z.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId InsertBlock(string blockFileName, IChPoint insertPoint, double rotation, double scaleX, double scaleY, double scaleZ)
        {
            return InsertBlock(blockFileName, insertPoint, rotation, scaleX, scaleY, scaleZ, rotation, "");
        }

        /// <summary>
        /// Inserts the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="insertPoint">The insert point.</param>
        /// <param name="rotation">The rotation.</param>
        /// <param name="scaleX">The scale x.</param>
        /// <param name="scaleY">The scale y.</param>
        /// <param name="scaleZ">The scale z.</param>
        /// <param name="angleAttribute">The angle attribute.</param>
        /// <param name="layer">The layer.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId InsertBlock(string blockFileName, IChPoint insertPoint, double rotation, double scaleX, double scaleY, double scaleZ,
            double angleAttribute, string layer)
        {
            errors.Length = 0;
            string blockName = GetBlockName(blockFileName);
            blockName = blockName.Trim();
            var blockId = new ChObjectId();

            //return ObjectId.Null;
            BlockReference br = GetBlockReference(blockFileName, insertPoint, blockName, ref blockId);
            if (br == null)
                return blockId;
            br.SetDatabaseDefaults();
            br.Rotation = rotation;
            if (layer != "")
            {
                var idLayer = ChLayers.GetObjectId(layer);
                if (!idLayer.IsNull)
                    br.LayerId = idLayer;
            }
            br.ScaleFactors = new Scale3d(scaleX, scaleY, scaleZ);
            ObjectId idBlk = ObjectId.Null;
            idBlk = ChEntity.AddEntityToDatabase(br);
            InsertAttribute(idBlk, blockName, angleAttribute);
            return idBlk;
        }

        /// <summary>
        /// Inserts the block by jigs.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <returns>ChObjectId of block or null</returns>
        public static ChObjectId InsertBlockJigs(string blockFileName)
        {
            var opers = GetOperacoesaDefaultJig();
            return InsertBlockJigs(blockFileName, ChPoint.Create(), opers, "", null);
        }

        private static Dictionary<JigAction, string> GetOperacoesaDefaultJig()
        {
            Dictionary<JigAction, string> opers = new Dictionary<JigAction, string>();
            opers.Add(JigAction.Move, "Ponto de inserção: ");
            opers.Add(JigAction.Rotate, "Angulo de inserção: ");
            return opers;
        }

        /// <summary>
        /// Inserts the block jigs.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="ptBase">The pt base.</param>
        /// <param name="actionsMessages">The actions messages.</param>
        /// <param name="layer">The layer.</param>
        /// <param name="atributos">The atributos.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId InsertBlockJigs(string blockFileName, IChPoint ptBase, Dictionary<JigAction, string> actionsMessages,
            string layer = "", ChAttributeCollection atributos = null)
        {
            errors.Length = 0;
            BlockReference br = GetBlockReference(blockFileName, ptBase);
            br.SetDatabaseDefaults();
            br.Rotation = 0;
            if (layer != "")
                br.Layer = layer;
            br.Position = new Point3d(ptBase.AsArray());

            var jig = new ChEntityJig(br, ptBase, actionsMessages, JigBehavior.OnlyShadow);
            if (jig.Jig())
            {
                br.Position = new Point3d(jig.PtDestino.AsArray());
                br.Rotation = jig.Angle;
                br.ScaleFactors = new Scale3d(jig.Scale);
                ObjectId id = ChEntity.AddEntityToDatabase(br);
                if (atributos != null && !id.IsNull)
                {
                    atributos.BlockReferenceObjectId = id;
                    SetAttributes(atributos);
                }
                return id;
            }
            return ObjectId.Null;
        }

        /// <summary>
        /// Determines whether [is block loaded] [the specified block name].
        /// </summary>
        /// <param name="blockName">Name of the block.</param>
        /// <param name="blockId">The block identifier.</param>
        /// <returns>
        ///   <c>true</c> if [is block loaded] [the specified block name]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsBlockLoaded(string blockName, ref ChObjectId blockId)
        {
            blockName = blockName.Trim();
            Database db = Application.DocumentManager.MdiActiveDocument.Database;

            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    BlockTable bt = trans.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;
                    blockId = bt[blockName];
                    if (blockId == ObjectId.Null)
                    {
                        trans.Commit();
                        return false;
                    }

                    trans.Commit();
                    return true;
                }
                catch (System.Exception e)
                {
                    errors.AppendLine(e.Message);
                    return false;
                }
            }
        }

        /// <summary>
        /// Determines whether [is block reference] [the specified block identifier].
        /// </summary>
        /// <param name="blockId">The block identifier.</param>
        /// <returns>
        ///   <c>true</c> if [is block reference] [the specified block identifier]; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsBlockReference(ChObjectId blockId)
        {
            using (Transaction trans = Application.DocumentManager.MdiActiveDocument.TransactionManager.StartTransaction())
            {
                BlockReference blkRef = trans.GetObject(blockId, OpenMode.ForRead) as BlockReference;
                trans.Commit();
                if (blkRef == null)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Loads the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <param name="blockName">Name of the block.</param>
        /// <param name="blockId">The block identifier.</param>
        /// <returns>True if bloc is loaded</returns>
        public static bool LoadBlock(string blockFileName, string blockName, ref ChObjectId blockId)
        {
            blockName = blockName.Trim();
            try
            {
                Database curDb = Application.DocumentManager.MdiActiveDocument.Database;
                Database blockDb = new Database(false, true);

#if ZWCAD
                var modo = FileShare.Read;
#else
                var modo = FileOpenMode.OpenForReadAndAllShare;
#endif

                blockDb.ReadDwgFile(blockFileName, modo, true, "");
                blockId = curDb.Insert(blockName, blockDb, true);

                return true;
            }
            catch (System.Exception e)
            {
                errors.AppendLine(e.Message);
                return false;
            }
            // }
        }

        /// <summary>
        /// Sets the attributes.
        /// </summary>
        /// <param name="atributos">The atributos.</param>
        /// <param name="changeAngle">if set to <c>true</c> [change angle].</param>
        public static void SetAttributes(ChAttributeCollection atributos, bool changeAngle = false)
        {
            if (atributos == null)
                return;
            var ent = new ChEditBlockReference(atributos.BlockReferenceObjectId, OpenMode.ForWrite);
            AttributeCollection atts;
            using (ent)
            {
                atts = ent.DbObject.AttributeCollection;
            }
            foreach (ObjectId id in atts)
            {
                var att = new ChEditAttributeReference(id);
                using (att)
                {
                    ChAttribute atributo = atributos.Find(x => x.TagString.Equals(att.DbObject.Tag));
                    if (atributo != null)
                    {
                        att.DbObject.UpgradeOpen();
                        att.DbObject.TextString = atributo.TextString;
                        if (changeAngle)
                            att.DbObject.Rotation = atributo.Angle;
                        att.Comit();
                    }
                }
            }
        }

        /// <summary>
        /// Tries the get position.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="objectId">The identifier.</param>
        /// <returns>True if get position of block</returns>
        public static bool TryGetPosition(ref IChPoint point, ChObjectId objectId)
        {
            var bl = new ChEditBlockReference(objectId);
            using (bl)
            {
                try
                {
                    point = bl.DbObject.Position.ToPoint();
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Tries the get position and angle.
        /// </summary>
        /// <param name="objectId">The object identifier.</param>
        /// <param name="point">The point.</param>
        /// <param name="angle">The angle.</param>
        /// <returns>True if get position and angle of block</returns>
        public static bool TryGetPosition(ChObjectId objectId, ref IChPoint point, ref double angle)
        {
            var bl = new ChEditBlockReference(objectId);
            using (bl)
            {
                try
                {
                    point = bl.DbObject.Position.ToPoint();
                    angle = bl.DbObject.Rotation;
                    return true;
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }


        /// <summary>
        /// Changes the layer of the sub entity.
        /// </summary>
        /// <param name="blockId">The identifier.</param>
        /// <param name="oldLayer">The old layer.</param>
        /// <param name="newLayer">The new layer.</param>
        /// <returns>True if successful</returns>
        public static bool ChangeLayerSubEntity(ChObjectId blockId, string oldLayer, string newLayer)
        {
            Database db = HostApplicationServices.WorkingDatabase;
            try
            {
                using (Transaction tr = db.TransactionManager.StartTransaction())
                {
                    var bref = tr.GetObject(blockId, OpenMode.ForWrite) as BlockReference;
                    if (bref == null)
                        return false;

                    var btrId = bref.BlockTableRecord;
                    if (btrId.IsNull)
                        return false;

                    var btr = tr.GetObject(btrId, OpenMode.ForWrite) as BlockTableRecord;
                    if (btr == null)
                        return false;

                    foreach (var item in btr)
                    {
                        var ent = tr.GetObject(item, OpenMode.ForWrite) as Entity;
                        if (ent == null)
                            continue;
                        if (ent.Layer.Equals(oldLayer, StringComparison.OrdinalIgnoreCase))
                            ent.Layer = newLayer;
                    }
                    tr.Commit();
                }
                return true;
            }
            catch (Exception e)
            {
                errors.AppendLine(e.Message);
                return false;
            }

        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Gets the name of the block.
        /// </summary>
        /// <param name="blockFileName">Name of the block file.</param>
        /// <returns>Name of block</returns>
        public static string GetBlockName(string blockFileName)
        {
            if (!File.Exists(blockFileName))
                return blockFileName;

            FileInfo arquivo = new FileInfo(blockFileName);

            if (arquivo.Exists)
                return arquivo.Name.Substring(0, arquivo.Name.IndexOf(arquivo.Extension)).Trim();

            return blockFileName;
        }

        /// <summary>
        /// Sets the attributes definition.
        /// </summary>
        /// <param name="blockDefinitionId">The block definition identifier.</param>
        /// <param name="bl">The bl.</param>
        internal static void SetAttributesDefinition(ChObjectId blockDefinitionId, BlockReference bl)
        {
            var ent = new ChEditDbObject<BlockTableRecord>(blockDefinitionId);
            using (ent)
            {
                foreach (ObjectId item in ent.DbObject)
                {
                    var obj = new ChEditDbObject<DBObject>(item);
                    using (obj)
                    {
                        AttributeDefinition attDef = obj.DbObject as AttributeDefinition;
                        if (attDef == null || attDef.Constant)
                            continue;
                        AttributeReference attRef = new AttributeReference();
                        attRef.SetDatabaseDefaults();
                        attRef.TextString = attDef.TextString;
                        attRef.SetAttributeFromBlock(attDef, bl.BlockTransform);
                        attRef.Rotation = bl.Rotation;
                        bl.AttributeCollection.AppendAttribute(attRef);
                    }
                }
            }
        }

        /// <summary>
        /// Inserts the attribute.
        /// </summary>
        /// <param name="idBlk">The identifier BLK.</param>
        /// <param name="blockName">Name of the block.</param>
        /// <param name="angleAttribute">The angle attribute.</param>
        private static void InsertAttribute(ChObjectId idBlk, string blockName, double angleAttribute)
        {
            blockName = blockName.Trim();

            if (idBlk.IsNull)
                return;
            Database db = Application.DocumentManager.MdiActiveDocument.Database;
            using (Transaction trans = db.TransactionManager.StartTransaction())
            {
                BlockReference blkRef = trans.GetObject(idBlk, OpenMode.ForWrite) as BlockReference;
                BlockTable bt = trans.GetObject(db.BlockTableId, OpenMode.ForWrite) as BlockTable;
                BlockTableRecord blockRef = trans.GetObject(bt[blockName], OpenMode.ForWrite) as BlockTableRecord;
                foreach (ObjectId item in blockRef)
                {
                    AttributeDefinition attDef = trans.GetObject(item, OpenMode.ForWrite) as AttributeDefinition;
                    if (attDef == null || attDef.Constant)
                        continue;

                    using (AttributeReference attRef = new AttributeReference())
                    {
                        try
                        {
                            attRef.SetDatabaseDefaults();
                            attRef.TextString = attDef.TextString;
                            attRef.SetAttributeFromBlock(attDef, blkRef.BlockTransform);
                            attRef.Rotation = ChAngles.Normalize(attRef.Rotation);
                            blkRef.AttributeCollection.AppendAttribute(attRef);
                        }
                        catch (Exception e)
                        {
                            errors.AppendLine(e.Message);
                            continue;
                        }
                        trans.AddNewlyCreatedDBObject(attRef, true);
                    }
                }
                trans.Commit();
            }
        }

        #endregion Private Methods
    }
}
