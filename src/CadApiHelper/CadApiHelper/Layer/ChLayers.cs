﻿#if ACAD
#if ACAD2013
            using Autodesk.AutoCAD.ApplicationServices;
            using Autodesk.AutoCAD.ApplicationServices.Core;
            using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
#else

using acApp = Autodesk.AutoCAD.ApplicationServices.Application;

#endif

using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Colors;


#endif
#if ZWCAD

using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.Colors;

#endif
#if BRCAD
    using Bricscad.ApplicationServices;
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
    using Teigha.Colors;
#endif

#if GCAD
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.DatabaseServices;
using GrxCAD.Colors;
#endif

using System.Collections.Generic;

using System.Linq;
using System;
using CadApiHelper.Core;

namespace CadApiHelper.Layer
{
    /// <summary>
    /// Manager layers
    /// </summary>
    public static class ChLayers
    {
        #region Public Methods

        /// <summary>
        /// Creates the layer on the current drawing.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Color of the index.</param>
        /// <param name="lineTypeId">The line type identifier.</param>
        /// <returns>True whether the layer was created.</returns>
        /// <exception cref="System.ArgumentNullException">layerName</exception>
        public static bool CreateLayer(string layerName, short colorIndex, ObjectId lineTypeId)
        {
            if (layerName is null)
                throw new ArgumentNullException(nameof(layerName));

            var cor = Color.FromColorIndex(ColorMethod.ByLayer, colorIndex);
            return CreateLayer(layerName, cor, lineTypeId);
        }

        /// <summary>
        /// Creates the layer on the current drawing.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="colorIndex">Index of the color.</param>
        /// <returns>True whether the layer was created.</returns>
        /// <exception cref="System.ArgumentNullException">layerName</exception>
        public static bool CreateLayer(string layerName, short colorIndex)
        {
            if (layerName is null)
            {
                throw new ArgumentNullException(nameof(layerName));
            }

            var cor = Color.FromColorIndex(ColorMethod.ByLayer, colorIndex);
            return CreateLayer(layerName, cor, ObjectId.Null);
        }

        /// <summary>
        /// Creates the layer on the current drawing.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="color">The color.</param>
        /// <returns>True whether the layer was created.</returns>
        public static bool CreateLayer(string layerName, Color color)
        {
            return CreateLayer(layerName, color, ObjectId.Null);
        }

        /// <summary>
        /// Creates the layer on the current drawing.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <returns>True whether the layer was created.</returns>
        public static bool CreateLayer(string layerName)
        {
            return CreateLayer(layerName, 7, ObjectId.Null);
        }

        /// <summary>
        /// Creates the layers on the current drawing.
        /// </summary>
        /// <param name="layers">The layers.</param>
        public static void CreateLayers(IEnumerable<ChLayerColor> layers)
        {
            var lays = GetLayers();
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;

            // using(TsDocument doc = new TsDocument())
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                LayerTable lt = acTrans.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (lt == null)
                    return;
                lt.UpgradeOpen();
                foreach (var l in layers)
                {
                    var t = lays.Find(x => x.Name.Equals(l.Name, StringComparison.OrdinalIgnoreCase));
                    if (t != null)
                    {
                        l.ObjectId = t.ObjectId;
                        continue;
                    }
                    var layerName = VerifyLayerName(l.Name);
                    try
                    {
                        var ltr = new LayerTableRecord();
                        ltr.Name = layerName;
                        ltr.Color = Color.FromColorIndex(ColorMethod.ByLayer, l.ColorIndex);
                        if (!l.LinetypeId.IsNull)
                            ltr.LinetypeObjectId = l.LinetypeId;

                        l.ObjectId = lt.Add(ltr);
                        acTrans.AddNewlyCreatedDBObject(ltr, true);
                    }
                    catch (System.Exception)
                    {
                    }
                }
                acTrans.Commit();
            };
        }

        /// <summary>
        /// Creates the layer on the current drawing.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="cor">The cor.</param>
        /// <param name="lineTypeId">The line type identifier.</param>
        /// <returns>True whether the layer was created.</returns>
        public static bool CreateLayer(string layerName, Color cor, ObjectId lineTypeId)
        {
            layerName = VerifyLayerName(layerName);
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;

            // using(TsDocument doc = new TsDocument())
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    LayerTable lt = acTrans.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                    if (lt == null)
                        return false;

                    if (lt.Has(layerName))
                        return false;

                    lt.UpgradeOpen();
                    LayerTableRecord ltr = new LayerTableRecord();
                    ltr.Name = layerName;
                    ltr.Color = cor;
                    if (!lineTypeId.IsNull)
                        ltr.LinetypeObjectId = lineTypeId;

                    lt.Add(ltr);
                    acTrans.AddNewlyCreatedDBObject(ltr, true);
                    acTrans.Commit();
                }
                catch (System.Exception)
                {
                    return false;
                }
            }
            return true;
        }

        private static string VerifyLayerName(string layerName)
        {
            if (!string.IsNullOrEmpty(layerName))
                layerName = layerName.Trim();
            return layerName;
        }

        /// <summary>
        /// Gets the layers of the current drawing.
        /// </summary>
        /// <returns></returns>
        public static List<ChLayerColor> GetLayers()
        {
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;
            var lista = new List<ChLayerColor>();

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                LayerTable lt = tr.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (lt == null)
                    return lista;

                foreach (ObjectId id in lt)
                {
                    LayerTableRecord ltr = tr.GetObject(id, OpenMode.ForRead) as LayerTableRecord;
                    if (ltr != null)
                        lista.Add(new ChLayerColor { Name = ltr.Name, ColorIndex = ltr.Color.ColorIndex, LinetypeId = ltr.LinetypeObjectId, ObjectId = ltr.ObjectId });
                }
            }
            return lista;
        }

        /// <summary>
        /// Gets the layer names of the current drawing.
        /// </summary>
        /// <returns>List of names layers of the current drawing</returns>
        public static List<string> GetLayerNames()
        {
            Database db = acApp.DocumentManager.MdiActiveDocument.Database;
            List<string> lista = new List<string>();

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                LayerTable lt = tr.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                if (lt == null)
                    return lista;

                foreach (ObjectId id in lt)
                {
                    LayerTableRecord ltr = tr.GetObject(id, OpenMode.ForRead) as LayerTableRecord;
                    if (ltr != null)
                        lista.Add(ltr.Name);
                }
            }
            return lista;
        }

        /// <summary>
        /// Makes the layer current.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <returns>True if the layer is current</returns>
        public static bool MakeLayerCurrent(string layerName)
        {
            layerName = VerifyLayerName(layerName);

            if (!HasLayer(layerName))
                return false;

            Database db = acApp.DocumentManager.MdiActiveDocument.Database;
            if (db == null)
                return false;

            var idLayer = GetObjectId(layerName);
            if (idLayer.IsNull)
                return false;

            using (Transaction tr = db.TransactionManager.StartTransaction())
            {
                db.Clayer = idLayer;
                tr.Commit();
            }

            return true;
        }

        /// <summary>
        /// Gets the object identifier of the layer.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="db">The database.</param>
        /// <returns>ChObjectId</returns>
        public static ChObjectId GetObjectId(string layerName, Database db = null)
        {
            layerName = VerifyLayerName(layerName);

            if (db == null)
                db = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    LayerTable lt = acTrans.GetObject(db.LayerTableId, OpenMode.ForRead) as LayerTable;
                    if (lt == null)
                        return ObjectId.Null;
                    return lt[layerName];
                }
                catch (System.Exception)
                {
                    return ObjectId.Null; ;
                }
            }
        }

        /// <summary>
        /// Sets the layer is on.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="isOn">if set to <c>true</c> [is on].</param>
        /// <param name="db">The database, if is null get current database drawing </param>
        public static void SetLayerIsOn(string layerName, bool isOn, Database db = null)
        {
            if (db == null)
                db = acApp.DocumentManager.MdiActiveDocument.Database;

            var layerId = GetObjectId(layerName, db);
            if (layerId.IsNull)
                return;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    LayerTableRecord ltr = acTrans.GetObject(layerId, OpenMode.ForWrite) as LayerTableRecord;
                    if (ltr == null)
                        return;
                    ltr.IsOff = !isOn;
                    acTrans.Commit();
                }
                catch (System.Exception)
                {
                    return;
                }
            }

        }

        /// <summary>
        /// Determines whether the specified layer name has layer.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <returns>
        ///   <c>true</c> if the specified layer name has layer; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasLayer(string layerName)
        {
            return !GetObjectId(layerName).IsNull;
        }
        #endregion Public Methods
    }
}
