﻿
using CadApiHelper.Core;

namespace CadApiHelper.Layer
{
    /// <summary>
    /// ChLayerColor instance
    /// </summary>
    public class ChLayerColor
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the index of the color.
        /// </summary>
        /// <value>
        /// The index of the color.
        /// </value>
        public short ColorIndex { get; set; }

        /// <summary>
        /// Gets or sets the linetype identifier.
        /// </summary>
        /// <value>
        /// The linetype identifier.
        /// </value>
        public ChObjectId LinetypeId { get; set; }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public ChObjectId ObjectId { get; set; }
    }
}
