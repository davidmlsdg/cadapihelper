﻿#if ACAD
#if ACAD2013
        using Autodesk.AutoCAD.ApplicationServices.Core;
        using Autodesk.AutoCAD.ApplicationServices;
        using acApp = Autodesk.AutoCAD.ApplicationServices;
#else

using Autodesk.AutoCAD.ApplicationServices;

using acApp = Autodesk.AutoCAD.ApplicationServices;
#endif
#endif
#if ZWCAD

using acApp = ZwSoft.ZwCAD.ApplicationServices;

using ZwSoft.ZwCAD.ApplicationServices;
#endif
#if BRCAD
    using acApp = Bricscad.ApplicationServices;
    using Bricscad.ApplicationServices;
#endif

#if GCAD
using acApp = GrxCAD.ApplicationServices;
using GrxCAD.ApplicationServices;
#endif

using System;

namespace CadApiHelper.Document
{
    /// <summary>
    /// ChLockUnlockDocument - Lock and Unlock document
    /// </summary>
    /// <seealso cref="System.IDisposable" />
    public class ChLockUnlockDocument : IDisposable
    {
        #region Private Fields

        private acApp.Document doc;
        private DocumentLock docLock;
        private static int qtLocks = 0;
        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChLockUnlockDocument"/> class.
        /// Lock current document
        /// </summary>
        public ChLockUnlockDocument()
        {
            LockDocument();
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// Unlock current document
        /// </summary>
        public void Dispose()
        {
            UnlockDocument();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Locks the document.
        /// </summary>
        private void LockDocument()
        {
            if (qtLocks == 0)
            {
                doc = acApp.Application.DocumentManager.MdiActiveDocument;
                docLock = doc.LockDocument();
            }
            qtLocks++;
        }

        /// <summary>
        /// Unlocks the document.
        /// </summary>
        private void UnlockDocument()
        {
            qtLocks--;
            if (qtLocks == 0 && docLock != null)
            {
                docLock.Dispose();
            }
        }

        /// <summary>
        /// Uns the lock document.
        /// </summary>
        /// <param name="doc">The document.</param>
        public static void UnLockDocument(acApp.Document doc)
        {
            var docLock = doc.LockDocument();
            docLock.Dispose();
            qtLocks = 0;
        }

        #endregion Private Methods
    }
}
