﻿using CadApiHelper.Core;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChPolyline instance
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IEnumerator&lt;CadApiHelper.Geometry.IChPolylineSegment&gt;" />
    /// <seealso cref="System.Collections.Generic.IEnumerable&lt;CadApiHelper.Geometry.IChPolylineSegment&gt;" />
    /// <seealso cref="System.IEquatable&lt;CadApiHelper.Geometry.IChPolyline&gt;" />
    public interface IChPolyline : IEnumerator<IChPolylineSegment>, IEnumerable<IChPolylineSegment>, IEquatable<IChPolyline>
    {
        #region Properties        
        /// <summary>
        /// Gets the area.
        /// </summary>
        /// <value>
        /// The area.
        /// </value>
        double Area { get; }

        /// <summary>
        /// Gets the area changed.
        /// </summary>
        /// <value>
        /// The area changed.
        /// </value>
        double AreaChanged { get; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="IChPolyline"/> is closed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if closed; otherwise, <c>false</c>.
        /// </value>
        bool Closed { get; }

        /// <summary>
        /// Gets the count segments.
        /// </summary>
        /// <value>
        /// The count segments.
        /// </value>
        int CountSegments { get; }

        /// <summary>
        /// Gets the count vertices.
        /// </summary>
        /// <value>
        /// The count vertices.
        /// </value>
        int CountVertices { get; }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        bool IsValid { get; }

        /// <summary>
        /// Gets the length.
        /// </summary>
        /// <value>
        /// The length.
        /// </value>
        double Length { get; }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        ChObjectId ObjectId { get; set; }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        object Tag { get; set; }

        /// <summary>
        /// Gets the vertices.
        /// </summary>
        /// <value>
        /// The vertices.
        /// </value>
        List<IChPoint> Vertices { get; }

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <value>
        /// The segments.
        /// </value>
        ReadOnlyCollection<IChPolylineSegment> Segments { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="point">The point.</param>
        void AddVertice(IChPoint point);

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="secondPointArc">The second point arc.</param>
        /// <param name="endPointArc">The end point arc.</param>
        void AddVertice(IChPoint endPointArc, IChPoint secondPointArc);

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="point">The point.</param>
        void AddVertice(int index, IChPoint point);

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="secondPointArc">The second point arc.</param>
        /// <param name="endPointArc">The end point arc.</param>
        void AddVertice(int index, IChPoint secondPointArc, IChPoint endPointArc);

        /// <summary>
        /// Fixes the vertices.
        /// </summary>
        /// <param name="toleranceDistanceSegmnents">The tolerance distance between segmnents.</param>
        /// <param name="toleranceAngleSegments">The tolerance angle between segments.</param>
        void FixVertices(double toleranceDistanceSegmnents = 0.0001, double toleranceAngleSegments = 0.0174532925);

        /// <summary>
        /// Gets the segment.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>IPolylineSegment</returns>
        IChPolylineSegment GetSegment(int index);

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <returns>List of IChPolylineSegment</returns>
        IEnumerable<IChPolylineSegment> GetSegments();

        /// <summary>
        /// Verifies the intersects between ChPolyline and IChPolylineSegment.
        /// </summary>
        /// <param name="segment">The segmento to be verify.</param>
        /// <returns>True whether intersect</returns>
        bool IntersectWith(IChPolylineSegment segment);

        /// <summary>
        /// Verifies the intersects between ChPolyline and IChPolylineSegment and return the list points intersect.
        /// </summary>
        /// <param name="segment">The segment.</param>
        /// <param name="pointsIntersects">The points intersects.</param>
        /// <returns>True whether intersect</returns>
        bool IntersectWith(IChPolylineSegment segment, List<IChPoint> pointsIntersects);

        /// <summary>
        /// Determines whether the specified point is on.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        ///   <c>true</c> if the specified point is on; otherwise, <c>false</c>.
        /// </returns>
        bool IsOn(IChPoint point);

        /// <summary>
        /// Determines whether [is point inside2 d] [the specified point].
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <returns>
        ///   <c>true</c> if [is point inside2 d] [the specified point]; otherwise, <c>false</c>.
        /// </returns>
        bool IsPointInside2D(IChPoint point, double tolerance = 0.001);

        /// <summary>
        /// Removes the last point.
        /// </summary>
        /// <returns>True whether the last point was removed</returns>
        bool RemoveLastPoint();

        /// <summary>
        /// Remove point as index of vertex passed as parameter
        /// </summary>
        /// <param name="index">index of the vertex to be removed</param>
        /// <returns>True whether the point was removed</returns>
        bool RemovePointAt(int index);

        #endregion Public Methods
    }
}