﻿using System;
using System.Collections.Generic;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// This class represent an arc
    /// </summary>
    [Serializable]
    public class ChArc : IChArc, IEquatable<IChSegment>, IEquatable<IChArc>, IEquatable<IChPolylineSegment>
    {
        #region Fields

        private IChPoint _centerPoint;
        private double _endAngle;
        private IChPoint _endPoint;
        private IChVector _normal = ChVector.Create(0, 0, 1);
        private double _radius;
        private double _startAngle;
        private IChPoint _startPoint;

        [NonSerialized]
        private bool isValid = true;

        [NonSerialized]
        private object tag;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Angle of the arc
        /// </summary>
        public double Angle
        {
            get
            {
                if (EndAngle >= StartAngle)
                    return EndAngle - StartAngle;
                else
                    return ChAngles.RAD360 - (StartAngle - EndAngle);
            }
        }

        /// <summary>
        /// Area of the arc
        /// </summary>
        public double Area
        {
            get
            {
                if (Radius == 0)
                    return 0;
                double sectorArea = Math.Pow(Radius, 2) * Angle / 2.0;
                var areaTriangulo = ChCalculateTriangleArea.CalculateArea(Radius, Radius, Angle);
                return sectorArea - areaTriangulo;
            }
        }

        /// <summary>
        /// Gets the middle point arc.
        /// </summary>
        /// <value>
        /// The middle point arc.
        /// </value>
        public IChPoint MiddlePointArc
        {
            get
            {
                List<double> angulos = new List<double>();
                double anguloCentral = 0;
                if (StartAngle > EndAngle)
                    angulos.AddRange(new List<double>() { EndAngle, StartAngle });
                else
                    angulos.AddRange(new List<double>() { StartAngle, EndAngle });
                anguloCentral = ((angulos[1] - angulos[0]) / 2.0) + angulos[0];
                return CenterPoint.PolarPoint(anguloCentral, Radius);
            }
        }

        /// <summary>
        /// Bulge of the arc
        /// </summary>
        public double Bulge
        {
            get
            {
                double deltaAngle = EndAngle - StartAngle;
                if (deltaAngle < 0)
                    deltaAngle += (2 * Math.PI);

                return Math.Tan(deltaAngle / 4) * Math.Sign(Normal.Z + Normal.Y + Normal.X);
            }
        }

        /// <summary>
        /// Center Point
        /// </summary>
        public IChPoint CenterPoint
        {
            get { return _centerPoint; }
            private set
            {
                _centerPoint = value;
            }
        }

        /// <summary>
        /// End angle
        /// </summary>
        public double EndAngle
        {
            get { return _endAngle; }
            private set { _endAngle = value; }
        }

        /// <summary>
        /// End point
        /// </summary>
        public IChPoint EndPoint
        {
            get { return _endPoint; }
            set { _endPoint = value; }
        }

        /// <summary>
        /// Check if arc is valid when create by Object Id
        /// </summary>
        public bool IsValid
        {
            get { return isValid; }
            private set { isValid = value; }
        }

        /// <summary>
        /// Length of the arc
        /// </summary>
        public double Length
        {
            get { return Perimeter; }
        }

        /// <summary>
        /// Normal vector
        /// </summary>
        public IChVector Normal
        {
            get { return _normal; }
            set { _normal = value; }
        }

        /// <summary>
        /// Perimeter or Length
        /// </summary>
        public double Perimeter
        {
            get
            {
                if (StartAngle > EndAngle)
                    return (ChAngles.RAD360 - StartAngle + EndAngle) * Radius;
                else
                    return (EndAngle - StartAngle) * Radius;
            }
        }

        /// <summary>
        /// Radius
        /// </summary>
        public double Radius
        {
            get { return _radius; }
            private set
            {
                _radius = value;
            }
        }

        /// <summary>
        /// Type of segment arc SegmentType.Arc
        /// </summary>
        public ChSegmentType SegmentType
        {
            get { return ChSegmentType.Arc; }
        }

        /// <summary>
        /// Start angle
        /// </summary>
        public double StartAngle
        {
            get { return _startAngle; }
            private set { _startAngle = value; }
        }

        /// <summary>
        /// Start point
        /// </summary>
        public IChPoint StartPoint
        {
            get { return _startPoint; }
            set { _startPoint = value; }
        }

        /// <summary>
        /// Tag
        /// </summary>
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }


        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="IChArc"/> class.
        /// </summary>
        /// <param name="startPoint">Start point</param>
        /// <param name="secondPoint">Second point</param>
        /// <param name="endPoint">End point</param>
        public ChArc(IChPoint startPoint, IChPoint secondPoint, IChPoint endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
            Arcby3Points(startPoint, secondPoint, endPoint);
        }

        /// <summary>
        /// Create an instance of the Arc by arc parameter
        /// </summary>
        /// <param name="arc">Arc</param>
        public ChArc(IChArc arc)
        {
            _centerPoint = arc.CenterPoint;
            _endPoint = arc.EndPoint;
            _startPoint = arc.StartPoint;
            _radius = arc.Radius;
            _normal = arc.Normal;
            _endAngle = arc.EndAngle;
            _startAngle = arc.StartAngle;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChArc"/> class.
        /// </summary>
        /// <param name="centerPoint">The center point.</param>
        /// <param name="endAngle">The end angle.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="normal">The normal.</param>
        /// <param name="radius">The radius.</param>
        /// <param name="startAngle">The start angle.</param>
        /// <param name="startPoint">The start point.</param>
        public ChArc(IChPoint centerPoint, double endAngle, IChPoint endPoint, IChVector normal, double radius, double startAngle, IChPoint startPoint)
        {
            _centerPoint = centerPoint;
            _endAngle = endAngle;
            _endPoint = endPoint;
            _normal = normal;
            _radius = radius;
            _startAngle = startAngle;
            _startPoint = startPoint;
        }


        #endregion Public Constructors

        #region Private Methods

        private bool Arcby3Points(IChPoint startPoint, IChPoint secondPoint, IChPoint endPoint)
        {
            IChPoint m1 = startPoint.Mid(secondPoint);
            IChPoint m2 = secondPoint.Mid(endPoint);

            IChPoint polar1 = m1.PolarPoint(startPoint.AngleFromXAxis(secondPoint) + Math.PI / 2.0, 1.0);
            IChPoint polar2 = m2.PolarPoint(secondPoint.AngleFromXAxis(endPoint) + Math.PI / 2.0, 1.0);
            var lineSegment = ChLineSegment.Create(m1, polar1);

            if (!lineSegment.InterseptPoint(ChLineSegment.Create(m2, polar2), ref _centerPoint))
                return false;

            if (IsClockwise(new List<IChPoint>() { startPoint, secondPoint, endPoint }))
            {
                StartAngle = CenterPoint.AngleFromXAxis(endPoint);
                EndAngle = CenterPoint.AngleFromXAxis(startPoint);
            }
            else
            {
                StartAngle = CenterPoint.AngleFromXAxis(startPoint);
                EndAngle = CenterPoint.AngleFromXAxis(endPoint);
            }

            Radius = CenterPoint.Distance(startPoint);
            return true;
        }

        /// <summary>
        /// Return if the points is Clockwise orientation
        /// </summary>
        /// <param name="points">Points</param>
        /// <returns>True if is clockwise</returns>
        private bool IsClockwise(List<IChPoint> points)
        {
            double sum = 0;
            for (int idxPoint = 0; idxPoint < points.Count; idxPoint++)
            {
                sum += (points[(idxPoint + 1) % points.Count].X - points[idxPoint].X) *
                       (points[(idxPoint + 1) % points.Count].Y - points[idxPoint].Y);
            }
            return sum > 0;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Create an instance of the IArc.
        /// </summary>
        /// <param name="startPoint">Start point</param>
        /// <param name="secondPoint">Second point</param>
        /// <param name="endPoint">End point</param>
        /// <returns>IChArc instance</returns>
        public static IChArc Create(IChPoint startPoint, IChPoint secondPoint, IChPoint endPoint)
        {
            return new ChArc(startPoint, secondPoint, endPoint);
        }

        /// <summary>
        /// Create an instance of the IArc.
        /// </summary>
        /// <param name="centerPoint">The center point.</param>
        /// <param name="endAngle">The end angle.</param>
        /// <param name="endPoint">The end point.</param>
        /// <param name="normal">The normal.</param>
        /// <param name="radius">The radius.</param>
        /// <param name="startAngle">The start angle.</param>
        /// <param name="startPoint">The start point.</param>
        /// <returns>IChArc instance</returns>
        public static IChArc Create(IChPoint centerPoint, double endAngle, IChPoint endPoint, IChVector normal,
            double radius, double startAngle, IChPoint startPoint)
        {
            return new ChArc(centerPoint, endAngle, endPoint, normal, radius, startAngle, startPoint);
        }

        /// <summary>
        /// Copy segment
        /// </summary>
        /// <returns>ISegment copy</returns>
        public IChSegment CopySegment()
        {
            var arco = new ChArc(this);
            return arco;
        }

        /// <summary>
        /// Checks if two object are equal
        /// </summary>
        /// <param name="obj">Object</param>
        /// <returns>True if two objects are equal</returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as IChSegment);
        }

        /// <summary>
        /// Check if two segments are equal
        /// </summary>
        /// <param name="other">Other segment</param>
        /// <returns>True if equals otherwise false</returns>
        public bool Equals(IChSegment other)
        {
            return Equals(other as IChArc);
        }

        /// <summary>
        /// Check if this instance Intersect With another ISegment
        /// </summary>
        /// <param name="segment">Segment</param>
        /// <returns>True if point is on the segment</returns>
        public bool IntersectWith(IChSegment segment)
        {
            var points = new List<IChPoint>();
            return IntersectWith(segment, points);
        }

        /// <summary>
        /// Check if this instance Intersect With another ISegment
        /// </summary>
        /// <param name="segment">Segment</param>
        /// <param name="points">Intersect points</param>
        /// <returns>True if point is on the segment</returns>
        public bool IntersectWith(IChSegment segment, List<IChPoint> points)
        {
            switch (segment.SegmentType)
            {
                case ChSegmentType.Arc:
                    var ar = segment as IChArc;
                    if (ar == null)
                        return false;
                    return ChIntersectionHelper.IntersectionArctWithArc(this, ar, points);

                case ChSegmentType.Line:
                    var lin = segment as ChLineSegment;
                    if (lin == null)
                        return false;
                    return ChIntersectionHelper.IntersectionLineSegmentWithArc(lin, this, points);

                case ChSegmentType.Circle:
                    var cir = segment as ChCircle;
                    if (cir == null)
                        return false;
                    return ChIntersectionHelper.IntersectionCircletWithArc(cir, this, points);

                default:
                    return false;
            }
        }

        /// <summary>
        /// Check if the point is on the arc
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns>True if the point is on the arc otherwise false</returns>
        public bool IsOn(IChPoint point)
        {
            return IsOn(point, ChPoint.Tolerance);
        }

        /// <summary>
        /// Check if the point is on the arc with a tolerance parameter
        /// </summary>
        /// <param name="point">Point</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>True if the point is on the arc otherwise false</returns>
        public bool IsOn(IChPoint point, double tolerance)
        {
            var vec1 = ChVector.Create(point.Subtract(CenterPoint));
            var dist = CenterPoint.Distance(point);
            if (Math.Abs(dist - Radius) > tolerance)
                return false;


            if ((this.StartAngle <= this.EndAngle) &&
                (this.StartAngle <= vec1.Angle && vec1.Angle <= this.EndAngle))
                return true;
            else if ((this.StartAngle > this.EndAngle) &&
                (this.StartAngle <= vec1.Angle && vec1.Angle <= 2 * Math.PI) || (0 <= vec1.Angle && vec1.Angle <= this.EndAngle))
                return true;

            return false;
        }

        /// <summary>
        /// Translation in 2d
        /// </summary>
        /// <param name="deltaX">Delta x</param>
        /// <param name="deltaY">Delta y</param>
        public void Translation2d(double deltaX, double deltaY)
        {
            StartPoint.Translacao2d(deltaX, deltaY);
            CenterPoint.Translacao2d(deltaX, deltaY);
            EndPoint.Translacao2d(deltaX, deltaY);
        }

        /// <summary>
        /// Equals the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(IChPolylineSegment other)
        {
            if (other == null)
                return false;

            return Equals(other as IChArc);

        }

        /// <summary>
        /// Equals the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>True whether two arc are equal</returns>
        public bool Equals(IChArc other)
        {
            if (other == null)
                return false;

            var list = new List<bool>
            {
                Radius == other.Radius,
                StartAngle == other.StartAngle,
                EndAngle == other.EndAngle,
                StartPoint == other.StartPoint,
                CenterPoint == other.CenterPoint,
                EndPoint == other.EndPoint,
            };
            return list.TrueForAll(x => x);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hashCode = -751559578;
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(_centerPoint);
            hashCode = hashCode * -1521134295 + _endAngle.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(_endPoint);
            hashCode = hashCode * -1521134295 + EqualityComparer<IChVector>.Default.GetHashCode(_normal);
            hashCode = hashCode * -1521134295 + _radius.GetHashCode();
            hashCode = hashCode * -1521134295 + _startAngle.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(_startPoint);
            hashCode = hashCode * -1521134295 + isValid.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(CenterPoint);
            hashCode = hashCode * -1521134295 + EndAngle.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(EndPoint);
            return hashCode;
        }

        #endregion Public Methods
    }
}