﻿#if ZWCAD
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
using Teigha.Geometry;
using Teigha.DatabaseServices;
#elif ACAD
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
#elif GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#endif

using CadApiHelper.Core;
using CadApiHelper.Entities.Edit;

using System;
using System.Collections.Generic;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Represent the segment of line
    /// </summary>
    [Serializable]
    public class ChLineSegment : ChBaseLine, IChLineSegment, IEquatable<IChSegment>, IEquatable<IChLineSegment>, IEquatable<IChPolylineSegment>
    {
        #region Fields

        private IChPoint _endPoint = new ChPoint();

        [NonSerialized]
        private bool _isValid = true;

        private IChPoint _startPoint = new ChPoint();

        [NonSerialized]
        private object tag = null;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Angle of line segment
        /// </summary>
        public double Angle
        {
            get
            {
                return ChVector.Create(StartPoint, EndPoint).Angle;
            }
        }

        /// <summary>
        /// Area of line segment = 0
        /// </summary>
        public double Area => 0;

        /// <summary>
        /// Return the end point of the line
        /// </summary>
        public IChPoint EndPoint
        {
            get { return _endPoint; }
            set
            {
                _endPoint = value;
            }
        }

        /// <summary>
        /// Returns if the line has its angle in the 1st or 4th quadrants
        /// </summary>
        /// <returns>If true the line angle is in the 1st or 4th quadrant, otherwise false</returns>
        public bool IsQuadrantOneOrFour
        {
            get
            {
                double distX = Math.Round(EndPoint.X - StartPoint.X, 5);
                double distY = Math.Round(EndPoint.Y - StartPoint.Y, 5);
                if (Math.Abs(distX) < 0.00001 && distY < 0)
                    return false;
                else if (distX < 0)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid { get => _isValid; set => _isValid = value; }

        /// <summary>
        /// Length of line segment
        /// </summary>
        public double Length => StartPoint.Distance(EndPoint);

        /// <summary>
        /// Returns the mid point of the segment
        /// </summary>
        public IChPoint MidPoint
        {
            get { return StartPoint.Mid(EndPoint); }
        }

        /// <summary>
        /// Type o segment = SegmentType.Line
        /// </summary>
        public ChSegmentType SegmentType => ChSegmentType.Line;

        /// <summary>
        /// Return the starting point of the line
        /// </summary>
        public IChPoint StartPoint
        {
            get { return _startPoint; }
            set
            {
                _startPoint = value;
            }
        }

        /// <summary>
        /// Tag
        /// </summary>
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        /// <summary>
        /// Retorna o Vetor Diretor do segmento
        /// </summary>
        public override IChVector VectorDiretor
        {
            get
            {
                return ChVector.Create(StartPoint, EndPoint);
            }
        }



        /// <summary>
        /// Gets the bulge.
        /// </summary>
        /// <value>
        /// The bulge.
        /// </value>
        public double Bulge { get { return 0.0; } }

        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Create an instance of LineSegment
        /// </summary>
        /// <param name="startPoint">Start point</param>
        /// <param name="endPoint">End Point</param>
        public ChLineSegment(IChPoint startPoint, IChPoint endPoint) : base(startPoint, endPoint)
        {
            StartPoint = startPoint;
            EndPoint = endPoint;
        }

        /// <summary>
        /// Create an instance of LineSegment
        /// </summary>
        /// <param name="id">ObjectId of the entity</param>
        public ChLineSegment(ChObjectId id)
        {
            var lin = new ChEditLine(id);
            using (lin)
            {
                if (lin.DbObject == null)
                {
                    IsValid = false;
                    return;
                }

                StartPoint = lin.DbObject.StartPoint.ToPoint();
                EndPoint = lin.DbObject.EndPoint.ToPoint();
                CalculateLineEquation(StartPoint, EndPoint);
            }
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Create an instance to ILineSegment.
        /// </summary>
        /// <param name="startPoint">Start point</param>
        /// <param name="endPoint">End point</param>
        /// <returns>ILineSegment instance</returns>
        public static IChLineSegment Create(IChPoint startPoint, IChPoint endPoint)
        {
            return new ChLineSegment(startPoint, endPoint);
        }

        /// <summary>
        /// Check if a line segment is the director vector in quadrant 1 or 4.
        /// If not in director vector in quadrant 1 or 4, change orientation of line segment
        /// </summary>
        public void ChangeLineQuadrantOneOrFour()
        {
            if (IsQuadrantOneOrFour)
                return;

            ChangeOrientation();
        }

        /// <summary>
        /// Function responsible for changing the direction of a segment.
        /// </summary>
        public void ChangeOrientation()
        {
            IChPoint p1 = StartPoint;
            StartPoint = EndPoint;
            EndPoint = p1;
        }

        /// <summary>
        /// Clone LineSegment
        /// </summary>
        /// <returns>LineSegment</returns>
        public IChLineSegment Clone()
        {
            return new ChLineSegment(StartPoint, EndPoint);
        }

        /// <summary>
        /// Copy segment
        /// </summary>
        /// <returns>ISegment</returns>
        public IChSegment CopySegment()
        {
            return Clone();
        }

        /// <summary>
        /// Return the distance from the point to the segment
        /// </summary>
        /// <param name="point">Point to be evaluate</param>
        /// <returns></returns>
        public double DistanceToPoint2d(IChPoint point)
        {
            return point.DistanceToLineSegment2D(this);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as IChLineSegment);
        }

        /// <summary>
        /// Verifies if two object is equal
        /// </summary>
        /// <param name="other">The object to be verified</param>
        /// <returns></returns>
        public bool Equals(IChSegment other)
        {
            if (other == null)
                return false;

            return Equals(other as ChLineSegment);
        }

        /// <summary>
        /// Verifies if two object is equal
        /// </summary>
        /// <param name="other">The object to be verified</param>
        /// <returns></returns>
        public bool Equals(IChLineSegment other)
        {
            if (other == null)
                return false;

            return StartPoint.Equals(other.StartPoint) && EndPoint.Equals(other.EndPoint);
        }

        /// <summary>
        /// Function responsible for producing a line segment with the director vector in quadrant 1 or 4.
        /// </summary>
        /// <returns>Line segment orientation to quadrant 1 or 4.</returns>
        public IChLineSegment GetLineQuadrantOneOrFour()
        {
            if (IsQuadrantOneOrFour)
                return this;

            var l = Clone();
            l.ChangeOrientation();
            return l;
        }

        /// <summary>
        /// Check if this instance Intersect With another ISegment
        /// </summary>
        /// <param name="segment">Segment</param>
        /// <returns>True if point is on the segment</returns>
        public bool IntersectWith(IChSegment segment)
        {
            var pts = new List<IChPoint>();
            return IntersectWith(segment, pts);
        }

        /// <summary>
        /// Check if this instance Intersect With another ISegment
        /// </summary>
        /// <param name="segment">Segment</param>
        /// <param name="points">Intersect points</param>
        /// <returns>True if point is on the segment</returns>
        public bool IntersectWith(IChSegment segment, List<IChPoint> points)
        {
            if (segment == null || points == null)
                return false;

            var pt = ChPoint.Create();
            switch (segment.SegmentType)
            {
                case ChSegmentType.Arc:
                    var arc = segment as IChArc;
                    if (arc == null)
                        return false;
                    return ChIntersectionHelper.IntersectionLineSegmentWithArc(this, arc, points);

                case ChSegmentType.Line:
                    var reta = segment as ChLineSegment;
                    bool result = InterseptPoint(reta, ref pt);
                    if (result)
                        points.Add(pt);
                    return result;

                case ChSegmentType.Circle:
                    var circle = segment as ChCircle;
                    if (circle == null)
                        return false;
                    return ChIntersectionHelper.IntersectionLineSegmentWithCircle(this, circle, points);

                default:
                    break;
            }
            return false;
        }

        /// <summary>
        ///  Check if the point is on the arc
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns>True if the point is on the arc otherwise false</returns>
        public bool IsOn(IChPoint point)
        {
            return OnPoint(point);
        }

        /// <summary>
        /// Check if point is on
        /// </summary>
        /// <param name="point">Point</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>True if the point is on the arc otherwise false</returns>
        public bool IsOn(IChPoint point, double tolerance)
        {
            return OnPoint(point, tolerance);
        }

        /// <summary>
        /// Check if the point lies segment
        /// </summary>
        /// <param name="pt"></param>
        /// <param name="tol"></param>
        /// <returns></returns>
        public override bool OnPoint(IChPoint pt, double tol)
        {
            if (!base.OnPoint(pt, tol))
                return false;
            if ((pt.Distance(EndPoint) < tol || pt.Distance(StartPoint) < tol))
                return false;
            var vt1 = ChVector.Create(pt, StartPoint);
            var vt2 = ChVector.Create(pt, EndPoint);
            if (Math.Abs(vt1.Modulo + vt2.Modulo - VectorDiretor.Modulo) < tol)
                return true;
            return false;
        }

        /// <summary>
        /// Return the smaller distance between segments
        /// </summary>
        /// <param name="segLine">Line segment</param>
        /// <returns></returns>
        public double SmallerDistanceToLineSegment2d(IChLineSegment segLine)
        {
            double minorDist;
            List<double> distances = new List<double>();

            IChPoint segLine1Pt1 = this.StartPoint;
            IChPoint segLine1Pt2 = this.EndPoint;
            IChPoint segLine2Pt1 = segLine.StartPoint;
            IChPoint segLine2Pt2 = segLine.EndPoint;

            distances.Add(this.DistanceToPoint2d(segLine2Pt1));
            distances.Add(this.DistanceToPoint2d(segLine2Pt2));
            distances.Add(segLine.DistanceToPoint2d(segLine1Pt1));
            distances.Add(segLine.DistanceToPoint2d(segLine1Pt2));

            minorDist = distances[0];
            for (int idxDist = 0; idxDist < distances.Count; idxDist++)
            {
                if (minorDist > distances[idxDist])
                    minorDist = distances[idxDist];
            }

            return minorDist;
        }

        /// <summary>
        /// Translation in 2d
        /// </summary>
        /// <param name="deltaX">Delta x</param>
        /// <param name="deltaY">Delta y</param>
        public void Translation2d(double deltaX, double deltaY)
        {
            Translation2d(deltaX, deltaY, ref _startPoint, ref _endPoint);
        }

        /// <summary>
        /// Equals the specified other.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>True whether two line segment are equal</returns>
        public bool Equals(IChPolylineSegment other)
        {
            if (other == null)
                return false;

            return Equals(other as IChLineSegment);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hashCode = -821736873;
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(_endPoint);
            hashCode = hashCode * -1521134295 + _isValid.GetHashCode();
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(_startPoint);
            return hashCode;
        }

        /// <summary>
        /// Gets the line3d.
        /// </summary>
        /// <returns>
        /// Line3d
        /// </returns>
        public Line3d GetLine3d()
        {
            return new Line3d(StartPoint.To3dPoint(), EndPoint.To3dPoint());
        }

        #endregion Public Methods
    }
}