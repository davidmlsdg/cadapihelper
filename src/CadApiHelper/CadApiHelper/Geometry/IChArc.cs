﻿namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChArc interface
    /// </summary>
    /// <seealso cref="CadApiHelper.Geometry.IChSegment" />
    /// /// <seealso cref="CadApiHelper.Geometry.IChPolylineSegment" />
    public interface IChArc : IChPolylineSegment
    {

        #region Properties

        /// <summary>
        /// Center Point
        /// </summary>
        IChPoint CenterPoint { get; }

        /// <summary>
        /// End angle
        /// </summary>
        double EndAngle { get; }

        /// <summary>
        /// Normal vector
        /// </summary>
        IChVector Normal { get; set; }

        /// <summary>
        /// Perimeter or Length
        /// </summary>
        double Perimeter { get; }

        /// <summary>
        /// Radius
        /// </summary>
        double Radius { get; }

        /// <summary>
        /// Start angle
        /// </summary>
        double StartAngle { get; }

        /// <summary>
        /// Gets the middle point arc.
        /// </summary>
        /// <value>
        /// The middle point arc.
        /// </value>
        IChPoint MiddlePointArc { get; }

        #endregion Properties
    }
}