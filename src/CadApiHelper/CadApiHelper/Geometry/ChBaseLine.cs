﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Class that represents a line described by the Line Equation A*x + B*y + C = 0
    /// </summary>
    public class ChBaseLine : IChBaseLine
    {
        #region Fields

        private double _a;
        private double _b;
        private double _c;

        #endregion Fields

        #region Properties

        /// <summary>
        /// >The a variable for the Line Equation
        /// </summary>
        public double A
        {
            get { return _a; }
            //set { m_a = value; }
        }

        /// <summary>
        /// >The b variable for the Line Equation
        /// </summary>
        public double B
        {
            get { return _b; }
            //set { m_b = value; }
        }

        /// <summary>
        /// >The c variable for the Line Equation
        /// </summary>
        public double C
        {
            get { return _c; }
            //set { m_c = value; }
        }

        /// <summary>
        /// Vector Normal
        /// </summary>
        public virtual IChVector Normal
        {
            get { return ChVector.Create(_a, _b); }
        }

        /// <summary>
        /// vector director
        /// </summary>
        public virtual IChVector VectorDiretor
        {
            get { return ChVector.Create(_b, -_a); }
        }

        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Create an instance of the BaseLine
        /// </summary>
        /// <param name="a">The a variable for the Line Equation</param>
        /// <param name="b">The b variable for the Line Equation</param>
        /// <param name="c">The c variable for the Line Equation</param>
        public ChBaseLine(double a, double b, double c)
        {
            _a = a;
            _b = b;
            _c = c;
        }

        /// <summary>
        /// Create an instance of the BseLine by LineSegment
        /// </summary>
        /// <param name="lineSegment"></param>
        public ChBaseLine(IChLineSegment lineSegment)
        {
            CalculateLineEquation(lineSegment.StartPoint, lineSegment.EndPoint);
        }

        /// <summary>
        /// Create an instance of the BaseLine by two points
        /// </summary>
        /// <param name="startPoint">Start Point</param>
        /// <param name="endPoint">End Point</param>
        public ChBaseLine(IChPoint startPoint, IChPoint endPoint)
        {
            CalculateLineEquation(startPoint, endPoint);
        }

        /// <summary>
        /// Create an instance of the BaseLine by one point and one vector
        /// </summary>
        /// <param name="point">Point</param>
        /// <param name="vector">Vector</param>
        public ChBaseLine(IChPoint point, IChVector vector)
        {
            CalculateLineEquation(point, point.RetangularPoint(vector.X, vector.Y));
        }

        /// <summary>
        /// Create an instance of BaseLine by one point and an angle
        /// </summary>
        /// <param name="point">Point</param>
        /// <param name="angle">Angle in radianos</param>
        public ChBaseLine(IChPoint point, double angle)
        {
            double qx = point.X + Math.Cos(angle);
            double qy = point.Y + Math.Sin(angle);
            CalculateLineEquation(point, ChPoint.Create(qx, qy));
        }

        /// <summary>
        /// Create an instance of BaseLine
        /// </summary>
        public ChBaseLine()
        {
            CalculateLineEquation(ChPoint.Create(), ChPoint.Create());
        }

        #endregion Public Constructors

        #region Private Methods

        private double GetX(IChBaseLine r, double y)
        {
            double x;
            x = (-r.B * y - r.C) / r.A;
            return x;
        }

        private double GetY(IChBaseLine r, double x)
        {
            double y;
            y = (-r.A * x - r.C) / r.B;
            return y;
        }

        private void RecalculatePoint(IChBaseLine r, ref double x, ref double y)
        {
            if (Math.Abs(_b) == 0.0)
            {
                y = (-r.C + (r.A * _c / _a)) / (r.B - (r.A * _b / _a));
                x = (-_b * y - _c) / _a;
                return;
            }
            else if (Math.Abs(r.B) == 0.0)
            {
                y = (-_c + (_a * r.C / r.A)) / (_b - (_a * r.B / r.A));
                x = (-r.B * y - r.C) / r.A;
                return;
            }
            double x1 = x, x2 = x, ya, yb, dist, taxaA, taxaB, taxa;
            ya = GetY(this, x1 - 0.5);
            yb = GetY(this, x1 + 0.5);
            taxaA = yb - ya;
            ya = GetY(r, x1 - 0.5);
            yb = GetY(r, x1 + 0.5);
            taxaB = yb - ya;
            ya = GetY(this, x1);
            yb = GetY(r, x1);
            if (Math.Abs(taxaA) > Math.Abs(taxaB))
                taxa = taxaA;
            else
                taxa = taxaB;
            int i = 0;
            while (Math.Abs(ya - yb) > 0.0001)
            {
                dist = ya - yb;
                x1 = x1 - dist / taxa;
                ya = GetY(this, x1);
                yb = GetY(r, x1);
                i++;
                if (i > 100)
                    return;
            }
            x = x1;
            y = ya;
        }

        #endregion Private Methods

        #region Protected Methods

        /// <summary>
        /// Calculate line equation by two points
        /// </summary>
        /// <param name="startPoint">Start point</param>
        /// <param name="endPoint">End point</param>
        protected void CalculateLineEquation(IChPoint startPoint, IChPoint endPoint) //CalculateLineEquation
        {
            _a = startPoint.Y - endPoint.Y;
            _b = endPoint.X - startPoint.X;
            _c = startPoint.X * endPoint.Y - endPoint.X * startPoint.Y;
            if (_a != 1 && Math.Abs(_a) > 0.0001)
            {
                double razao = 1 / _a;
                _a *= razao;
                _b *= razao;
                _c *= razao;
            }
            else if (Math.Abs(_a) < 0.0001 && Math.Abs(_b) > 0.0001 && _b != 1)
            {
                double razao = 1 / _b;
                _b *= razao;
                _c *= razao;
            }
        }

        #endregion Protected Methods

        #region Public Methods

        /// <summary>
        /// Function returns the X to the point on top of the line in the y-coordinate passed.
        /// </summary>
        /// <param name="y">y coordinate of the point on top of the line</param>
        /// <returns>coordinate x</returns>
        public double CoordinateX(double y)
        {
            return _a == 0 ? 0 : ((-_b * y) - _c) / _a;
        }

        /// <summary>
        /// Function returns the y to the point on top of the straight line in the passed x coordinate.
        /// </summary>
        /// <param name="x">x coordinate of the point on top of the line</param>
        /// <returns>coordinate y</returns>
        public double CoordinateY(double x)
        {
            return _b == 0 ? 0 : ((-_a * x) - _c) / _b;
        }

        /// <summary>
        /// Return the diametrically opposite points
        /// </summary>
        /// <param name="referencePoint">Reference point</param>
        /// <param name="distance">Distance</param>
        /// <returns>Return the diametrically opposite points</returns>
        public Tuple<IChPoint, IChPoint> DiametricallyOppositePoints(IChPoint referencePoint, double distance)//referencePoint
        {
            double ang = this.VectorDiretor.Angle;

            ang = ChAngles.Normalize(ang);
            if (ang > 3 * Math.PI / 2.0)
                ang = 2 * Math.PI - ang;

            var pt1 = ChPoint.Create();
            var pt2 = ChPoint.Create();

            pt1.X = referencePoint.X + Math.Cos(ang) * distance;
            pt1.Y = referencePoint.Y + Math.Sin(ang) * distance;
            pt2.X = referencePoint.X - Math.Cos(ang) * distance;
            pt2.Y = referencePoint.Y - Math.Sin(ang) * distance;

            return new Tuple<IChPoint, IChPoint>(pt1, pt2);
        }

        /// <summary>
        /// Function responsible for calculating the perpendicular distance from a point to the line.
        /// </summary>
        /// <param name="pt">point you want to calculate the distance</param>
        /// <returns>Return the distance from the point to the line</returns>
        public double DistanceToPerpendicularPoint(IChPoint pt)
        {
            var onPt = ChPoint.Create();
            ProjectedPoint(pt, ref onPt);
            return pt.Distance(onPt);
        }

        /// <summary>
        /// Calculate the distance of line
        /// </summary>
        /// <param name="baseLine">Line</param>
        /// <returns>Distance of line</returns>
        public double DistanceToReta(IChBaseLine baseLine)
        {
            var denominator = Math.Sqrt(Math.Pow(this.A, 2) + Math.Pow(this.B, 2));
            return (IsParallel(baseLine) || denominator == 0) ? 0 : (Math.Abs(this.C - baseLine.C) / denominator);
        }

        /// <summary>
        /// Function responsible for detecting the intersection between two lines.
        /// </summary>
        /// <param name="r"></param>
        /// <param name="ptIntersect">Point intersect</param>
        /// <returns>
        /// Returns true if there is only one intersection point. Returns false if the two lines are parallel.
        /// </returns>
        public virtual bool InterseptPoint(IChBaseLine r, ref IChPoint ptIntersect)
        {
            if (r == null)
                return false;

            if (Math.Abs(r.A) < 0.0001 && Math.Abs(r.B) < 0.0001 && Math.Abs(r.C) < 0.0001)
                return false;
            if (IsParallel(r))
                return false;
            double x = 0, y = 0;

            if (Math.Abs(_a) < 0.0001)
            {
                y = -_c / _b;
                x = (-r.C - (r.B * y)) / r.A;
            }
            else if (Math.Abs(_b) < 0.0001)
            {
                x = -_c / _a;
                y = (-r.C - (r.A * x)) / r.B;
            }
            else
            {
                y = (-r.C + (r.A * _c / _a)) / (r.B - (r.A * _b / _a));
                x = (-_b * y - _c) / _a;
            }
            if (Math.Abs(_a * x + _b * y + _c) > 0.0001 ||
                Math.Abs(r.A * x + r.B * y + r.C) > 0.0001)
                RecalculatePoint(r, ref x, ref y);
            ptIntersect = ChPoint.Create(x, y);
            return true;
        }

        /// <summary>
        /// Check if two lines are parallel.
        /// </summary>
        /// <param name="baseLine">Line to be verify</param>
        /// <returns>Returns true if the two lines are parallel</returns>
        public bool IsParallel(IChBaseLine baseLine)
        {
            if (Math.Abs(_a - baseLine.A) < 0.0001 && Math.Abs(_b - baseLine.B) < 0.0001)
                return true;
            return false;
        }

        /// <summary>
        /// Check whether two lines are perpendicular.
        /// </summary>
        /// <param name="baseLine">Line to be verify</param>
        /// <returns>Returns true if the two lines are perpendicular</returns>
        public bool IsPerpendicular(IChBaseLine baseLine)
        {
            var ang = VectorDiretor.AngleBetween(baseLine.VectorDiretor);
            if (Math.Abs(Math.Cos(ang)) < 0.00001)
                return true;
            return false;
        }

        /// <summary>
        /// Checks whether a point is contained in the line.
        /// </summary>
        /// <param name="point">Point to be verify</param>
        /// <returns>True if the point is contained in the line</returns>
        public virtual bool OnPoint(IChPoint point)
        {
            return OnPoint(point, ChPoint.Tolerance);
        }

        /// <summary>
        /// Checks whether a point is contained in the line using tolerance parameter.
        /// </summary>
        /// <param name="point">Point to be verify</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>True if the point is contained in the line</returns>
        public virtual bool OnPoint(IChPoint point, double tolerance)
        {
            if (Math.Abs(_a * point.X + _b * point.Y + _c) < tolerance)
                return true;
            return false;
        }

        /// <summary>
        /// Returns if point is above or below the line.
        /// </summary>
        /// <param name="pt">Ponto to know relative position</param>
        /// <returns>
        /// Positive value whether the point is above
        /// Zero whether the point is on the line
        /// Negative value whether the point is bellow
        /// </returns>
        public double PointRelationLinePosition(IChPoint pt) //
        {
            return _a * pt.X + _b * pt.Y + _c;
        }

        /// <summary>
        /// Returns the projection of a point on a line.
        /// </summary>
        /// <param name="pointReference">Reference point</param>
        /// <param name="projectedPoint">Projected Point</param>
        /// <returns>Returns true if the projected point belongs to the line.</returns>
        public bool ProjectedPoint(IChPoint pointReference, ref IChPoint projectedPoint)
        {
            ChBaseLine r = new ChBaseLine(pointReference, Normal);
            InterseptPoint(r, ref projectedPoint);
            return OnPoint(projectedPoint);
        }

        /// <summary>
        /// Returns the projection of a point on a line.
        /// </summary>
        /// <param name="pointReference">Reference point</param>
        /// <param name="projectedPoint">Projected Point</param>
        /// <param name="angle">Projection Angle</param>
        /// <returns>Returns true if the projected point belongs to the line.</returns>
        public bool ProjectedPoint(IChPoint pointReference, ref IChPoint projectedPoint, double angle)
        {
            var vec = ChVector.Create(angle);
            ChBaseLine r = new ChBaseLine(pointReference, vec);
            InterseptPoint(r, ref projectedPoint);
            return OnPoint(projectedPoint);
        }

        /// <summary>
        /// Returns the projection of a point on a line. With tolerance after using the line equation
        /// </summary>
        /// <param name="pointReference">Reference point</param>
        /// <param name="projectedPoint">Projected Point</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>Returns true if the projected point belongs to the line.</returns>
        public bool ProjectedPointTolerance(IChPoint pointReference, ref IChPoint projectedPoint, double tolerance)
        {
            ChBaseLine r = new ChBaseLine(pointReference, Normal);
            InterseptPoint(r, ref projectedPoint);
            return OnPoint(projectedPoint, tolerance);
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return string.Format("{0}*x + {1}*y + {2} = 0", A.ToString("n3"), B.ToString("n3"), C.ToString("n3"));
        }

        /// <summary>
        /// Translation 2d two points
        /// </summary>
        /// <param name="deltaX">Delta X</param>
        /// <param name="deltaY">Delta y</param>
        /// <param name="startPoint">Start Point</param>
        /// <param name="endPoint">End Point</param>
        public void Translation2d(double deltaX, double deltaY, ref IChPoint startPoint, ref IChPoint endPoint) //Translation2d
        {
            if (_a == 0)
            {
                startPoint = ChPoint.Create(1, (-C / B) + deltaY);
                endPoint = ChPoint.Create(2, (-C / B) + deltaY);
            }
            else if (_b == 0)
            {
                startPoint = ChPoint.Create((-C / A) + deltaX, 1);
                endPoint = ChPoint.Create((-C / A) + deltaX, 2);
            }
            else
            {
                startPoint = ChPoint.Create(-1, CoordinateY(-1)).Sum(ChPoint.Create(deltaX, deltaY));
                endPoint = ChPoint.Create(1, CoordinateY(1)).Sum(ChPoint.Create(deltaX, deltaY));
            }

            CalculateLineEquation(startPoint, endPoint);
        }

        #endregion Public Methods
    }
}