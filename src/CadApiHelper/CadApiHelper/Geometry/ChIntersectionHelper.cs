﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Static class Intersection Helper
    /// </summary>
    public static class ChIntersectionHelper
    {
        #region Private Methods

        /// <summary>
        /// Checks the points in arc limits with.
        /// </summary>
        /// <param name="arc">The arc.</param>
        /// <param name="intersection1">The intersection1.</param>
        /// <param name="intersection2">The intersection2.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>True whether points on arc</returns>
        private static bool ChecksPointsInArcLimitsWith(IChArc arc, IChPoint intersection1, IChPoint intersection2, List<IChPoint> pts)
        {
            if (arc.IsOn(intersection1, 0.001))
                pts.Add(intersection1);

            if (arc.IsOn(intersection2, 0.001))
                pts.Add(intersection2);

            return pts.Count > 0;
        }

        /// <summary>
        /// Checks the points in arc limits with line segment.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <param name="arc">The arc.</param>
        /// <param name="intersection1">The intersection1.</param>
        /// <param name="intersection2">The intersection2.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>True whether points on arc</returns>
        private static bool ChecksPointsInArcLimitsWithLineSegment(IChLineSegment lineSegment, IChArc arc, IChPoint intersection1, IChPoint intersection2, List<IChPoint> pts)
        {
            if (arc.IsOn(intersection1) && lineSegment.OnPoint(intersection1))
                pts.Add(intersection1);

            if (arc.IsOn(intersection2) && lineSegment.OnPoint(intersection2))
                pts.Add(intersection2);

            return pts.Count > 0;
        }

        /// <summary>
        /// Gets the circle line intersection point.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <param name="circle">The circle.</param>
        /// <returns>List of points intersect</returns>
        private static List<IChPoint> GetCircleLineIntersectionPoint(IChLineSegment lineSegment, IChCircle circle)
        {
            double baX = lineSegment.EndPoint.X - lineSegment.StartPoint.X;
            double baY = lineSegment.EndPoint.Y - lineSegment.StartPoint.Y;
            double caX = circle.Center.X - lineSegment.StartPoint.X;
            double caY = circle.Center.Y - lineSegment.StartPoint.Y;

            double a = baX * baX + baY * baY;
            double bBy2 = baX * caX + baY * caY;
            double c = caX * caX + caY * caY - circle.Radius * circle.Radius;

            double pBy2 = bBy2 / a;
            double q = c / a;

            double disc = pBy2 * pBy2 - q;
            if (disc < 0)
            {
                return new List<IChPoint>();
            }
            // if disc == 0 ... dealt with later
            double tmpSqrt = Math.Sqrt(disc);
            double abScalingFactor1 = -pBy2 + tmpSqrt;
            double abScalingFactor2 = -pBy2 - tmpSqrt;

            ChPoint p1 = new ChPoint(lineSegment.StartPoint.X - baX * abScalingFactor1, lineSegment.StartPoint.Y - baY * abScalingFactor1);
            if (disc == 0)
            { // abScalingFactor1 == abScalingFactor2
                return new List<IChPoint> { p1 };
            }
            ChPoint p2 = new ChPoint(lineSegment.StartPoint.X - baX * abScalingFactor2, lineSegment.StartPoint.Y - baY * abScalingFactor2);
            return new List<IChPoint> { p1, p2 };
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Intersections the arc with arc.
        /// </summary>
        /// <param name="arc1">The arc1.</param>
        /// <param name="arc2">The arc2.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>>True whether two segment intersect</returns>
        public static bool IntersectionArctWithArc(IChArc arc1, IChArc arc2, List<IChPoint> pts)
        {
            var circle1 = ChCircle.Create(arc1.CenterPoint, arc1.Radius);
            var circle2 = ChCircle.Create(arc2.CenterPoint, arc2.Radius);
            if (!IntersectionCircletWithCircle(circle1, circle2, pts) || pts == null || pts.Count == 0)
                return false;
            var intersection1 = pts.FirstOrDefault();
            var intersection2 = pts.LastOrDefault();
            pts.Clear();
            return ChecksPointsInArcLimitsWith(arc1, intersection1, intersection2, pts) && ChecksPointsInArcLimitsWith(arc2, intersection1, intersection2, pts);
        }

        /// <summary>
        /// Intersections the circlet with arc.
        /// </summary>
        /// <param name="circle">The circle.</param>
        /// <param name="arc">The arc.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>>True whether two segment intersect</returns>
        public static bool IntersectionCircletWithArc(IChCircle circle, IChArc arc, List<IChPoint> pts)
        {
            var circle2 = ChCircle.Create(arc.CenterPoint, arc.Radius);
            if (!IntersectionCircletWithCircle(circle, circle2, pts) || pts == null || pts.Count == 0)
                return false;
            var intersection1 = pts.FirstOrDefault();
            var intersection2 = pts.LastOrDefault();
            pts.Clear();
            return ChecksPointsInArcLimitsWith(arc, intersection1, intersection2, pts);
        }

        /// <summary>
        /// Intersections the circlet with circle.
        /// </summary>
        /// <param name="circle1">The circle1.</param>
        /// <param name="circle2">The circle2.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>>True whether two circle intersect</returns>
        public static bool IntersectionCircletWithCircle(IChCircle circle1, IChCircle circle2, List<IChPoint> pts)
        {
            double c1x = circle1.Center.X;
            double c1y = circle1.Center.Y;
            double r1 = circle1.Radius;
            double c2x = circle2.Center.X;
            double c2y = circle2.Center.Y;
            double r2 = circle2.Radius;

            if (circle1.Center.Distance(circle2.Center) > r1 + r2)
                return false;

            if (c1x == c2x && c1y == c2y)
            {
                if (r1 == r2)
                    return true;
                else
                    return false;
            }

            double alfa = -Math.Pow(c1x, 2) + Math.Pow(c2x, 2) - Math.Pow(c1y, 2) + Math.Pow(c2y, 2) + Math.Pow(r1, 2) - Math.Pow(r2, 2);
            double beta = c1x - c2x;
            double gama = -c1y + c2y;
            double a = 0;
            double b = 0;
            double c = 0;
            double discriminante = 0;

            if (beta == 0)
            {
                double teta = -Math.Pow(c1y, 2) + Math.Pow(c2y, 2) + Math.Pow(r1, 2) - Math.Pow(r2, 2);
                a = 1;
                b = -2 * c1x;
                c = (Math.Pow(c1x, 2)) + (Math.Pow(c1y, 2)) + (Math.Pow(teta, 2) / (4 * Math.Pow(gama, 2))) - ((teta * c1y) / gama) - Math.Pow(r1, 2);
                discriminante = Math.Pow(b, 2) - 4 * a * c;
                if (discriminante < 0)
                {
                    return false;
                }
                else
                {
                    double temp = teta / (2 * gama);
                    double intersectionPt1X = (-b - Math.Sqrt(discriminante)) / (2 * a);
                    double intersectionPt1Y = temp;
                    pts.Add(ChPoint.Create(intersectionPt1X, intersectionPt1Y));
                    if (discriminante == 0)
                        return true;
                    double intersectionPt2X = (-b + Math.Sqrt(discriminante)) / (2 * a);
                    double intersectionPt2Y = temp;
                    pts.Add(ChPoint.Create(intersectionPt2X, intersectionPt2Y));
                    return true;
                }
            }
            else if (gama == 0)
            {
                double epsilon = -Math.Pow(c1x, 2) + Math.Pow(c2x, 2) + Math.Pow(r1, 2) - Math.Pow(r2, 2);
                a = 1;
                b = -2 * c1y;
                c = (Math.Pow(epsilon, 2) / (4 * Math.Pow(beta, 2))) + ((c1x * epsilon) / beta) + Math.Pow(c1x, 2) + Math.Pow(c1y, 2) - Math.Pow(r1, 2);
                discriminante = Math.Pow(b, 2) - 4 * a * c;
                if (discriminante < 0)
                {
                    return false;
                }
                else
                {
                    double temp = epsilon / (-2 * beta);
                    double intersectionPt1X = temp;
                    double intersectionPt1Y = (-b - Math.Sqrt(discriminante)) / (2 * a);
                    pts.Add(ChPoint.Create(intersectionPt1X, intersectionPt1Y));
                    if (discriminante == 0)
                        return true;
                    double intersectionPt2X = temp;
                    double intersectionPt2Y = (-b + Math.Sqrt(discriminante)) / (2 * a);
                    pts.Add(ChPoint.Create(intersectionPt2X, intersectionPt2Y));
                    return true;
                }
            }
            else
            {
                //baskhara
                a = 1 + (Math.Pow(beta, 2) / Math.Pow(gama, 2));
                b = (-2 * c1x) + (beta * alfa / Math.Pow(gama, 2)) - (2 * beta * c1y / gama);
                c = Math.Pow(c1x, 2) + Math.Pow(c1y, 2) + (Math.Pow(alfa, 2) / (4 * Math.Pow(gama, 2))) - (alfa * c1y / gama) - Math.Pow(r1, 2);

                discriminante = Math.Pow(b, 2) - 4 * a * c;
                if (discriminante < 0)//acho que isso não precisava verificar mais
                    return false;
                else if (discriminante > 0)
                {
                    double intersectionPt2X = (-b - Math.Sqrt(discriminante)) / (2 * a);
                    double intersectionPt2Y = (2 * intersectionPt2X * beta + alfa) / (2 * gama);
                    pts.Add(ChPoint.Create(intersectionPt2X, intersectionPt2Y));
                }
                double intersectionPt1X = (-b + Math.Sqrt(discriminante)) / (2 * a);
                double intersectionPt1Y = (2 * intersectionPt1X * beta + alfa) / (2 * gama);
                pts.Add(ChPoint.Create(intersectionPt1X, intersectionPt1Y));
                return true;
            }
        }

        /// <summary>
        /// Intersections the line segment with arc.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <param name="arc">The arc.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>True whether two segment intersect</returns>
        public static bool IntersectionLineSegmentWithArc(IChLineSegment lineSegment, IChArc arc, List<IChPoint> pts)
        {
            bool res = false;
            var circle = ChCircle.Create(arc.CenterPoint, arc.Radius);

            if (IntersectionLineSegmentWithCircle(lineSegment, circle, pts))
            {
                var intersection1 = pts.FirstOrDefault();
                var intersection2 = pts.LastOrDefault();
                return ChecksPointsInArcLimitsWithLineSegment(lineSegment, arc, intersection1, intersection2, pts);
            }
            return res;
        }

        /// <summary>
        /// Intersections the line segment with circle.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <param name="circle">The circle.</param>
        /// <param name="pts">The PTS.</param>
        /// <returns>>True whether two segment intersect</returns>
        public static bool IntersectionLineSegmentWithCircle(IChLineSegment lineSegment, IChCircle circle, List<IChPoint> pts)
        {
            var points = GetCircleLineIntersectionPoint(lineSegment, circle);
            foreach (var p in points)
            {
                if (circle.IsOn(p) && lineSegment.IsOn(p))
                    pts.Add(p);
            }
            return pts.Count > 0;
        }

        #endregion Public Methods
    }
}