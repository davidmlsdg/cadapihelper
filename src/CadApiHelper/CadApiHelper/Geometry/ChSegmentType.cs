﻿namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Segment type
    /// </summary>
    public enum ChSegmentType
    {
        /// <summary>
        /// The arc
        /// </summary>
        Arc,

        /// <summary>
        /// The line
        /// </summary>
        Line,

        /// <summary>
        /// The circle
        /// </summary>
        Circle,
    }
}