﻿namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChPoint interface
    /// </summary>
    public interface IChPoint
    {
        #region Properties

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        object Tag { get; set; }

        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        double X { get; set; }

        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        double Y { get; set; }

        /// <summary>
        /// Gets or sets the z.
        /// </summary>
        /// <value>
        /// The z.
        /// </value>
        double Z { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Angle 2d.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Angle 2d</returns>
        double Angle2D(IChPoint point);

        /// <summary>
        /// Angles from x axis.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Angles from x axis.</returns>
        double AngleFromXAxis(IChPoint point);

        /// <summary>
        /// The array [x,y,z].
        /// </summary>
        /// <returns>The array [x,y,z].
        /// </returns>
        double[] AsArray();

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>True whether two points are equal</returns>
        int CompareTo(IChPoint other);

        /// <summary>
        /// Distances the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Distance between two points</returns>
        double Distance(IChPoint point);

        /// <summary>
        /// Distance between two points in 2d.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Distance between two points in 2d.</returns>
        double Distance2D(IChPoint point);

        /// <summary>
        /// Distance to line segment in 2d.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <returns>Distance to line segment in 2d.</returns>
        double DistanceToLineSegment2D(IChLineSegment lineSegment);

        /// <summary>
        /// Divides the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Point</returns>
        IChPoint Divide(IChPoint point);

        /// <summary>
        /// Verify whether two points are equal.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>True whether points are equal</returns>
        bool Equals(IChPoint point);

        /// <summary>
        /// Verify whether two points are equal using tolerance.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <returns>True whether points are equal</returns>
        bool Equals(IChPoint point, double tolerance);

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        bool Equals(object obj);

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        int GetHashCode();

        /// <summary>
        /// Greater the than.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>True whether point is greater than point parameter</returns>
        bool GreaterThan(IChPoint point);

        /// <summary>
        /// Lesses the than.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>True whether point is less than point parameter</returns>
        bool LessThan(IChPoint point);

        /// <summary>
        /// Determines the maximum of the parameters.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>The max point applying the greatest value of x,y,z</returns>
        IChPoint Max(IChPoint point);

        /// <summary>
        /// Mid the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>The mid point between actual point and point parameter</returns>
        IChPoint Mid(IChPoint point);

        /// <summary>
        /// Determines the minimum of the parameters.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>The max point applying the greatest value of x,y,z</returns>
        IChPoint Min(IChPoint point);

        /// <summary>
        /// Multiplies the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Point created by multiplying x,y,z</returns>
        IChPoint Multiply(IChPoint point);

        /// <summary>
        /// Polars the point.
        /// </summary>
        /// <param name="angle">The angle.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>New point by distance and angle</returns>
        IChPoint PolarPoint(double angle, double distance);

        /// <summary>
        /// Retangular the point.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <returns>Point sum coordinate x and y</returns>
        IChPoint RetangularPoint(double x, double y);

        /// <summary>
        /// Retangular the point.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        /// <returns>>Point sum coordinate x, y and z</returns>
        IChPoint RetangularPoint(double x, double y, double z);

        /// <summary>
        /// Spherical the point.
        /// </summary>
        /// <param name="angleXY">The angle xy.</param>
        /// <param name="angleXZ">The angle xz.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>Spherical the point.</returns>
        IChPoint SphericalPoint(double angleXY, double angleXZ, double distance);

        /// <summary>
        /// Subtracts the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>The subtract point</returns>
        IChPoint Subtract(IChPoint point);

        /// <summary>
        /// Sums the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>The sum point</returns>
        IChPoint Sum(IChPoint point);

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        string ToString();

        /// <summary>
        /// Translacao2ds the specified delta x.
        /// </summary>
        /// <param name="deltaX">The delta x.</param>
        /// <param name="deltaY">The delta y.</param>
        void Translacao2d(double deltaX, double deltaY);

        #endregion Public Methods
    }
}