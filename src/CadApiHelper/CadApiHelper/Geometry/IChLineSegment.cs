﻿#if ZWCAD
using ZwSoft.ZwCAD.Geometry;
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
using Teigha.Geometry;
using Teigha.DatabaseServices;
#elif ACAD
using Autodesk.AutoCAD.Geometry;
using Autodesk.AutoCAD.DatabaseServices;
#elif GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#endif


namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChLineSegment instance
    /// </summary>
    /// <seealso cref="CadApiHelper.Geometry.IChBaseLine" />
    /// <seealso cref="CadApiHelper.Geometry.IChPolylineSegment" />
    public interface IChLineSegment : IChBaseLine, IChPolylineSegment
    {
        #region Properties

        /// <summary>
        /// Gets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        double Angle { get; }


        /// <summary>
        /// Gets a value indicating whether this instance is quadrant one or four.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is quadrant one or four; otherwise, <c>false</c>.
        /// </value>
        bool IsQuadrantOneOrFour { get; }

        /// <summary>
        /// Gets the mid point.
        /// </summary>
        /// <value>
        /// The mid point.
        /// </value>
        IChPoint MidPoint { get; }


        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Changes the line quadrant one or four.
        /// </summary>
        void ChangeLineQuadrantOneOrFour();

        /// <summary>
        /// Changes the orientation.
        /// </summary>
        void ChangeOrientation();

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>ILineSegment clone</returns>
        IChLineSegment Clone();

        /// <summary>
        /// Distances to point2d.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Distance</returns>
        double DistanceToPoint2d(IChPoint point);

        /// <summary>
        /// Checks whether two ILineSegment are equal.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>True whether ILineSegment are equal</returns>
        bool Equals(IChLineSegment other);

        /// <summary>
        /// Gets the line quadrant one or four.
        /// </summary>
        /// <returns>Returns the ILineSegment orientation is quadrant one or four</returns>
        IChLineSegment GetLineQuadrantOneOrFour();

        /// <summary>
        /// Smaller the distance to line segment2d.
        /// </summary>
        /// <param name="segLine">The line.</param>
        /// <returns>Smaller the distance</returns>
        double SmallerDistanceToLineSegment2d(IChLineSegment segLine);

        /// <summary>
        /// Retorna o Vetor Diretor do segmento
        /// </summary>
        IChVector VectorDiretor { get; }

        /// <summary>
        /// Gets the line3d.
        /// </summary>
        /// <returns>Line3d</returns>
        Line3d GetLine3d();

        #endregion Public Methods
    }
}