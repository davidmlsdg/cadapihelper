﻿using CadApiHelper.Core;
using CadApiHelper.Entities.Edit;

using System;
using System.Collections.Generic;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// This class represent Circle
    /// </summary>
    [Serializable]
    public class ChCircle : IChCircle, IEquatable<IChSegment>, IEquatable<IChCircle>
    {
        #region Fields

        private IChPoint _center = new ChPoint();

        [NonSerialized]
        private bool _isValid = true;

        private double _radius = 0;

        [NonSerialized]
        private object tag;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Area of Circle
        /// </summary>
        public double Area
        {
            get
            {
                return ChAngles.PI * Math.Pow(Radius, 2);
            }
        }

        /// <summary>
        /// Center Point
        /// </summary>
        public IChPoint Center
        {
            get { return _center; }
            set { _center = value; }
        }

        /// <summary>
        /// Diameter
        /// </summary>
        public double Diameter
        {
            get
            {
                return Radius * 2;
            }
        }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid { get => _isValid; set => _isValid = value; }

        /// <summary>
        /// Length of circle
        /// </summary>
        public double Length
        {
            get
            {
                return Perimeter;
            }
        }

        /// <summary>
        /// Perimeter
        /// </summary>
        public double Perimeter
        {
            get
            {
                return Radius * ChAngles.PI * 2;
            }
        }

        /// <summary>
        /// Radius of circle
        /// </summary>
        public double Radius
        {
            get
            {
                return _radius;
            }
            set
            {
                _radius = value;
            }
        }

        /// <summary>
        /// Segment Type
        /// </summary>
        public ChSegmentType SegmentType => ChSegmentType.Circle;

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Create an instance of Circle by ObjectId
        /// </summary>
        /// <param name="id">ObjectId</param>
        public ChCircle(ChObjectId id)
        {
            var circ = new ChEditCircle(id);
            using (circ)
            {
                if (circ.DbObject == null)
                {
                    IsValid = false;
                    return;
                }

                _center = new ChPoint(circ.DbObject.Center.X, circ.DbObject.Center.Y, circ.DbObject.Center.Z);
                Radius = circ.DbObject.Radius;
            }
        }

        /// <summary>
        /// Create an instance of Circle
        /// </summary>
        /// <param name="center">Center Point</param>
        /// <param name="radius">Radius</param>
        public ChCircle(IChPoint center, double radius)
        {
            Center = center;
            Radius = radius;
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Create an instance of the ICircle
        /// </summary>
        /// <param name="center">Center point</param>
        /// <param name="radius">Radius</param>
        /// <returns>ICircle</returns>
        public static IChCircle Create(IChPoint center, double radius)
        {
            return new ChCircle(center, radius);
        }

        /// <summary>
        /// Copy of Segment
        /// </summary>
        /// <returns>Segment copy</returns>
        public IChSegment CopySegment()
        {
            return new ChCircle(Center, Radius);
        }

        /// <summary>
        /// Distance of center circle
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns>Distance of center circle</returns>
        public double DistanceOfCenter(IChPoint point)
        {
            return Center.Distance(point);
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            return Equals(obj as IChCircle);
        }

        /// <summary>
        /// Check if two segments are equal
        /// </summary>
        /// <param name="other">Other segment</param>
        /// <returns>True if two segments are equals otherwise false</returns>
        public bool Equals(IChSegment other)
        {
            if (other == null || other.SegmentType != ChSegmentType.Circle)
                return false;

            return Equals((IChCircle)other);
        }

        /// <summary>
        /// Checks whether icircle are equal.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns></returns>
        public bool Equals(IChCircle other)
        {
            if (other == null)
                return false;

            var cir = other as ChCircle;
            return cir.Center.Equals(Center) && cir.Radius.Equals(Radius);
        }

        /// <summary>
        /// Check if this instance Intersect With another ISegment
        /// </summary>
        /// <param name="segment">Segment</param>
        /// <returns>True if point is on the circle</returns>
        public bool IntersectWith(IChSegment segment)
        {
            var pts = new List<IChPoint>();
            return IntersectWith(segment, pts);
        }

        /// <summary>
        /// Check if this instance Intersect With another ISegment
        /// </summary>
        /// <param name="segment">Segment</param>
        /// <param name="points">Intersect points</param>
        /// <returns>True if point is on the circle</returns>
        public bool IntersectWith(IChSegment segment, List<IChPoint> points)
        {
            if (segment == null)
                return false;

            switch (segment.SegmentType)
            {
                case ChSegmentType.Arc:
                    var arc = segment as IChArc;
                    if (arc == null)
                        return false;

                    return ChIntersectionHelper.IntersectionCircletWithArc(this, arc, points);

                case ChSegmentType.Line:
                    var lin = segment as ChLineSegment;
                    if (lin == null)
                        return false;
                    return ChIntersectionHelper.IntersectionLineSegmentWithCircle(lin, this, points);

                case ChSegmentType.Circle:
                    var circle = segment as ChCircle;
                    if (circle == null)
                        return false;
                    return ChIntersectionHelper.IntersectionCircletWithCircle(this, circle, points);

                default:
                    return false;
            }
        }

        /// <summary>
        /// Check if point is inside
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns>True if point is on the circle</returns>
        public bool IsInside(IChPoint point)
        {
            var dist = Math.Abs(Center.Distance(point));
            return dist <= Radius;
        }

        /// <summary>
        /// Check if point is on
        /// </summary>
        /// <param name="point">Point</param>
        /// <returns>True if point is on the circle</returns>
        public bool IsOn(IChPoint point)
        {
            return IsOn(point, ChPoint.Tolerance);
        }

        /// <summary>
        /// Check if point is on
        /// </summary>
        /// <param name="point">Point</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns></returns>
        public bool IsOn(IChPoint point, double tolerance)
        {
            var dist = Center.Distance(point);
            var diff = Math.Abs(dist - Radius);
            return diff <= tolerance;
        }

        /// <summary>
        /// Translation 2d
        /// </summary>
        /// <param name="deltaX">Delta x</param>
        /// <param name="deltaY">Delta Y</param>
        public void Translation2d(double deltaX, double deltaY)
        {
            Center.Translacao2d(deltaX, deltaY);
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hashCode = 346259955;
            hashCode = hashCode * -1521134295 + EqualityComparer<IChPoint>.Default.GetHashCode(_center);
            hashCode = hashCode * -1521134295 + _isValid.GetHashCode();
            hashCode = hashCode * -1521134295 + _radius.GetHashCode();
            return hashCode;
        }

        #endregion Public Methods
    }
}