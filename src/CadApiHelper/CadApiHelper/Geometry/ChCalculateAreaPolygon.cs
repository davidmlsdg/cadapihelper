﻿using System;
using System.Collections.Generic;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Calculate polygon area
    /// </summary>
    public static class ChCalculateAreaPolygon
    {
        /// <summary>
        /// Calculates the area of polygons.
        /// </summary>
        /// <param name="pontos">The pontos.</param>
        /// <returns>Area</returns>
        public static double CalculateArea(List<IChPoint> pontos)
        {
            double sumX0Y1 = 0;
            double sumX1Y0 = 0;
            var vertices = new List<IChPoint>();
            vertices.AddRange(pontos);
            for (int idxPt = vertices.Count - 1; idxPt >= 0; idxPt--)
                for (int i = 0; i < idxPt; i++)
                    if (vertices[idxPt].X == vertices[i].X && vertices[idxPt].Y == vertices[i].Y)
                    {
                        vertices.RemoveAt(idxPt);
                        break;
                    }
            for (int idxPoint = 0; idxPoint < vertices.Count; idxPoint++)
            {
                sumX0Y1 += vertices[idxPoint].X * vertices[(idxPoint + 1) % vertices.Count].Y;
                sumX1Y0 += vertices[(idxPoint + 1) % vertices.Count].X * vertices[idxPoint].Y;
            }

            return Math.Abs((sumX0Y1 - sumX1Y0) / 2.0);
        }
    }
}
