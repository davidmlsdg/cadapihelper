﻿namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChVector instance
    /// </summary>
    public interface IChVector
    {
        #region Properties

        /// <summary>
        /// Gets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        double Angle { get; }

        /// <summary>
        /// Gets the inverse angle.
        /// </summary>
        /// <value>
        /// The inverse angle.
        /// </value>
        double InverseAngle { get; }

        /// <summary>
        /// Gets the modulo.
        /// </summary>
        /// <value>
        /// The modulo.
        /// </value>
        double Modulo { get; }

        /// <summary>
        /// Gets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        double X { get; }

        /// <summary>
        /// Gets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        double Y { get; }

        /// <summary>
        /// Gets the z.
        /// </summary>
        /// <value>
        /// The z.
        /// </value>
        double Z { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Angles the between.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>Angle between two vectors</returns>
        double AngleBetween(IChVector vector);

        /// <summary>
        /// Divides the specified right.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator /.</returns>
        IChVector Divide(double right);

        /// <summary>
        /// Normalizes this instance.
        /// </summary>
        /// <returns>Normalize vector</returns>
        IChVector Normalize();

        /// <summary>
        /// Scalars the product.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>Scalar product</returns>
        double ScalarProduct(IChVector vector);

        /// <summary>
        /// Subtracts the specified vector.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>The result of the subtract operator.</returns>
        IChVector Subtract(IChVector vector);

        /// <summary>
        /// Sums the specified vector.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>The result of the operator +.</returns>
        IChVector Sum(IChVector vector);

        /// <summary>
        /// Vectors the product.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>Vector product></returns>
        IChVector VectorProduct(IChVector vector);

        #endregion Public Methods
    }
}