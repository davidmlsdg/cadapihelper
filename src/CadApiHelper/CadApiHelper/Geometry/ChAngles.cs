﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Manipulate and convert Angles Returns most used angles in radians Constants for
    /// use in calculating angles
    /// </summary>
    public class ChAngles
    {
        #region Properties

        /// <summary>
        /// Returns PI value in 14 decimal places
        /// </summary>
        public static double PI
        { get { return 3.141592653589793; } }

        /// <summary>
        /// Returns the 0 degree angle in radians
        /// </summary>
        public static double RAD0
        { get { return 0; } }

        /// <summary>
        /// Returns 135 degree angle in radians
        /// </summary>
        public static double RAD135
        { get { return 3 * PI / 4; } }

        /// <summary>
        /// Returns 180 degree angle in radians
        /// </summary>
        public static double RAD180
        { get { return PI; } }

        /// <summary>
        /// Returns 225 degree angle in radians
        /// </summary>
        public static double RAD225
        { get { return 5 * PI / 4; } }

        /// <summary>
        /// Returns 270 degree angle in radians
        /// </summary>
        public static double RAD270
        { get { return 3 * PI / 2; } }

        /// <summary>
        /// Returns the angle of 315 degrees in radians
        /// </summary>
        public static double RAD315
        { get { return 7 * PI / 4; } }

        /// <summary>
        /// Return 360 degree angle in radians
        /// </summary>
        public static double RAD360
        { get { return 2 * PI; } }

        /// <summary>
        /// Returns the 45 degree angle in radians
        /// </summary>
        public static double RAD45
        { get { return PI / 4; } }

        /// <summary>
        /// Returns 90 degree angle in radians
        /// </summary>
        public static double RAD90
        { get { return PI / 2; } }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Degrees for formatted text
        /// </summary>
        /// <param name="degrees">Degrees</param>
        /// <param name="formato">Format</param>
        /// <returns>Formatted text</returns>
        public static string DegreesToString(double degrees, ChStringDegreesFormat formato = ChStringDegreesFormat.DegreesMinutesSeconds)
        {
            var Graus = (int)Math.Floor(degrees);
            var delta = degrees - Graus;
            var seconds = (int)Math.Floor(3600.0 * delta);
            var Segundos = seconds % 60;
            var Minutos = (int)Math.Floor(seconds / 60.0);
            switch (formato)
            {
                case ChStringDegreesFormat.Degrees:
                    return string.Format("{0}°", Graus);

                case ChStringDegreesFormat.DegreesMinutes:
                    return string.Format("{0}° {1:00}'", Graus, Minutos);

                case ChStringDegreesFormat.DegreesMinutesSeconds:
                default:
                    return string.Format("{0}° {1:00}' {2:00}\"", Graus, Minutos, Segundos);
            }
        }

        /// <summary>
        /// Normalizes the angle to the reading direction from 0º to 90º and from 270º to 360º
        /// </summary>
        /// <param name="angle">Angle</param>
        /// <returns>Angle normalized</returns>
        public static double Normalize(double angle)
        {
            while (angle >= ChAngles.RAD360)
                angle -= ChAngles.RAD360;

            if (angle < ChAngles.RAD0)
                angle += ChAngles.RAD360;

            if (angle > ChAngles.RAD90 && angle <= ChAngles.RAD270)
                return angle >= RAD180 ? angle - RAD180 : angle + RAD180;

            return angle;
        }

        /// <summary>
        /// Convert angle in radians to degrees
        /// </summary>
        /// <param name="radiansAngle">Angle in radians</param>
        /// <returns>Angle in degrees</returns>
        public static double ToDegrees(double radiansAngle)
        {
            return radiansAngle * 180 / PI;
        }

        /// <summary>
        /// Convert angle in degrees to radians
        /// </summary>
        /// <param name="degreesAngle">Angle in degrees</param>
        /// <returns>Angle in radians</returns>
        public static double ToRad(double degreesAngle)
        {
            return degreesAngle * PI / 180;
        }

        #endregion Public Methods
    }
}