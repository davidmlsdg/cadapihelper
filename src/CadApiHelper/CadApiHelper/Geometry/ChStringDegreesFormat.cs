﻿namespace CadApiHelper.Geometry
{
    /// <summary>
    /// String Degrees Format
    /// </summary>
    public enum ChStringDegreesFormat
    {
        /// <summary>
        /// The degrees
        /// </summary>
        Degrees,

        /// <summary>
        /// The degrees and minutes
        /// </summary>
        DegreesMinutes,

        /// <summary>
        /// The degrees, minutes and seconds
        /// </summary>
        DegreesMinutesSeconds,
    }
}