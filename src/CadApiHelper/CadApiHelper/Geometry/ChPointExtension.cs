﻿#if ZWCAD

using ZwSoft.ZwCAD.Geometry;

#endif

#if BRCAD
using Teigha.Geometry;
using Teigha.DatabaseServices;
using acApp = Bricscad.ApplicationServices;
using Bricscad.EditorInput;
#endif

#if ACAD
using Autodesk.AutoCAD.Geometry;
#endif

#if GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Geometry;
#endif

using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Point Extension
    /// </summary>
    public static class ChPointExtension
    {
        #region Public Methods

        /// <summary>
        /// Angles from x axis by two points.
        /// </summary>
        /// <param name="pta">The point a.</param>
        /// <param name="point">The point.</param>
        /// <returns></returns>
        public static double AngleFromXAxis(this Point2d pta, Point2d point)
        {
            return pta.GetVectorTo(point).Angle;
        }

        /// <summary>
        /// Determines the maximum of the parameters.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <returns>Max point</returns>
        public static Point3d Max(this Point3d point1, Point3d point2)
        {
            return new Point3d(Math.Max(point1.X, point2.X), Math.Max(point1.Y, point2.Y), Math.Max(point1.Z, point2.Z));
        }

        /// <summary>
        /// Determines the maximum of the parameters.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <returns>Max Point</returns>
        public static Point2d Max(this Point2d point1, Point2d point2)
        {
            return new Point2d(Math.Max(point1.X, point2.X), Math.Max(point1.Y, point2.Y));
        }

        /// <summary>
        /// Mids the specified point2.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <returns>The mid point between two points</returns>
        public static Point3d Mid(this Point3d point1, Point3d point2)
        {
            double x = (point1.X + point2.X) / 2;
            double y = (point1.Y + point2.Y) / 2;
            double z = (point1.Z + point2.Z);
            if (z > 0)
                z /= 2;

            return new Point3d(x, y, z);
        }

        /// <summary>
        /// Mids the specified point2.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <returns>The mid point between two point</returns>
        public static Point2d Mid(this Point2d point1, Point2d point2)
        {
            double x = (point1.X + point2.X) / 2;
            double y = (point1.Y + point2.Y) / 2;
            return new Point2d(x, y);
        }

        /// <summary>
        /// Determines the minimum of the parameters.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <returns>The min point</returns>
        public static Point3d Min(this Point3d point1, Point3d point2)
        {
            return new Point3d(Math.Min(point1.X, point2.X), Math.Min(point1.Y, point2.Y), Math.Min(point1.Z, point2.Z));
        }

        /// <summary>
        /// Determines the minimum of the parameters.
        /// </summary>
        /// <param name="point1">The point1.</param>
        /// <param name="point2">The point2.</param>
        /// <returns>The min point</returns>
        public static Point2d Min(this Point2d point1, Point2d point2)
        {
            return new Point2d(Math.Min(point1.X, point2.X), Math.Min(point1.Y, point2.Y));
        }

        /// <summary>
        /// Polars the point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="angle">The angle.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>The polar point</returns>
        public static Point3d PolarPoint(this Point3d point, double angle, double distance)
        {
            return new Point3d(point.X + distance * Math.Cos(angle),
                     point.Y + distance * Math.Sin(angle), point.Z);
        }

        /// <summary>
        /// Polars the point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="angle">The angle.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>The polar point</returns>
        public static Point2d PolarPoint(this Point2d point, double angle, double distance)
        {
            return new Point2d(point.X + distance * Math.Cos(angle),
                     point.Y + distance * Math.Sin(angle));
        }

        /// <summary>
        /// Converts to 2d point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Point</returns>
        public static Point2d To2dPoint(this IChPoint point)
        {
            return new Point2d(point.X, point.Y);
        }

        /// <summary>
        /// Converts to 3dpoint.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Point</returns>
        public static Point3d To3dPoint(this IChPoint point)
        {
            return new Point3d(point.X, point.Y, point.Z);
        }

        /// <summary>
        /// Converts to point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>Point</returns>
        public static IChPoint ToPoint(this Point2d point)
        {
            return new ChPoint(point.X, point.Y);
        }

        /// <summary>
        /// Converts to point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>point</returns>
        public static IChPoint ToPoint(this Point3d point)
        {
            return new ChPoint(point.X, point.Y, point.Z);
        }

        #endregion Public Methods
    }
}