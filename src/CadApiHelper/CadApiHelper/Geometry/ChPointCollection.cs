﻿#if ZWCAD
using ZwSoft.ZwCAD.Geometry;
#elif BRCAD
using Teigha.Geometry;
using Teigha.DatabaseServices;
using acApp = Bricscad.ApplicationServices;
using Bricscad.EditorInput;
#elif ACAD
using Autodesk.AutoCAD.Geometry;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Geometry;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// ChPointCollection
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List&lt;CadApiHelper.Geometry.IChPoint&gt;" />
    public class ChPointCollection : List<IChPoint>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        public ChPointCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        /// <param name="capacity">The number of elements that the new list can initially store.</param>
        public ChPointCollection(int capacity) : base(capacity)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        /// <param name="collection">The collection whose elements are copied to the new list.</param>
        public ChPointCollection(IEnumerable<IChPoint> collection) : base(collection)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        public ChPointCollection(List<Point2d> points)
        {
            foreach (Point2d p in points)
                Add(ChPoint.Create(p));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        public ChPointCollection(List<Point3d> points)
        {
            foreach (Point3d p in points)
                Add(ChPoint.Create(p));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        public ChPointCollection(Point2dCollection points)
        {
            foreach (Point2d p in points)
                Add(ChPoint.Create(p));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPointCollection"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        public ChPointCollection(Point3dCollection points)
        {
            foreach (Point2d p in points)
                Add(ChPoint.Create(p));
        }

        /// <summary>
        /// Creates the ChPointCollection.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns>ChPointCollection</returns>
        public static ChPointCollection Create(List<Point2d> points)
        {
            return new ChPointCollection(points);
        }

        /// <summary>
        /// Creates the ChPointCollection.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns>ChPointCollection</returns>
        public static ChPointCollection Create(List<Point3d> points)
        {
            return new ChPointCollection(points);
        }

        /// <summary>
        /// Creates the ChPointCollection.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns>ChPointCollection</returns>
        public static ChPointCollection Create(Point2dCollection points)
        {
            return new ChPointCollection(points);
        }


        /// <summary>
        /// Creates the ChPointCollection.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <returns>ChPointCollection</returns>
        public static ChPointCollection Create(Point3dCollection points)
        {
            return new ChPointCollection(points);
        }
        /// <summary>
        /// Gets the point2d collection.
        /// </summary>
        /// <returns>Point2dCollection</returns>
        public Point2dCollection GetPoint2dCollection()
        {
            Point2dCollection lista = new Point2dCollection();
            foreach (IChPoint item in this)
                lista.Add(item.To2dPoint());

            return lista;
        }

        /// <summary>
        /// Gets the point3d collection.
        /// </summary>
        /// <returns>Point3dCollection</returns>
        public Point3dCollection GetPoint3dCollection()
        {
            Point3dCollection lista = new Point3dCollection();
            foreach (IChPoint item in this)
                lista.Add(item.To3dPoint());

            return lista;
        }

    }
}
