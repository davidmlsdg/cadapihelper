﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChPolylineSegment instance
    /// </summary>
    /// <seealso cref="CadApiHelper.Geometry.IChSegment" />
    /// <seealso cref="System.IEquatable&lt;CadApiHelper.Geometry.IChPolylineSegment&gt;" />
    public interface IChPolylineSegment : IChSegment, IEquatable<IChPolylineSegment>
    {
        /// <summary>
        /// Gets or sets the start point.
        /// </summary>
        /// <value>
        /// The start point.
        /// </value>
        IChPoint StartPoint { get; set; }

        /// <summary>
        /// Gets or sets the end point.
        /// </summary>
        /// <value>
        /// The end point.
        /// </value>
        IChPoint EndPoint { get; set; }

        /// <summary>
        /// Gets the bulge.
        /// </summary>
        /// <value>
        /// The bulge.
        /// </value>
        double Bulge { get; }

        /// <summary>
        /// Gets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        double Angle { get; }
    }
}