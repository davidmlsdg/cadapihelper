﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// ChVector instance
    /// </summary>
    [Serializable]
    public class ChVector : IChVector
    {
        #region Fields

        private double _x;
        private double _y;
        private double _z;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the angle.
        /// </summary>
        /// <value>
        /// The angle.
        /// </value>
        public double Angle
        {
            get
            {
                if (Modulo < 0.0001)
                    return 0.0;
                var ang = Math.Acos(_x / Modulo);
                if (_y < 0)
                    ang = (2 * Math.PI) - ang;
                return ang;
            }
        }

        /// <summary>
        /// Gets the inverse angle.
        /// </summary>
        /// <value>
        /// The inverse angle.
        /// </value>
        public double InverseAngle
        {
            get
            {
                if (Modulo < 0.0001)
                    return 0.0;
                var ang = Math.Acos(-_x / Modulo);
                if (-_y < 0)
                    ang = (2 * Math.PI) - ang;
                return ang;
            }
        }

        /// <summary>
        /// Gets the modulo.
        /// </summary>
        /// <value>
        /// The modulo.
        /// </value>
        public double Modulo
        { get { return Math.Sqrt(this.ScalarProduct(this)); } }

        /// <summary>
        /// Gets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public double X
        { get { return _x; } }

        /// <summary>
        /// Gets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public double Y
        { get { return _y; } }

        /// <summary>
        /// Gets the z.
        /// </summary>
        /// <value>
        /// The z.
        /// </value>
        public double Z
        { get { return _z; } }

        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        public ChVector()
        { }

        /// <summary>
        /// Creates this instance.
        /// </summary>
        /// <returns>IVector</returns>
        public static IChVector Create()
        {
            return new ChVector();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        public ChVector(double x, double y, double z)
        {
            StartupVector(x, y, z);
        }

        /// <summary>
        /// Creates the specified x.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <param name="z">The z.</param>
        /// <returns>IVector</returns>
        public static IChVector Create(double x, double y, double z)
        {
            return new ChVector(x, y, z);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        public ChVector(IChPoint startPoint, IChPoint endPoint)
        {
            _x = endPoint.X - startPoint.X;
            _y = endPoint.Y - startPoint.Y;
            _z = 0;
        }

        /// <summary>
        /// Creates the specified start point.
        /// </summary>
        /// <param name="startPoint">The start point.</param>
        /// <param name="endPoint">The end point.</param>
        /// <returns>IVector</returns>
        public static IChVector Create(IChPoint startPoint, IChPoint endPoint)
        {
            return new ChVector(startPoint, endPoint);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public ChVector(double x, double y)
        {
            StartupVector(x, y, 0);
        }

        /// <summary>
        /// Creates the specified x.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>IVector</returns>
        public static IChVector Create(double x, double y)
        {
            return new ChVector(x, y);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        /// <param name="array">The array [x,y,z].</param>
        public ChVector(double[] array)
        {
            if (array == null)
                return;
            if (array.Length == 2)
                StartupVector(array[0], array[1], 0);
            if (array.Length >= 3)
                StartupVector(array[0], array[1], array[2]);
        }

        /// <summary>
        /// Creates the specified array.
        /// </summary>
        /// <param name="array">The array.</param>
        /// <returns>IVector</returns>
        public static IChVector Create(double[] array)
        {
            return new ChVector(array);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        /// <param name="angle">The angle.</param>
        public ChVector(double angle)
        {
            _x = Math.Cos(angle);
            _y = Math.Sin(angle);
            _z = 0;
        }

        /// <summary>
        /// Creates the specified angle.
        /// </summary>
        /// <param name="angle">The angle.</param>
        /// <returns>IVector</returns>
        public static IChVector Create(double angle)
        {
            return new ChVector(angle);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChVector"/> class.
        /// </summary>
        /// <param name="point">The point.</param>
        public ChVector(IChPoint point)
        {
            _x = point.X;
            _y = point.Y;
            _z = 0;
        }

        /// <summary>
        /// Creates the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>IVector</returns>
        public static IChVector Create(IChPoint point)
        {
            return new ChVector();
        }

        #endregion Public Constructors

        #region Private Methods

        private void StartupVector(double x, double y, double z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static ChVector operator -(ChVector left, ChVector right)
        {
            return new ChVector(left.X - right.X, left.Y - right.Y, left.Z - right.Z);
        }

        /// <summary>
        /// Subtracts the specified vector.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>The result of the subtract operator.</returns>
        public IChVector Subtract(IChVector vector)
        {
            return new ChVector(X - vector.X, Y - vector.Y, Z - vector.Z);
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static ChVector operator *(double left, ChVector right)
        {
            return new ChVector(left * right.X, left * right.Y, left * right.Z);
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator. *</returns>
        public static IChVector Multiply(double left, IChVector right)
        {
            return new ChVector(left * right.X, left * right.Y, left * right.Z);
        }

        /// <summary>
        /// Implements the operator /.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static ChVector operator /(ChVector left, double right)
        {
            return new ChVector(left.X / right, left.Y / right, left.Z / right);
        }

        /// <summary>
        /// Divides the specified right.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the operator /.</returns>
        public IChVector Divide(double right)
        {
            return new ChVector(X / right, Y / right, Z / right);
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static ChVector operator +(ChVector left, ChVector right)
        {
            return new ChVector(left.X + right.X, left.Y + right.Y, left.Z + right.Z);
        }

        /// <summary>
        /// Sums the specified vector.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>The result of the operator +.</returns>
        public IChVector Sum(IChVector vector)
        {
            return new ChVector(X + vector.X, Y + vector.Y, Z + vector.Z);
        }

        /// <summary>
        /// Angles the between.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>Angle between two vectors</returns>
        public double AngleBetween(IChVector vector)
        {
            double cos = this.ScalarProduct(vector) / (Modulo * vector.Modulo);
            if (cos > 1.0)
                cos = 1.0;
            else if (cos < -1.0)
                cos = -1.0;
            return Math.Acos(cos);
        }

        /// <summary>
        /// Normalizes this instance.
        /// </summary>
        /// <returns>Normalize vector</returns>
        public IChVector Normalize()
        {
            if (this.Modulo < 0.0001)
                return ChVector.Create();
            return this / this.Modulo;
        }

        /// <summary>
        /// Scalars the product.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>Scalar product</returns>
        public double ScalarProduct(IChVector vector) //ScalarProduct
        {
            return (_x * vector.X) + (_y * vector.Y) + (_z * vector.Z);
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("X = {0}; Y = {1} [Mod = {2}; Âng = {3}]", Math.Round(_x, 3), Math.Round(_y, 3)
                , Math.Round(Modulo, 3), Math.Round(Angle, 5));
        }

        /// <summary>
        /// Vectors the product.
        /// </summary>
        /// <param name="vector">The vector.</param>
        /// <returns>Vector product></returns>
        public IChVector VectorProduct(IChVector vector) //
        {
            return ChVector.Create((_y * vector.Z) - (_z * vector.Y), -(_x * vector.Z) + (_z * vector.X), (_x * vector.Y) - (_y * vector.X));
        }

        #endregion Public Methods
    }
}