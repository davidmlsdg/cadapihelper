﻿#if ZWCAD
using ZwSoft.ZwCAD.Geometry;
#elif BRCAD
using Teigha.Geometry;
using Teigha.DatabaseServices;
using acApp = Bricscad.ApplicationServices;
using Bricscad.EditorInput;
#elif ACAD
using Autodesk.AutoCAD.Geometry;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
using GrxCAD.Geometry;
#endif

using System;
using System.Collections.Generic;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// The Point class
    /// </summary>
    /// <seealso cref="CadApiHelper.Geometry.IChPoint" />
    /// <seealso cref="System.IEquatable&lt;CadApiHelper.Geometry.IChPoint&gt;" />
    /// <seealso cref="System.Collections.Generic.IEqualityComparer&lt;CadApiHelper.Geometry.IChPoint&gt;" />
    /// <seealso cref="System.IComparable&lt;CadApiHelper.Geometry.IChPoint&gt;" />
    [Serializable]
    public class ChPoint : IChPoint, IEquatable<IChPoint>, IEqualityComparer<IChPoint>, IComparable<IChPoint>
    {
        #region Fields

        private double _x;
        private double _y;
        private double _z;

        [NonSerialized]
        private object tag;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the origin point.
        /// </summary>
        /// <value>
        /// The origin point (0,0,0)
        /// </value>
        public static IChPoint Origin
        {
            get { return new ChPoint(); }
        }

        /// <summary>
        /// Global tolerance for compare point
        /// </summary>
        public static double Tolerance
        { get { return 0.0001; } }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        /// <summary>
        /// Gets or sets the x.
        /// </summary>
        /// <value>
        /// The x.
        /// </value>
        public double X
        {
            get { return _x; }
            set { _x = value; }
        }

        /// <summary>
        /// Gets or sets the y.
        /// </summary>
        /// <value>
        /// The y.
        /// </value>
        public double Y
        {
            get { return _y; }
            set { _y = value; }
        }

        /// <summary>
        /// Gets or sets the z.
        /// </summary>
        /// <value>
        /// The z.
        /// </value>
        public double Z
        {
            get { return _z; }
            set { _z = value; }
        }

        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPoint"/> class.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        public ChPoint(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
            tag = null;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPoint"/> class.
        /// </summary>
        public ChPoint()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPoint"/> class.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        public ChPoint(double x, double y) : this(x, y, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPoint"/> class.
        /// </summary>
        /// <param name="point">The point.</param>
        public ChPoint(Point2d point) : this(point.X, point.Y, 0)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPoint"/> class.
        /// </summary>
        /// <param name="point">The point.</param>
        public ChPoint(Point3d point) : this(point.X, point.Y, point.Z)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPoint"/> class.
        /// </summary>
        /// <param name="point">The point.</param>
        public ChPoint(IChPoint point) : this(point.X, point.Y, point.Z)
        {
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Create an instance of IPoint
        /// </summary>
        /// <param name="x">X Coordinate</param>
        /// <param name="y">Y Coordinate</param>
        /// <param name="z">Z Coordinate</param>
        /// <returns>IPont instance</returns>
        public static IChPoint Create(double x = 0, double y = 0, double z = 0)
        {
            return new ChPoint(x, y, z);
        }

        /// <summary>
        /// Creates the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>IPont instance</returns>
        public static IChPoint Create(Point3d point)
        {
            return new ChPoint(point.X, point.Y, point.Z);        
        }

        /// <summary>
        /// Creates the specified point.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>IPont instance</returns>
        public static IChPoint Create(Point2d point)
        {
            return new ChPoint(point.X, point.Y);
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="ChPoint"/> to <see cref="Point2d"/>.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Point2d(ChPoint point)
        {
            return new Point2d(point.X, point.Y);
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="ChPoint"/> to <see cref="Point3d"/>.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator Point3d(ChPoint point)
        {
            return new Point3d(point.AsArray());
        }

        /// <summary>
        /// Implements the operator -.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static IChPoint operator -(ChPoint left, ChPoint right)
        {
            left.X -= right.X;
            left.Y -= right.Y;
            left.Z -= right.Z;

            return left;
        }

        /// <summary>
        /// Implements the operator !=.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator !=(ChPoint left, ChPoint right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Implements the operator *.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static IChPoint operator *(ChPoint left, ChPoint right)
        {
            left.X *= right.X;
            left.Y *= right.Y;
            left.Z *= right.Z;

            return left;
        }

        /// <summary>
        /// Implements the operator /.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static IChPoint operator /(ChPoint left, ChPoint right)
        {
            left.X /= right.X;
            left.Y /= right.Y;
            left.Z /= right.Z;

            return left;
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static IChPoint operator +(ChPoint left, ChPoint right)
        {
            left.X += right.X;
            left.Y += right.Y;
            left.Z += right.Z;

            return left;
        }

        /// <summary>
        /// Implements the operator +.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static ChPoint operator +(ChPoint left, Vector2d right)
        {
            left.X += right.X;
            left.Y += right.Y;

            return left;
        }

        /// <summary>
        /// Implements the operator &lt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator <(ChPoint left, ChPoint right)
        {
            if (left.X < right.X)
                return true;
            else if (left.X == right.X)
            {
                if (left.Y < right.Y)
                    return true;
                else if (left.Y == right.Y)
                    return left.Z < right.Z;
            }
            return false;
        }

        /// <summary>
        /// Implements the operator ==.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator ==(ChPoint left, ChPoint right)
        {
            return left.Equals(right);
        }

        /// <summary>
        /// Implements the operator &gt;.
        /// </summary>
        /// <param name="left">The left.</param>
        /// <param name="right">The right.</param>
        /// <returns>
        /// The result of the operator.
        /// </returns>
        public static bool operator >(ChPoint left, ChPoint right)
        {
            if (left.X > right.X)
                return true;
            else if (left.X == right.X)
            {
                if (left.Y > right.Y)
                    return true;
                else if (left.Y == right.Y)
                    return left.Z > right.Z;
            }
            return false;
        }

        /// <summary>
        /// Angle 2d.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        /// Angle 2d
        /// </returns>
        public double Angle2D(IChPoint point)
        {
            double dtheta = 0;
            double theta1 = 0;
            double theta2 = 0;

            theta1 = Math.Atan2(this.Y, this.X);
            theta2 = Math.Atan2(point.Y, point.X);
            dtheta = theta2 - theta1;
            while (dtheta > Math.PI)
                dtheta -= 2 * Math.PI;
            while (dtheta < -Math.PI)
                dtheta += 2 * Math.PI;

            return dtheta;
        }

        /// <summary>
        /// Angles from x axis.
        /// </summary>
        /// <param name="ponto">The ponto.</param>
        /// <returns>Angles from x axis</returns>
        public double AngleFromXAxis(IChPoint ponto)
        {
            double result = Math.Atan2(this.Y - ponto.Y, ponto.X - this.X) * -1;
            if (result < 0)
                result += ChAngles.RAD360;
            return result;
        }

        /// <summary>
        /// Ases the array.
        /// </summary>
        /// <returns>Array [x,y,z]</returns>
        public double[] AsArray()
        {
            return new double[3] { X, Y, Z };
        }

        /// <summary>
        /// Compares to.
        /// </summary>
        /// <param name="other">The other.</param>
        /// <returns>
        /// True whether two points are equal
        /// </returns>
        public int CompareTo(IChPoint other)
        {
            if (this.Equals(other))
                return 0;
            return GreaterThan(other) ? 1 : -1;
        }

        /// <summary>
        /// Distances the specified ponto.
        /// </summary>
        /// <param name="ponto">The ponto.</param>
        /// <returns>Distance between two points</returns>
        public double Distance(IChPoint ponto)
        {
            double distX = X - ponto.X;
            double distY = Y - ponto.Y;
            double distZ = Z - ponto.Z;
            return Math.Sqrt(Math.Pow(distX, 2.0) + Math.Pow(distY, 2) + Math.Pow(distZ, 2));
        }

        /// <summary>
        /// Distance between two points in 2d.
        /// </summary>
        /// <param name="ponto">The ponto.</param>
        /// <returns>Distance between two points in 2d.</returns>
        public double Distance2D(IChPoint ponto)
        {
            double distX = X - ponto.X;
            double distY = Y - ponto.Y;
            return Math.Sqrt(Math.Pow(distX, 2.0) + Math.Pow(distY, 2));
        }

        /// <summary>
        /// Distance to line segment in 2d.
        /// </summary>
        /// <param name="lineSegment">The line segment.</param>
        /// <returns>
        /// Distance to line segment in 2d.
        /// </returns>
        public double DistanceToLineSegment2D(IChLineSegment lineSegment) //straight segment
        {
            var linePt1 = lineSegment.StartPoint;
            var linePt2 = lineSegment.EndPoint;
            double deltaX = linePt2.X - linePt1.X;
            double deltaY = linePt2.Y - linePt1.Y;
            double u = 0;

            if (deltaX == 0 && deltaY == 0)
                return Distance(linePt1);

            u = ((this.X - linePt1.X) * deltaX + (this.Y - linePt1.Y) * deltaY) / (deltaX * deltaX + deltaY * deltaY);

            if (u < 0)
                return this.Distance(linePt1);
            else if (u > 1)
                return this.Distance(linePt2);
            else
            {
                IChPoint temp = new ChPoint(linePt1.X + u * deltaX, linePt1.Y + u * deltaY);
                return this.Distance(temp);
            }
        }

        /// <summary>
        /// Divides the specified right.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the divide operator.</returns>
        public IChPoint Divide(IChPoint right)
        {
            IChPoint left = new ChPoint(X, Y, Z);
            left.X /= right.X;
            left.Y /= right.Y;
            left.Z /= right.Z;

            return left;
        }

        /// <summary>
        /// Verifies whether two points are equal
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>True whether points are equal</returns>
        public bool Equals(IChPoint point)
        {
            return this.X == point.X && Y == point.Y && Z == point.Z;
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj is IChPoint)
                return Equals((IChPoint)obj, Tolerance);
            return false;
        }

        /// <summary>
        /// Equalses the specified x.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        /// <returns>True whether points are equal</returns>
        bool IEqualityComparer<IChPoint>.Equals(IChPoint x, IChPoint y)
        {
            return x.Equals(y);
        }

        /// <summary>
        /// Verify whether two points are equal using tolerance.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <returns>
        /// True whether points are equal
        /// </returns>
        public bool Equals(IChPoint point, double tolerance)
        {
            if (point == null)
                return false;

            return Math.Abs(X - point.X) <= tolerance &&
                Math.Abs(Y - point.Y) <= tolerance &&
                Math.Abs(Z - point.Z) <= tolerance;
        }



        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <param name="obj">The object.</param>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.
        /// </returns>
        int IEqualityComparer<IChPoint>.GetHashCode(IChPoint obj)
        {
            return obj.GetHashCode();
        }

        /// <summary>
        /// Greater the than.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the greater than operator.</returns>
        public bool GreaterThan(IChPoint right)
        {
            IChPoint left = new ChPoint(X, Y, Z);
            if (left.X > right.X)
                return true;
            else if (left.X == right.X)
            {
                if (left.Y > right.Y)
                    return true;
                else if (left.Y == right.Y)
                    return left.Z > right.Z;
            }
            return false;
        }

        /// <summary>
        /// Lesses the than.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the less than operator.</returns>
        public bool LessThan(IChPoint right)
        {
            IChPoint left = new ChPoint(X, Y, Z);
            if (left.X < right.X)
                return true;
            else if (left.X == right.X)
            {
                if (left.Y < right.Y)
                    return true;
                else if (left.Y == right.Y)
                    return left.Z < right.Z;
            }
            return false;
        }

        /// <summary>
        /// Determines the maximum of the parameters.
        /// </summary>
        /// <param name="pt">The pt.</param>
        /// <returns>The max point</returns>
        public IChPoint Max(IChPoint pt)
        {
            if (X >= pt.X && Y >= pt.Y)
                return this;
            else
                return pt;
        }

        /// <summary>
        /// Mids the specified pt.
        /// </summary>
        /// <param name="pt">The pt.</param>
        /// <returns>The mid point between two points</returns>
        public IChPoint Mid(IChPoint pt)
        {
            double x = (X + pt.X) / 2;
            double y = (Y + pt.Y) / 2;
            double z = (Z + pt.Z);
            if (z > 0)
                z /= 2;

            return new ChPoint(x, y, z);
        }

        /// <summary>
        /// Determines the minimum of the parameters.
        /// </summary>
        /// <param name="pt">The pt.</param>
        /// <returns>The min point between min coordinates</returns>
        public IChPoint Min(IChPoint pt)
        {
            if (X <= pt.X && Y <= pt.Y)
                return this;
            else
                return pt;
        }

        /// <summary>
        /// Multiplies the specified right.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the multiply operator.</returns>
        public IChPoint Multiply(IChPoint right)
        {
            IChPoint left = new ChPoint(X, Y, Z);
            left.X *= right.X;
            left.Y *= right.Y;
            left.Z *= right.Z;

            return left;
        }

        //// <summary>
        /// Polars the point.
        /// </summary>
        /// <param name="angle">The angle.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>New point by distance and angle</returns>
        public IChPoint PolarPoint(double angulo, double distancia)
        {
            return new ChPoint(X + distancia * Math.Cos(angulo), Y + distancia * Math.Sin(angulo), Z);
        }

        /// <summary>
        /// Retangular the point.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <returns>Point sum coordinate x and y</returns>
        public IChPoint RetangularPoint(double x, double y)
        {
            return new ChPoint(X + x, Y + y, Z);
        }

        /// <summary>
        /// Retangular the point.
        /// </summary>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="z">The z coordinate.</param>
        /// <returns>>Point sum coordinate x, y and z</returns>
        public IChPoint RetangularPoint(double x, double y, double z)
        {
            return new ChPoint(X + x, Y + y, Z + z);
        }

        /// <summary>
        /// Spherical the point.
        /// </summary>
        /// <param name="angleXY">The angle xy.</param>
        /// <param name="angleXZ">The angle xz.</param>
        /// <param name="distance">The distance.</param>
        /// <returns>Spherical the point.</returns>
        public IChPoint SphericalPoint(double angleXY, double angleXZ, double distance)
        {
            return new ChPoint(X + distance * Math.Cos(angleXY) * Math.Sin(angleXZ)
                , Y + distance * Math.Sin(angleXY) * Math.Sin(angleXZ)
                , Z + distance * Math.Cos(angleXZ));
        }

        /// <summary>
        /// Subtracts the specified right.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the subtract operator.</returns>
        public IChPoint Subtract(IChPoint right)
        {
            IChPoint left = new ChPoint(X, Y, Z);
            left.X -= right.X;
            left.Y -= right.Y;
            left.Z -= right.Z;

            return left;
        }

        /// <summary>
        /// Sums the specified right.
        /// </summary>
        /// <param name="right">The right.</param>
        /// <returns>The result of the sum operator.</returns>
        public IChPoint Sum(IChPoint right)
        {
            IChPoint left = new ChPoint(X, Y, Z);
            left.X += right.X;
            left.Y += right.Y;
            left.Z += right.Z;

            return left;
        }

        /// <summary>
        /// Converts to string.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String" /> that represents this instance.
        /// </returns>
        public override string ToString()
        {
            return string.Format("X: {0} *|* Y: {1} *|* Z: {2}", Math.Round(X, 4)
                                , Math.Round(Y, 4), Math.Round(Z, 4));
        }

        /// <summary>
        /// Translacao2ds the specified delta x.
        /// </summary>
        /// <param name="deltaX">The delta x.</param>
        /// <param name="deltaY">The delta y.</param>
        public void Translacao2d(double deltaX, double deltaY)
        {
            X += deltaX;
            Y += deltaY;
        }

        /// <summary>
        /// Returns a hash code for this instance.
        /// </summary>
        /// <returns>
        /// A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table. 
        /// </returns>
        public override int GetHashCode()
        {
            int hashCode = -307843816;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            hashCode = hashCode * -1521134295 + Z.GetHashCode();
            return hashCode;
        }

        #endregion Public Methods
    }
}