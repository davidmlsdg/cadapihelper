﻿using System;
using System.Collections.Generic;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChSegment instance
    /// </summary>
    public interface IChSegment : IEquatable<IChSegment>
    {
        #region Properties

        /// <summary>
        /// Length
        /// </summary>
        double Area { get; }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        bool IsValid { get; }

        /// <summary>
        /// Length
        /// </summary>
        double Length { get; }

        /// <summary>
        /// Segment Type
        /// </summary>
        ChSegmentType SegmentType { get; }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        object Tag { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Copy of Segment
        /// </summary>
        /// <returns>Segment copy</returns>
        IChSegment CopySegment();

        /// <summary>
        /// Check if two segments are equal
        /// </summary>
        /// <param name="other">Other segment</param>
        /// <returns>True if two segments are equals otherwise false</returns>
        bool Equals(object other);

        /// <summary>
        /// Intersects the with.
        /// </summary>
        /// <param name="segment">The segment.</param>
        /// <param name="points">List of points.</param>
        /// <returns>True whether segment and points intersect</returns>
        bool IntersectWith(IChSegment segment, List<IChPoint> points);

        /// <summary>
        /// Intersects the with.
        /// </summary>
        /// <param name="segment">The segment.</param>
        /// <returns>True whether two segment intersect</returns>
        bool IntersectWith(IChSegment segment);

        /// <summary>
        /// Determines whether the specified point is on.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        ///   <c>true</c> if the specified point is on; otherwise, <c>false</c>.
        /// </returns>
        bool IsOn(IChPoint point);

        /// <summary>
        /// Determines whether the specified point is on.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <param name="tolerance">The tolerance.</param>
        /// <returns>
        ///   <c>true</c> if the specified point is on; otherwise, <c>false</c>.
        /// </returns>
        bool IsOn(IChPoint point, double tolerance);

        /// <summary>
        /// Translation in 2d
        /// </summary>
        /// <param name="deltaX">Delta x</param>
        /// <param name="deltaY">Delta y</param>
        void Translation2d(double deltaX, double deltaY);

        #endregion Public Methods
    }
}