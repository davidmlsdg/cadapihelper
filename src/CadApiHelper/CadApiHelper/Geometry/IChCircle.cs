﻿namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChCircle instance
    /// </summary>
    /// <seealso cref="CadApiHelper.Geometry.IChSegment" />
    public interface IChCircle : IChSegment
    {
        #region Properties

        /// <summary>
        /// Gets or sets the center.
        /// </summary>
        /// <value>
        /// The center.
        /// </value>
        IChPoint Center { get; set; }

        /// <summary>
        /// Gets the diameter.
        /// </summary>
        /// <value>
        /// The diameter.
        /// </value>
        double Diameter { get; }

        /// <summary>
        /// Gets the perimeter.
        /// </summary>
        /// <value>
        /// The perimeter.
        /// </value>
        double Perimeter { get; }

        /// <summary>
        /// Gets or sets the radius.
        /// </summary>
        /// <value>
        /// The radius.
        /// </value>
        double Radius { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Distances the of center.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns></returns>
        double DistanceOfCenter(IChPoint point);

        /// <summary>
        /// Determines whether the specified point is inside.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        ///   <c>true</c> if the specified point is inside; otherwise, <c>false</c>.
        /// </returns>
        bool IsInside(IChPoint point);

        #endregion Public Methods
    }
}