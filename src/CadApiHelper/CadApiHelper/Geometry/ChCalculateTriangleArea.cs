﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// Calculate area of triangle
    /// </summary>
    public static class ChCalculateTriangleArea
    {
        #region Public Methods

        /// <summary>
        /// Calculates the area of triangle.
        /// </summary>
        /// <param name="sideA">The side a.</param>
        /// <param name="sideB">The side b.</param>
        /// <param name="angleBetweenAB">The angle between ab.</param>
        /// <returns>Area of triangle</returns>
        public static double CalculateArea(double sideA, double sideB, double angleBetweenAB)
        {
            return sideA * sideB * Math.Sin(angleBetweenAB) / 2.0;
        }

        /// <summary>
        /// Calculates the area.
        /// </summary>
        /// <param name="baseTriangle">The base triangle.</param>
        /// <param name="height">The height.</param>
        /// <returns>Area of triangle</returns>
        public static double CalculateArea(double baseTriangle, double height)
        {
            return baseTriangle * height / 2.0;
        }

        #endregion Public Methods
    }
}