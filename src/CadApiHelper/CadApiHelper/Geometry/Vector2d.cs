﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// </summary>
    [Serializable]
    public class Vector2d
    {
        private double m_x;
        private double m_y;

        public Vector2d()
        {
        }

        public Vector2d(double x, double y)
        {
            m_x = x;
            m_y = y;
        }

        /// <summary>
        /// Construtora responsável por gerar um vetor ligando a origem ao ponto pt
        /// </summary>
        /// <param name="pt"></param>
        public Vector2d(Point pt)
        {
            m_x = pt.X;
            m_y = pt.Y;
        }

        /// <summary>
        /// Construtora Responsável por gerar um vetor ligando dois pontos.
        /// </summary>
        /// <param name="ptI">Ponto Inicial</param>
        /// <param name="ptF">Ponto Final</param>
        public Vector2d(Point ptI, Point ptF)
        {
            m_x = ptF.X - ptI.X;
            m_y = ptF.Y - ptI.Y;
        }

        /// <summary>
        /// Construtora Responsável por gerar um vetor unitario apartir de um angulo
        /// </summary>
        /// <param name="ang"></param>
        public Vector2d(double ang)
        {
            m_x = Math.Cos(ang);
            m_y = Math.Sin(ang);
        }

        /// <summary>
        /// Angulo entre o vetor e o eixo X.
        /// </summary>
        public double Angle
        {
            get
            {
                if (Module < 0.0001)
                    return 0.0;
                var ang = Math.Acos(m_x / Module);
                if (m_y < 0)
                    ang = (2 * Math.PI) - ang;
                return ang;
            }
        }

        /// <summary>
        /// Angulo inverso entre o vetor e o eixo X.
        /// </summary>
        public double AngleInverse
        {
            get
            {
                if (Module < 0.0001)
                    return 0.0;
                var ang = Math.Acos(-m_x / Module);
                if (-m_y < 0)
                    ang = (2 * Math.PI) - ang;
                return ang;
            }
        }

        /// <summary>
        /// Intensidade do vetor.
        /// </summary>
        public double Module
        {
            get
            {
                return Math.Sqrt(this.ProdutoEscalar(this));
            }
        }

        public double X
        {
            get { return m_x; }
            //set { m_x = value; }
        }

        public double Y
        {
            get { return m_y; }
            //set { m_y = value; }
        }

        /// <summary>
        /// Função responsável por gerar um vetor a partir de seu angulo e intensidade.
        /// </summary>
        /// <param name="angulo">Ângulo do vetor</param>
        /// <param name="modulo">Intensidade do vetor</param>
        /// <returns></returns>
        public static Vector2d GetVetorPolar(double angulo, double modulo)
        {
            return new Vector2d(Math.Cos(angulo) * modulo, Math.Sin(angulo) * modulo);
        }

        /// <summary>
        /// Subtração de Vetores
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector2d operator -(Vector2d left, Vector2d right)
        {
            return new Vector2d(left.X - right.X, left.X - right.X);
        }

        /// <summary>
        /// Multiplicação de um vetor por um escalar
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector2d operator *(double left, Vector2d right)
        {
            return new Vector2d(left * right.X, left * right.X);
        }

        /// <summary>
        /// Multiplicação de um vetor por um escalar
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector2d operator *(Vector2d left, double right)
        {
            return right * left;
        }

        /// <summary>
        /// Divisão de um vetor por um escalar.
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector2d operator /(Vector2d left, double right)
        {
            return new Vector2d(left.X / right, left.X / right);
        }

        /// <summary>
        /// Soma de Vetores
        /// </summary>
        /// <param name="left"></param>
        /// <param name="right"></param>
        /// <returns></returns>
        public static Vector2d operator +(Vector2d left, Vector2d right)
        {
            return new Vector2d(left.X + right.X, left.X + right.X);
        }

        /// <summary>
        /// Funções responsável pelo cálculo do ângulo entre dois vetores.
        /// </summary>
        /// <param name="vet"></param>
        /// <returns></returns>
        public double AnguloEntre(Vector2d vet)
        {
            double cos = this.ProdutoEscalar(vet) / (Module * vet.Module);
            if (cos > 1.0)
                cos = 1.0;
            else if (cos < -1.0)
                cos = -1.0;
            return Math.Acos(cos);
        }

        /// <summary>
        /// Função responsável por normalizar um vetor. (Transformando ele em um vetor unitário).
        /// </summary>
        /// <returns>Vetor unitário</returns>
        public Vector2d Normalizar()
        {
            return this / this.Module;
        }

        /// <summary>
        /// Cálculo do produto interno de dois vetores.
        /// </summary>
        /// <param name="vet"></param>
        public double ProdutoEscalar(Vector2d vet)
        {
            return m_x * vet.X + m_y * vet.Y;
        }

        /// <summary>
        /// Função responsável por rotacionar um vetor.
        /// </summary>
        /// <param name="ang">Ângulo que o vetor será rotacionado.</param>
        /// <returns>Vetor rotacionado</returns>
        public Vector2d rotacionar(double ang)
        {
            return GetVetorPolar(Angle + ang, Module);
        }

        public override string ToString()
        {
            return string.Format("X = {0}; Y = {1} [Mod = {2}; Âng = {3}]", Math.Round(m_x, 3), Math.Round(m_y, 3)
                , Math.Round(Module, 3), Math.Round(Angle, 5));
        }
    }
}