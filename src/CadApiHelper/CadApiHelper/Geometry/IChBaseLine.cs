﻿using System;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// IChBaseLine interface
    /// </summary>
    public interface IChBaseLine
    {
        #region Properties

        /// <summary>
        /// >The a variable for the Line Equation
        /// </summary>
        double A { get; }

        /// <summary>
        /// >The b variable for the Line Equation
        /// </summary>
        double B { get; }

        /// <summary>
        /// >The c variable for the Line Equation
        /// </summary>
        double C { get; }

        /// <summary>
        /// Vector Normal
        /// </summary>
        IChVector Normal { get; }

        /// <summary>
        /// vector director
        /// </summary>
        IChVector VectorDiretor { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Function returns the X to the point on top of the line in the y-coordinate passed.
        /// </summary>
        /// <param name="y">y coordinate of the point on top of the line</param>
        /// <returns>coordinate x</returns>
        double CoordinateX(double y);

        /// <summary>
        /// Function returns the y to the point on top of the straight line in the passed x coordinate.
        /// </summary>
        /// <param name="x">x coordinate of the point on top of the line</param>
        /// <returns>coordinate y</returns>
        double CoordinateY(double x);

        /// <summary>
        /// Return the diametrically opposite points
        /// </summary>
        /// <param name="referencePoint">Reference point</param>
        /// <param name="distance">Distance</param>
        /// <returns>Return the diametrically opposite points</returns>
        Tuple<IChPoint, IChPoint> DiametricallyOppositePoints(IChPoint referencePoint, double distance);

        /// <summary>
        /// Function responsible for calculating the perpendicular distance from a point to the line.
        /// </summary>
        /// <param name="pt">point you want to calculate the distance</param>
        /// <returns>Return the distance from the point to the line</returns>
        double DistanceToPerpendicularPoint(IChPoint pt);

        /// <summary>
        /// Calculate the distance of line
        /// </summary>
        /// <param name="baseLine">Line</param>
        /// <returns>Distance of line</returns>
        double DistanceToReta(IChBaseLine baseLine);

        /// <summary>
        /// Checks the intersection between two lines.
        /// </summary>
        /// <param name="r"></param>
        /// <param name="ptIntersect">Point intersect</param>
        /// <returns>
        /// Returns true if there is only one intersection point. Returns false if the two lines are parallel.
        /// </returns>
        bool InterseptPoint(IChBaseLine r, ref IChPoint ptIntersect);

        /// <summary>
        /// Check if two lines are parallel.
        /// </summary>
        /// <param name="baseLine">Line to be verify</param>
        /// <returns>Returns true if the two lines are parallel</returns>
        bool IsParallel(IChBaseLine baseLine);

        /// <summary>
        /// Check whether two lines are perpendicular.
        /// </summary>
        /// <param name="baseLine">Line to be verify</param>
        /// <returns>Returns true if the two lines are perpendicular</returns>
        bool IsPerpendicular(IChBaseLine baseLine);

        /// <summary>
        /// Checks whether a point is contained in the line.
        /// </summary>
        /// <param name="point">Point to be verify</param>
        /// <returns>True if the point is contained in the line</returns>
        bool OnPoint(IChPoint point);

        /// <summary>
        /// Checks whether a point is contained in the line using tolerance parameter.
        /// </summary>
        /// <param name="point">Point to be verify</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>True if the point is contained in the line</returns>
        bool OnPoint(IChPoint point, double tolerance);

        /// <summary>
        /// Returns if point is above or below the line.
        /// </summary>
        /// <param name="pt">Ponto to know relative position</param>
        /// <returns>
        /// Positive value whether the point is above
        /// Zero whether the point is on the line
        /// Negative value whether the point is bellow
        /// </returns>
        double PointRelationLinePosition(IChPoint pt);

        /// <summary>
        /// Returns the projection of a point on a line.
        /// </summary>
        /// <param name="pointReference">Reference point</param>
        /// <param name="projectedPoint">Projected Point</param>
        /// <returns>Returns true if the projected point belongs to the line.</returns>
        bool ProjectedPoint(IChPoint pointReference, ref IChPoint projectedPoint);

        /// <summary>
        /// Returns the projection of a point on a line.
        /// </summary>
        /// <param name="pointReference">Reference point</param>
        /// <param name="projectedPoint">Projected Point</param>
        /// <param name="angle">Projection Angle</param>
        /// <returns>Returns true if the projected point belongs to the line.</returns>
        bool ProjectedPoint(IChPoint pointReference, ref IChPoint projectedPoint, double angle);

        /// <summary>
        /// Returns the projection of a point on a line. With tolerance after using the line equation
        /// </summary>
        /// <param name="pointReference">Reference point</param>
        /// <param name="projectedPoint">Projected Point</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>Returns true if the projected point belongs to the line.</returns>
        bool ProjectedPointTolerance(IChPoint pointReference, ref IChPoint projectedPoint, double tolerance);

        #endregion Public Methods
    }
}