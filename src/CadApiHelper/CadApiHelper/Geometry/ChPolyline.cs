﻿#if ACAD

using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;

#endif
#if ZWCAD

using ZwSoft.ZwCAD.DatabaseServices;

#endif
#if BRCAD
    using ds = Teigha.DatabaseServices;
    using Teigha.Geometry;
#endif

#if GCAD
using GrxCAD.DatabaseServices;
using GrxCAD.Geometry;
#endif

using CadApiHelper.Core;
using CadApiHelper.Entities.Edit;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace CadApiHelper.Geometry
{
    /// <summary>
    /// ChPolyline class
    /// </summary>
    [Serializable]
    public class ChPolyline : IChPolyline
    {
        #region Fields

        [NonSerialized]
        private ChObjectId objectId;

        [NonSerialized]
        private int position = -1;

        private List<IChPolylineSegment> segments = new List<IChPolylineSegment>();

        [NonSerialized]
        private object tag = null;

        #endregion Fields

        #region Properties

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <value>
        /// The segments.
        /// </value>
        public ReadOnlyCollection<IChPolylineSegment> Segments
        {
            get { return segments.AsReadOnly(); }
        }

        /// <summary>
        /// Área de dentro da polilinha, não está preparada para retornar a área correta de
        /// polilinhas que possuam segmentos se atravessando
        /// </summary>
        public double Area
        {
            //get { return getArea(); }
            get
            {
                double area = ChCalculateAreaPolygon.CalculateArea(Vertices); //CalculateAreaPolygon
                for (int i = 0; i < segments.Count; i++)
                {
                    if (segments[i].SegmentType == ChSegmentType.Line)
                        continue;

                    IChArc arc = segments[i] as IChArc;
                    if (arc == null)
                        continue;

                    if (IsPointInside2D(arc.MiddlePointArc))//inside
                        area -= arc.Area;
                    else
                        area += arc.Area;
                }

                return area;
            }
        }

        /// <summary>
        /// Gets the area changed.
        /// </summary>
        /// <value>
        /// The area changed.
        /// </value>
        public double AreaChanged { get; private set; }

        /// <summary>
        /// Gets a value indicating whether this <see cref="ChPolyline"/> is closed.
        /// </summary>
        /// <value>
        ///   <c>true</c> if closed; otherwise, <c>false</c>.
        /// </value>
        public bool Closed
        {
            get
            {
                if (segments.Count > 0)
                    return segments.First().StartPoint.Equals(segments.Last().EndPoint);
                return false;
            }
        }

        /// <summary>
        /// Count segments
        /// </summary>
        public int CountSegments
        {
            get
            {
                if (segments == null)
                    return 0;
                return segments.Count;
            }
        }

        /// <summary>
        /// Count vertices
        /// </summary>
        public int CountVertices
        {
            get
            {
                if (Vertices == null)
                    return 0;
                return Vertices.Count;
            }
        }

        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        public IChPolylineSegment Current
        {
            get
            {
                try
                {
                    return segments[position];
                }
                catch (ArgumentOutOfRangeException e)
                {
                    throw e;
                }
            }
        }

        /// <summary>
        /// Gets the current.
        /// </summary>
        /// <value>
        /// The current.
        /// </value>
        object IEnumerator.Current
        {
            get { return Current; }
        }

        /// <summary>
        /// Return whether the polyline is valid
        /// </summary>
        public bool IsValid
        {
            get { return GetVertices().Count > 0; }
        }

        /// <summary>
        /// Gets the length.
        /// </summary>
        /// <value>
        /// The length.
        /// </value>
        public double Length
        {
            get
            {
                double comp = 0;
                foreach (IChPolylineSegment item in this)
                    comp += item.Length;

                return comp;
            }
        }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public ChObjectId ObjectId
        {
            get { return objectId; }
            set { objectId = value; }
        }

        /// <summary>
        /// Gets or sets the tag.
        /// </summary>
        /// <value>
        /// The tag.
        /// </value>
        public object Tag
        {
            get { return tag; }
            set { tag = value; }
        }

        /// <summary>
        /// Gets the vertices's.
        /// </summary>
        /// <value>
        /// The vertices's.
        /// </value>
        public List<IChPoint> Vertices
        {
            get { return GetVertices(); }
        }

        #endregion Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPolyline"/> class.
        /// </summary>
        /// <param name="id">ObjectId</param>
        /// <param name="correctPolyline">Define whether the orientation of polyline to be correct </param>
        /// <param name="clockwise">Define the orientation of the correct polyline</param>
        public ChPolyline(ChObjectId id, bool correctPolyline = true, bool clockwise = true)//
        {
            ObjectId = id;
            var ent = new ChEditEntity(id);
            using (ent)
            {
                if (ent.DbObject == null)
                    return;
                if (ent.DbObject is Line)
                {
                    var l = ent.DbObject as Line;
                    segments = SetSegmentsByPoints(new List<IChPoint> { l.StartPoint.ToPoint(), l.EndPoint.ToPoint() });
                    return;
                }

                if (ent.DbObject is Polyline)
                {
                    var pol = ent.DbObject as Polyline;

                    if (correctPolyline)
                    {
                        bool isPolySendidoHorario = GetAlgebricAreaByPolyline(pol) > 0;

                        if (isPolySendidoHorario != clockwise)
                            pol.ReverseCurve();
                    }
                    CreateByPolyline(pol);
                }
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPolyline"/> class.
        /// </summary>
        /// <param name="polyline">The polyline.</param>
        public ChPolyline(Polyline polyline)
        {
            if (polyline != null)
            {
                ObjectId = polyline.ObjectId;
                CreateByPolyline(polyline);
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPolyline"/> class.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        public ChPolyline(List<IChPoint> points, bool closed)
        {
            segments = SetSegmentsByPoints(points);
            if (closed)
                SetClosedPoint();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPolyline"/> class.
        /// </summary>
        /// <param name="segments">The segments.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        public ChPolyline(List<IChPolylineSegment> segments, bool closed)
        {
            this.segments.AddRange(segments);
            if (closed)
                SetClosedPoint();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPolyline"/> class.
        /// </summary>
        /// <param name="segments">The segments.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        public ChPolyline(List<IChLineSegment> segments, bool closed)
        {
            this.segments.AddRange(segments);
            if (closed)
                SetClosedPoint();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChPolyline"/> class.
        /// </summary>
        public ChPolyline()
        {
        }

        #endregion Public Constructors

        #region Private Methods

        /// <summary>
        /// Adds the vertice arc.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="secondPointArc">The second point arc.</param>
        /// <param name="endPointArc">The end point arc.</param>
        private void AddVerticeArc(int index, IChPoint secondPointArc, IChPoint endPointArc)
        {
            int numeroVerticesAtual = GetVertices().Count;

            if (numeroVerticesAtual == 0)
            {
                return;
            }

            if (index > numeroVerticesAtual || index < 0)
                index = numeroVerticesAtual;

            if (index < numeroVerticesAtual)
                DefineSegmento(index, endPointArc, secondPointArc);
            else if (index == numeroVerticesAtual)
            {
                if (Closed)
                    DefineSegmento(index - 1, endPointArc, secondPointArc);
                else
                {
                    IChPolylineSegment novaLinha;
                    novaLinha = ChArc.Create(segments.Last().EndPoint, secondPointArc, endPointArc);
                    segments.Add(novaLinha);
                }
            }
        }

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="point">The point.</param>
        private void AddVerticeLine(int index, IChPoint point)
        {
            int numeroVerticesAtual = GetVertices().Count;

            if (numeroVerticesAtual == 0)
            {
                return;
            }

            if (index > numeroVerticesAtual || index < 0)
                index = numeroVerticesAtual;

            if (index < numeroVerticesAtual)
                DefineSegmento(index, point, null);
            else if (index == numeroVerticesAtual)
            {
                if (Closed)
                    DefineSegmento(index - 1, point, null);
                else
                {
                    segments.Add(ChLineSegment.Create(segments.Last().EndPoint, point));
                }
            }
        }


        private double ArcAlgebraicArea(double rad, double ang)
        {
            return rad * rad * (ang - Math.Sin(ang)) / 2.0;
        }

        private void CorrigeSequenciaSegmentosLinha(List<IChPolylineSegment> segCorrigindo, List<IChPolylineSegment> segCorrigidos, IChLineSegment seg, double tolComp, double tolAng)
        {
            if (segCorrigindo.Count() == 0)
            {
                if (!Closed)
                    segCorrigidos.Add(seg);
                return;
            }
            var segProx = segCorrigindo[0] as IChLineSegment;
            if (segProx == null)
            {
                segCorrigidos.Add(seg);
                return;
            }
            segCorrigindo.RemoveAt(0);
            double ang = seg.VectorDiretor.AngleBetween(segProx.VectorDiretor);
            if (seg.Length < tolComp || ang < tolAng)
            {
                seg.EndPoint = segProx.EndPoint;
                CorrigeSequenciaSegmentosLinha(segCorrigindo, segCorrigidos, seg, tolComp, tolAng);
            }
            else
            {
                segCorrigidos.Add(seg);
                CorrigeSequenciaSegmentosLinha(segCorrigindo, segCorrigidos, segProx, tolComp, tolAng);
            }
        }

        private void CreateByPolyline(Polyline pol)
        {
            for (int i = 0; i < pol.NumberOfVertices; i++)
            {
                switch (pol.GetSegmentType(i))
                {
                    case SegmentType.Arc:
                        var c = pol.GetArcSegmentAt(i);
                        segments.Add(ChArc.Create(c.Center.ToPoint(), c.EndAngle, c.EndPoint.ToPoint(), ChVector.Create(c.Normal.X, c.Normal.Y, c.Normal.Z),
                            c.Radius, c.StartAngle, c.StartPoint.ToPoint()));
                        break;

                    case SegmentType.Line:
                        var l = pol.GetLineSegmentAt(i);
                        segments.Add(ChLineSegment.Create(l.StartPoint.ToPoint(), l.EndPoint.ToPoint()));
                        break;

                    default:
                        break;
                }
            }
            if (pol.Closed && pol.NumberOfVertices > 0)
                SetClosedPoint();
        }

        private void DefineSegmento(int indice, IChPoint ponto, IChPoint pontoNoArco = null)
        {
            var linhaInicial = segments[indice];
            IChPolylineSegment novaLinhaProxima;
            var novaLinhaInicial = ChLineSegment.Create(linhaInicial.StartPoint, ponto);

            if (pontoNoArco == null)
                novaLinhaProxima = ChLineSegment.Create(ponto, linhaInicial.EndPoint);
            else
                novaLinhaProxima = ChArc.Create(ponto, pontoNoArco, linhaInicial.EndPoint);

            segments.RemoveAt(indice);
            segments.Insert(indice, novaLinhaProxima);
            segments.Insert(indice, novaLinhaInicial);
        }

        //Area changed
        private double GetAlgebricAreaByPolyline(Polyline pl)
        {
            var p0 = pl.GetPoint2dAt(0);
            double area = 0.0;
            var bulge = pl.GetBulgeAt(0);
            int last = pl.NumberOfVertices - 1;

            if (bulge != 0.0)
            {
                var arc = pl.GetArcSegment2dAt(0);
                double arcRadius = arc.Radius;
                double arcAngle = 4.0 * Math.Atan(bulge);
                area += ArcAlgebraicArea(arcRadius, arcAngle);
            }
            for (int i = 1; i < last; i++)
            {
                area += TriangleAlgebraicArea(p0.ToPoint(), pl.GetPoint2dAt(i).ToPoint(), pl.GetPoint2dAt(i + 1).ToPoint());
                bulge = pl.GetBulgeAt(i);
                if (bulge != 0.0)
                {
                    var arc = pl.GetArcSegment2dAt(i);
                    double arcRadius = arc.Radius;
                    double arcAngle = 4.0 * Math.Atan(bulge);
                    area += ArcAlgebraicArea(arcRadius, arcAngle);
                }
            }
            bulge = pl.GetBulgeAt(last);
            if (bulge != 0.0)
            {
                var arc = pl.GetArcSegment2dAt(last);
                double arcRadius = arc.Radius;
                double arcAngle = 4.0 * Math.Atan(bulge);
                area += ArcAlgebraicArea(arcRadius, arcAngle);
            }
            return area;
        }

        private List<IChPoint> GetVertices()
        {
            var points = new List<IChPoint>();
            for (int idxSegmentos = 0; idxSegmentos < segments.Count; idxSegmentos++)
            {
                points.Add(segments[idxSegmentos].StartPoint);
            }
            // if (Closed == true && m_segmentos.Count > 1)
            if (segments.Count >= 1)
            {
                points.Add(segments[segments.Count - 1].EndPoint);
            }
            return points;
        }

        private void SetClosedPoint()
        {
            if (!IsValid || Closed)
                return;
            if (segments.First().StartPoint.Distance(segments.Last().EndPoint) > 0.00001)
                segments.Add(ChLineSegment.Create(segments.Last().StartPoint, segments.First().EndPoint));
        }

        private List<IChPolylineSegment> SetSegmentsByPoints(List<IChPoint> pontos)
        {
            var segmentos = new List<IChPolylineSegment>();
            if (pontos.Count > 1)
            {
                for (int i = 1; i < pontos.Count; i++)
                    segmentos.Add(ChLineSegment.Create(pontos[i - 1], pontos[i]));
            }
            else if (pontos.Count == 1)
            {
                segmentos.Add(ChLineSegment.Create(pontos[0], pontos[0]));
            }
            return segmentos;
        }

        private double TriangleAlgebraicArea(IChPoint p0, IChPoint p1, IChPoint p2)
        {
            return (((p1.X - p0.X) * (p2.Y - p0.Y)) - ((p2.X - p0.X) * (p1.Y - p0.Y))) / 2.0;
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Creates new instance of the IChPolyline.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="correctPolyline">if set to <c>true</c> [correct polyline].</param>
        /// <param name="clockwise">if set to <c>true</c> [clockwise].</param>
        /// <returns>IChPolyline</returns>
        public static IChPolyline Create(ChObjectId id, bool correctPolyline = true, bool clockwise = true)
        {
            return new ChPolyline(id, correctPolyline, clockwise);
        }

        /// <summary>
        /// Creates new instance of the IChPolyline.
        /// </summary>
        /// <param name="points">The points.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        /// <returns>IChPolyline</returns>
        public static IChPolyline Create(List<IChPoint> points, bool closed)
        {
            return new ChPolyline(points, closed);
        }

        /// <summary>
        /// Creates new instance of the IChPolyline.
        /// </summary>
        /// <param name="segments">The segments.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        /// <returns>IChPolyline</returns>
        public static IChPolyline Create(List<IChPolylineSegment> segments, bool closed)
        {
            return new ChPolyline(segments, closed);
        }

        /// <summary>
        /// Creates new instance of the IChPolyline.
        /// </summary>
        /// <param name="segments">The segments.</param>
        /// <param name="closed">if set to <c>true</c> [closed].</param>
        /// <returns>IChPolyline</returns>
        public static IChPolyline Create(List<IChLineSegment> segments, bool closed)
        {
            return new ChPolyline(segments, closed);
        }

        /// <summary>
        /// Creates new instance of the IChPolyline.
        /// </summary>
        /// <returns>IChPolyline</returns>
        public static IChPolyline Create()
        {
            return new ChPolyline();
        }

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="point">The point.</param>
        public void AddVertice(IChPoint point)
        {
            AddVertice(GetVertices().Count, point);
        }

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="point">The point.</param>
        public void AddVertice(int index, IChPoint point)
        {
            AddVerticeLine(index, point);
        }

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="secondPointArc">The second point arc.</param>
        /// <param name="endPointArc">The end point arc.</param>
        public void AddVertice(IChPoint secondPointArc, IChPoint endPointArc)
        {
            AddVerticeArc(GetVertices().Count, secondPointArc, endPointArc);
        }

        /// <summary>
        /// Adds the vertice.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="secondPointArc">The second point arc.</param>
        /// <param name="endPointArc">The end point arc.</param>
        public void AddVertice(int index, IChPoint secondPointArc, IChPoint endPointArc)
        {
            AddVerticeArc(index, secondPointArc, endPointArc);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        public void Dispose()
        {
        }

        /// <summary>
        /// Determines whether the specified <see cref="System.Object" />, is equal to this instance.
        /// </summary>
        /// <param name="obj">The <see cref="System.Object" /> to compare with this instance.</param>
        /// <returns>
        ///   <c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.
        /// </returns>
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            return Equals(obj as IChPolylineSegment);
        }

        /// <summary>
        /// Checks whether two IChPolyline are equals.
        /// </summary>
        /// <param name="other">The IChPolyline to be checked.</param>
        /// <returns>True whether two IChPolyline are equals.</returns>
        public bool Equals(IChPolyline other)
        {
            if (other == null || this.CountSegments != other.CountSegments)
                return false;

            for (int i = 0; i < CountSegments; i++)
            {
                if (!GetSegment(i).Equals(other.GetSegment(i)))
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Fixes the vertices.
        /// </summary>
        /// <param name="toleranceDistanceSegmnents">The tolerance distance between segmnents.</param>
        /// <param name="toleranceAngleSegments">The tolerance angle between segments.</param>
        public void FixVertices(double toleranceDistanceSegmnents = 0.0001, double toleranceAngleSegments = 0.0174532925)//
        {
            int n = segments.Count;
            if (n < 2)
                return;
            var segmentos = new List<IChPolylineSegment>();
            var segmentosAtuais = new List<IChPolylineSegment>();
            segmentosAtuais.AddRange(segments);
            if (Closed)
                segmentosAtuais.Add(segments[0]);
            while (segmentosAtuais.Count() > 0)
            {
                IChPolylineSegment seg = segmentosAtuais[0];
                var segAtual = segmentosAtuais[0] as IChLineSegment;
                segmentosAtuais.RemoveAt(0);
                if (segAtual == null)
                    segmentos.Add(seg);
                else
                    CorrigeSequenciaSegmentosLinha(segmentosAtuais, segmentos, segAtual, toleranceDistanceSegmnents, toleranceAngleSegments);
            }
            if (segmentos.Count > 1 && (segmentos[0] == segmentos[segmentos.Count - 1]))
                segmentos.RemoveAt(segmentos.Count - 1);

            segments = segmentos;
        }

        /// <summary>
        /// IEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        public IEnumerator<IChPolylineSegment> GetEnumerator()
        {
            return segments.GetEnumerator();
        }

        /// <summary>
        /// IEnumerator
        /// </summary>
        /// <returns>IEnumerator</returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return segments.GetEnumerator();
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            int hashCode = -1222351512;
            hashCode = hashCode * -1521134295 + EqualityComparer<List<IChPolylineSegment>>.Default.GetHashCode(segments);
            hashCode = hashCode * -1521134295 + IsValid.GetHashCode();
            return hashCode;
        }

        /// <summary>
        /// Gets the segment.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <returns>IPolylineSegment</returns>
        public IChPolylineSegment GetSegment(int index)
        {
            return segments.ElementAtOrDefault(index);
        }

        /// <summary>
        /// Gets the segments.
        /// </summary>
        /// <returns>List of IChPolylineSegment</returns>
        public IEnumerable<IChPolylineSegment> GetSegments()
        {
            var segmentos = new List<IChPolylineSegment>();
            for (int i = 0; i < segments.Count; i++)
            {
                segmentos.Add(segments[i].CopySegment() as IChPolylineSegment);
            }
            return segmentos;
        }

        /// <summary>
        /// Verifies the intersects between ChPolyline and IChPolylineSegment.
        /// </summary>
        /// <param name="segment">The segmento to be verify.</param>
        /// <returns>True whether intersect</returns>
        public bool IntersectWith(IChPolylineSegment segment)
        {
            var ptsTemp = new List<IChPoint>();
            return IntersectWith(segment, ptsTemp);
        }

        /// <summary>
        /// Verifies the intersects between ChPolyline and IChPolylineSegment and return the list points intersect.
        /// </summary>
        /// <param name="segment">The segment.</param>
        /// <param name="pointsIntersects">The points intersects.</param>
        /// <returns>True whether intersect</returns>
        public bool IntersectWith(IChPolylineSegment segment, List<IChPoint> pointsIntersects)
        {
            for (int idxSegs = 0; idxSegs < segments.Count; idxSegs++)
            {
                if (segments[idxSegs].IntersectWith(segment, pointsIntersects))
                    return true;
            }
            return false;
        }

        /// <summary>
        /// Determines whether the specified point is on the segments.
        /// </summary>
        /// <param name="point">The point.</param>
        /// <returns>
        ///   <c>true</c> if the specified point is on; otherwise, <c>false</c>.
        /// </returns>
        public bool IsOn(IChPoint point)
        {
            for (int idxSegment = 0; idxSegment < segments.Count; idxSegment++)
            {
                if (segments[idxSegment].IsOn(point))
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Checks whether the point ins inside polyline
        /// </summary>
        /// <param name="point">The point</param>
        /// <param name="tolerance">Tolerance</param>
        /// <returns>True whether the point is inside the polyline</returns>
        public bool IsPointInside2D(IChPoint point, double tolerance = 0.001)
        {
            var vertices = Vertices;
            double totalAngle = 0;

            if (vertices.Count < 3)
                return false;

            for (int idxVert = 0; idxVert < vertices.Count; idxVert++)
            {
                var p1 = ChPoint.Create(vertices[idxVert].X - point.X, vertices[idxVert].Y - point.Y);
                var p2 = ChPoint.Create(vertices[(idxVert + 1) % vertices.Count].X - point.X, vertices[(idxVert + 1) % vertices.Count].Y - point.Y);

                totalAngle += p1.Angle2D(p2);
            }

            if (Math.Abs(totalAngle) < Math.PI - 0.009)
                return false;
            else
                return true;
        }

        /// <summary>
        /// Move Next
        /// </summary>
        /// <returns></returns>
        public bool MoveNext()
        {
            position++;
            return (position < segments.Count);
        }

        /// <summary>
        /// Removes the last point.
        /// </summary>
        /// <returns>True whether the last point was removed</returns>
        public bool RemoveLastPoint()
        {
            return RemovePointAt(GetVertices().Count());
        }

        /// <summary>
        /// Remove point as index of vertex passed as parameter
        /// </summary>
        /// <param name="index">index of the vertex to be removed</param>
        /// <returns>True whether the point was removed</returns>
        public bool RemovePointAt(int index)
        {
            int numeroVerticesAtual = GetVertices().Count;
            if (index > numeroVerticesAtual || index < 0)
                return false;

            if (index > 0 && index < numeroVerticesAtual)
            {
                var startPoint = segments[index - 1].EndPoint;
                var endPoint = segments[index - 1].StartPoint;
                segments.RemoveAt(index);
                segments.RemoveAt(index - 1);
                segments.Insert(index - 1, ChLineSegment.Create(startPoint, endPoint));
            }
            else if (Closed)
            {
                if (segments.Count > 0)
                    segments.RemoveAt(segments.Count - 1);

                if (segments.Count > 0)
                    segments.RemoveAt(0);

                if (segments.Count > 0)
                    SetClosedPoint();
            }
            else
            {
                if (index == numeroVerticesAtual)
                {
                    if (segments.Count > 0)
                        segments.RemoveAt(segments.Count - 1);
                }
                else if (index == 0)
                {
                    if (segments.Count > 0)
                        segments.RemoveAt(0);
                }
            }
            return true;
        }

        /// <summary>
        /// Reset
        /// </summary>
        public void Reset()
        {
            position = -1;
        }

        #endregion Public Methods
    }
}