﻿#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.ApplicationServices;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
#elif BRCAD
using Teigha.DatabaseServices;
using Bricscad.ApplicationServices;
using acApp = Bricscad.ApplicationServices.Application;
#elif GCAD
using GrxCAD.DatabaseServices;
using acApp = GrxCAD.ApplicationServices.Application;
#endif

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CadApiHelper.Dictionary
{
    /// <summary>
    /// ChDictionary
    /// </summary>
    public class ChDictionary
    {
        #region Private Fields

        private string dictionaryName;
        private ObjectId dictObjectId = new ObjectId();
        private Database db = null;

        #endregion Private Fields

        #region Public Properties

        /// <summary>
        /// Gets or sets the name of the dictionary.
        /// </summary>
        /// <value>
        /// The name of the dictionary.
        /// </value>
        public string DictionaryName
        {
            get
            {
                return dictionaryName;
            }
            set
            {
                dictionaryName = value;
                CreateDictionary();
            }
        }

        /// <summary>
        /// Gets or sets the dictionary object identifier.
        /// </summary>
        /// <value>
        /// The dictionary object identifier.
        /// </value>
        public ObjectId DictObjectId
        {
            get { return dictObjectId; }
            set { dictObjectId = value; }
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="ChDictionary"/> class.
        /// </summary>
        /// <param name="dictName">Name of the dictionary.</param>
        /// <param name="database">The database.</param>
        public ChDictionary(string dictName, Database database = null)
        {
            dictionaryName = dictName;
            if (database == null)
                database = acApp.DocumentManager.MdiActiveDocument.Database;
            db = database;
            CreateDictionary();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChDictionary"/> class.
        /// </summary>
        /// <param name="objId">The object identifier.</param>
        public ChDictionary(ObjectId objId)
        {
            dictObjectId = objId;
            db = objId.Database;
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Adds the xrecord.
        /// </summary>
        /// <param name="xrecKey">The xrec key.</param>
        /// <param name="rb">The rb.</param>
        /// <returns>True if xrecord was added</returns>
        public bool AddXrecord(string xrecKey, ResultBuffer rb)
        {
            if (dictObjectId.IsNull)
                return false;

            if (rb == null)
                return false;

            ObjectId id;
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    DBDictionary dict = acTrans.GetObject(dictObjectId, OpenMode.ForWrite, true) as DBDictionary;
                    Xrecord xrec = new Xrecord();
                    xrec.Data = rb;
                    id = dict.SetAt(xrecKey, xrec);
                    acTrans.AddNewlyCreatedDBObject(xrec, true);
                    acTrans.Commit();
                }
                catch (System.Exception ex)
                {
                    Debug.Print("Class:{0} Message:{1}", this.GetType(), ex.Message);
                    return false;
                }
            }
            return CheckResult(id);
        }

        /// <summary>
        /// Adds the xrecord.
        /// </summary>
        /// <param name="rb">The rb.</param>
        /// <returns>True if xrecord was added</returns>
        public bool AddXrecord(ResultBuffer rb)
        {
            return AddXrecord(DateTime.Now.Ticks.ToString(), rb);
        }

        /// <summary>
        /// Deletes all x records.
        /// </summary>
        public void DelAllXRecords()
        {
            if (dictObjectId.IsNull)
                return;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    DBDictionary dict = acTrans.GetObject(dictObjectId, OpenMode.ForWrite) as DBDictionary;
                    using (DbDictionaryEnumerator interator = dict.GetEnumerator())
                    {
                        while (interator.MoveNext())
                        {
                            Xrecord xrec = acTrans.GetObject(interator.Value, OpenMode.ForWrite) as Xrecord;
                            if (xrec != null)
                                xrec.Erase();
                        }
                    }
                    acTrans.Commit();
                }
                catch (Exception ex)
                {
                    Debug.Print("Classe:{0} Mensagem:{1}", this.GetType(), ex.Message);
                    acTrans.Abort();
                }
            }
        }

        /// <summary>
        /// Deletes the xrecord by xreckey.
        /// </summary>
        /// <param name="xrecKey">The xrec key.</param>
        /// <returns></returns>
        public bool DelXRecord(string xrecKey)
        {
            if (dictObjectId.IsNull)
                return false;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    DBDictionary dict = acTrans.GetObject(dictObjectId, OpenMode.ForWrite) as DBDictionary;
                    dict.Remove(xrecKey);
                    acTrans.Commit();
                }
                catch (System.Exception ex)
                {
                    Debug.Print("Class:{0} Message:{1}", this.GetType(), ex.Message);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Gets all xrecord keys.
        /// </summary>
        /// <returns>List of keys</returns>
        public List<string> GetAllXRecKeys()
        {
            if (dictObjectId.IsNull)
                return null;

            List<string> lista = new List<string>();
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    List<ResultBuffer> rb = new List<ResultBuffer>();
                    DBDictionary dict = acTrans.GetObject(dictObjectId, OpenMode.ForRead, true) as DBDictionary;
                    using (DbDictionaryEnumerator interator = dict.GetEnumerator())
                    {
                        while (interator.MoveNext())
                        {
                            lista.Add(interator.Key);
                        }
                    }
                    acTrans.Commit();
                }
                catch (Exception ex)
                {
                    Debug.Print("Classe:{0} Mensagem:{1}", this.GetType(), ex.Message);
                    acTrans.Abort();
                    return lista;
                }
            }
            return lista;
        }

        /// <summary>
        /// Gets the x record.
        /// </summary>
        /// <param name="xrecKey">The xrec key.</param>
        /// <returns>ResultBuffer</returns>
        public ResultBuffer GetXRecord(string xrecKey)
        {
            if (dictObjectId.IsNull)
                return null;

            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    DBDictionary dict = acTrans.GetObject(dictObjectId, OpenMode.ForRead, true) as DBDictionary;
                    Xrecord xrec = dict.GetAt(xrecKey).GetObject(OpenMode.ForRead, true) as Xrecord;
                    acTrans.Commit();

                    if (xrec == null)
                        return null;
                    return xrec.Data;
                }
                catch (System.Exception ex)
                {
                    Debug.Print("Classe:{0} Mensagem:{1}", this.GetType(), ex.Message);
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets all x records.
        /// </summary>
        /// <returns>List<ResultBuffer></returns>
        public List<ResultBuffer> GetAllXRecords()
        {
            if (dictObjectId.IsNull)
                return null;
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    List<ResultBuffer> rb = new List<ResultBuffer>();
                    DBDictionary dict = acTrans.GetObject(dictObjectId, OpenMode.ForRead, true) as DBDictionary;
                    using (DbDictionaryEnumerator interator = dict.GetEnumerator())
                    {
                        while (interator.MoveNext())
                        {
                            Xrecord xrec = acTrans.GetObject(interator.Value, OpenMode.ForRead, true) as Xrecord;
                            if (xrec != null)
                                rb.Add(xrec.Data);
                        }
                    }
                    acTrans.Commit();
                    return rb;
                }
                catch (Exception)
                {
                    acTrans.Abort();
                    return null;
                }
            }
        }

        #endregion Public Methods

        #region Private Methods

        private bool CheckResult(ObjectId id)
        {
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                try
                {
                    Xrecord dict = acTrans.GetObject(id, OpenMode.ForRead, true) as Xrecord;
                    if (dict != null && dict.Data != null)
                    {
                        return true;
                    }
                }
                catch (System.Exception ex)
                {
                    Debug.Print("Classe:{0} Mensagem:{1}", this.GetType(), ex.Message);
                    return false;
                }
            }
            return false;
        }

        /// <summary>
        /// Cria um dicionario
        /// </summary>
        /// <param name="dictName">Nome do dicionario</param>
        private void CreateDictionary()
        {
            using (Transaction acTrans = db.TransactionManager.StartTransaction())
            {
                //if (acApp.DocumentManager.MdiActiveDocument.LockMode() == ZwSoft.ZwCAD.ApplicationServices.DocumentLockMode.)
                DBDictionary dicts = acTrans.GetObject(db.NamedObjectsDictionaryId, OpenMode.ForWrite) as DBDictionary;
                DBDictionary dict;

                try
                {
                    dict = acTrans.GetObject(dicts.GetAt(dictionaryName), OpenMode.ForWrite) as DBDictionary;
                    dictObjectId = dict.ObjectId;
                }
                catch (Exception)
                {
                    dict = new DBDictionary();
                    dictObjectId = dicts.SetAt(dictionaryName, dict);
                    acTrans.AddNewlyCreatedDBObject(dict, true);
                    acTrans.Commit();
                }
            }
        }

        #endregion Private Methods
    }
}
