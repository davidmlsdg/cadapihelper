﻿#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
using Teigha.DatabaseServices;
#elif GCAD
using GrxCAD.DatabaseServices;
#endif

using System;
using System.IO;

namespace CadApiHelper.Dictionary
{
    /// <summary>
    /// ChHelperSerializable
    /// </summary>
    public static class ChHelperSerializable
    {
        #region Private Fields

        private const int kMaxChunkSize = 127;

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// ResultBuffer to stream.
        /// </summary>
        /// <param name="resBuf">The resource buf.</param>
        /// <returns>MemoryStream</returns>
        public static MemoryStream ResBufToStream(ResultBuffer resBuf)
        {
            MemoryStream ms = new MemoryStream();
            TypedValue[] values = resBuf.AsArray();

            // Start from 1 to skip application name

            for (int i = 0; i < values.Length; i++)
            {
                byte[] datachunk = (byte[])values[i].Value;
                ms.Write(datachunk, 0, datachunk.Length);
            }
            ms.Position = 0;

            return ms;
        }

        /// <summary>
        /// Streams to ResultBuffer.
        /// </summary>
        /// <param name="memoryStream">The memory stream.</param>
        /// <returns>ResultBuffer</returns>
        public static ResultBuffer StreamToResBuf(MemoryStream memoryStream)
        {
            ResultBuffer resBuf = new ResultBuffer();

            for (int i = 0; i < memoryStream.Length; i += kMaxChunkSize)
            {
                int length = (int)Math.Min(memoryStream.Length - i, kMaxChunkSize);
                byte[] datachunk = new byte[length];
                memoryStream.Read(datachunk, 0, length);
                resBuf.Add(
                  new TypedValue(
                    (int)DxfCode.ExtendedDataBinaryChunk, datachunk));
            }

            return resBuf;
        }

        #endregion Public Methods
    }
}
