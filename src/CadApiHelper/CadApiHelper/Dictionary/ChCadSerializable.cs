﻿#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
#elif ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
    using exCadException = Teigha.Runtime.Exception;
    using Teigha.DatabaseServices;
    using acApp = Bricscad.ApplicationServices.Application;
#elif GCAD
    using GrxCAD.DatabaseServices;
    using CadException = GrxCAD.Runtime.Exception;
#endif

using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Diagnostics;
using CadApiHelper.Core;
using System.Reflection;
using CadApiHelper.Entities;

namespace CadApiHelper.Dictionary
{
    /// <summary>
    /// Serialize on the Cad
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ChCadSerializable<T> where T : class
    {
        #region Public Methods

        /// <summary>
        /// Gets the instance from dictionary by xrecord.
        /// </summary>
        /// <param name="dict">The dictionary.</param>
        /// <param name="xrecKey">The xrec key.</param>
        /// <param name="binder">The binder.</param>
        /// <returns>T instance or null</returns>
        public T GetInstanceFromDictionary(ChDictionary dict, string xrecKey, SerializationBinder binder = null)
        {
            var rb = dict.GetXRecord(xrecKey);
            if (rb != null)
                return GetInstanceFromResBuf(rb, binder);

            return null;
        }

        /// <summary>
        /// Gets the instance from dictionary br xrecord.
        /// </summary>
        /// <param name="dictionaryName">Name of the dictionary.</param>
        /// <param name="xrecKey">The xrec key.</param>
        /// <param name="db">The database.</param>
        /// <param name="binder">The binder.</param>
        /// <returns>T instance or null</returns>
        public T GetInstanceFromDictionary(string dictionaryName, string xrecKey, Database db = null, SerializationBinder binder = null)
        {
            var dict = new ChDictionary(dictionaryName, db);
            return GetInstanceFromDictionary(dict, xrecKey, binder);
        }

        /// <summary>
        /// Cria uma instancia de uma classe T a partir do ObjectId do dicionario
        /// </summary>
        /// <param name="dictId">Objectid do dicionario</param>
        /// <param name="xrecKey">Chave do Xrecord</param>
        /// <param name="binder">Binder para localização da classe</param>
        /// <returns>Retorna uma instancia de uma classe T ou null</returns>
        public T GetInstanceFromDictionary(ChObjectId dictId, string xrecKey, SerializationBinder binder = null)
        {
            var dict = new ChDictionary(dictId);
            return GetInstanceFromDictionary(dict, xrecKey, binder);
        }

        /// <summary>
        /// Gets the list instance from dictionary.
        /// </summary>
        /// <param name="dict">The dictionary.</param>
        /// <returns>List of T instance</returns>
        public List<T> GetListInstanceFromDictionary(ChDictionary dict)
        {
            List<ResultBuffer> rbs = dict.GetAllXRecords();
            List<T> lista = new List<T>();

            for (int i = 0; i < rbs.Count; i++)
            {
                T t = null;
                if (rbs[i] == null)
                    continue;

                object o = GetInstanceFromResBuf(rbs[i]);
                t = o as T;
                if (t != null)
                    lista.Add(t);
            }

            return lista;
        }

        /// <summary>
        /// Gets the list instance from dictionary.
        /// </summary>
        /// <param name="dictionaryName">Name of the dictionary.</param>
        /// <returns>List of T instance</returns>
        public List<T> GetListInstanceFromDictionary(string dictionaryName)
        {
            var dict = new ChDictionary(dictionaryName);
            return GetListInstanceFromDictionary(dict);
        }

        /// <summary>
        /// Gets the list instance from dictionary.
        /// </summary>
        /// <param name="dictId">The dictionary identifier.</param>
        /// <returns>List of T instance</returns>
        public List<T> GetListInstanceFromDictionary(ObjectId dictId)
        {
            var dict = new ChDictionary(dictId);
            return GetListInstanceFromDictionary(dict);
        }

        /// <summary>
        /// Gets the instance from resource buf.
        /// </summary>
        /// <param name="resBuf">The resource buf.</param>
        /// <param name="binder">The binder.</param>
        /// <returns>T instance or null</returns>
        public T GetInstanceFromResBuf(ResultBuffer resBuf, SerializationBinder binder = null)
        {
            var bf = new BinaryFormatter();
            if (binder != null)
                bf.Binder = binder;

            if (resBuf == null || resBuf.AsArray().Length == 0)
                return null;

            try
            {
                var ms = ChHelperSerializable.ResBufToStream(resBuf);
                return bf.Deserialize(ms) as T;
            }
            catch (Exception e)
            {
                Debug.Print("TsCadSerializable - Mensagem:{0}", e.Message);
                return null;
            }
        }

        /// <summary>
        /// Gets the instance from xdata.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="entId">The entity identifier.</param>
        /// <returns></returns>
        public T GetInstanceFromXData(string appName, ObjectId entId)
        {
            var rb = ChEntity.GetXdata(entId, appName);

            if (rb == null)
                return null;
            var rbNew = new ResultBuffer();
            foreach (TypedValue item in rb)
            {
                if (item.TypeCode != 1001)
                    rbNew.Add(item);
            }
            return GetInstanceFromResBuf(rbNew);
        }

        /// <summary>
        /// Adds to dictionary.
        /// </summary>
        /// <param name="dict">The dictionary.</param>
        /// <param name="xrecKey">The xrec key.</param>
        /// <param name="t">The instance to add.</param>
        /// <returns>True if the instance was added</returns>
        public bool AddToDictionary(ChDictionary dict, string xrecKey, T t)
        {
            var rb = ConvertToResBuf(t);
            if (rb != null)
            {
                dict.AddXrecord(xrecKey, rb);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Adds the range to dictionary.
        /// </summary>
        /// <param name="dict">The dictionary.</param>
        /// <param name="lista">The lista.</param>
        /// <returns>True if the list of instance was added</returns>
        public bool AddRangeToDictionary(ChDictionary dict, List<T> lista)
        {
            for (int i = 0; i < lista.Count; i++)
            {
                var rb = ConvertToResBuf(lista[i]);
                if (rb != null)
                    dict.AddXrecord(i.ToString(), rb);
            }

            return true;
        }

        /// <summary>
        /// Adds to dictionary.
        /// </summary>
        /// <param name="dictionaryName">Name of the dictionary.</param>
        /// <param name="xrecKey">The xrec key.</param>
        /// <param name="t">The t.</param>
        /// <param name="db">The database.</param>
        /// <returns>True if the instance was added</returns>
        public bool AddToDictionary(string dictionaryName, string xrecKey, T t, Database db = null)
        {
            var dict = new ChDictionary(dictionaryName, db);
            return AddToDictionary(dict, xrecKey, t);
        }

        /// <summary>
        /// Sets to x data.
        /// </summary>
        /// <param name="appName">Name of the application.</param>
        /// <param name="id">The identifier.</param>
        /// <param name="t">The instance to set in xdata.</param>
        /// <returns>True if the instance was added</returns>
        public bool SetToXData(string appName, ObjectId id, T t)
        {
            var rb = ConvertToResBuf(t);
            if (rb == null)
                return false;

            var rbNew = new ResultBuffer();
            rbNew.Add(new TypedValue(1001, appName));
            foreach (TypedValue item in rb)
                rbNew.Add(item);

            return ChEntity.SetXdata(id, appName, rbNew.AsArray());
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Converts instance T to ResultBuffer.
        /// </summary>
        /// <param name="t">The t.</param>
        /// <returns>ResultBuffer</returns>
        private ResultBuffer ConvertToResBuf(T t)
        {
            var bf = new BinaryFormatter();
            var ms = new MemoryStream();
            bf.Serialize(ms, t);
            ms.Position = 0;

            var resBuf = ChHelperSerializable.StreamToResBuf(ms);

            return resBuf;
        }

        #endregion Private Methods
    }
}
