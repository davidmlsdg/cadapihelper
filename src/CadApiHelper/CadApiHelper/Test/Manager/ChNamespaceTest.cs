﻿using CadApiHelper.Test.Ui;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Windows.Media;

namespace CadApiHelper.Test.Manager
{
    public class ChNamespaceTest : ModelViewBase
    {
        private string nameSpace;
        private SolidColorBrush colorStatus;
        private bool passed;

        public bool Passsed
        {
            get { return passed; }
            set
            {
                SetField(ref passed, value, "Passsed");
                ColorStatus = new SolidColorBrush(value ? Colors.Green : Colors.Red);
            }
        }

        public SolidColorBrush ColorStatus
        {
            get { return colorStatus; }
            set { SetField(ref colorStatus, value, "ColorStatus"); }
        }
        public ObservableCollection<ChClassTest> Classes { get; set; }

        public string NameSpace { get => nameSpace; set => SetField(ref nameSpace, value, "NameSpace"); }

        public ChNamespaceTest(string name, Type classType, IEnumerable<MethodInfo> methods)
        {
            NameSpace = name;
            Classes = new ObservableCollection<ChClassTest> { new ChClassTest(classType, methods) };
        }

        public int GetFailTests()
        {
            var result = 0;
            foreach (var cl in Classes)
            {
                result += cl.Methods.Count(x => x.Passed == false);
            }
            return result;
        }

        public int GetPassedTests()
        {
            var result = 0;
            foreach (var cl in Classes)
            {
                result += cl.Methods.Count(x => x.Passed == true);
            }
            return result;
        }

        public int GetTotalTests()
        {
            var result = 0;
            foreach (var cl in Classes)
            {
                result += cl.Methods.Count;
            }
            return result;
        }
    }
}