﻿using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Exceptions;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace CadApiHelper.Test.Manager
{
    /// <summary>
    ///
    /// </summary>
    public class ChUnitTestManager
    {
        public Assembly Assembly { get; }

        public ChNamespacesUnitTest NamespacesUnitTest { get; private set; }

        public ChUnitTestManager(Assembly assembly)
        {
            Assembly = assembly;
        }

        public static void ExecuteMethod(Type classType, ChMethodTest methodTest, bool debugger = false)
        {
            object item = Activator.CreateInstance(classType);
            try
            {
                methodTest.Passed = false;
                if (debugger)
                {
                    var prs = classType.GetProperties();
                    var pr = prs.FirstOrDefault(x => x.Name == "BreakDebugger");
                    if (pr != null)
                        pr.SetValue(item, true, new object[] { });
                }
                System.Diagnostics.Debugger.Launch();
                object ret = methodTest.Method.Invoke(item, new object[] { });
                if (debugger)
                {
                    var prs = classType.GetProperties();
                    var pr = prs.FirstOrDefault(x => x.Name == "BreakDebugger");
                    if (pr != null)
                        pr.SetValue(item, false, new object[] { });
                }
                methodTest.Passed = true;
            }
            catch (ChBaseAssertException bex)
            {
                methodTest.Message = bex.Message;
                methodTest.Trace = bex.StackTrace;
                methodTest.BaseAssertMessage = bex.BaseAssertMessage;
            }
            catch (StackOverflowException sofe)
            {
                methodTest.Message = sofe.Message;
            }
            catch (Exception ex)
            {
                if (ex.InnerException is ChBaseAssertException)
                {
                    var bex = ex.InnerException as ChBaseAssertException;
                    methodTest.Message = bex.Message;
                    methodTest.Trace = bex.StackTrace;
                    methodTest.BaseAssertMessage = bex.BaseAssertMessage;
                }
                else
                {
                    methodTest.Message = ex.Message;
                    methodTest.Trace = ex.StackTrace;
                }
            }
        }

        public void Execute()
        {
            foreach (var ns in NamespacesUnitTest.NamespaceTests)
            {
                foreach (var unitTest in ns.Classes)
                {
                    ;
                    foreach (var methodTest in unitTest.Methods)
                    {
                        ExecuteMethod(unitTest.ClassType, methodTest);
                    }
                }
            }
        }

        public void GetClassTest()
        {
            NamespacesUnitTest = new ChNamespacesUnitTest();
            var types = Assembly.GetTypes();
            foreach (var type in types)
            {
                var methods = GetMethods(type);
                if (methods == null || methods.Count == 0)
                    continue;
                NamespacesUnitTest.Add(type.Namespace, type, methods);
            }
        }

        private List<MethodInfo> GetMethods(Type type)
        {
            var list = new List<MethodInfo>();

            foreach (var m in type.GetMethods())
            {
                if (m.IsPublic && m.GetCustomAttributes(typeof(ChFactAttribute), true).Any())
                    list.Add(m);
            }
            return list;
        }
    }
}