﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;

namespace CadApiHelper.Test.Manager
{
    public class ChNamespacesUnitTest
    {
        public ObservableCollection<ChNamespaceTest> NamespaceTests { get; set; }

        public ChNamespacesUnitTest()
        {
            NamespaceTests = new ObservableCollection<ChNamespaceTest>();
        }

        internal void Add(string @namespace, Type classType, IEnumerable<MethodInfo> methods)
        {
            var ns = NamespaceTests.FirstOrDefault(x => x.NameSpace == @namespace);
            if (ns == null)
            {
                NamespaceTests.Add(new ChNamespaceTest(@namespace, classType, methods));
                return;
            }
            var cl = ns.Classes.FirstOrDefault(x => x.Name == classType.Name);
            if (cl == null)
            {
                ns.Classes.Add(new ChClassTest(classType, methods));
                return;
            }
        }

        internal int GetTotalTests()
        {
            var result = 0;
            foreach (var ns in NamespaceTests)
            {
                result += ns.GetTotalTests();
            }
            return result;
        }
    }
}