﻿using CadApiHelper.Test.Ui;

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows.Media;

namespace CadApiHelper.Test.Manager
{
    public class ChClassTest : ModelViewBase
    {
        private Type classType;
        private string name;
        private SolidColorBrush colorStatus;
        private bool passed;

        public bool Passed
        {
            get { return passed; }
            set
            {
                SetField(ref passed, value, "Passed");
                ColorStatus = new SolidColorBrush(value ? Colors.Green : Colors.Red);
            }
        }

        public SolidColorBrush ColorStatus
        {
            get { return colorStatus; }
            set { SetField(ref colorStatus, value, "ColorStatus"); }
        }

        public Type ClassType { get => classType; set => SetField(ref classType, value, "ClassType"); }

        public ObservableCollection<ChMethodTest> Methods { get; set; }

        public string Name { get => name; set => SetField(ref name, value, "Name"); }

        public ChClassTest(Type classType, IEnumerable<MethodInfo> methods)
        {
            ClassType = classType;
            Methods = new ObservableCollection<ChMethodTest>();
            foreach (MethodInfo method in methods)
            {
                Methods.Add(new ChMethodTest(method.Name, method, classType));
            }
            Name = classType.Name;
        }
    }
}