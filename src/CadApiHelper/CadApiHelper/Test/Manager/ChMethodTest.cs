﻿using CadApiHelper.Test.Ui;

using System;
using System.Reflection;
using System.Windows.Media;

namespace CadApiHelper.Test.Manager
{
    public class ChMethodTest : ModelViewBase
    {
        private string baseAssertMessage;

        private string message;
        private MethodInfo method;
        private string name;
        private bool passed;
        private string trace;

        private Type classType;

        public Type ClassType
        {
            get { return classType; }
            set
            {
                SetField(ref classType, value, "ClassType");
            }
        }


        public string BaseAssertMessage
        {
            get => baseAssertMessage;
            set => SetField(ref baseAssertMessage, value, "BaseAssertMessage");
        }

        private SolidColorBrush colorStatus;
        public SolidColorBrush ColorStatus
        {
            get { return colorStatus; }
            set { SetField(ref colorStatus, value, "ColorStatus"); }
        }

        public string Message { get => message; set => SetField(ref message, value, "Message"); }

        public MethodInfo Method
        {
            get => method; internal set => SetField(ref method, value, "Method");
        }

        public string Name
        {
            get
            {
                if (name.StartsWith("Void ", System.StringComparison.OrdinalIgnoreCase))
                    return name.Replace("Void ", "");
                return name;
            }
            set
            {
                SetField(ref name, value, "Name");
            }
        }

        public bool Passed
        {
            get
            {
                return passed;
            }
            set
            {
                SetField(ref passed, value, "Passed");
                ColorStatus = new SolidColorBrush(value ? Colors.Green : Colors.Red);
            }
        }

        public string Trace { get => trace; set => SetField(ref trace, value, "Trace"); }

        public ChMethodTest(string name, MethodInfo method, Type classType)
        {
            Name = name;
            Method = method;
            ClassType = classType;
        }
    }
}