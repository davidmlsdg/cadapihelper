using CadApiHelper.Test.Exceptions;

using System;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that a value is within a given range.
        /// </summary>
        /// <param name="actual">The actual value to be evaluated</param>
        /// <param name="low">The (inclusive) low value of the range</param>
        /// <param name="high">The (inclusive) high value of the range</param>
        /// <param name="userMessage">User message</param>
        public static void InRange<T>(T actual, T low, T high, string userMessage = "")
            where T : IComparable
        {
            if (actual.CompareTo(low) < 0 && actual.CompareTo(high) > 0)
                throw new ChBaseAssertException(actual.ToString(), $"Value in {low} and {high}",
                    GetNonNullString(userMessage), "Assert.InRange");
        }

        /// <summary>
		/// Verifies that a value is not within a given range, using the default comparer.
		/// </summary>
		/// <typeparam name="T">The type of the value to be compared</typeparam>
		/// <param name="actual">The actual value to be evaluated</param>
		/// <param name="low">The (inclusive) low value of the range</param>
		/// <param name="high">The (inclusive) high value of the range</param>
        /// <param name="userMessage">User message</param>
		public static void NotInRange<T>(T actual, T low, T high, string userMessage = "")
            where T : IComparable
        {
            if (actual.CompareTo(low) >= 0 && actual.CompareTo(high) <= 0)
                throw new ChBaseAssertException(actual.ToString(), $"Value in {low} and {high}",
                    GetNonNullString(userMessage), "Assert.NotInRange");
        }
    }
}