﻿using CadApiHelper.Geometry;
using Xunit;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Xunit
{
    public partial class Assert
    {

        /// <summary>
        /// Verifies that the given collection is equal
        /// another given collection and equal elements
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="collection2">Another collection.</param>
        /// <param name="tolerance">Tolerance</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static void CollectionContains(IEnumerable<IChPoint> collection, IEnumerable<IChPoint> collection2, double tolerance = 0, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(collection2), collection2);

            foreach (var item in collection)
            {
                if (!collection2.Any(x => x.Equals(item, tolerance)))
                    throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");
            }
        }

        /// <summary>
        /// Verifies that the given collection is equal
        /// another given collection and equal elements
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="collection2">Another collection.</param>
        /// <param name="tolerance">Tolerance</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static void Equal(IEnumerable<IChPoint> collection, IEnumerable<IChPoint> collection2, double tolerance = 0, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(collection2), collection2);

            if (collection.Count() != collection2.Count())
                throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");

            foreach (var item in collection)
            {
                if (!collection2.Any(x => x.Equals(item, tolerance)))
                    throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");
            }
        }
    }
}