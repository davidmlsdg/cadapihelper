using CadApiHelper.Test.Exceptions;

using System;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that an object is exactly the given type or a derived type
        /// </summary>
        /// <param name="expectedType"></param>
        /// <param name="object"></param>
        /// <param name="userMessage">The user message</param>
        public static void IsAssignableFrom(Type expectedType, object @object, string userMessage = "")
        {
            if (@object == null)
                throw new ArgumentNullException("@object");

            var objectType = @object.GetType();
            if (!expectedType.IsAssignableFrom(objectType))
                throw new ChBaseAssertException(objectType.Name, expectedType.Name,
                    GetNonNullString(userMessage), "Assert.IsAssignableFrom");
        }

        /// <summary>
        /// Verifies that an object is not exactly the given type.
        /// </summary>
        /// <param name="expectedType">The type the object should not be</param>
        /// <param name="object">The object to be evaluated</param>
        /// <param name="userMessage">The user message</param>
        public static void IsNotType(Type expectedType, object @object, string userMessage = "")
        {
            var objectType = @object.GetType();
            if (@object != null && expectedType.Equals(objectType))
                throw new ChBaseAssertException(objectType.Name, expectedType.Name,
                    GetNonNullString(userMessage), "Assert.IsNotType");
        }

        /// <summary>
        /// Verifies that an object is exactly the given type.
        /// </summary>
        /// <param name="expectedType">The type the object should be</param>
        /// <param name="object">The object to be evalueted</param>
        /// <param name="userMessage">The user message</param>
        public static void IsType(Type expectedType, object @object, string userMessage = "")
        {
            if (@object == null)
                throw new ArgumentNullException("@object");

            var objectType = @object.GetType();
            if (!expectedType.Equals(objectType))
                throw new ChBaseAssertException(objectType.Name, expectedType.Name,
                    GetNonNullString(userMessage), "Assert.IsType");
        }
    }
}