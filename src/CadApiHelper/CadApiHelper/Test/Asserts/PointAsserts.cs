using CadApiHelper.Geometry;
using System;
using System.Text.RegularExpressions;

namespace CadApiHelper.Test.Asserts
{
    public partial class Assert
    {

                /// <summary>
        /// Verifies that two points are equal
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string to be inspected</param>
        /// <param name="tolerance">The tolerance parameter for validate the comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void Equal(IChPoint expected, IChPoint actual, double tolerance = 0, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(expected), expected);
            GuardArgumentNotNull(nameof(actual), actual);

            if (!actual.Equals(expected, tolerance))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

    }
}