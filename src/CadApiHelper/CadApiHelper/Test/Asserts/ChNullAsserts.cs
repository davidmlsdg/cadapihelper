using CadApiHelper.Test.Exceptions;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that an object reference is not null
        /// </summary>
        /// <param name="obj">The object to be validate</param>
        /// <param name="userMessage">User Message</param>
        public static void NotNull(object obj, string userMessage = "")
        {
            if (obj == null)
                throw new ChBaseAssertException("null", "not null",
                    GetNonNullString(userMessage), "Assert.NotNull");
        }

        /// <summary>
        /// Verifies that an object reference is null
        /// </summary>
        /// <param name="obj">The object to be validate</param>
        /// <param name="userMessage">User Message</param>
        public static void Null(object obj, string userMessage = "")
        {
            if (obj != null)
                throw new ChBaseAssertException("not null", "null",
                    GetNonNullString(userMessage), "Assert.Null");
        }
    }
}