using CadApiHelper.Test.Exceptions;

using System;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that two objects are equal, using equalityComparer
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        /// <typeparam name="T">The type of the object to be compared</typeparam>
        public static void Equal<T>(T expected, T actual, string userMessage = "")
            where T : IEquatable<T>
        {
            if (expected == null && actual == null)
                return;

            if (expected == null && actual != null)
                throw new ChBaseAssertException(actual.ToString(), "null",
                    GetNonNullString(userMessage), "Assert.Equal");

            if (expected != null && actual == null)
                throw new ChBaseAssertException("null", expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");

            if (!actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two objects are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(object expected, object actual, string userMessage = "")
        {
            if (expected == null && actual == null)
                return;

            if (expected == null && actual != null)
                throw new ChBaseAssertException(actual.ToString(), "null",
                    GetNonNullString(userMessage), "Assert.Equal");

            if (expected != null && actual == null)
                throw new ChBaseAssertException("null", expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");

            if (!actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two <see cref="int"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(int expected, int actual, string userMessage = "")
        {
            if (!actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two <see cref="double"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(double expected, double actual, string userMessage = "")
        {
            Equal(expected, actual, 0, userMessage);
        }

        /// <summary>
        /// Verifies that two <see cref="double"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="tolerance">The allowed difference between values</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(double expected, double actual, double tolerance, string userMessage = "")
        {
            if (double.IsNaN(tolerance) || double.IsNaN(expected) || double.IsNaN(actual))
                throw new ArgumentException("Tolerance, expected and actual values must be a valid number", nameof(tolerance));

            if (Math.Abs(expected - actual) > tolerance)
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two <see cref="float"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(float expected, float actual, string userMessage = "")
        {
            Equal(expected, actual, 0, userMessage);
        }

        /// <summary>
        /// Verifies that two <see cref="float"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="tolerance">The allowed difference between values</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(float expected, float actual, float tolerance, string userMessage = "")
        {
            if (float.IsNaN(tolerance) || float.IsNaN(expected) || float.IsNaN(actual))
                throw new ArgumentException("Tolerance, expected and actual values must be a valid number", nameof(tolerance));

            if (Math.Abs(expected - actual) > tolerance)
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two <see cref="short"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(short expected, short actual, string userMessage = "")
        {
            if (!actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two <see cref="decimal"/> values are equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="tolerance">The allowed difference between values</param>
        /// <param name="userMessage">User message</param>
        public static void Equal(decimal expected, decimal actual, string userMessage = "")
        {
            if (!actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two objects are not equal, using equalityComparer
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        /// <typeparam name="T">The type of the object to be compared</typeparam>
        public static void NotEqual<T>(T expected, T actual, string userMessage = "")
            where T : IEquatable<T>
        {
            if (expected == null && actual == null)
                return;

            if (expected == null && actual != null)
                throw new ChBaseAssertException(actual.ToString(), "null",
                    GetNonNullString(userMessage), "Assert.NotEqual");

            if (expected != null && actual == null)
                throw new ChBaseAssertException("null", expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");

            if (actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that two objects are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(object expected, object actual, string userMessage = "")
        {
            if (expected == null && actual == null)
                return;

            if (expected == null && actual != null)
                throw new ChBaseAssertException(actual.ToString(), "null",
                    GetNonNullString(userMessage), "Assert.NotEqual");

            if (expected != null && actual == null)
                throw new ChBaseAssertException("null", expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");

            if (actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that two <see cref="int"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(int expected, int actual, string userMessage = "")
        {
            if (actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that two <see cref="double"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(double expected, double actual, string userMessage = "")
        {
            Equal(expected, actual, 0, userMessage);
        }

        /// <summary>
        /// Verifies that two <see cref="double"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="tolerance">The allowed difference between values</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(double expected, double actual, double tolerance, string userMessage = "")
        {
            if (double.IsNaN(tolerance) || double.IsNaN(expected) || double.IsNaN(actual))
                throw new ArgumentException("Tolerance, expected and actual values must be a valid number", nameof(tolerance));

            if (Math.Abs(expected - actual) <= tolerance)
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that two <see cref="float"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(float expected, float actual, string userMessage = "")
        {
            Equal(expected, actual, 0, userMessage);
        }

        /// <summary>
        /// Verifies that two <see cref="float"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="tolerance">The allowed difference between values</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(float expected, float actual, float tolerance, string userMessage = "")
        {
            if (float.IsNaN(tolerance) || float.IsNaN(expected) || float.IsNaN(actual))
                throw new ArgumentException("Tolerance, expected and actual values must be a valid number", nameof(tolerance));

            if (Math.Abs(expected - actual) <= tolerance)
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that two <see cref="short"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(short expected, short actual, string userMessage = "")
        {
            if (actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that two <see cref="decimal"/> values are not equal,
        /// </summary>
        /// <param name="expected">The expected value</param>
        /// <param name="actual">The value to compared with expected value</param>
        /// <param name="userMessage">User message</param>
        public static void NotEqual(decimal expected, decimal actual, string userMessage = "")
        {
            if (actual.Equals(expected))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }
    }
}