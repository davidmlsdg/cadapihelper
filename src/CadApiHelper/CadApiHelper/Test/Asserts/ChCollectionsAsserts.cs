﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Exceptions;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that all items in the collection pass when executed against
        /// action.
        /// </summary>
        /// <typeparam name="T">The type of the object to be verified</typeparam>
        /// <param name="collection">The collection</param>
        /// <param name="action">The action to test each item against</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the collection contains at least one non-matching element</exception>
        public static void All<T>(IEnumerable<T> collection, Action<T> action, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(action), action);

            All(collection, (item, index) => action(item), userMessage);
        }

        /// <summary>
        /// Verifies that all items in the collection pass when executed against
        /// action. The item index is provided to the action, in addition to the item.
        /// </summary>
        /// <typeparam name="T">The type of the object to be verified</typeparam>
        /// <param name="collection">The collection</param>
        /// <param name="action">The action to test each item against</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the collection contains at least one non-matching element</exception>
        public static void All<T>(IEnumerable<T> collection, Action<T, int> action, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(action), action);

            var errors = new Stack<Tuple<int, object, Exception>>();
            var idx = 0;

            foreach (var item in collection)
            {
                try
                {
                    action(item, idx);
                }
                catch (Exception ex)
                {
                    errors.Push(new Tuple<int, object, Exception>(idx, item, ex));
                }

                ++idx;
            }

            if (errors.Count > 0)
                throw ChBaseAssertException.AllException(idx, errors.ToArray().ToString(), userMessage, "Assert.All");
        }

        /// <summary>
        /// Verifies that a collection contains exactly a given number of elements, which meet
        /// the criteria provided by the element inspectors.
        /// </summary>
        /// <typeparam name="T">The type of the object to be verified</typeparam>
        /// <param name="collection">The collection to be inspected</param>
        /// <param name="elementInspectors">The element inspectors, which inspect each element in turn. The
        /// total number of element inspectors must exactly match the number of elements in the collection.</param>
        public static void Collection<T>(IEnumerable<T> collection, params Action<T>[] elementInspectors)
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(elementInspectors), elementInspectors);

            var elements = collection.ToArray();
            var expectedCount = elementInspectors.Length;
            var actualCount = elements.Length;

            if (expectedCount != actualCount)
                throw ChBaseAssertException.CollectionException(expectedCount, actualCount);

            for (var idx = 0; idx < actualCount; idx++)
            {
                try
                {
                    elementInspectors[idx](elements[idx]);
                }
                catch (Exception)
                {
                    throw ChBaseAssertException.CollectionException(expectedCount, actualCount);
                }
            }
        }

        /// <summary>
        /// Verifies that the given collection contains a
        /// element of the given type which matches the given predicate. The
        /// collection may or may not contain other values which do not
        /// match the given predicate.
        /// </summary>
        /// <typeparam name="T">The collection type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="predicate">The item matching predicate.</param>
        /// <param name="userMessage">User Message</param>
        /// <returns>The single item in the filtered collection.</returns>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static T Contains<T>(IEnumerable<T> collection, Predicate<T> predicate, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(predicate), predicate);

            return Contains(collection, predicate, "(filter expression)", userMessage);
        }


        /// <summary>
        /// Verifies that the given collection contains the given count elements
        /// </summary>
        /// <typeparam name="T">The collection type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="count">NUmber elements in collection</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the collection does not contain
        /// exactly count elements.</exception>
        public static void Count<T>(IEnumerable<T> collection, int count, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);

            if (collection.Count() != count)
                throw new ChBaseAssertException(collection.Count().ToString(), count.ToString(), userMessage, "Assert.Count");
        }

        /// <summary>
        /// Verifies that a collection is empty.
        /// </summary>
        /// <param name="collection">The collection to be inspected</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ArgumentNullException">Thrown when the collection is null</exception>
        /// <exception cref="ChBaseAssertException">Thrown when the collection is not empty</exception>
        public static void Empty(IEnumerable collection, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);

            var enumerator = collection.GetEnumerator();
            try
            {
                if (enumerator.MoveNext())
                    throw ChBaseAssertException.EmptyException(userMessage, "Assert.Empty");
            }
            finally
            {
                (enumerator as IDisposable)?.Dispose();
            }
        }

        /// <summary>
        /// Verifies that the given collection is equal
        /// another given collection and equal elements
        /// </summary>
        /// <typeparam name="T">The collection type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="collection2">Another collection.</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static void Equal<T>(IEnumerable<T> collection, IEnumerable<T> collection2, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(collection2), collection2);

            if (collection.Count() != collection2.Count())
                throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");

            foreach (var item in collection)
            {
                if (!collection2.Any(x => x.Equals(item)))
                    throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");
            }
        }

        /// <summary>
        /// Verifies that the given collection is equal
        /// another given collection and equal elements
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="collection2">Another collection.</param>
        /// <param name="tolerance">Tolerance</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static void CollectionContains(IEnumerable<IChPoint> collection, IEnumerable<IChPoint> collection2, double tolerance = 0, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(collection2), collection2);

            foreach (var item in collection)
            {
                if (!collection2.Any(x => x.Equals(item, tolerance)))
                    throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");
            }
        }

        /// <summary>
        /// Verifies that the given collection is equal
        /// another given collection and equal elements
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="collection2">Another collection.</param>
        /// <param name="tolerance">Tolerance</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static void Equal(IEnumerable<IChPoint> collection, IEnumerable<IChPoint> collection2, double tolerance = 0, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(collection2), collection2);

            if (collection.Count() != collection2.Count())
                throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");

            foreach (var item in collection)
            {
                if (!collection2.Any(x => x.Equals(item, tolerance)))
                    throw new ChBaseAssertException(collection.Count().ToString(), collection2.Count().ToString(), userMessage, "Assert.Equal");
            }
        }

        /// <summary>
        /// Verifies that a collection is not empty.
        /// </summary>
        /// <param name="collection">The collection to be inspected</param>
        /// <param name="userMessage">User Message</param>
        /// <exception cref="ArgumentNullException">Thrown when a null collection is passed</exception>
        /// <exception cref="ChBaseAssertException">Thrown when the collection is empty</exception>
        public static void NotEmpty(IEnumerable collection, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);

            var enumerator = collection.GetEnumerator();
            try
            {
                if (!enumerator.MoveNext())
                    throw ChBaseAssertException.NotEmptyException(userMessage, "Assert.NotEmpty");
            }
            finally
            {
                (enumerator as IDisposable)?.Dispose();
            }
        }

        /// <summary>
        /// Verifies that the given collection contains only a single
        /// element of the given type.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="userMessage">User Message</param>
        /// <returns>The single item in the collection.</returns>
        /// <exception cref="ChBaseAssertException">Thrown when the collection does not contain
        /// exactly one element.</exception>
        public static object Single(IEnumerable collection, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);

            return Single(collection.Cast<object>(), userMessage);
        }

        /// <summary>
        /// Verifies that the given collection contains only a single
        /// element of the given value. The collection may or may not
        /// contain other values.
        /// </summary>
        /// <param name="collection">The collection.</param>
        /// <param name="expected">The value to find in the collection.</param>
        /// <param name="userMessage">User Message</param>
        /// <returns>The single item in the collection.</returns>
        /// <exception cref="ChBaseAssertException">Thrown when the collection does not contain
        /// exactly one element.</exception>
        public static void Single(IEnumerable collection, object expected, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);

            GetSingleResult(collection.Cast<object>(), item => object.Equals(item, expected), expected.ToString(), userMessage);
        }

        /// <summary>
        /// Verifies that the given collection contains only a single
        /// element of the given type.
        /// </summary>
        /// <typeparam name="T">The collection type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="userMessage">User Message</param>
        /// <returns>The single item in the collection.</returns>
        /// <exception cref="ChBaseAssertException">Thrown when the collection does not contain
        /// exactly one element.</exception>
        public static T Single<T>(IEnumerable<T> collection, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);

            return GetSingleResult(collection, null, null, userMessage);
        }

        /// <summary>
        /// Verifies that the given collection contains only a single
        /// element of the given type which matches the given predicate. The
        /// collection may or may not contain other values which do not
        /// match the given predicate.
        /// </summary>
        /// <typeparam name="T">The collection type.</typeparam>
        /// <param name="collection">The collection.</param>
        /// <param name="predicate">The item matching predicate.</param>
        /// <param name="userMessage">User Message</param>
        /// <returns>The single item in the filtered collection.</returns>
        /// <exception cref="ChBaseAssertException">Thrown when the filtered collection does
        /// not contain exactly one element.</exception>
        public static T Single<T>(IEnumerable<T> collection, Predicate<T> predicate, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(collection), collection);
            GuardArgumentNotNull(nameof(predicate), predicate);

            return GetSingleResult(collection, predicate, "(filter expression)", userMessage);
        }

        private static T Contains<T>(IEnumerable<T> collection, Predicate<T> predicate, string expectedArgument, string userMessage = "")
        {
            var count = 0;
            var result = default(T);

            foreach (var item in collection)
                if (predicate == null || predicate(item))
                    if (++count == 1)
                        result = item;

            if (count == 0)
                throw ChBaseAssertException.SingleException_Empty(expectedArgument, userMessage, "Assert.Contains");
            return result;
        }

        private static T GetSingleResult<T>(IEnumerable<T> collection, Predicate<T> predicate, string expectedArgument, string userMessage = "")
        {
            var count = 0;
            var result = default(T);

            foreach (var item in collection)
                if (predicate == null || predicate(item))
                    if (++count == 1)
                        result = item;

            switch (count)
            {
                case 0:
                    throw ChBaseAssertException.SingleException_Empty(expectedArgument, userMessage, "Assert.Single");
                case 1:
                    return result;

                default:
                    throw ChBaseAssertException.SingleException_MoreThanOne(count, expectedArgument, userMessage, "Assert.Single");
            }
        }
    }
}