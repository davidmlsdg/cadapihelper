﻿using System;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        internal static void GuardArgumentNotNull(string argName, object argValue)
        {
            if (argValue == null)
                throw new ArgumentNullException(argName);
        }
    }
}