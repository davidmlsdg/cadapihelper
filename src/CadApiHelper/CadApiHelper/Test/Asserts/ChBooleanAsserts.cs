﻿using CadApiHelper.Test.Exceptions;

namespace CadApiHelper.Test.Asserts
{
    /// <summary>
    /// Assert class for unit test
    /// </summary>
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that the condition is false.
        /// </summary>
        /// <param name="condition">Condition to assert</param>
        /// <param name="userMessage">User message</param>
        public static void False(bool condition, string userMessage = "")
        {
            if (condition)
                throw new ChBaseAssertException(condition.ToString(), false.ToString(),
                GetNonNullString(userMessage), "Assert.False");
        }

        /// <summary>
        /// Verifies that the condition is false.
        /// </summary>
        /// <param name="condition">Condition to assert</param>
        /// <param name="userMessage">User message</param>
        public static void False(bool? condition, string userMessage = "")
        {
            if (condition == null)
                throw new ChBaseAssertException("null", false.ToString(),
                GetNonNullString(userMessage), "Assert.False");

            False(condition.Value, userMessage);
        }

        /// <summary>
        /// Verifies that the condition is true.
        /// </summary>
        /// <param name="condition">Condition to assert</param>
        /// <param name="userMessage">User message</param>
        public static void True(bool condition, string userMessage = "")
        {
            if (!condition)
                throw new ChBaseAssertException(condition.ToString(), true.ToString(),
                GetNonNullString(userMessage), "Assert.False");
        }

        /// <summary>
        /// Verifies that the condition is true.
        /// </summary>
        /// <param name="condition">Condition to assert</param>
        /// <param name="userMessage">User message</param>
        public static void True(bool? condition, string userMessage = "")
        {
            if (condition == null)
                throw new ChBaseAssertException("null", true.ToString(),
                GetNonNullString(userMessage), "Assert.False");

            True(condition.Value, userMessage);
        }

        private static string GetNonNullString(string value)
        {
            if (value == null)
                return "";
            return value;
        }
    }
}