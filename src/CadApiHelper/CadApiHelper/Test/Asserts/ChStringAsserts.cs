using CadApiHelper.Geometry;
using CadApiHelper.Test.Exceptions;

using System;
using System.Text.RegularExpressions;

namespace CadApiHelper.Test.Asserts
{
    public partial class ChAssert
    {
        /// <summary>
        /// Verifies that a string contains a given sub-string, using the current culture
        /// </summary>
        /// <param name="expectedSubstring">The sub-string expected to be in the string</param>
        /// <param name="actualString">The string to be inspected</param>
        /// <param name="userMessage">User Message</param>
        public static void Contains(string expectedSubstring, string actualString, string userMessage = "")
        {
            if (expectedSubstring == null)
                throw new ArgumentNullException("expectedSubstring");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (!actualString.Contains(expectedSubstring))
                throw new ChBaseAssertException(actualString, expectedSubstring,
                    GetNonNullString(userMessage), "Assert.Contains");
        }

        /// <summary>
        /// Verifies that a string not contains a given sub-string, using the current culture
        /// </summary>
        /// <param name="expectedSubstring">The sub-string expected to be in the string</param>
        /// <param name="actualString">The string to be inspected</param>
        /// <param name="userMessage">User Message</param>
        public static void DoesNotContain(string expectedSubstring, string actualString, string userMessage = "")
        {
            if (expectedSubstring == null)
                throw new ArgumentNullException("expectedSubstring");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (actualString.Contains(expectedSubstring))
                throw new ChBaseAssertException(actualString, expectedSubstring,
                GetNonNullString(userMessage), "Assert.DoesNotContain");
        }

        /// <summary>
        /// Verifies that a string not end with a given string
        /// </summary>
        /// <param name="expectedEndString">The expected end string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void DoesNotEndsWith(string expectedEndString, string actualString, string userMessage = "")
        {
            DoesNotEndsWith(expectedEndString, actualString, StringComparison.CurrentCulture, userMessage);
        }

        /// <summary>
        /// Verifies that a string not end with a given string
        /// </summary>
        /// <param name="expectedEndString">The expected end string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void DoesNotEndsWith(string expectedEndString, string actualString,
            StringComparison stringComparison, string userMessage = "")
        {
            if (expectedEndString == null)
                throw new ArgumentNullException("expectedEndString");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (!actualString.EndsWith(expectedEndString, stringComparison))
                throw new ChBaseAssertException(actualString, expectedEndString,
                GetNonNullString(userMessage), "Assert.DoesNotEndsWith");
        }

        /// <summary>
        /// Verifies that a string not match with expected pattern string
        /// </summary>
        /// <param name="expectedRegexPattern">The expected pattern string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void DoesNotMatch(string expectedRegexPattern, string actualString, string userMessage = "")
        {
            if (expectedRegexPattern == null)
                throw new ArgumentNullException("expectedRegexPattern");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (Regex.IsMatch(actualString, expectedRegexPattern))
                throw new ChBaseAssertException(actualString, expectedRegexPattern,
                    GetNonNullString(userMessage), "Assert.DoesNotMatch");
        }

        /// <summary>
        /// Verifies that a string not start with a given string
        /// </summary>
        /// <param name="expectedStartString">The expected start string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void DoesNotStartsWith(string expectedStartString, string actualString, string userMessage = "")
        {
            DoesNotStartsWith(expectedStartString, actualString, StringComparison.CurrentCulture, userMessage);
        }

        /// <summary>
        /// Verifies that a string not start with a given string
        /// </summary>
        /// <param name="expectedStartString">The expected start string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void DoesNotStartsWith(string expectedStartString, string actualString,
            StringComparison stringComparison, string userMessage = "")
        {
            if (expectedStartString == null)
                throw new ArgumentNullException("expectedStartString");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (actualString.StartsWith(expectedStartString, stringComparison))
                throw new ChBaseAssertException(actualString, expectedStartString,
                GetNonNullString(userMessage), "Assert.DoesNotStartsWith");
        }

        /// <summary>
        /// Verifies that a string end with a given string
        /// </summary>
        /// <param name="expectedEndString">The expected end string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void EndsWith(string expectedEndString, string actualString, string userMessage = "")
        {
            EndsWith(expectedEndString, actualString, StringComparison.CurrentCulture, userMessage);
        }

        /// <summary>
        /// Verifies that a string end with a given string
        /// </summary>
        /// <param name="expectedEndString">The expected end string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void EndsWith(string expectedEndString, string actualString,
            StringComparison stringComparison, string userMessage = "")
        {
            if (expectedEndString == null)
                throw new ArgumentNullException("expectedEndString");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (!actualString.EndsWith(expectedEndString, stringComparison))
                throw new ChBaseAssertException(actualString, expectedEndString,
                GetNonNullString(userMessage), "Assert.EndsWith");
        }

        /// <summary>
        /// Verifies that a string is equal
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void Equal(string expected, string actual, string userMessage = "")
        {
            Equal(expected, actual, StringComparison.CurrentCulture, userMessage);
        }

        /// <summary>
        /// Verifies that a string is equal
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparispn</param>
        /// <param name="userMessage">The user message</param>
        public static void Equal(string expected, string actual,
            StringComparison stringComparison, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(expected), expected);
            GuardArgumentNotNull(nameof(actual), actual);

            if (!actual.Equals(expected, stringComparison))
                throw new ChBaseAssertException(actual, expected,
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that two points are equal
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string to be inspected</param>
        /// <param name="tolerance">The tolerance parameter for validate the comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void Equal(IChPoint expected, IChPoint actual, double tolerance = 0, string userMessage = "")
        {
            GuardArgumentNotNull(nameof(expected), expected);
            GuardArgumentNotNull(nameof(actual), actual);

            if (!actual.Equals(expected, tolerance))
                throw new ChBaseAssertException(actual.ToString(), expected.ToString(),
                    GetNonNullString(userMessage), "Assert.Equal");
        }

        /// <summary>
        /// Verifies that a string match with expected pattern string
        /// </summary>
        /// <param name="expectedRegexPattern">The expected pattern string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void Matches(string expectedRegexPattern, string actualString, string userMessage = "")
        {
            if (expectedRegexPattern == null)
                throw new ArgumentNullException("expectedRegexPattern");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (!Regex.IsMatch(actualString, expectedRegexPattern))
                throw new ChBaseAssertException(actualString, expectedRegexPattern,
                    GetNonNullString(userMessage), "Assert.Matches");
        }

        /// <summary>
        /// Verifies that a string is not equal
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void NotEqual(string expected, string actual, string userMessage = "")
        {
            Equal(expected, actual, StringComparison.CurrentCulture, userMessage);
        }

        /// <summary>
        /// Verifies that a string is not equal
        /// </summary>
        /// <param name="expected">The expected string</param>
        /// <param name="actual">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparispn</param>
        /// <param name="userMessage">The user message</param>
        public static void NotEqual(string expected, string actual,
            StringComparison stringComparison, string userMessage = "")
        {
            if (expected == null)
                throw new ArgumentNullException("expected");

            if (actual == null)
                throw new ArgumentNullException("actual");

            if (actual.Equals(expected, stringComparison))
                throw new ChBaseAssertException(actual, expected,
                    GetNonNullString(userMessage), "Assert.NotEqual");
        }

        /// <summary>
        /// Verifies that a string start with a given string
        /// </summary>
        /// <param name="expectedStartString">The expected start string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="userMessage">The user message</param>
        public static void StartsWith(string expectedStartString, string actualString, string userMessage = "")
        {
            StartsWith(expectedStartString, actualString, StringComparison.CurrentCulture, userMessage);
        }

        /// <summary>
        /// Verifies that a string start with a given string
        /// </summary>
        /// <param name="expectedStartString">The expected start string</param>
        /// <param name="actualString">The actual string to be inspected</param>
        /// <param name="stringComparison">The string comparison</param>
        /// <param name="userMessage">The user message</param>
        public static void StartsWith(string expectedStartString, string actualString,
            StringComparison stringComparison, string userMessage = "")
        {
            if (expectedStartString == null)
                throw new ArgumentNullException("expectedStartString");

            if (actualString == null)
                throw new ArgumentNullException("actualString");

            if (!actualString.StartsWith(expectedStartString, stringComparison))
                throw new ChBaseAssertException(actualString, expectedStartString,
                GetNonNullString(userMessage), "Assert.StartsWith");
        }
    }
}