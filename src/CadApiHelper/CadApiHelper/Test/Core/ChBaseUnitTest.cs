﻿namespace CadApiHelper.Test.Core
{
    public abstract class ChBaseUnitTest
    {
        public bool BreakDebugger { get; set; } = false;
    }
}
