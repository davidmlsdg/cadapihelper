using System;
using System.Text;

namespace CadApiHelper.Test.Exceptions
{
    public class ChBaseAssertException : Exception
    {
        public ChBaseAssertException(string actualValue, string expectedValue,
            string userMessage, string assertTest)
        {
            ActualValue = actualValue;
            ExpectedValue = expectedValue;
            UserMessage = userMessage;
            AssertTest = assertTest;
            BaseAssertMessage = GetMessage();
        }

        public string ActualValue { get; set; }

        public string AssertTest { get; set; }

        public string BaseAssertMessage { get; set; }

        public string ExpectedValue { get; set; }

        public string UserMessage { get; set; }

        internal static ChBaseAssertException AllException(int idx, string erros, string userMessage, string assertName)
        {
            return new ChBaseAssertException(idx.ToString(), erros, userMessage, assertName);
        }

        internal static ChBaseAssertException CollectionException(int expectedCount, int actualCount)
        {
            return new ChBaseAssertException(actualCount.ToString(), expectedCount.ToString(), "", "Assert.Collection");
        }

        internal static ChBaseAssertException EmptyException(string userMessage, string assertName)
        {
            return new ChBaseAssertException("Empty", "Not Empty", userMessage, assertName);
        }

        internal static ChBaseAssertException NotEmptyException(string userMessage, string assertName)
        {
            return new ChBaseAssertException("Not Empty", "Empty", userMessage, assertName);
        }

        internal static ChBaseAssertException SingleException_Empty(string expectedArgument, string userMessage, string assertName)
        {
            return new ChBaseAssertException("Empty", expectedArgument, userMessage, assertName);
        }

        internal static ChBaseAssertException SingleException_MoreThanOne(int count, string expectedArgument, string userMessage, string assertName)
        {
            return new ChBaseAssertException(count.ToString(), expectedArgument, userMessage, assertName);
        }

        private string GetMessage()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"AssertTest: {GetStringNotNull(AssertTest)} *** UserMessage: {GetStringNotNull(UserMessage)}");
            sb.AppendLine($"ActualValue: {GetStringNotNull(ActualValue)} *** ExpectedValue:{GetStringNotNull(ExpectedValue)}");
            sb.AppendLine($"Message: \n{GetStringNotNull(Message)}");
            sb.AppendLine();
            sb.AppendLine($"StackTrace: \n{GetStringNotNull(StackTrace)}");
            return sb.ToString();
        }

        private string GetStringNotNull(string str)
        {
            if (string.IsNullOrEmpty(str))
                return "";
            return str;
        }
    }
}