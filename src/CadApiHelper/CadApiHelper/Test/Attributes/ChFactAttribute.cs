﻿using System;

namespace CadApiHelper.Test.Attributes
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = false)]
    public class ChFactAttribute : System.Attribute
    {
        public ChFactAttribute()
        {
        }
    }
}