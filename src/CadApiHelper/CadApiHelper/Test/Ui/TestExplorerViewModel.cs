﻿using CadApiHelper.Test.Manager;

using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;

namespace CadApiHelper.Test.Ui
{
    internal class TestExplorerViewModel : ModelViewBase
    {
        private ChUnitTestManager _unitTestManager;
        private SolidColorBrush backgroundProgressbar;
        private string failed;
        private bool isIndeterminate;
        private string passed;
        private string resultTest;
        private object selectedNode;
        private string status;
        private string totalTests;

        public SolidColorBrush BackgroundProgressbar
        {
            get { return backgroundProgressbar; }
            set
            {
                SetField(ref backgroundProgressbar, value, "BackgroundProgressbar");
            }
        }

        public string Failed
        {
            get { return failed; }
            set
            {
                SetField(ref failed, value, "Failed");
            }
        }

        public bool IsIndeterminate
        {
            get { return isIndeterminate; }
            set
            {
                SetField(ref isIndeterminate, value, "IsIndeterminate");
            }
        }

        public string Passed
        {
            get { return passed; }
            set
            {
                SetField(ref passed, value, "Passed");
            }
        }

        public string ResultTest
        {
            get { return resultTest; }
            set
            {
                SetField(ref resultTest, value, "ResultTest");
            }
        }

        public RelayCommand RunAllCmd { get; set; }

        public RelayCommand RunCmd { get; set; }
        public RelayCommand RunAtDebugCmd { get; set; }

        public RelayCommand RunFailedCmd { get; set; }

        public object SelectedNode
        {
            get { return selectedNode; }
            private set
            {
                SetField(ref selectedNode, value, "SelectedNode");
                RunCmd?.RaiseCanExecuteChanged();
                RunAtDebugCmd?.RaiseCanExecuteChanged();
            }
        }

        public string Status
        {
            get { return status; }
            set
            {
                SetField(ref status, value, "Status");
            }
        }

        public ObservableCollection<ChNamespaceTest> Tests { get; set; }

        public string TotalTests
        {
            get { return totalTests; }
            set
            {
                SetField(ref totalTests, value, "TotalTests");
            }
        }

        public TestExplorerViewModel(ChUnitTestManager unitTestManager)
        {
            _unitTestManager = unitTestManager;
            Tests = new ObservableCollection<ChNamespaceTest>();
            RunAllCmd = new RelayCommand(OnRunAllCmd);
            RunCmd = new RelayCommand(OnRunCmd, CanRunCmd);
            RunAtDebugCmd = new RelayCommand(OnRunAtDebugCmd, CanRunAtDebugCmd);
            RunFailedCmd = new RelayCommand(OnRunFailedCmd);
        }

        private void OnRunAtDebugCmd()
        {
            SetStartupRun();
            if (SelectedNode is ChNamespaceTest)
            {
                ExecuteTestNamespace(SelectedNode as ChNamespaceTest);
            }
            else if (SelectedNode is ChClassTest)
            {
                ExecuteClassTestMethods(SelectedNode as ChClassTest);
            }
            else if (SelectedNode is ChMethodTest)
            {
                var method = SelectedNode as ChMethodTest;
                ChUnitTestManager.ExecuteMethod(method.ClassType, method, true);
            }
            SetEndRun();
        }

        private bool CanRunAtDebugCmd()
        {
            return SelectedNode != null;
        }

        public void Trv_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ResultTest = "";
            SelectedNode = e.NewValue;

            if (SelectedNode is ChMethodTest)
            {
                ChMethodTest test = (ChMethodTest)SelectedNode;
                ResultTest = GetMessage(test);
            }
        }

        internal void Load()
        {
            _unitTestManager.GetClassTest();
            Tests.Clear();
            BackgroundProgressbar = new SolidColorBrush(Colors.Gray);
            IsIndeterminate = false;
            foreach (var item in _unitTestManager.NamespacesUnitTest.NamespaceTests)
            {
                Tests.Add(item);
            }

            TotalTests = _unitTestManager.NamespacesUnitTest.GetTotalTests().ToString();
            Passed = "0";
            Failed = "0";
        }

        private static void ExecuteClassTestMethods(ChClassTest cl, bool? failed = null)
        {
            foreach (var m in cl.Methods)
            {
                if (failed == null || (failed != null && failed.Value == true && !m.Passed))
                    ChUnitTestManager.ExecuteMethod(cl.ClassType, m);
            }
        }

        private static void ExecuteTestNamespace(ChNamespaceTest ts, bool? failed = null)
        {
            foreach (var cl in ts.Classes)
            {
                ExecuteClassTestMethods(cl, failed);
                if (!cl.Methods.Any(x => !x.Passed))
                    cl.Passed = true;
            }
        }

        private bool CanRunCmd()
        {
            return SelectedNode != null;
        }

        private string GetMessage(ChMethodTest test)
        {
            var sb = new StringBuilder();
            sb.Append($"Name: {test.Name}");
            sb.Append($"Passed: {test.Passed}");
            sb.Append($"BaseMessage: {(test.BaseAssertMessage ?? "")}");
            sb.Append($"Message: {(test.Message ?? "")}");
            sb.Append($"Trace: {(test.Trace ?? "")}");
            return sb.ToString();
        }

        private void OnRunAllCmd()
        {
            SetStartupRun();
            foreach (var ts in Tests)
            {
                ExecuteTestNamespace(ts);
                if (!ts.Classes.Any(x => !x.Passed))
                    ts.Passsed = true;
            }
            SetEndRun();
        }

        private void SetEndRun()
        {
            SetResultsPassesFailed();
            ResultTest = "";
            SelectedNode = null;
            Status = "";
            IsIndeterminate = false;
            BackgroundProgressbar = new SolidColorBrush(Colors.Gray);
        }

        private void SetStartupRun()
        {
            Status = "Runing";
            IsIndeterminate = true;
            BackgroundProgressbar = new SolidColorBrush(Colors.Green);
        }

        private void OnRunCmd()
        {
            SetStartupRun();
            if (SelectedNode is ChNamespaceTest)
            {
                ExecuteTestNamespace(SelectedNode as ChNamespaceTest);
            }
            else if (SelectedNode is ChClassTest)
            {
                ExecuteClassTestMethods(SelectedNode as ChClassTest);
            }
            else if (SelectedNode is ChMethodTest)
            {
                var method = SelectedNode as ChMethodTest;
                ChUnitTestManager.ExecuteMethod(method.ClassType, method);
            }
            SetEndRun();
        }

        private void OnRunFailedCmd()
        {
            SetStartupRun();
            foreach (var ts in Tests)
            {
                ExecuteTestNamespace(ts, true);
            }
            SetEndRun();
        }

        private void SetResultsPassesFailed()
        {
            var countPassed = 0;
            var countFailed = 0;

            foreach (var ts in Tests)
            {
                countFailed += ts.GetFailTests();
                countPassed = ts.GetPassedTests();
            }

            Passed = countPassed.ToString();
            Failed = countFailed.ToString();
        }
    }
}