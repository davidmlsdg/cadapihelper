﻿using System;
using System.Windows.Input;

namespace CadApiHelper.Test.Ui
{
    public class RelayCommand : ICommand
    {
        private Func<bool> targetCanExecuteMethod;
        private Action targetExecuteMethod;

        public RelayCommand(Action targetExecuteMethod)
        {
            this.targetExecuteMethod = targetExecuteMethod;
        }

        public RelayCommand(Action targetExecuteMethod, Func<bool> targetCanExecuteMethod) : this(targetExecuteMethod)
        {
            this.targetCanExecuteMethod = targetCanExecuteMethod;
        }

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter)
        {
            if (targetCanExecuteMethod != null)
                return targetCanExecuteMethod();
            if (targetExecuteMethod != null)
                return true;
            return false;
        }

        public void Execute(object parameter)
        {
            if (targetExecuteMethod != null)
                targetExecuteMethod();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private Action onIncluirFabricanteCommand;
        private Func<T, bool> targetCanExecuteMethod;
        private Action<T> targetExecuteMethod;

        public RelayCommand(Action<T> targetExecuteMethod)
        {
            this.targetExecuteMethod = targetExecuteMethod;
        }

        public RelayCommand(Action onIncluirFabricanteCommand)
        {
            this.onIncluirFabricanteCommand = onIncluirFabricanteCommand;
        }

        public RelayCommand(Action<T> targetExecuteMethod, Func<T, bool> targetCanExecuteMethod) : this(targetExecuteMethod)
        {
            this.targetCanExecuteMethod = targetCanExecuteMethod;
        }

        public event EventHandler CanExecuteChanged = delegate { };

        public bool CanExecute(object parameter)
        {
            if (targetCanExecuteMethod != null)
            {
                T param = (T)parameter;
                return targetCanExecuteMethod(param);
            }
            if (targetExecuteMethod != null)
                return true;
            return false;
        }

        public void Execute(object parameter)
        {
            if (targetExecuteMethod != null)
                targetExecuteMethod((T)parameter)
                    ;
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged(this, EventArgs.Empty);
        }
    }
}