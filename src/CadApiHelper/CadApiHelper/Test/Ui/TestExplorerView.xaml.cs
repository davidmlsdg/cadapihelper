﻿using CadApiHelper.Test.Manager;

using System.Reflection;
using System.Windows;

namespace CadApiHelper.Test.Ui
{
    /// <summary>
    /// Interaction logic for TestExplorerView.xaml
    /// </summary>
    public partial class TestExplorerView : Window
    {
        private TestExplorerViewModel _modelView;
        private ChUnitTestManager _unitTestManager;

        public TestExplorerView(Assembly assembly)
        {
            InitializeComponent();
            _unitTestManager = new ChUnitTestManager(assembly);
            _modelView = new TestExplorerViewModel(_unitTestManager);
            DataContext = _modelView;
            Loaded += TestExplorerView_Loaded;
            trv.SelectedItemChanged += _modelView.Trv_SelectedItemChanged;
        }

        private void TestExplorerView_Loaded(object sender, RoutedEventArgs e)
        {
            _modelView.Load();
        }
    }
}