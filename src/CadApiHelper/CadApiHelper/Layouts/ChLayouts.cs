﻿#if ACAD
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
#endif

#if ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.Geometry;
#endif

#if BRCAD
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
    using Teigha.Geometry;
#endif

using System.Collections.Generic;
using CadApiHelper.Core;
using CadApiHelper.Geometry;
using CadApiHelper.EditorInput;
using CadApiHelper.Entities.Edit;
using CadApiHelper.Entities;

namespace CadApiHelper.Layouts
{
    /// <summary>
    /// ChLayouts - Manager Layouts
    /// </summary>
    public static class ChLayouts
    {
        /// <summary>
        /// Gets the layout names.
        /// </summary>
        /// <param name="database">The database.</param>
        /// <returns></returns>
        public static List<string> GetLayoutNames(Database database = null)
        {
            List<string> list = new List<string>();
            if (database == null)
                database = acApp.DocumentManager.MdiActiveDocument.Database;

            using (Transaction tr = database.TransactionManager.StartTransaction())
            {
                DBDictionary layoutDic = tr.GetObject(database.LayoutDictionaryId, OpenMode.ForRead, false) as DBDictionary;
                foreach (DBDictionaryEntry entry in layoutDic)
                {
                    ObjectId layoutId = entry.Value;
                    Layout layout = tr.GetObject(layoutId, OpenMode.ForRead) as Layout;
                    if (layout != null)
                        list.Add(layout.LayoutName);
                }
                tr.Commit();
            }
            return list;
        }

        /// <summary>
        /// Gets the active layout.
        /// </summary>
        /// <returns></returns>
        public static string GetActiveLayout()
        {
            LayoutManager layoutMgr = LayoutManager.Current;
            if (layoutMgr == null)
                return "";
            return layoutMgr.CurrentLayout;
        }

        /// <summary>
        /// Sets the active layout.
        /// </summary>
        /// <param name="layoutName">Name of the layout.</param>
        /// <returns>True if the layout was activate</returns>
        public static bool SetActiveLayout(string layoutName)
        {
            LayoutManager layoutMgr = LayoutManager.Current;
            if (layoutMgr == null)
                return false;

            var id = layoutMgr.GetLayoutId(layoutName);
            if (id.IsValid)
            {
                layoutMgr.CurrentLayout = layoutName;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Deletes the layout.
        /// </summary>
        /// <param name="layoutName">Name of the layout.</param>
        /// <returns>True if layout was deleted</returns>
        public static bool DeleteLayout(string layoutName)
        {
            LayoutManager layoutMgr = LayoutManager.Current;
            if (layoutMgr == null)
                return false;

            var id = layoutMgr.GetLayoutId(layoutName);
            if (id.IsValid)
            {
                layoutMgr.DeleteLayout(layoutName);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Deletes all layouts.
        /// </summary>
        public static void DeleteAllLayouts()
        {
            // Get the current document and database
            var acDoc = acApp.DocumentManager.MdiActiveDocument;
            Database acCurDb = acDoc.Database;

            // Get the layout dictionary of the current database
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {
                DBDictionary lays = acTrans.GetObject(acCurDb.LayoutDictionaryId, OpenMode.ForRead) as DBDictionary;

                List<string> lista = new List<string>();

                foreach (DBDictionaryEntry item in lays)
                {
                    lista.Add(item.Key);
                }

                LayoutManager acLayoutMgr = LayoutManager.Current;
                foreach (string s in lista)
                {
                    if (s != "Model")
                        acLayoutMgr.DeleteLayout(s);
                }

                // Abort the changes to the database
                acTrans.Commit();
            }
        }


        /// <summary>
        /// Changes the view port.
        /// </summary>
        /// <param name="layoutId">The layout identifier.</param>
        /// <param name="maxExt">The maximum ext.</param>
        /// <param name="minExt">The minimum ext.</param>
        /// <returns>True if viewport was changed</returns>
        public static bool ChangeViewPort(ChObjectId layoutId, IChPoint maxExt, IChPoint minExt)
        {
            LayoutManager acLayoutMgr = LayoutManager.Current;
            var ed = ChEditor.GetEditor();

            if (layoutId.IsNull || !layoutId.IsValid)
                return false;

            var layout = new ChEditDbObject<Layout>(layoutId, OpenMode.ForRead);

            foreach (ObjectId layId in layout.DbObject.GetViewports())
            {
                var ve = new ChEditDbObject<Viewport>(layId, OpenMode.ForWrite);
                //ve.DbObject.Layer = "MV";
                Viewport vp = ve.DbObject;
                vp.Width = 297;
                vp.Height = 210;
                vp.CenterPoint =ChPoint.Create(148.5, 105, 0).To3dPoint();
                ObjectId id = ChEntity.AddEntityToDatabase(vp);

                // get the screen aspect ratio to calculate the height and width
                double mScrRatio;
                // width/height
                mScrRatio = (vp.Width / vp.Height);

                Extents3d mExtents = new Extents3d();
                mExtents.Set(minExt.To3dPoint(), maxExt.To3dPoint());

                // prepare Matrix for DCS to WCS transformation
                Matrix3d matWCS2DCS;
                matWCS2DCS = Matrix3d.PlaneToWorld(vp.ViewDirection);
                matWCS2DCS = Matrix3d.Displacement(
                    vp.ViewTarget - Point3d.Origin)
                    * matWCS2DCS;
                matWCS2DCS = Matrix3d.Rotation(
                    -vp.TwistAngle, vp.ViewDirection,
                    vp.ViewTarget) * matWCS2DCS;
                matWCS2DCS = matWCS2DCS.Inverse();

                // tranform the extents to the DCS defined by the viewdir
                mExtents.TransformBy(matWCS2DCS);

                // width of the extents in current view
                double mWidth;
                mWidth = (mExtents.MaxPoint.X - mExtents.MinPoint.X);

                // height of the extents in current view
                double mHeight;
                mHeight = (mExtents.MaxPoint.Y - mExtents.MinPoint.Y);

                // get the view center point
                Point2d mCentPt = new Point2d(
                    ((mExtents.MaxPoint.X + mExtents.MinPoint.X) * 0.5),
                    ((mExtents.MaxPoint.Y + mExtents.MinPoint.Y) * 0.5));

                // check if the width 'fits' in current window, if not then get the new height as per
                // the viewports aspect ratio
                if (mWidth > (mHeight * mScrRatio))
                    mHeight = mWidth / mScrRatio;

                // set the viewport parameters
                if (vp.Number == 2)
                {
                    vp.UpgradeOpen();
                    // set the view height - adjusted by 1%
                    vp.ViewHeight = mHeight * 1.01;
                    // set the view center
                    vp.ViewCenter = mCentPt;
                    vp.Visible = true;
                    vp.On = true;
                    vp.UpdateDisplay();
                    ed.SwitchToModelSpace();
                    acApp.SetSystemVariable("CVPORT", vp.Number);
                    //Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("CVPORT", vp.Number);
                }
                if (vp.Number == 3)
                {
                    vp.UpgradeOpen();
                    vp.ViewHeight = mHeight * 1.25;
                    //set the view center
                    vp.ViewCenter = mCentPt;
                    vp.Visible = true;
                    vp.On = true;
                    vp.UpdateDisplay();
                    ed.SwitchToModelSpace();
                    acApp.SetSystemVariable("CVPORT", vp.Number);
                    //Autodesk.AutoCAD.ApplicationServices.Core.Application.SetSystemVariable("CVPORT", vp.Number);
                }
                ve.Comit();
            }
            layout.Comit();
            return true;
        }

        /// <summary>
        /// Gets all layouts ids.
        /// </summary>
        /// <returns>List<ChObjectId></returns>
        public static List<ChObjectId> GetAllLayoutsIds()
        {
            // Get the current document and database
            var acDoc = acApp.DocumentManager.MdiActiveDocument;
            Database acCurDb = acDoc.Database;
            var lista = new List<ChObjectId>();

            // Get the layout dictionary of the current database
            using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
            {
                DBDictionary lays = acTrans.GetObject(acCurDb.LayoutDictionaryId, OpenMode.ForRead) as DBDictionary;

                foreach (DBDictionaryEntry item in lays)
                {
                    if (item.Key.ToUpper() != "MODEL")
                        lista.Add(item.Value);
                }
                acTrans.Commit();
            }
            return lista;
        }

        /// <summary>
        /// Determines whether the specified layout name has layout.
        /// </summary>
        /// <param name="layoutName">Name of the layout.</param>
        /// <returns>
        ///   <c>true</c> if the specified layout name has layout; otherwise, <c>false</c>.
        /// </returns>
        public static bool HasLayout(string layoutName)
        {
            LayoutManager layoutMgr = LayoutManager.Current;
            if (layoutMgr == null)
                return false;

            var id = layoutMgr.GetLayoutId(layoutName);
            return id.IsValid;
        }

        /// <summary>
        /// Creates the layout.
        /// </summary>
        /// <param name="layoutName">Name of the layout.</param>
        /// <returns>True if the layout was created</returns>
        public static ObjectId CreateLayout(string layoutName)
        {
            LayoutManager layoutMgr = LayoutManager.Current;
            if (layoutMgr == null)
                return ObjectId.Null;

            var id = layoutMgr.GetLayoutId(layoutName);
            if (id.IsValid)
                return id;

            return layoutMgr.CreateLayout(layoutName);
        }

        /// <summary>
        /// Imports the layout.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="layoutNameImport">The layout name import.</param>
        /// <param name="newLayoutName">New name of the layout.</param>
        public static void ImportLayout(string fileName, string layoutNameImport, string newLayoutName)
        {
            // Get the current document and database
            var acDoc = acApp.DocumentManager.MdiActiveDocument;
            Database acCurDb = acDoc.Database;

            // Create a new database object and open the drawing into memory
            Database acExDb = new Database(false, true);
            acExDb.ReadDwgFile(fileName, FileOpenMode.OpenForReadAndAllShare, true, "");

            // Create a transaction for the external drawing
            using (Transaction acTransEx = acExDb.TransactionManager.StartTransaction())
            {
                // Get the layouts dictionary
                DBDictionary layoutsEx =
                    acTransEx.GetObject(acExDb.LayoutDictionaryId,
                                        OpenMode.ForRead) as DBDictionary;

                // Check to see if the layout exists in the external drawing
                if (layoutsEx.Contains(layoutNameImport) == true)
                {
                    // Get the layout and block objects from the external drawing
                    Layout layEx =
                        layoutsEx.GetAt(layoutNameImport).GetObject(OpenMode.ForRead) as Layout;
                    BlockTableRecord blkBlkRecEx =
                        acTransEx.GetObject(layEx.BlockTableRecordId,
                                            OpenMode.ForRead) as BlockTableRecord;

                    // Get the objects from the block associated with the layout
                    ObjectIdCollection idCol = new ObjectIdCollection();
                    foreach (ObjectId id in blkBlkRecEx)
                    {
                        idCol.Add(id);
                    }

                    // Create a transaction for the current drawing
                    using (Transaction acTrans = acCurDb.TransactionManager.StartTransaction())
                    {
                        // Get the block table and create a new block
                        // then copy the objects between drawings
                        BlockTable blkTbl =
                            acTrans.GetObject(acCurDb.BlockTableId,
                                              OpenMode.ForWrite) as BlockTable;

                        using (BlockTableRecord blkBlkRec = new BlockTableRecord())
                        {
                            int layoutCount = layoutsEx.Count - 1;

                            blkBlkRec.Name = "*Paper_Space" + layoutCount.ToString();
                            blkTbl.Add(blkBlkRec);
                            acTrans.AddNewlyCreatedDBObject(blkBlkRec, true);
                            acExDb.WblockCloneObjects(idCol,
                                                      blkBlkRec.ObjectId,
                                                      new IdMapping(),
                                                      DuplicateRecordCloning.Ignore,
                                                      false);

                            // Create a new layout and then copy properties between drawings
                            DBDictionary layouts =
                                acTrans.GetObject(acCurDb.LayoutDictionaryId,
                                                  OpenMode.ForWrite) as DBDictionary;

                            using (Layout lay = new Layout())
                            {
                                lay.LayoutName = newLayoutName;
                                lay.AddToLayoutDictionary(acCurDb, blkBlkRec.ObjectId);
                                acTrans.AddNewlyCreatedDBObject(lay, true);
                                lay.CopyFrom(layEx);

                                DBDictionary plSets =
                                    acTrans.GetObject(acCurDb.PlotSettingsDictionaryId,
                                                      OpenMode.ForRead) as DBDictionary;

                                // Check to see if a named page setup was assigned to the layout,
                                // if so then copy the page setup settings
                                if (lay.PlotSettingsName != "")
                                {
                                    // Check to see if the page setup exists
                                    if (plSets.Contains(lay.PlotSettingsName) == false)
                                    {
                                        plSets.UpgradeOpen();

                                        using (PlotSettings plSet = new PlotSettings(lay.ModelType))
                                        {
                                            plSet.PlotSettingsName = lay.PlotSettingsName;
                                            plSet.AddToPlotSettingsDictionary(acCurDb);
                                            acTrans.AddNewlyCreatedDBObject(plSet, true);

                                            DBDictionary plSetsEx =
                                                acTransEx.GetObject(acExDb.PlotSettingsDictionaryId,
                                                                    OpenMode.ForRead) as DBDictionary;

                                            PlotSettings plSetEx =
                                                plSetsEx.GetAt(lay.PlotSettingsName).GetObject(
                                                               OpenMode.ForRead) as PlotSettings;

                                            plSet.CopyFrom(plSetEx);
                                        }
                                    }
                                }
                            }
                        }

                        // Regen the drawing to get the layout tab to display
                        acDoc.Editor.Regen();

                        // Save the changes made
                        acTrans.Commit();
                    }
                }
                // Discard the changes made to the external drawing file
                acTransEx.Abort();
            }

            // Close the external drawing file
            acExDb.Dispose();
        }
    }
}
