﻿using CadApiHelper.Core;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CadApiHelper.Layouts
{
    /// <summary>
    /// ChLayoutName
    /// </summary>
    public class ChLayoutName
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChLayoutName"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="objectId">The object identifier.</param>
        public ChLayoutName(string name, ChObjectId objectId)
        {
            Name = name;
            ObjectId = objectId;
            Number = 1;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChLayoutName"/> class.
        /// </summary>
        public ChLayoutName()
        {
            Number = 1;
        }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>
        /// The name.
        /// </value>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the number.
        /// </summary>
        /// <value>
        /// The number.
        /// </value>
        public int Number { get; set; }

        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public ChObjectId ObjectId { get; set; }
    }
}
