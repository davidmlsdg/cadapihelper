﻿#if ACAD
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.ApplicationServices.Core;
using acApp = Autodesk.AutoCAD.ApplicationServices.Core.Application;
using Autodesk.AutoCAD.DatabaseServices;
using acDb = Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.PlottingServices;
#endif
#if ZWCAD
using acApp = ZwSoft.ZwCAD.ApplicationServices.Application;
using ZwSoft.ZwCAD.DatabaseServices;
using acDb = ZwSoft.ZwCAD.DatabaseServices;
using ZwSoft.ZwCAD.EditorInput;
using ZwSoft.ZwCAD.PlottingServices;
#endif
#if BRCAD
    using acApp = Bricscad.ApplicationServices.Application;
    using Teigha.DatabaseServices;
    using acDb = Teigha.DatabaseServices;
    using Bricscad.EditorInput;
    using Bricscad.PlottingServices;
#endif

using System.Collections.Generic;
using System.Collections.Specialized;


namespace CadApiHelper.Plot
{
    /// <summary>
    /// ChPlot - Manager Plot
    /// </summary>
    public class ChPlot
    {
        #region Public Methods

        /// <summary>
        /// Gets the CTB list.
        /// </summary>
        /// <returns>StringCollection</returns>
        public static StringCollection GetCtbList()
        {
            PlotSettingsValidator psv = PlotSettingsValidator.Current;
            return psv.GetPlotStyleSheetList();
        }

        /// <summary>
        /// Gets the paper list.
        /// </summary>
        /// <param name="printerName">Name of the printer.</param>
        /// <returns>StringCollection</returns>
        public static StringCollection GetPaperList(string printerName)
        {
            PlotSettingsValidator psv = PlotSettingsValidator.Current;
            PlotSettings ps = new PlotSettings(true);
            using (ps)
            {
                psv.SetPlotConfigurationName(ps, printerName, null);
                psv.RefreshLists(ps);
                return psv.GetCanonicalMediaNameList(ps);
            }
        }

        /// <summary>
        /// Gets the print list.
        /// </summary>
        /// <returns>StringCollection</returns>
        static public StringCollection GetPrintList()
        {
            PlotSettingsValidator psv = PlotSettingsValidator.Current;
            return psv.GetPlotDeviceList();
        }

        /// <summary>
        /// Multis the sheet plot.
        /// </summary>
        /// <param name="deviceName">Name of the device.</param>
        /// <param name="paperName">Name of the paper.</param>
        /// <param name="fileName">Name of the file.</param>
        /// <param name="ctb">The CTB.</param>
        /// <param name="orderLayoutsNames">The order layouts names.</param>
        static public void MultiSheetPlot(string deviceName, string paperName, string fileName, string ctb, List<string> orderLayoutsNames)
        {
            var doc = acApp.DocumentManager.MdiActiveDocument;
            Editor ed = doc.Editor;
            Database db = doc.Database;

            Transaction tr = db.TransactionManager.StartTransaction();
            using (tr)
            {
                BlockTable bt = tr.GetObject(db.BlockTableId, OpenMode.ForRead) as BlockTable;

                PlotInfo pi = new PlotInfo();
                PlotInfoValidator piv = new PlotInfoValidator();
                piv.MediaMatchingPolicy = MatchingPolicy.MatchEnabled;

                if (PlotFactory.ProcessPlotState == ProcessPlotState.NotPlotting)
                {
                    PlotEngine pe = PlotFactory.CreatePublishEngine();
                    using (pe)
                    {
                        ObjectIdCollection layoutsToPlot = new ObjectIdCollection();
                        List<BlockTableRecord> layouts = new List<BlockTableRecord>();

                        foreach (ObjectId btrId in bt)
                        {
                            BlockTableRecord btr = tr.GetObject(btrId, OpenMode.ForRead) as BlockTableRecord;
                            if (btr.IsLayout && btr.Name.ToUpper() != BlockTableRecord.ModelSpace.ToUpper())
                            {
                                if (orderLayoutsNames == null)
                                    layoutsToPlot.Add(btrId);
                                else
                                    layouts.Add(btr);
                            }
                        }

                        if (orderLayoutsNames != null)
                        {
                            foreach (string s in orderLayoutsNames)
                            {
                                BlockTableRecord btrp = layouts.Find(x => x.Name.ToUpper() == s.ToUpper());
                                if (btrp != null)
                                    layoutsToPlot.Add(btrp.ObjectId);
                            }
                        }

                        PlotProgressDialog ppd = new PlotProgressDialog(false, layoutsToPlot.Count, true);
                        using (ppd)
                        {
                            int numSheet = 1;

                            foreach (ObjectId btrId in layoutsToPlot)
                            {
                                BlockTableRecord btr = (BlockTableRecord)tr.GetObject(btrId, OpenMode.ForRead);
                                Layout lo = tr.GetObject(btr.LayoutId, OpenMode.ForRead) as Layout;

                                PlotSettings ps = new PlotSettings(lo.ModelType);
                                ps.CopyFrom(lo);

                                PlotSettingsValidator psv = PlotSettingsValidator.Current;

                                psv.SetPlotType(ps, acDb.PlotType.Extents);
                                psv.SetUseStandardScale(ps, true);
                                psv.SetStdScaleType(ps, StdScaleType.ScaleToFit);
                                psv.SetPlotCentered(ps, true);
                                if (ctb != "")
                                    psv.SetCurrentStyleSheet(ps, ctb);

                                psv.SetPlotConfigurationName(ps, deviceName, paperName);
                                pi.Layout = btr.LayoutId;
                                LayoutManager.Current.CurrentLayout = lo.LayoutName;

                                pi.OverrideSettings = ps;
                                piv.Validate(pi);

                                if (numSheet == 1)
                                {
                                    ppd.set_PlotMsgString(PlotMessageIndex.DialogTitle, "Impressão em progresso");
                                    ppd.set_PlotMsgString(PlotMessageIndex.CancelJobButtonMessage, "Cancelar a impressão");
                                    ppd.set_PlotMsgString(PlotMessageIndex.CancelSheetButtonMessage, "Cancel Sheet");
                                    ppd.set_PlotMsgString(PlotMessageIndex.SheetSetProgressCaption, "Sheet Set Progress");
                                    ppd.set_PlotMsgString(PlotMessageIndex.SheetProgressCaption, "Sheet Progress");
                                    ppd.LowerPlotProgressRange = 0;
                                    ppd.UpperPlotProgressRange = 100;
                                    ppd.PlotProgressPos = 0;

                                    // Let's start the plot, at last

                                    ppd.OnBeginPlot();
                                    ppd.IsVisible = true;
                                    pe.BeginPlot(ppd, null);

                                    // We'll be plotting a single document
                                    bool ploToFile = true;
                                    if (fileName == "")
                                        ploToFile = false;

                                    pe.BeginDocument(pi, doc.Name, null, 1, ploToFile, fileName);
                                }

                                // Which may contain multiple sheets

                                ppd.StatusMsgString = "Plotting " + doc.Name.Substring(doc.Name.LastIndexOf("\\") + 1) + " - sheet " + numSheet.ToString() + " of " + layoutsToPlot.Count.ToString();
                                ppd.OnBeginSheet();

                                ppd.LowerSheetProgressRange = 0;
                                ppd.UpperSheetProgressRange = 100;
                                ppd.SheetProgressPos = 0;

                                PlotPageInfo ppi = new PlotPageInfo();
                                pe.BeginPage(ppi, pi, (numSheet == layoutsToPlot.Count), null);
                                pe.BeginGenerateGraphics(null);
                                ppd.SheetProgressPos = 50;
                                pe.EndGenerateGraphics(null);

                                // Finish the sheet
                                pe.EndPage(null);
                                ppd.SheetProgressPos = 100;
                                ppd.OnEndSheet();
                                numSheet++;
                            }

                            // Finish the document

                            pe.EndDocument(null);

                            // And finish the plot

                            ppd.PlotProgressPos = 100;
                            ppd.OnEndPlot();
                            pe.EndPlot(null);
                        }
                    }
                }
                else
                {
                    ed.WriteMessage("\nAnother plot is in progress.");
                }
            }
        }

        #endregion Public Methods
    }
}
