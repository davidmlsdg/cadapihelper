﻿#if ZWCAD
using ZwSoft.ZwCAD.DatabaseServices;
#elif BRCAD
    using Teigha.DatabaseServices;
#elif ACAD
using Autodesk.AutoCAD.DatabaseServices;
#endif
#if GCAD
using GrxCAD.DatabaseServices;
#endif

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CadApiHelper.Core
{
    /// <summary>
    /// Collection of the ChObjectId
    /// </summary>
    /// <seealso cref="System.Collections.Generic.List&lt;CadApiHelper.Core.ChObjectId&gt;" />
    public class ChObjectIdCollection : List<ChObjectId>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ChObjectIdCollection"/> class.
        /// </summary>
        public ChObjectIdCollection()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChObjectIdCollection"/> class.
        /// </summary>
        /// <param name="ids">The ids.</param>
        public ChObjectIdCollection(ObjectIdCollection ids)
        {
            if (ids == null)
                return;

            for (int i = 0; i < ids.Count; i++)
                this.Add(ids[i]);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChObjectIdCollection"/> class.
        /// </summary>
        /// <param name="ids">The ids.</param>
        public ChObjectIdCollection(ObjectId[] ids)
        {
            if (ids == null)
                return;

            for (int i = 0; i < ids.Length; i++)
                this.Add(ids[i]);
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="ChObjectIdCollection"/> to <see cref="ObjectIdCollection"/>.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ObjectIdCollection(ChObjectIdCollection ids)
        {
            ObjectIdCollection objIds = new ObjectIdCollection();
            if (ids == null)
                return objIds;

            for (int i = 0; i < ids.Count; i++)
                objIds.Add(ids[i]);
            return objIds;
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="ObjectIdCollection"/> to <see cref="ChObjectIdCollection"/>.
        /// </summary>
        /// <param name="ids">The ids.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ChObjectIdCollection(ObjectIdCollection ids)
        {
            ChObjectIdCollection objIds = new ChObjectIdCollection();
            if (ids == null)
                return objIds;

            for (int i = 0; i < ids.Count; i++)
                objIds.Add(ids[i]);
            return objIds;
        }
    }
}
