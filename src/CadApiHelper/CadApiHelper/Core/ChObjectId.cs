﻿#if ZWCAD

using ZwSoft.ZwCAD.DatabaseServices;

#endif
#if BRCAD
    using Teigha.DatabaseServices;
#endif
#if ACAD
using Autodesk.AutoCAD.DatabaseServices;
#endif
#if GCAD
using GrxCAD.DatabaseServices;
#endif

namespace CadApiHelper.Core
{
    /// <summary>
    /// ChObjectId wrapper ObjectId
    /// </summary>
    public class ChObjectId
    {
        private ObjectId m_objectId = ObjectId.Null;

        /// <summary>
        /// Initializes a new instance of the <see cref="ChObjectId"/> class.
        /// </summary>
        public ChObjectId()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ChObjectId"/> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        public ChObjectId(ObjectId id)
        {
            m_objectId = id;
        }

        /// <summary>
        /// Gets a value indicating whether this instance is effectively erased.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is effectively erased; otherwise, <c>false</c>.
        /// </value>
        public bool IsEffectivelyErased
        { get { return ObjectId.IsEffectivelyErased; } }

        /// <summary>
        /// Gets a value indicating whether this instance is erased.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is erased; otherwise, <c>false</c>.
        /// </value>
        public bool IsErased
        { get { return ObjectId.IsErased; } }

        /// <summary>
        /// Gets a value indicating whether this instance is null.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is null; otherwise, <c>false</c>.
        /// </value>
        public bool IsNull
        { get { return ObjectId.IsNull; } }

        /// <summary>
        /// Gets a value indicating whether this instance is resident.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is resident; otherwise, <c>false</c>.
        /// </value>
        public bool IsResident
        { get { return ObjectId.IsResident; } }

        /// <summary>
        /// Returns true if ... is valid.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is valid; otherwise, <c>false</c>.
        /// </value>
        public bool IsValid
        { get { return ObjectId.IsValid; } }


        /// <summary>
        /// Gets or sets the object identifier.
        /// </summary>
        /// <value>
        /// The object identifier.
        /// </value>
        public ObjectId ObjectId
        {
            get { return m_objectId; }
            set { m_objectId = value; }
        }

        /// <summary>
        /// Performs an implicit conversion from <see cref="ChObjectId"/> to <see cref="ObjectId"/>.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ObjectId(ChObjectId id)
        {
            if (id == null)
                return ObjectId.Null;
            return id.ObjectId;
        }


        /// <summary>
        /// Performs an implicit conversion from <see cref="ObjectId"/> to <see cref="ChObjectId"/>.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The result of the conversion.
        /// </returns>
        public static implicit operator ChObjectId(ObjectId id)
        {
            return new ChObjectId(id);
        }
    }
}