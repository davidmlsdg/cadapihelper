﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

using System.Collections.Generic;
using System.Linq;

namespace CadApiHelperTests.Geometry
{
    public class LineSegmentTests : ChBaseUnitTest
    {
        [ChFact]
        public void ChangeOrientation_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();

            // Act
            lineSegment.ChangeOrientation();

            // Assert
            var tol = 0.25;
            ChAssert.Equal(lineSegment.StartPoint, ChPoint.Create(50, 50), tol);
            ChAssert.Equal(lineSegment.EndPoint, ChPoint.Create(), tol);
        }

        [ChFact]
        public void CopySegment_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();

            // Act
            var result = lineSegment.CopySegment();

            // Assert
            ChAssert.Equal(lineSegment, result);
        }

        [ChFact]
        public void DistanceToPoint2d_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            IChPoint point = ChPoint.Create(10);

            // Act
            var result = lineSegment.DistanceToPoint2d(point);

            // Assert
            ChAssert.Equal(result, 7.0710678118654755);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            IChSegment line = GetLineSegment();
            IChSegment line2 = ChLineSegment.Create(ChPoint.Create(100, 100), ChPoint.Create(200, 200));
            IChCircle circle = ChCircle.Create(ChPoint.Origin, 10);
            IChArc arc = ChArc.Create(ChPoint.Create(10), ChPoint.Create(20, 20), ChPoint.Create(20, 15));

            // Act
            var resultLine = lineSegment.Equals(line);
            var resultLine2 = lineSegment.Equals(line2);
            var resultArc = lineSegment.Equals(arc);
            var resultCircle = lineSegment.Equals(circle);

            // Assert
            ChAssert.True(resultLine);
            ChAssert.False(resultLine2);
            ChAssert.False(resultArc);
            ChAssert.False(resultCircle);
        }

        [ChFact]
        public void GetLineQuadrantOneOrFour_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();

            // Act
            var result = lineSegment.GetLineQuadrantOneOrFour();

            // Assert
            ChAssert.Equal(lineSegment, result);
        }


        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            var lineIntersect = ChLineSegment.Create(ChPoint.Create(50), ChPoint.Create(0, 50));
            var lineNotIntersect = ChLineSegment.Create(ChPoint.Create(200, 200), ChPoint.Create(500, 500));
            var circleIntersect = ChCircle.Create(ChPoint.Create(20, 20), 10);
            var circleNotIntersect = ChCircle.Create(ChPoint.Create(400, 400), 5);
            var arcIntersect = ChArc.Create(ChPoint.Create(30, 20), ChPoint.Create(10, 20), ChPoint.Create(5, 10));
            var arcNotIntersect = ChArc.Create(ChPoint.Create(300, 200), ChPoint.Create(100, 200), ChPoint.Create(50, 100));
            // Act
            var result_lineIntersect = lineSegment.IntersectWith(lineIntersect);
            var result_circleIntersect = lineSegment.IntersectWith(circleIntersect);
            var result_arcIntersect = lineSegment.IntersectWith(arcIntersect);
            var result_lineNotIntersect = lineSegment.IntersectWith(lineNotIntersect);
            var result_circleNotIntersect = lineSegment.IntersectWith(circleNotIntersect);
            var result_arcNotIntersect = lineSegment.IntersectWith(arcNotIntersect);

            // Assert
            ChAssert.True(result_lineIntersect);
            ChAssert.True(result_circleIntersect);
            ChAssert.True(result_arcIntersect);
            ChAssert.False(result_lineNotIntersect);
            ChAssert.False(result_circleNotIntersect);
            ChAssert.False(result_arcNotIntersect);
        }

        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();

            //From point:  X= 50.0000  Y= 0.0000  Z= 0.0000 To point:  X = 0.0000  Y = 50.0000  Z = 0.0000
            var lineIntersect = ChLineSegment.Create(ChPoint.Create(50), ChPoint.Create(0, 50));
            var lineNotIntersect = ChLineSegment.Create(ChPoint.Create(200, 200), ChPoint.Create(500, 500));
            var ptIntersectLine = ChPoint.Create(25, 25);

            var circleIntersect = ChCircle.Create(ChPoint.Create(20, 20), 10);
            var circleNotIntersect = ChCircle.Create(ChPoint.Create(400, 400), 5);
            var ptsIntersectCircle = new List<IChPoint> { ChPoint.Create(12.9289, 12.9289), ChPoint.Create(27.0711, 27.0711) };

            var arcIntersect = ChArc.Create(ChPoint.Create(30, 20), ChPoint.Create(10, 20), ChPoint.Create(5, 10));
            var arcNotIntersect = ChArc.Create(ChPoint.Create(300, 200), ChPoint.Create(100, 200), ChPoint.Create(50, 100));
            var ptsIntersectArc = new List<IChPoint> { ChPoint.Create(5.3365, 5.3365), ChPoint.Create(23.4105, 23.4105) };

            List<IChPoint> pointsLineIntersect = new List<IChPoint>();
            List<IChPoint> pointscircleIntersect = new List<IChPoint>();
            List<IChPoint> pointsArcIntersect = new List<IChPoint>();

            List<IChPoint> pointsLineNotIntersect = new List<IChPoint>();
            List<IChPoint> pointscircleNotIntersect = new List<IChPoint>();
            List<IChPoint> pointsArcNotIntersect = new List<IChPoint>();

            // Act
            var result_lineIntersect = lineSegment.IntersectWith(lineIntersect, pointsLineIntersect);
            var result_circleIntersect = lineSegment.IntersectWith(circleIntersect, pointscircleIntersect);
            var result_arcIntersect = lineSegment.IntersectWith(arcIntersect, pointsArcIntersect);

            var result_lineNotIntersect = lineSegment.IntersectWith(lineNotIntersect, pointsLineNotIntersect);
            var result_circleNotIntersect = lineSegment.IntersectWith(circleNotIntersect, pointscircleNotIntersect);
            var result_arcNotIntersect = lineSegment.IntersectWith(arcNotIntersect, pointsArcNotIntersect);

            // Assert
            ChAssert.True(result_lineIntersect);
            ChAssert.Single(pointsLineIntersect);
            ChAssert.Equal(pointsLineIntersect.FirstOrDefault(), ptIntersectLine, 0.25);

            ChAssert.True(result_circleIntersect);
            ChAssert.Equal(pointscircleIntersect, ptsIntersectCircle, 0.25);

            ChAssert.True(result_arcIntersect);
            ChAssert.CollectionContains(pointsArcIntersect, ptsIntersectArc, 0.25);

            ChAssert.False(result_lineNotIntersect);
            ChAssert.Empty(pointsLineNotIntersect);

            ChAssert.False(result_circleNotIntersect);
            ChAssert.Empty(pointscircleNotIntersect);

            ChAssert.False(result_arcNotIntersect);
            ChAssert.Empty(pointsArcNotIntersect);
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            IChPoint point = ChPoint.Create(25, 25);

            // Act
            var result = lineSegment.IsOn(point);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            IChPoint point = ChPoint.Create(25, 25);
            double tolerance = 0.5;

            // Act
            var result = lineSegment.IsOn(point, tolerance);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void SmallerDistanceToLineSegment2d_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            var segLine = ChLineSegment.Create(ChPoint.Create(10), ChPoint.Create(20));

            // Act
            var result = lineSegment.SmallerDistanceToLineSegment2d(segLine);

            // Assert
            ChAssert.Equal(result, 7.07, 0.5);
        }

        [ChFact]
        public void Translation2d_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lineSegment = GetLineSegment();
            double deltaX = 5;
            double deltaY = 5;

            // Act
            lineSegment.Translation2d(deltaX, deltaY);

            // Assert
            ChAssert.Equal(lineSegment.StartPoint, ChPoint.Create(4, 4), 0.1);
            ChAssert.Equal(lineSegment.EndPoint, ChPoint.Create(6, 6), 0.1);
        }

        private static IChLineSegment GetLineSegment()
        {
            return ChLineSegment.Create(ChPoint.Create(0, 0), ChPoint.Create(50, 50));
        }
    }
}