﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

namespace CadApiHelperTests.Geometry
{
    public class VectorTests : ChBaseUnitTest
    {
        [ChFact]
        public void AngleBetween_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();

            // Arrange
            var vector = new ChVector(ChAngles.RAD45);
            ChVector vt = new ChVector(ChAngles.RAD180);

            // Act
            var result = vector.AngleBetween(
                vt);

            // Assert
            ChAssert.Equal(result, 2.35, 0.25);
        }

        [ChFact]
        public void Normalize_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();

            // Arrange
            var vector = new ChVector(ChAngles.RAD225);

            // Act
            var result = vector.Normalize();

            // Assert
            ChAssert.Equal(vector.Angle, ChAngles.RAD225, 0.1);
        }

        [ChFact]
        public void ScalarProduct_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();

            // Arrange
            var vector = new ChVector(ChAngles.RAD45);
            ChVector vt = new ChVector(ChAngles.RAD45);

            // Act
            var result = vector.ScalarProduct(vt);

            // Assert
            ChAssert.Equal(result, 1);
        }

        [ChFact]
        public void ToString_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();

            // Arrange
            var vector = new ChVector(ChAngles.RAD180);

            // Act
            var result = vector.ToString();

            // Assert
            ChAssert.Equal(result, "X = -1; Y = 0 [Mod = 1; Âng = 3.14159]");
        }

        [ChFact]
        public void VectorProduct_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();

            // Arrange
            var vector = new ChVector(ChAngles.RAD90);
            ChVector vt = new ChVector(ChAngles.RAD90);

            // Act
            var result = vector.VectorProduct(vt);

            // Assert
            ChAssert.Equal(result.Angle, 0);
        }
    }
}