﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

namespace CadApiHelperTests.Geometry
{
    public class AnglesTests : ChBaseUnitTest
    {
        [ChFact]
        public void ConvertToDegrees_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            ChAssert.Equal(0, ChAngles.ToDegrees(ChAngles.RAD0));
            ChAssert.Equal(45, ChAngles.ToDegrees(ChAngles.RAD45));
            ChAssert.Equal(90, ChAngles.ToDegrees(ChAngles.RAD90));
            ChAssert.Equal(135, ChAngles.ToDegrees(ChAngles.RAD135));
            ChAssert.Equal(180, ChAngles.ToDegrees(ChAngles.RAD180));

            ChAssert.Equal(225, ChAngles.ToDegrees(ChAngles.RAD225));
            ChAssert.Equal(270, ChAngles.ToDegrees(ChAngles.RAD270));
            ChAssert.Equal(315, ChAngles.ToDegrees(ChAngles.RAD315));
            ChAssert.Equal(360, ChAngles.ToDegrees(ChAngles.RAD360));
        }

        [ChFact]
        public void ConvertToRad_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            ChAssert.Equal(ChAngles.RAD0, ChAngles.ToRad(0));
            ChAssert.Equal(ChAngles.RAD45, ChAngles.ToRad(45));
            ChAssert.Equal(ChAngles.RAD90, ChAngles.ToRad(90));
            ChAssert.Equal(ChAngles.RAD135, ChAngles.ToRad(135));
            ChAssert.Equal(ChAngles.RAD180, ChAngles.ToRad(180));
            ChAssert.Equal(ChAngles.RAD225, ChAngles.ToRad(225));
            ChAssert.Equal(ChAngles.RAD270, ChAngles.ToRad(270));
            ChAssert.Equal(ChAngles.RAD315, ChAngles.ToRad(315));
            ChAssert.Equal(ChAngles.RAD360, ChAngles.ToRad(360));
        }

        [ChFact]
        public void DegreesToString_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            ChAssert.Equal("0°", ChAngles.DegreesToString(0, ChStringDegreesFormat.Degrees));
            ChAssert.Equal("10° 30'", ChAngles.DegreesToString(10.50, ChStringDegreesFormat.DegreesMinutes));
            ChAssert.Equal("10° 30' 00\"", ChAngles.DegreesToString(10.50, ChStringDegreesFormat.DegreesMinutesSeconds));
        }

        [ChFact]
        public void Normalize_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            ChAssert.Equal(ChAngles.Normalize(ChAngles.RAD270), ChAngles.RAD90);
            ChAssert.Equal(ChAngles.Normalize(ChAngles.RAD45), ChAngles.RAD45);
        }
    }
}