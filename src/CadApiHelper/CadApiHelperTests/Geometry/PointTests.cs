﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

using System.Linq;

namespace CadApiHelperTests.Geometry
{
    public class PointTests : ChBaseUnitTest
    {
        [ChFact]
        public void Angle2D_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint p = ChPoint.Create(50, 50);

            // Act
            var result = point.Angle2D(p);

            // Assert
            ChAssert.Equal(0.7853, result, 0.1);
        }

        [ChFact]
        public void AngleFromXAxis_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint ponto = ChPoint.Create(10, 10);

            // Act
            var result = point.AngleFromXAxis(ponto);

            // Assert
            ChAssert.Equal(result, ChAngles.RAD45, 0.1);
        }

        [ChFact]
        public void AsArray_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();

            // Act
            var result = point.AsArray();

            // Assert
            ChAssert.Count(result, 3);
            ChAssert.Equal(result.Count(x => x == 0), 3);
        }

        [ChFact]
        public void CompareTo_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint pEqual = ChPoint.Create();
            IChPoint pNotEqual = ChPoint.Create(20);

            // Act
            var resultEqual = point.CompareTo(pEqual);
            var resultNotEqual = point.CompareTo(pNotEqual);

            // Assert
            ChAssert.Equal(resultEqual, 0);
            ChAssert.NotEqual(resultNotEqual, 0);
        }

        [ChFact]
        public void Distance_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint ponto = ChPoint.Create(10);

            // Act
            var result = point.Distance(ponto);

            // Assert
            ChAssert.Equal(result, 10, 0.1);
        }

        [ChFact]
        public void Distance2D_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint ponto = ChPoint.Create(10);

            // Act
            var result = point.Distance2D(ponto);

            // Assert
            ChAssert.Equal(result, 10, 0.1);
        }

        [ChFact]
        public void DistanceToLineSegment2D_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChLineSegment segmentoReta = ChLineSegment.Create(ChPoint.Create(10), ChPoint.Create(20));

            // Act
            var result = point.DistanceToLineSegment2D(segmentoReta);

            // Assert
            ChAssert.Equal(result, 10.0, 0.1);
        }

        [ChFact]
        public void Divide_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create(10, 10, 10);
            IChPoint right = ChPoint.Create(2, 2, 2);

            // Act
            var result = point.Divide(right);

            // Assert
            ChAssert.Equal(result, ChPoint.Create(5, 5, 5), 0.1);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint pEqual = ChPoint.Create();
            IChPoint pNotEqual = ChPoint.Create(10, 10);

            // Act
            var resultEqual = point.Equals(pEqual);
            var resultNotEqual = point.Equals(pNotEqual);

            // Assert
            ChAssert.True(resultEqual);
            ChAssert.False(resultNotEqual);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            object obj = new object();

            // Act
            var result = point.Equals(obj);

            // Assert
            ChAssert.False(result);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior2()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint other = ChPoint.Create(0.05, 0.05, 0.05);
            double tol = 0.1;

            // Act
            var resultEqual = point.Equals(other, tol);
            var resultNotEqual = point.Equals(other, 0.04);

            // Assert
            ChAssert.True(resultEqual);
            ChAssert.False(resultNotEqual);
        }

        [ChFact]
        public void GreaterThen_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint right = ChPoint.Create(10, 10, 10);

            // Act
            var result = right.GreaterThan(point);
            var resultFalse = point.GreaterThan(right);

            // Assert
            ChAssert.True(result);
            ChAssert.False(resultFalse);
        }

        [ChFact]
        public void Max_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint pt = ChPoint.Create(10, 10, 10);

            // Act
            var result = point.Max(pt);

            // Assert
            ChAssert.Equal(result, pt);
        }

        [ChFact]
        public void Mid_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint pt = ChPoint.Create(10);

            // Act
            var result = point.Mid(pt);

            // Assert
            ChAssert.Equal(result, ChPoint.Create(5));
        }

        [ChFact]
        public void Min_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create(10, 10, 10);
            IChPoint pt = ChPoint.Create();

            // Act
            var result = point.Min(pt);

            // Assert
            ChAssert.Equal(result, pt);
        }

        [ChFact]
        public void Multiply_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create(1, 1, 1);
            IChPoint right = ChPoint.Create(2, 2, 2);

            // Act
            var result = point.Multiply(right);

            // Assert
            ChAssert.Equal(result, right);
        }

        [ChFact]
        public void PolarPoint_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            double angulo = ChAngles.RAD90;
            double distancia = 20;

            // Act
            var result = point.PolarPoint(angulo, distancia);

            // Assert
            ChAssert.Equal(result, ChPoint.Create(0, 20, 0), 0.1);
        }

        [ChFact]
        public void RetangularPoint_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            double x = 10;
            double y = 20;

            // Act
            var result = point.RetangularPoint(x, y);

            // Assert
            ChAssert.Equal(result, ChPoint.Create(10, 20), 0.1);
        }

        [ChFact]
        public void RetangularPoint_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            double x = 10;
            double y = 20;
            double z = 30;

            // Act
            var result = point.RetangularPoint(x, y, z);

            // Assert
            ChAssert.Equal(result, ChPoint.Create(10, 20, 30), 0.1);
        }

        [ChFact]
        public void SmallestThen_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint right = ChPoint.Create(10, 10);

            // Act
            var result = point.LessThan(right);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void SphericalPoint_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            double anguloXY = ChAngles.RAD180;
            double anguloXZ = ChAngles.RAD90;
            double distancia = 20;
            var expectedPoint = ChPoint.Create(-20);

            // Act
            var result = point.SphericalPoint(anguloXY, anguloXZ, distancia);

            // Assert
            ChAssert.Equal(result, expectedPoint, 0.5);
        }

        [ChFact]
        public void Subtraction_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create(20, 20, 20);
            IChPoint right = ChPoint.Create(10, 10, 10);

            // Act
            var result = point.Subtract(right);

            // Assert
            ChAssert.Equal(result, right);
        }

        [ChFact]
        public void Sum_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            IChPoint right = ChPoint.Create(10, 10, 10);

            // Act
            var result = point.Sum(right);

            // Assert
            ChAssert.Equal(result, right);
        }

        [ChFact]
        public void Translacao2d_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var point = ChPoint.Create();
            double deltaX = 10;
            double deltaY = 10;

            // Act
            point.Translacao2d(deltaX, deltaY);

            // Assert
            ChAssert.Equal(point, ChPoint.Create(10, 10));
        }
    }
}