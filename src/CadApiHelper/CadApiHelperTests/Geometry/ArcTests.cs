﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

using System.Collections.Generic;

namespace CadApiHelperTests.Geometry
{
    public class ArcTests : ChBaseUnitTest
    {

        [ChFact]
        public void CopySegment_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();

            // Act
            var result = arc.CopySegment();

            // Assert
            ChAssert.Equal(result, arc);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();
            object obj = arc.CopySegment();

            // Act
            var result = arc.Equals(
                obj);

            // Assert
            ChAssert.Equal(obj, arc);
            ChAssert.NotEqual(new object(), arc);
        }


        //Center point:  X= 32.5000  Y= -22.5000  Z= 0.0000
        //Radius:  39.5285
        //Start Point:  X= 20.0000  Y= 15.0000  Z= 0.0000
        //End Point:  X= 0.0000  Y= 0.0000  Z= 0.0000
        //Arc Length:  25.4366
        //Start angle:  108
        //End angle:  145

        [ChFact]
        public void GetCircularArcBy3Point_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();

            double angle = ChAngles.ToRad(37);
            double area = 33.9852;
            IChPoint centerPoint = new ChPoint(32.5000, -22.5000);
            double endAngle = ChAngles.ToRad(145);
            IChPoint endPoint = new ChPoint(20, 15);
            double length = 25.4366;
            double radius = 39.5285;
            ChSegmentType segmentType = ChSegmentType.Arc;
            double startAngle = ChAngles.ToRad(108);
            IChPoint startPoint = new ChPoint(0, 0);

            // Assert
            ChAssert.Equal(angle, arc.Angle, 0.1);
            ChAssert.Equal(area, arc.Area, 1);
            ChAssert.True(centerPoint.Equals(arc.CenterPoint, 0.25));
            ChAssert.True(System.Math.Abs(endAngle - arc.EndAngle) <= 0.25 || System.Math.Abs(endAngle - arc.StartAngle) <= 0.25);
            ChAssert.True(endPoint.Equals(arc.EndPoint, 0.25));
            ChAssert.Equal(length, arc.Length, 0.25);
            ChAssert.Equal(radius, arc.Radius, 0.1);
            ChAssert.Equal(segmentType, arc.SegmentType);
            ChAssert.True(System.Math.Abs(startAngle - arc.StartAngle) <= 0.25 || System.Math.Abs(startAngle - arc.EndAngle) <= 0.25);
            ChAssert.True(startPoint.Equals(arc.StartPoint, 0.25));
        }

        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();
            IChSegment segment = new ChLineSegment(new ChPoint(15, 5), new ChPoint(5, 13));

            // Act
            var result = arc.IntersectWith(segment);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();
            IChSegment segment = new ChLineSegment(new ChPoint(15, 5), new ChPoint(5, 13));
            List<IChPoint> points = new List<IChPoint>();

            // Act
            var result = arc.IntersectWith(segment, points);

            // Assert
            ChAssert.True(result);
            ChAssert.True(points.Count > 0);
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();
            IChPoint point = new ChPoint(10, 10);

            // Act
            var result = arc.IsOn(point);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();
            IChPoint point = new ChPoint(10, 10);
            double tolerance = 0.25;

            // Act
            var result = arc.IsOn(point, tolerance);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void Translation2d_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var arc = GetArc();
            double deltaX = 5;
            double deltaY = 0;
            IChPoint centerPoint = new ChPoint(32.5000 + 5, -22.5000);
            // Act
            arc.Translation2d(deltaX, deltaY);

            // Assert
            ChAssert.True(centerPoint.Equals(arc.CenterPoint, 0.25));
        }

        private static IChArc GetArc()
        {
            // Arrange
            return ChArc.Create(new ChPoint(0, 0), new ChPoint(10, 10), new ChPoint(20, 15));
        }
    }
}