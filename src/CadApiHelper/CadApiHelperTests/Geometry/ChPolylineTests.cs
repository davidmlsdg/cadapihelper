﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

using System.Collections.Generic;
using System.Linq;

namespace CadApiHelperTests.Geometry
{
    public class ChPolylineTests : ChBaseUnitTest
    {

        [ChFact]
        public void AddVertice_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);
            IChPoint point = ChPoint.Create(200);

            // Act
            chPolyline.AddVertice(point);

            // Assert
            ChAssert.Equal(chPolyline.CountSegments, 2);
            ChAssert.Equal(chPolyline.Vertices.Last(), point, 0.1);
            ChAssert.Equal(chPolyline.GetSegments().LastOrDefault().SegmentType, ChSegmentType.Line);
        }

        [ChFact]
        public void AddVertice_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);
            IChPoint point = ChPoint.Create(200);
            int index = 2;

            // Act
            chPolyline.AddVertice(index, point);

            // Assert
            ChAssert.Equal(chPolyline.CountSegments, 2);
            ChAssert.Equal(chPolyline.Vertices.Last(), point, 0.1);
            ChAssert.Equal(chPolyline.GetSegments().LastOrDefault().SegmentType, ChSegmentType.Line);
        }

        [ChFact]
        public void AddVertice_StateUnderTest_ExpectedBehavior2()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);
            IChPoint secondPointArc = ChPoint.Create(150, 50);
            IChPoint endPointArc = ChPoint.Create(200, 0);

            // Act
            chPolyline.AddVertice(secondPointArc, endPointArc);

            // Assert
            ChAssert.Equal(chPolyline.CountSegments, 2);
            ChAssert.Equal(chPolyline.Vertices.Last(), endPointArc, 0.1);
            ChAssert.Equal(chPolyline.GetSegments().LastOrDefault().SegmentType, ChSegmentType.Arc);
        }

        [ChFact]
        public void AddVertice_StateUnderTest_ExpectedBehavior3()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);
            IChPoint secondPointArc = ChPoint.Create(150, 50);
            IChPoint endPointArc = ChPoint.Create(200, 0);
            int index = 2;

            // Act
            chPolyline.AddVertice(
                index,
                secondPointArc,
                endPointArc);

            // Assert
            ChAssert.Equal(chPolyline.CountSegments, 2);
            ChAssert.Equal(chPolyline.Vertices.Last(), endPointArc, 0.1);
            ChAssert.Equal(chPolyline.GetSegments().LastOrDefault().SegmentType, ChSegmentType.Arc);
        }


        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);
            object obj = new object();

            // Act
            var result = chPolyline.Equals(obj);

            // Assert
            ChAssert.False(result);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);
            IChPolyline other = ChPolyline.Create(new List<IChLineSegment> { ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)) }, false);

            // Act
            var result = chPolyline.Equals(other);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void FixVertices_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();

            // Arrange
            var segments = new List<IChLineSegment>
            {
                ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)),
                ChLineSegment.Create(ChPoint.Create(100.001), ChPoint.Create(200)),
            };
            var chPolyline = new ChPolyline(segments, false);
            double toleranceDistanceSegmnents = 0.002;
            double toleranceAngleSegments = 0.01;

            // Act
            chPolyline.FixVertices(toleranceDistanceSegmnents, toleranceAngleSegments);

            // Assert
            ChAssert.Count(chPolyline.Segments, 1);
        }



        [ChFact]
        public void GetSegment_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var lin1 = ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100));
            var segments = new List<IChLineSegment>
            {
                lin1,
                ChLineSegment.Create(ChPoint.Create(100.001), ChPoint.Create(200)),
            };
            var chPolyline = new ChPolyline(segments, false);
            int index = 0;

            // Act
            var result = chPolyline.GetSegment(index);

            // Assert
            ChAssert.NotNull(result);
            ChAssert.Equal(lin1, result);
        }

        [ChFact]
        public void GetSegments_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var segments = new List<IChLineSegment>
            {
                ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)),
                ChLineSegment.Create(ChPoint.Create(100.001), ChPoint.Create(200)),
            };
            var chPolyline = new ChPolyline(segments, false);

            // Act
            var result = chPolyline.GetSegments();

            // Assert
            ChAssert.Count(result, 2);
        }

        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment>
                                        {
                                            ChLineSegment.Create(ChPoint.Create(0,50), ChPoint.Create(100,50)) ,
                                            ChLineSegment.Create(ChPoint.Create(100,50), ChPoint.Create(200,50)) ,
                                        }
                    , false);
            IChPolylineSegment segment = ChLineSegment.Create(ChPoint.Create(50, 0), ChPoint.Create(50, 100));


            // Act
            var result = chPolyline.IntersectWith(segment);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var chPolyline = ChPolyline.Create(new List<IChLineSegment>
                                        {
                                            ChLineSegment.Create(ChPoint.Create(0,50), ChPoint.Create(100,50)) ,
                                            ChLineSegment.Create(ChPoint.Create(100,50), ChPoint.Create(200,50)) ,
                                        }
                    , false);
            IChPolylineSegment segment = ChLineSegment.Create(ChPoint.Create(50, 0), ChPoint.Create(50, 100));
            List<IChPoint> pointsIntersects = new List<IChPoint>();
            var intersectPoint = ChPoint.Create(50, 50);
            // Act
            var result = chPolyline.IntersectWith(segment, pointsIntersects);

            // Assert
            ChAssert.True(result);
            ChAssert.NotNull(pointsIntersects);
            ChAssert.Count(pointsIntersects, 1);
            ChAssert.Contains(pointsIntersects, x => x.Equals(intersectPoint, 0.1));
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var segments = new List<IChLineSegment>
            {
                ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)),
                ChLineSegment.Create(ChPoint.Create(100), ChPoint.Create(200)),
            };
            var chPolyline = new ChPolyline(segments, false);
            IChPoint point = ChPoint.Create(50);

            // Act
            var result = chPolyline.IsOn(point);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IsPointInside2D_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var segments = new List<IChLineSegment>
            {
                ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)),
                ChLineSegment.Create(ChPoint.Create(100), ChPoint.Create(100,100)),
                ChLineSegment.Create(ChPoint.Create(100,100), ChPoint.Create(0,100)),
                ChLineSegment.Create(ChPoint.Create(0,100), ChPoint.Create()),
            };
            var chPolyline = new ChPolyline(segments, true);
            IChPoint point = ChPoint.Create(50, 50);
            double tolerance = 0.01;

            // Act
            var result = chPolyline.IsPointInside2D(point, tolerance);

            // Assert
            ChAssert.True(result);
        }



        [ChFact]
        public void RemoveLastPoint_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var segments = new List<IChLineSegment>
            {
                ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)),
                ChLineSegment.Create(ChPoint.Create(100), ChPoint.Create(200)),
            };
            var chPolyline = new ChPolyline(segments, false);

            // Act
            var result = chPolyline.RemoveLastPoint();

            // Assert
            ChAssert.Equal(chPolyline.CountSegments, 1);
            ChAssert.Equal(chPolyline.CountVertices, 2);
            ChAssert.Equal(chPolyline.Last().EndPoint, ChPoint.Create(100), 0.1);
        }

        [ChFact]
        public void RemovePointAt_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            var segments = new List<IChLineSegment>
            {
                ChLineSegment.Create(ChPoint.Create(), ChPoint.Create(100)),
                ChLineSegment.Create(ChPoint.Create(100), ChPoint.Create(200)),
            };
            var chPolyline = new ChPolyline(segments, false);
            int index = 1;

            // Act
            var result = chPolyline.RemovePointAt(index);

            // Assert
            ChAssert.Equal(chPolyline.CountSegments, 1);
            ChAssert.Equal(chPolyline.CountVertices, 2);
            //ChAssert.Equal(chPolyline.First().StartPoint, ChPoint.Create(), 0.1);
            //ChAssert.Equal(chPolyline.Last().EndPoint, ChPoint.Create(200), 0.1);
        }

    }
}
