﻿using CadApiHelper.Geometry;
using CadApiHelper.Test.Asserts;
using CadApiHelper.Test.Attributes;
using CadApiHelper.Test.Core;

using System.Collections.Generic;


namespace CadApiHelperTests.Geometry
{
    public class CircleTests : ChBaseUnitTest
    {
        [ChFact]
        public void CopySegment_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();

            // Act
            var result = circle.CopySegment();

            // Assert
            ChAssert.Equal(result, circle);
        }

        [ChFact]
        public void DistanceOfCenter_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();
            IChPoint point = new ChPoint(25, 25);
            var distance = 35.355339059327;
            // Act
            var result = circle.DistanceOfCenter(point);

            // Assert
            ChAssert.Equal(distance, result, 0.25);
        }

        [ChFact]
        public void Equals_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();
            IChSegment other = circle.CopySegment();

            // Act
            var result1 = circle.Equals(other);
            var result2 = circle.Equals(new object());

            // Assert
            ChAssert.True(result1);
            ChAssert.False(result2);
        }



        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChPoint secondPoint = new ChPoint(25, 43.3013);
            IChCircle circle = GetCircle();
            IChSegment segmentCircle = new ChCircle(new ChPoint(0, 50), 50);
            IChSegment segmentArc = new ChArc(new ChPoint(82.576, 57.1340), secondPoint, new ChPoint(0, 0));
            IChSegment segmentLine = new ChLineSegment(new ChPoint(0, 0), new ChPoint(50, 50));
            IChSegment segmentLine2 = new ChLineSegment(new ChPoint(100, 100), new ChPoint(250, 250));

            // Act
            var resultIntersectWithCircle = circle.IntersectWith(segmentCircle);
            var resultIntersectWithArc = circle.IntersectWith(segmentArc);
            var resultIntersectWithLine = circle.IntersectWith(segmentLine);
            var resultIntersectWithLineNotIntersect = circle.IntersectWith(segmentLine2);

            // Assert
            ChAssert.True(resultIntersectWithCircle);
            ChAssert.True(resultIntersectWithArc);
            ChAssert.True(resultIntersectWithLine);
            ChAssert.False(resultIntersectWithLineNotIntersect);
        }

        [ChFact]
        public void IntersectWith_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChPoint secondPoint = new ChPoint(25, 43.3013);
            IChCircle circle = GetCircle();
            IChSegment segmentCircle = new ChCircle(new ChPoint(0, 50), 50);
            IChSegment segmentArc = new ChArc(new ChPoint(82.576, 57.1340), secondPoint, new ChPoint(0, 0));
            IChSegment segmentLine = new ChLineSegment(new ChPoint(0, 0), new ChPoint(50, 50));
            IChSegment segmentLine2 = new ChLineSegment(new ChPoint(100, 100), new ChPoint(250, 250));
            List<IChPoint> ptsIntersectCircle = new List<IChPoint>();
            List<IChPoint> ptsIntersectArc = new List<IChPoint>();
            List<IChPoint> ptsIntersectLine = new List<IChPoint>();
            List<IChPoint> ptsIntersectLineNotIntersect = new List<IChPoint>();

            // Act
            var resultIntersectWithCircle = circle.IntersectWith(segmentCircle, ptsIntersectCircle);
            var resultIntersectWithArc = circle.IntersectWith(segmentArc, ptsIntersectArc);
            var resultIntersectWithLine = circle.IntersectWith(segmentLine, ptsIntersectLine);
            var resultIntersectWithLineNotIntersect = circle.IntersectWith(segmentLine2, ptsIntersectLineNotIntersect);

            // Assert
            var tol = 0.25;
            //From point:  X= 25.0000  Y= -43.3013  Z= 0.0000
            //To point:  X = 25.0000  Y = 43.3013  Z = 0.0000
            ChAssert.True(resultIntersectWithCircle);
            ChAssert.Count(ptsIntersectCircle, 2);
            ChAssert.Contains(ptsIntersectCircle, x => x.Equals(new ChPoint(-43.3013, 25), tol));
            ChAssert.Contains(ptsIntersectCircle, x => x.Equals(new ChPoint(43.3013, 25), tol));

            ChAssert.True(resultIntersectWithArc);
            //ChAssert.Count(ptsIntersectArc, 1);
            ChAssert.Contains(ptsIntersectArc, x => x.Equals(new ChPoint(10.6854, -48.8449), tol));

            ChAssert.True(resultIntersectWithLine);
            ChAssert.Count(ptsIntersectLine, 1);//35.3553  Y = 35.3553
            ChAssert.Contains(ptsIntersectLine, x => x.Equals(new ChPoint(35.3553, 35.3553), tol));

            ChAssert.False(resultIntersectWithLineNotIntersect);
            ChAssert.Empty(ptsIntersectLineNotIntersect);
        }

        [ChFact]
        public void IsInside_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();
            IChPoint point = new ChPoint(-25.1642, 14.1564);

            // Act
            var result = circle.IsInside(point);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();
            IChPoint point = new ChPoint(50, 0);

            // Act
            var result = circle.IsOn(point);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void IsOn_StateUnderTest_ExpectedBehavior1()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();
            IChPoint point = new ChPoint(50, 0);
            double tolerance = 0.5;

            // Act
            var result = circle.IsOn(point, tolerance);

            // Assert
            ChAssert.True(result);
        }

        [ChFact]
        public void Translation2d_StateUnderTest_ExpectedBehavior()
        {
            if (BreakDebugger)
                System.Diagnostics.Debugger.Break();
            // Arrange
            IChCircle circle = GetCircle();
            double deltaX = 0;
            double deltaY = 5;
            IChPoint point = ChPoint.Create();
            point.Y += deltaY;
            // Act
            circle.Translation2d(deltaX, deltaY);

            // Assert
            ChAssert.True(point.Equals(circle.Center, 0.25));
        }

        private static IChCircle GetCircle()
        {
            return ChCircle.Create(ChPoint.Create(0, 0), 50);
        }
    }
}