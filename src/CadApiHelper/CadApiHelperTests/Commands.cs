﻿using CadApiHelper.Test.Ui;

using System.Reflection;

using ZwSoft.ZwCAD.Runtime;

namespace CadApiHelperTests
{
    public class Commands
    {
        [CommandMethod("StartTests")]
        public void StartTests()
        {
            var assembly = Assembly.GetExecutingAssembly();
            var dlg = new TestExplorerView(assembly);
            dlg.ShowDialog();
        }
    }
}